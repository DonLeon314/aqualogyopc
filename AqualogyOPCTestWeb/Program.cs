﻿using AqualogyOPCTestWeb.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AqualogyOPCTestWeb.ServiceReference1;
using System.Diagnostics;
using System.IO;

namespace AqualogyOPCTestWeb
{
    class Program
    {
        static void Main( string[] args )
        {

            String f = GetLastFile( DateTime.Now, @"c:\Dev\AqualogyOPC\Target\Debug\Log\" );

            Debug.WriteLine( f );

//             AqualogyOPCRestServiceClient client1 = new AqualogyOPCRestServiceClient();
// 
//             using( AqualogyOPCRestServiceClient client = new AqualogyOPCRestServiceClient( "WSHttpBinding_IAqualogyOPCRestService",
//                                                                     "http://localhost:9001/AqualogyOPCRestService" ) )
//             {
// 
//                 var r = client.GetAllConfigurations();
// 
// 
//                 return;
//             }
        }

        private static String GetLastFile( DateTime date, String path )
        {
            String fn_mask = date.ToString( "yyyyMMdd_Service" ) + "_{0}.log";

            List<String> files = new List<String>();

            var r = Directory.EnumerateFiles( path, String.Format( fn_mask, "*" ) );

            foreach( var f in r )
            {
                files.Add( f );       
            }

            // Normalize
            files.RemoveAll( ( x ) => CalcLogFileIndex( x ) == -1 );
            files.Sort( LogFileComparator );

            // Get last
            String result = files.LastOrDefault();

            if( String.IsNullOrWhiteSpace( result ) )
            {
                result = String.Format( path + fn_mask, 0 );
            }
//             else
//             {
//                 result = String.Format( path + fn_mask, CalcLastFileIndex( fileName ) );
//             }

            return result;
        }

        private static Int32 CalcLastFileIndex( String fileName, String fileMask )
        {
            Int32 idx = CalcLogFileIndex( fileName );
            
            idx++;

            return idx;

        }

        private static Int32 CalcLogFileIndex( String fileName )
        {
            Int32 result = -1;

            Int32 pos1 = fileName.LastIndexOf( '_' );
            if( pos1 != -1 )
            {
                Int32 pos2 = fileName.LastIndexOf( '.' );
                if( pos2 != -1 )
                {
                    String str_result = fileName.Substring( pos1 + 1, pos2 - pos1 - 1 );

                    if( false == Int32.TryParse( str_result, out result ) )
                    {
                        result = -1;
                    }
                }
            }

            return result;
        }

        private static Int32 LogFileComparator( String item1, String item2 )
        {
            Int32 idx1 = CalcLogFileIndex( item1 );
            Int32 idx2 = CalcLogFileIndex( item2 );

            return idx1.CompareTo( idx2 );

        }
    }
}
