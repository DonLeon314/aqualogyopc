﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AqualogyOPCKernel.OPCUAClient.OPCUAAlarm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.OPCUAClient.OPCUAAlarm.Tests
{
    [TestClass()]
    public class OPCUAAlarmTests
    {
        [TestMethod()]
        public void SetAlarmTest()
        {
            using( OPCUAAlarm alarm = new OPCUAAlarm() )
            {
                var result = alarm.SetAlarm( @"//Leon:53530/OPCUA/SimulationServer", @"ns=5;s=Wr" );
            }
        }
    }
}