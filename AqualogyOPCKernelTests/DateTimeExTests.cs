﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AqualogyOPCKernel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Tests
{
    [TestClass()]
    public class DateTimeExTests
    {

        [TestMethod()]
        public void IsWithinTest()
        {
            //   9 aug 2018 J 08:13:00
            DateTime time = new DateTime( 2018, 8, 9, 8, 13, 0 );

            // true
            Boolean r1 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LMX" );
            Assert.IsTrue( r1 );

            // false
            Boolean r2 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LM" );
            Assert.IsFalse( r2 );

            // false
            Boolean r3 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LMJ" );
            Assert.IsFalse( r3 );


            //   9 aug 2018 J 14:00:00
            time = new DateTime( 2018, 8, 8, 14, 0, 0 );

            // false
            r1 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LMX" );
            Assert.IsFalse( r1 );

            // false
            r2 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LM" );
            Assert.IsFalse( r2 );

            // false
            r3 = time.IsWithin( new TimeSpan( 15, 0, 0 ), new TimeSpan( 9, 0, 0 ), "LMJ" );
            Assert.IsFalse( r3 );




            // false
            Assert.IsFalse( time.IsWithin( new TimeSpan( 8, 0, 0 ), new TimeSpan( 15, 0, 0 ), "LMXsd" ) );

            // false
            Assert.IsFalse( time.IsWithin( new TimeSpan( 12, 0, 0 ), new TimeSpan( 15, 0, 0 ), "LMXsd" ) );

            // true
            Assert.IsTrue( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 15, 0, 0 ), "LMxsd" ) );

            Assert.IsFalse( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 11, 0, 0 ), "LMxsd" ) );

            Assert.IsFalse( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 15, 0, 0 ), "LMjsd" ) );


            Assert.IsFalse( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 20, 0, 0 ), "LMjsd" ) );

            Assert.IsTrue( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 20, 0, 0 ), "LMxsd" ) );

            Assert.IsFalse( time.IsWithin( new TimeSpan( 20, 0, 0 ), new TimeSpan( 20, 0, 0 ), "sd" ) );

            Assert.IsFalse( time.IsWithin( new TimeSpan( 0, 0, 0 ), new TimeSpan( 0, 0, 0 ), "xsd" ) );

            Assert.IsTrue( time.IsWithin( new TimeSpan( 8, 0, 0 ), new TimeSpan( 8, 0, 0 ), "jsd" ) );

            Assert.IsFalse( time.IsWithin( new TimeSpan( 8, 0, 0 ), new TimeSpan( 8, 0, 0 ), "xsd" ) );


            Assert.IsFalse( time.IsWithin( new TimeSpan( 11, 0, 0 ), new TimeSpan( 11, 0, 0 ), "xsd" ) );

            Assert.IsTrue( time.IsWithin( new TimeSpan( 11, 0, 0 ), new TimeSpan( 11, 0, 0 ), "jsd" ) );


        }

        [TestMethod()]
        public void IsWithin2Test()
        {
            DateTime c_time = new DateTime( 2018, 9, 21, 13, 39, 34 );
            TimeSpan start_time = new TimeSpan( 8, 0, 0 );
            TimeSpan finish_time = new TimeSpan( 18, 0, 0 );

            Boolean r = c_time.IsWithin2( start_time, finish_time, "LMXJV" );

        }
    }
}