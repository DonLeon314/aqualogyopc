﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AqualogyOPCKernel.Model.Utils.Tests
{
    [TestClass()]
    public class OPCTagUniqueKeyComparerTests
    {
        [TestMethod()]
        public void GetHashCodeTest()
        {
            OPCTagUniqueKey k1 = new OPCTagUniqueKey( 10, 20 );
            OPCTagUniqueKey k2 = new OPCTagUniqueKey( 20, 10 );

            OPCTagUniqueKey k3 = new OPCTagUniqueKey( 20, 10 );

            var v1 = k1.GetHashCode();
            var v2 = k2.GetHashCode();

            var v3 = k3.GetHashCode();

            Assert.IsFalse( v1 == v2 );


            Debug.WriteLine( String.Format( "Hash1:{0}", "123".GetHashCode() ) );
            Debug.WriteLine( String.Format( "Hash2:{0}", "123".GetHashCode() ) );
            Debug.WriteLine( String.Format( "Hash3:{0}", "321".GetHashCode() ) );

            HashSet<OPCTagUniqueKey> c = new HashSet<OPCTagUniqueKey>();

            c.Add( k1 );
            c.Add( k2 );
        }
    }
}