﻿using AqualogyOPCKernel;
using AqualogyOPCKernel.Config;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Threading;
using AqualogyOPCRestService;

namespace AqualogyOPCService
{
    public partial class AqualogyOPCService:
                ServiceBase,
                IAqualogyOPCRestService
    {
        private OPCKernel _Kernel = null;
        private ServiceInformator _Informator = new ServiceInformator();

        protected System.Threading.Thread _thread;
        private AutoResetEvent _StopEvent = new AutoResetEvent( false );

        private AqualogyOPCServiceConfig _Config;
        
        private ServiceHost _ServiceHost = null;

        /// <summary>
        /// Sync service
        /// </summary>
        private Object _SyncObjectCommander = new Object();

        /// <summary>
        /// Default constructor 
        /// </summary>
        public AqualogyOPCService()
        {
            InitializeComponent();
            this.CanStop = true;
            this.CanPauseAndContinue = true;            
            this.AutoLog = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart( string[] args )
        {
            try
            {

                this._Informator.Info( String.Format( "OnStart Thread:{0}", Thread.CurrentThread.ManagedThreadId ), AqualogyOPCKernel.Interfaces.eInfoType.Debug );

                System.IO.Directory.SetCurrentDirectory( System.AppDomain.CurrentDomain.BaseDirectory );
                
#if DEBUG
                Thread.Sleep( 20000 );
#endif
                StartProcessing();

                if( false == String.IsNullOrWhiteSpace( this._Config.HttpServicePoint ) )
                {
                    StartHost();
                }

                //StartProcessing();
                
            }
            catch( System.Exception ex )
            {                
                this._Informator.Info( ex );
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iAqualogyOPCConnector"></param>
        /// <returns></returns>
        private Boolean StartHost()
        {

            if( this._ServiceHost != null )
            {
                this._ServiceHost.Close();
            }

            String address_HTTP = String.Format( @"http:{0}/AqualogyOPCRestService", this._Config.HttpServicePoint );
                        

            this._ServiceHost = new ServiceHost( this, new Uri( address_HTTP ) );

            //Add Endpoint to Host
            this._ServiceHost.AddServiceEndpoint( typeof( IAqualogyOPCRestService ),
                                                    new WSHttpBinding(), "" );

            //Metadata Exchange
            ServiceMetadataBehavior metadata_behavior = this._ServiceHost.Description.Behaviors.Find<ServiceMetadataBehavior>();
            if( metadata_behavior == null )
            {
                metadata_behavior = new ServiceMetadataBehavior();
                this._ServiceHost.Description.Behaviors.Add( metadata_behavior );
            }
            metadata_behavior.HttpGetEnabled = true;


            ServiceBehaviorAttribute service_behavior = this._ServiceHost.Description.Behaviors.Find<ServiceBehaviorAttribute>();

            if( service_behavior == null )
            {
                service_behavior = new ServiceBehaviorAttribute();
                this._ServiceHost.Description.Behaviors.Add( service_behavior );
            }

            service_behavior.InstanceContextMode = InstanceContextMode.Single;
            service_behavior.IncludeExceptionDetailInFaults = false;

            //Open
            this._ServiceHost.Open();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void StopHost()
        {
            if( this._ServiceHost != null )
            {
                this._ServiceHost.Close();                
                this._ServiceHost = null;
            }
        }

        /// <summary>
        ///
        /// </summary>
        protected override void OnStop()
        {
            this._Informator.Info( String.Format( "OnStop Thread:{0}", Thread.CurrentThread.ManagedThreadId ), AqualogyOPCKernel.Interfaces.eInfoType.Debug );
                       

            if ( this._Kernel != null )
            {
                this._Kernel.StopProcessing();
                this._Kernel = null;
            }

            StopHost();

        }

#region IAqualogyOPCRestService

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public AqualogyOPCConfigorationInfo[] GetConfigurations()
        {
            lock( this._SyncObjectCommander )
            {
                try
                {

                    if( this._Kernel != null )
                    {
                        return this._Kernel.GetConfigurations(); // Load all configs
                    }
                    else
                    {
                        return OPCKernel.AvaulableConfigs();

                    }

                } catch( Exception ex )
                {
                    if( this._Informator != null )
                    {
                        this._Informator.Info( ex );
                    }

                    throw new FaultException<AqualogyOPCRestServiceFault>( new AqualogyOPCRestServiceFault( ex.Message ) ); 
                }
            }

        }

        /// <summary>
        /// Stop OPC processing
        /// </summary>
        /// <returns></returns>
        public Boolean StopProcessing()
        {
            lock( this._SyncObjectCommander )
            {
                try
                {

                    //For debug
                    // throw new Exception( "Test send error!" );

                    if( this._Kernel != null )
                    {
                        this._Kernel.StopProcessing();
                        this._Kernel = null;
                    }

                }catch( Exception ex )
                {
                    if( this._Informator != null )
                    {
                        this._Informator.Info( ex );
                    }

                    throw new FaultException<AqualogyOPCRestServiceFault>( new AqualogyOPCRestServiceFault( ex.Message ) );
                }
        }

            return true;
        }

        /// <summary>
        /// Start OPC processing
        /// </summary>
        /// <returns></returns>
        public Boolean StartProcessing()
        {
            lock( this._SyncObjectCommander )
            {
                try
                { 
                    if( this._Kernel == null )
                    {
                        this._Config = AqualogyOPCServiceConfig.Create();

                        if( this._Config.AutoStartProcessing )
                        {
                            this._Informator.AttachIOPCInfoProcess( this._Config );

                            this._Kernel = OPCKernel.CreateKernelFromBBDD( this._Config, this._Informator );

                            this._Kernel.StartProcessing();
                        }
                    }
                }catch( Exception ex )
                {
                    if( this._Informator != null )
                    {
                        this._Informator.Info( ex );
                    }

                    throw new FaultException<AqualogyOPCRestServiceFault>( new AqualogyOPCRestServiceFault( ex.Message ) );
                }
            }

            return true;
        }

        /// <summary>
        /// Restart OPCProcessing
        /// </summary>
        /// <returns></returns>
        public Boolean RestartProcessing()
        {
            lock( this._SyncObjectCommander )
            {
                try
                { 
                    if( this._Kernel != null )
                    {
                        StopProcessing();
                    }

                    StartProcessing();

                }catch( Exception ex )
                {
                    if( this._Informator != null )
                    {
                        this._Informator.Info( ex );
                    }

                    throw new FaultException<AqualogyOPCRestServiceFault>( new AqualogyOPCRestServiceFault( ex.Message ) );
                }
            }

            return true;
        }

        #endregion
    }
}
