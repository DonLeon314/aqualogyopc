﻿using AqualogyOPCKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AqualogyOPCService
{
    /// <summary>
    /// 
    /// </summary>
    internal class ServiceInformator:
            IOPCInfo
    {
        private Object _SyncObject = new Object();
        private String _LogFolder = String.Empty;
        private IOPCInfoProcess _IOPCInfoProcess = null;
        private String _FileName = String.Empty;

        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceInformator()
        {
            this._LogFolder = AppDomain.CurrentDomain.BaseDirectory;
            this._LogFolder += "Log";
            Boolean r = Directory.Exists( this._LogFolder );
            if (r == false)
            {
                Directory.CreateDirectory( this._LogFolder );
            }

            Info( String.Format( "------------- {0} ----------------", DateTime.Now.ToLocalTime().ToString() ), eInfoType.Message );

            InfoModules();
        }

        private void InfoModules()
        {
            Info( " --- Service ---", eInfoType.Message );
            InfoModule( @"AqualogyOPCService.exe" );
            InfoModule( @"OPCConfigModel.dll" );            
            InfoModule( @"AqualogyOPCKernel.dll" );
            InfoModule( @"AqualogyOPCRestService.dll" );

            Info( " --- MS Entity Framework ---", eInfoType.Message );
            InfoModule( @"EntityFramework.dll" );
            InfoModule( @"EntityFramework.SqlServer.dll" );

            Info( " --- OPC UA Client ---", eInfoType.Message );
            InfoModule( @"Opc.Ua.Client.dll" );
            InfoModule( @"Opc.Ua.Core.dll" );

            Info( " --- SQLite client ---", eInfoType.Message );
            InfoModule( @"x86\SQLite.Interop.dll" );
            InfoModule( @"x64\SQLite.Interop.dll" );
            InfoModule( @"System.Data.SQLite.dll" );
            InfoModule( @"System.Data.SQLite.EF6.dll");
            InfoModule( @"System.Data.SQLite.Linq.dll");
        }

        private void InfoModule( String moduleName )
        {
            try
            {
                String path = AppDomain.CurrentDomain.BaseDirectory;
                String assembly = path + moduleName;

                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo( assembly );

                String msg = String.Format( "Module:{0} Version:{1} Description:{2}",
                                             assembly, myFileVersionInfo.FileVersion, myFileVersionInfo.FileDescription );

                Info( msg, eInfoType.Message );
            }
            catch( Exception ex )
            {
                Info( ex );
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfoProcess"></param>
        public void AttachIOPCInfoProcess( IOPCInfoProcess iOPCInfoProcess )
        {
            this._IOPCInfoProcess = iOPCInfoProcess;
        }

        /// <summary>
        /// process message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="infoType"></param>
        public void Info( String message, eInfoType infoType )
        {

            try
            {
                if( this._IOPCInfoProcess == null || this._IOPCInfoProcess.ProcessInfo( infoType ) )
                {
                    lock( this._SyncObject )
                    {
                        String msg = String.Format( "{0} | {1,-9} | {2}", DateTime.Now.ToString( "yyyy.MM.dd HH:mm:ss.ffff" ), infoType.ToString(), message );
                        //Debug.WriteLine( msg );
                        String fn = CalcFileName(); //this._LogFolder + @"\" + DateTime.Now.ToString( "yyyyMMdd_Service" ) + ".log";
                        if( String.IsNullOrWhiteSpace( fn ) == false )
                        {
                            File.AppendAllText( fn, msg + "\r\n" );
                        }
                    }
                }
            } catch( Exception ex )
            {
                Debug.WriteLine( ex.Message );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String CalcNewFileName()
        {
            DateTime current_dt = DateTime.Now;
            return this._LogFolder + @"\" + current_dt.ToString( "yyyyMMdd_HHmmss_Service" ) + ".log";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String CalcFileName()
        {
            if( String.IsNullOrWhiteSpace( _FileName ) )
            {
                return ( this._FileName = CalcNewFileName() );
            }

            if( File.Exists( this._FileName ) )
            {
                long file_size = new System.IO.FileInfo( this._FileName ).Length;
                if( file_size > this._IOPCInfoProcess.MaxLogFile() * 1024 * 1024 )
                {
                    return ( this._FileName = CalcNewFileName() );
                }
            }

            return this._FileName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        public void Info( Exception exception )
        {
            String message;

            message = String.Format( "Error!!! Message:{0} Source:{1} StackTrace:{2}",
                                exception.Message,
                                exception.Source,
                                exception.StackTrace );

            Info( message, eInfoType.Error );
        }
    }
}
