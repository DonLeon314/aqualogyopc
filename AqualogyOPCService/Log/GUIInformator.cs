﻿using AqualogyOPCKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCService
{
    /// <summary>
    /// 
    /// </summary>
    internal class GUIInformator:
            IOPCInfo
    {
        private String _LogFolder = String.Empty;
        private Object _SyncObject = new Object();
        private IOPCInfoProcess _IOPCInfoProcess = null;

        /// <summary>
        /// 
        /// </summary>
        public GUIInformator()
        {
            this._LogFolder = AppDomain.CurrentDomain.BaseDirectory;
            this._LogFolder += "Log";
            Boolean r = Directory.Exists( this._LogFolder );
            if (r == false)
            {
                Directory.CreateDirectory( this._LogFolder );
            }

            Info( String.Format( "------------- {0} ----------------", DateTime.Now.ToLocalTime().ToString() ), eInfoType.Message );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfoProcess"></param>
        public void AttachIOPCInfoProcess( IOPCInfoProcess iOPCInfoProcess )
        {
            this._IOPCInfoProcess = iOPCInfoProcess;
        }

        /// <summary>
        ///  Process message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="infoType"></param>
        public void Info( String message, eInfoType infoType )
        {
            if( this._IOPCInfoProcess == null || this._IOPCInfoProcess.ProcessInfo( infoType ) )
            {
                String msg = String.Format( "{0} | {1,-9} | {2}", DateTime.Now, infoType.ToString(), message );
                lock( this._SyncObject )
                {
                    Console.WriteLine( msg );

                    String fn = this._LogFolder + @"\" + DateTime.Now.ToString( "yyyyMMdd_GUI" ) + ".log";

                    if ( String.IsNullOrWhiteSpace( fn ) == false )
                    {
                        File.AppendAllText( fn, msg + "\r\n" );
                    }
                }
            }
        }

        public void Info( Exception exception )
        {
            String message;

            message = String.Format( "Error!!! Message:{0} Source:{1} StackTrace:{2}",
                                exception.Message,
                                exception.Source,
                                exception.StackTrace );

            Info( message, eInfoType.Error );
        }
    }
}
