﻿using AqualogyOPCKernel;
using AqualogyOPCKernel.Config;
using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace AqualogyOPCService
{
    static class Program
    {
        private static Boolean ParameterProcessing( String[] args )
        {
            if( Environment.UserInteractive )
            {   // User interactive
                try
                {
                    if( args.Length == 1 && 0 == String.Compare( args[0], "Install", true ) )
                    {
                        AqualogyOPCInstallService();
                    }
                    else
                    {
                        if( args.Length == 1 && 0 == String.Compare( args[0], "Uninstall", true ) )
                        {
                            AqualogyOPCUninstallService();
                        }
                        else
                        {                            
//                                 Console.WriteLine( "AqualogyOPCService in GUI mode!" );
// 
//                                 DateTime time = new DateTime( 2018, 6, 13, 3, 0, 0 );
//                                 TimeSpan s = new TimeSpan( 8, 0, 0 );
//                                 TimeSpan e = new TimeSpan( 7, 59, 59 );
// 
//                                 var res = time.CalcWorkTime( s, e );
// 
//                             return true;


                            GUIInformator informator = new GUIInformator();
                            try
                            {
                                //TODO: clear
                                //String cfg_file = AppDomain.CurrentDomain.BaseDirectory + "AqualogyOPC.xml";
                                //AqualogyOPCServiceConfig config = AqualogyOPCServiceConfig.Create( cfg_file );
                                                                
                                AqualogyOPCServiceConfig config = AqualogyOPCServiceConfig.Create();

                                OPCKernel kernel = null;

//TODO:Clear
//                                 if (config != null && config.CountConfigurations > 0)
//                                 {
//                                     informator.AttachIOPCInfoProcess( config );
//                                     kernel = OPCKernel.CreateKernel( config, informator );
//                                 }

                                informator.AttachIOPCInfoProcess( config );
                                kernel = OPCKernel.CreateKernelFromBBDD( config, informator );
            
                                if (kernel != null)
                                {
                                    kernel.StartProcessing();
                                }

                                Console.WriteLine( "Press any key to stop processing..." );
                                Console.ReadKey();

                                if (kernel != null)
                                {
                                    Console.WriteLine( "Stop threads..." );
                                    kernel.StopProcessing();
                                    Console.WriteLine( "Stop - OK!" );
                                }

                                Console.WriteLine( "Press any key to exit..." );

                                Console.ReadKey();

                            } catch (Exception ex)
                            {
                                informator.Info( ex );
                            }
                        }
                    }
                }
                catch( Exception ex )
                {
                    Console.WriteLine( String.Format( "Error:{0}", ex.Message ) );
                }
            }

            return true;
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main( string[] args )
        {
            if( Environment.UserInteractive )
            {
                if( args == null || args.Length == 0 )
                {
                    args = new String[] { "debug" };
                }

                ParameterProcessing( args );                               
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new AqualogyOPCService()
                };
                ServiceBase.Run( ServicesToRun );
            }
        }

        private static void AqualogyOPCInstallService()
        {
            String[] parameters = new String[1];
            parameters[0] = System.Windows.Forms.Application.ExecutablePath;
            ManagedInstallerClass.InstallHelper( parameters );

        }

        /// <summary>
        /// Uninstall Windows service
        /// </summary>
        private static void AqualogyOPCUninstallService()
        {
            String[] parameters = new String[2];
            parameters[0] = @"/u";
            parameters[1] = System.Windows.Forms.Application.ExecutablePath;

            ManagedInstallerClass.InstallHelper( parameters );
        }

    }
}
