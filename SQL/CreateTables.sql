   
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/31/2018 16:11:04
-- Generated from EDMX file: C:\Dev\aqualogyopc\AqualogyOPCConfigModel\AqualogyOPCConfigDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AqualogyOPC];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ConfigurationOPCServerOPC]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServerOPCs] DROP CONSTRAINT [FK_ConfigurationOPCServerOPC];
GO
IF OBJECT_ID(N'[dbo].[FK_ServerOPCAlarmTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AlarmTags] DROP CONSTRAINT [FK_ServerOPCAlarmTag];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupAlarmTag_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupAlarmTag] DROP CONSTRAINT [FK_GroupAlarmTag_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupAlarmTag_AlarmTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupAlarmTag] DROP CONSTRAINT [FK_GroupAlarmTag_AlarmTag];
GO
IF OBJECT_ID(N'[dbo].[FK_UserEMail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EMails] DROP CONSTRAINT [FK_UserEMail];
GO
IF OBJECT_ID(N'[dbo].[FK_UserPhone]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Phones] DROP CONSTRAINT [FK_UserPhone];
GO
IF OBJECT_ID(N'[dbo].[FK_ConfigurationOPCGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Groups] DROP CONSTRAINT [FK_ConfigurationOPCGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_ConfigurationOPCUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_ConfigurationOPCUser];
GO
IF OBJECT_ID(N'[dbo].[FK_UserWorkTime]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_UserWorkTime];
GO
IF OBJECT_ID(N'[dbo].[FK_UserGroup_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserGroups] DROP CONSTRAINT [FK_UserGroup_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_UserGroup_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserGroups] DROP CONSTRAINT [FK_UserGroup_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[EMails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EMails];
GO
IF OBJECT_ID(N'[dbo].[Phones]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Phones];
GO
IF OBJECT_ID(N'[dbo].[Groups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Groups];
GO
IF OBJECT_ID(N'[dbo].[WorkTimes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkTimes];
GO
IF OBJECT_ID(N'[dbo].[ConfigurationOPCs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConfigurationOPCs];
GO
IF OBJECT_ID(N'[dbo].[AlarmTags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AlarmTags];
GO
IF OBJECT_ID(N'[dbo].[ServerOPCs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServerOPCs];
GO
IF OBJECT_ID(N'[dbo].[GroupAlarmTag]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupAlarmTag];
GO
IF OBJECT_ID(N'[dbo].[UserGroups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserGroups];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(256)  NOT NULL,
    [SecondName] nvarchar(256)  NULL,
    [Channel] int  NOT NULL,
    [ConfigurationOPCId] int  NOT NULL,
    [WorkTime_Id] int  NULL
);
GO

-- Creating table 'EMails'
CREATE TABLE [dbo].[EMails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EMailValue] nvarchar(256)  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'Phones'
CREATE TABLE [dbo].[Phones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PhoneValue] nvarchar(25)  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'Groups'
CREATE TABLE [dbo].[Groups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(256)  NOT NULL,
    [ConfigurationOPCId] int  NOT NULL
);
GO

-- Creating table 'WorkTimes'
CREATE TABLE [dbo].[WorkTimes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StartTime] time  NOT NULL,
    [EndTime] time  NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'ConfigurationOPCs'
CREATE TABLE [dbo].[ConfigurationOPCs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(256)  NOT NULL
);
GO

-- Creating table 'AlarmTags'
CREATE TABLE [dbo].[AlarmTags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Path] nvarchar(1000)  NOT NULL,
    [LargeMessage] nvarchar(512)  NOT NULL,
    [ShortMessage] nvarchar(256)  NOT NULL,
    [Priority] int  NOT NULL,
    [ServerOPCId] int  NOT NULL
);
GO

-- Creating table 'ServerOPCs'
CREATE TABLE [dbo].[ServerOPCs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Host] nvarchar(512)  NOT NULL,
    [ProgId] nvarchar(512)  NOT NULL,
    [Name] nvarchar(256)  NOT NULL,
    [ConfigurationOPCId] int  NOT NULL
);
GO

-- Creating table 'GroupAlarmTags'
CREATE TABLE [dbo].[GroupAlarmTags] (
    [Group_Id] int  NOT NULL,
    [AlarmTag_Id] int  NOT NULL
);
GO

-- Creating table 'UserGroups'
CREATE TABLE [dbo].[UserGroups] (
    [Group_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EMails'
ALTER TABLE [dbo].[EMails]
ADD CONSTRAINT [PK_EMails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Phones'
ALTER TABLE [dbo].[Phones]
ADD CONSTRAINT [PK_Phones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [PK_Groups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WorkTimes'
ALTER TABLE [dbo].[WorkTimes]
ADD CONSTRAINT [PK_WorkTimes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfigurationOPCs'
ALTER TABLE [dbo].[ConfigurationOPCs]
ADD CONSTRAINT [PK_ConfigurationOPCs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AlarmTags'
ALTER TABLE [dbo].[AlarmTags]
ADD CONSTRAINT [PK_AlarmTags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ServerOPCs'
ALTER TABLE [dbo].[ServerOPCs]
ADD CONSTRAINT [PK_ServerOPCs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Group_Id], [AlarmTag_Id] in table 'GroupAlarmTags'
ALTER TABLE [dbo].[GroupAlarmTags]
ADD CONSTRAINT [PK_GroupAlarmTags]
    PRIMARY KEY CLUSTERED ([Group_Id], [AlarmTag_Id] ASC);
GO

-- Creating primary key on [Group_Id], [User_Id] in table 'UserGroups'
ALTER TABLE [dbo].[UserGroups]
ADD CONSTRAINT [PK_UserGroups]
    PRIMARY KEY CLUSTERED ([Group_Id], [User_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ConfigurationOPCId] in table 'ServerOPCs'
ALTER TABLE [dbo].[ServerOPCs]
ADD CONSTRAINT [FK_ConfigurationOPCServerOPC]
    FOREIGN KEY ([ConfigurationOPCId])
    REFERENCES [dbo].[ConfigurationOPCs]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfigurationOPCServerOPC'
CREATE INDEX [IX_FK_ConfigurationOPCServerOPC]
ON [dbo].[ServerOPCs]
    ([ConfigurationOPCId]);
GO

-- Creating foreign key on [ServerOPCId] in table 'AlarmTags'
ALTER TABLE [dbo].[AlarmTags]
ADD CONSTRAINT [FK_ServerOPCAlarmTag]
    FOREIGN KEY ([ServerOPCId])
    REFERENCES [dbo].[ServerOPCs]
        ([Id])
    ON DELETE cascade ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ServerOPCAlarmTag'
CREATE INDEX [IX_FK_ServerOPCAlarmTag]
ON [dbo].[AlarmTags]
    ([ServerOPCId]);
GO

-- Creating foreign key on [Group_Id] in table 'GroupAlarmTags'
ALTER TABLE [dbo].[GroupAlarmTags]
ADD CONSTRAINT [FK_GroupAlarmTag_Group]
    FOREIGN KEY ([Group_Id])
    REFERENCES [dbo].[Groups]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [AlarmTag_Id] in table 'GroupAlarmTags'
ALTER TABLE [dbo].[GroupAlarmTags]
ADD CONSTRAINT [FK_GroupAlarmTag_AlarmTag]
    FOREIGN KEY ([AlarmTag_Id])
    REFERENCES [dbo].[AlarmTags]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupAlarmTag_AlarmTag'
CREATE INDEX [IX_FK_GroupAlarmTag_AlarmTag]
ON [dbo].[GroupAlarmTags]
    ([AlarmTag_Id]);
GO

-- Creating foreign key on [UserId] in table 'EMails'
ALTER TABLE [dbo].[EMails]
ADD CONSTRAINT [FK_UserEMail]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserEMail'
CREATE INDEX [IX_FK_UserEMail]
ON [dbo].[EMails]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'Phones'
ALTER TABLE [dbo].[Phones]
ADD CONSTRAINT [FK_UserPhone]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPhone'
CREATE INDEX [IX_FK_UserPhone]
ON [dbo].[Phones]
    ([UserId]);
GO

-- Creating foreign key on [ConfigurationOPCId] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [FK_ConfigurationOPCGroup]
    FOREIGN KEY ([ConfigurationOPCId])
    REFERENCES [dbo].[ConfigurationOPCs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfigurationOPCGroup'
CREATE INDEX [IX_FK_ConfigurationOPCGroup]
ON [dbo].[Groups]
    ([ConfigurationOPCId]);
GO

-- Creating foreign key on [ConfigurationOPCId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_ConfigurationOPCUser]
    FOREIGN KEY ([ConfigurationOPCId])
    REFERENCES [dbo].[ConfigurationOPCs]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfigurationOPCUser'
CREATE INDEX [IX_FK_ConfigurationOPCUser]
ON [dbo].[Users]
    ([ConfigurationOPCId]);
GO

-- Creating foreign key on [WorkTime_Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_UserWorkTime]
    FOREIGN KEY ([WorkTime_Id])
    REFERENCES [dbo].[WorkTimes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserWorkTime'
CREATE INDEX [IX_FK_UserWorkTime]
ON [dbo].[Users]
    ([WorkTime_Id]);
GO

-- Creating foreign key on [Group_Id] in table 'UserGroups'
ALTER TABLE [dbo].[UserGroups]
ADD CONSTRAINT [FK_UserGroup_Group]
    FOREIGN KEY ([Group_Id])
    REFERENCES [dbo].[Groups]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [User_Id] in table 'UserGroups'
ALTER TABLE [dbo].[UserGroups]
ADD CONSTRAINT [FK_UserGroup_User]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserGroup_User'
CREATE INDEX [IX_FK_UserGroup_User]
ON [dbo].[UserGroups]
    ([User_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------