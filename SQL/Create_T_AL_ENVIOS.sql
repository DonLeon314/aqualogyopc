USE TEst
GO

/****** Object:  Table [dbo].[T_AL_ENVIOS]    Script Date: 6/19/2018 5:05:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T_AL_ENVIOS](
	[nId] [int] IDENTITY(1,1) NOT NULL,
	[dFechaHora] [datetime] NOT NULL,
	[nIdNotificacion] [int] NULL,
	[sCliente] [nvarchar](1000) NOT NULL,
	[sCanal] [nvarchar](1000) NULL,
	[sAsunto] [nvarchar](1000) NULL,
	[sTextoEmail] [nvarchar](max) NULL,
	[sTextoSms] [nvarchar](140) NULL,
	[sEstado] [nvarchar](50) NOT NULL,
	[dFechaEstado] [datetime] NULL,
	[bEmail] [bit] NULL,
	[bSms] [bit] NULL,
	[nReintentos] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[nId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


