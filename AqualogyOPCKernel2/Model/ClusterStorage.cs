﻿
using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model.Utils;
using OPCConfigModel2;
using OPCConfigModel2.ModelBBDD;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AqualogyOPCKernel2.Model
{
    public class ClusterStorage
    {
        private Dictionary<OpcCluster, List<WorkerSmallInfo> > _ClusterToUsersInfo = 
                                new Dictionary<OpcCluster, List<WorkerSmallInfo>>( new OpcClusterEqualityComparer() );
        
        private IWorkerStorage _IWorkerStorage = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ClusterStorage( IWorkerStorage iWorkerStorage )
        {
            this._IWorkerStorage = iWorkerStorage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public void LoadFromBBDD( OPCConfigContext2 dbContext )
        {
            // Load clusters
            this._ClusterToUsersInfo.Clear();
            foreach( var bbdd_alarm_cluster in dbContext.AlarmClusters )
            {
                OpcCluster cluster = new OpcCluster( bbdd_alarm_cluster.AlarmTypeId, bbdd_alarm_cluster.AlarmGroupId );


                List<WorkerSmallInfo> users_info = null;
                if( this._ClusterToUsersInfo.ContainsKey( cluster ) )
                {
                    users_info = this._ClusterToUsersInfo[cluster];                    
                }
                else
                {
                    
                    users_info = new List<WorkerSmallInfo>();
                    this._ClusterToUsersInfo.Add( cluster, users_info );
                }

                WorkerSmallInfo worker_info = new WorkerSmallInfo( bbdd_alarm_cluster.WorkerId, bbdd_alarm_cluster.WorkTImeSelector,
                                                                   bbdd_alarm_cluster.Worker.Configurations.Select( c => c.Id ).ToList() );

                users_info.Add( worker_info );
            }

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarmType"></param>
        /// <param name="alagmGroup"></param>
        /// <param name="configurationId"></param>
        /// <returns></returns>
        public IEnumerable<OpcWorker> FindWorkers( String alarmType, Int32 alagmGroup, Int32 configurationId )
        {
            DateTime current_time = DateTime.Now; // Current time

            OpcCluster key_cluster = new OpcCluster( alarmType, alagmGroup );

            if( this._ClusterToUsersInfo.ContainsKey( key_cluster ) )
            {
                var user_infos = this._ClusterToUsersInfo[key_cluster];

                foreach( var user_info in user_infos )
                {
                    if( user_info.PresentInConfiguration( configurationId ) )
                    {
                        if( this._IWorkerStorage != null )
                        {
                            OpcWorker worker = this._IWorkerStorage.GetWorkerById( user_info.WorkerId );
                            if( worker != null )
                            {
                                Boolean is_online = worker.IsOnline( current_time );
                                Boolean need_send = false;
                                switch( user_info.WorkTImeSelector )
                                {
                                    case AlarmCluster.eWorkTImeSelector.eAll:
                                        need_send = true;
                                        break;
                                    case AlarmCluster.eWorkTImeSelector.eInWorkTIme:
                                        need_send = is_online;
                                        break;
                                    case AlarmCluster.eWorkTImeSelector.eOutWorkTime:
                                        need_send = !is_online;
                                        break;
                                }

                                Debug.WriteLine( String.Format( "User:{0} Online:{1} Selector:'{2}' Result:{3}",
                                                                worker.Alias, is_online, user_info.WorkTImeSelector, need_send ) );

                                if( need_send )
                                { // YES!!!! - NEED SEND ALARM MESSAGES TO USER
                                    yield return this._IWorkerStorage.GetWorkerById( user_info.WorkerId );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
