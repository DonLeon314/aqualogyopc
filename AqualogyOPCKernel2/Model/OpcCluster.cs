﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class OpcCluster
    {        
        public String AlarmType
        {
            get;
            private set;
        }

        public Int32 AlarmGroup
        {
            get;
            private set;
        }

        public OpcCluster( String alarmType, Int32 alarmGroup )
        {
            this.AlarmType = alarmType;
            this.AlarmGroup = alarmGroup;            
        }


    }
}
