﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class OpcWorkTime
    {
        public Int32 Id
        {
            get;
            private set;
        }

        public TimeSpan StartTime
        {
            get;
            private set;
        }

        public TimeSpan EndTime
        {
            get;
            private set;
        }

        public String WeekDays
        {
            get;
            private set;
        }

        public OpcWorkTime( Int32 id, String weekDays, TimeSpan startTime, TimeSpan endTime )
        {
            this.Id = id;
            this.WeekDays = weekDays;
            this.StartTime = startTime;
            this.EndTime = endTime;
        }
    }
}
