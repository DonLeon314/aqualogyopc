﻿using OPCConfigModel2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    internal class HotOpcWorkTimes
    {
        #region Collection
        private class OpcWorkTimeCollection:
                KeyedCollection<Int32, OpcWorkTime>
        {
            public OpcWorkTimeCollection()
            {

            }

            protected override Int32 GetKeyForItem( OpcWorkTime item )
            {
                return item.Id;
            }
        }
        #endregion

        private OpcWorkTimeCollection _Collection = new OpcWorkTimeCollection();

        private DateTime? _LastLoadVersion = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        public HotOpcWorkTimes()
        {

        }

        /// <summary>
        /// Load from BBDD
        /// </summary>
        /// <param name="dbContext"></param>
        public void LoadFromBBDD( OPCConfigContext2 dbContext )
        {
            this._Collection.Clear();

            foreach( var wt in dbContext.WorkTimes )
            {
                this._Collection.Add( new OpcWorkTime( wt.Id, wt.Description, wt.StartTime, wt.EndTime ) );    
            }
        }

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpcWorkTime GetById( Int32 id )
        {

            CheckAndReload();

            OpcWorkTime result = null;

            if( this._Collection.Contains( id ) )
            {
                result = this._Collection[id];
            }

            return result;
        }

        private void CheckAndReload()
        {
            using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
            {
                CheckAndReload( dbContext );
            }
        }

        private void CheckAndReload( OPCConfigContext2 dbContext )
        {
            if( NeedLoad( dbContext ) )
            {
                LoadFromBBDD( dbContext );
            }
        }

        private Boolean NeedLoad( OPCConfigContext2 dbContext )
        {            
            Int64 v = dbContext.GetParameter( "HOT", "WorkTimes", 0 );
                
            DateTime bbdd_version;

            if( v == 0 )
            {
                bbdd_version = DateTime.Now;
            }
            else
            {
                bbdd_version = new DateTime( v );
            }

            if( this._LastLoadVersion.HasValue == false )
            {
                this._LastLoadVersion = bbdd_version;
                return true;
            }

            Boolean result = ( this._LastLoadVersion.Value < bbdd_version );

            this._LastLoadVersion = bbdd_version;

            return result;
        }
    }
}
