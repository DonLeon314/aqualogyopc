﻿using AqualogyOPCKernel2.Interfaces;
using OPCConfigModel2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class OpcTagClientStorage:
            ITagClientSource
    {
        #region collection
        private class TagClientItem
        {
            public Int32 Id
            {
                get;
                private set;
            }

            public String Description
            {
                get;
                private set;
            }

            public TagClientItem( Int32 id, String description )
            {
                this.Id = id;
                this.Description = description;
            }
        }

        private class TagClientItemCollection:
                    KeyedCollection<Int32, TagClientItem>
        {
            public TagClientItemCollection()
            {
            }

            protected override Int32 GetKeyForItem( TagClientItem item )
            {
                return item.Id;
            }
        }

        private TagClientItemCollection _Collection = new TagClientItemCollection();

        #endregion


        /// <summary>
        /// 
        /// </summary>
        public OpcTagClientStorage()
        {
        }


        /// <summary>
        /// 
        /// </summary>
        public void ReloadFromBBDD()
        {
            this._Collection.Clear();

            using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
            {
                foreach( var bbdd_tagclient in dbContext.TagClients )
                {
                    this._Collection.Add( new TagClientItem( bbdd_tagclient.Id, bbdd_tagclient.Description ) );
                }
            }
        }


        #region interface ITagClientSource
        public String GetClientDescription( Int32 idTagClient )
        {          

            if( this._Collection.Contains( idTagClient ) )
            {
                return this._Collection[idTagClient].Description;   
            }

            return null;
        }
        #endregion
    }
}
