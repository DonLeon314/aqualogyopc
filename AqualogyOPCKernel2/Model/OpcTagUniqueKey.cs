﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AqualogyOPCKernel2.Model
{
    public class OpcCTagUniqueKey
    {
        /// <summary>
        /// Tag ID
        /// </summary>
        public Int32 Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Configuration ID
        /// </summary>
        public Int32 ConfigId
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Tag ID</param>
        /// <param name="configId">Configuration ID</param>
        public OpcCTagUniqueKey( Int32 id, Int32 configId )
        {
            this.Id = id;
            this.ConfigId = configId;
        }
    }
}
