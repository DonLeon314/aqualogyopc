﻿using AqualogyOPCKernel2.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class Alarm
    {

        private UniqueCollection _Phones = new UniqueCollection();
        private UniqueCollection _EMails = new UniqueCollection();
        private Boolean _NeedSend = false;

        public EventHandler<PropertyChangeArgs> OnChangeProperty = (o, e) => 
        { 
            Debug.WriteLine( String.Format("Change property '{0}' in Alarm {1}->{2}", e.PropertyName, e.OldValue, e.NewValue ) ); 
        };

        public Int32 ConfigurationId
        {
            get;
            private set;
        }

        public String Key
        {
            get;
            private set;
        }

        public DateTime AlarmTime
        {
            get;
            private set;
        }

        public String AlarmType
        {
            get;
            private set;
        }

        public Int32 AlarmGroup
        {
            get;
            private set;
        }

        public String LargeMessage
        {
            get;
            private set;
        }

        public String ShortMessage
        {
            get;
            private set;
        }

        public IEnumerable<String> Phones
        {
            get
            {
                return this._Phones;
            }
        }

        public IEnumerable<String> EMails
        {
            get
            {
                return this._EMails;
            }
        }

        public Boolean NeedSend
        {
            get
            {
                return this._NeedSend;
            }

            set
            {
                if( this._NeedSend != value )
                {
                    Boolean old_value = this._NeedSend;
                    this._NeedSend = value;
                    this.OnChangeProperty( this, new PropertyChangeArgs( "NeedSend", value, old_value ) );

                }
            }
        }

        /// <summary>
        /// Client id
        /// </summary>
        public Int32 ClientId
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String PhonesChannel()
        {
            StringBuilder sb = new StringBuilder();

            foreach( String phone in this._Phones )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( ";" );
                }
                sb.Append( phone );
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String EMailsChannel()
        {
            StringBuilder sb = new StringBuilder();

            foreach( String email in this._EMails )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( ";" );
                }
                sb.Append( email );
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public void AddEMail( String email )
        {
            email = email.Trim();

            if( false == this._EMails.Contains( email ) )
            {
                this._EMails.Add( email );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emails"></param>
        public void AddEMails( IEnumerable<String> emails )
        {
            foreach( String email in emails )
            {
                this.AddEMail( email );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void InitializeEMails()
        {
            this._EMails.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        public void AddPhone( String phone )
        {
            phone = phone.Trim();

            if( false == this._Phones.Contains( phone ) )
            {
                this._Phones.Add( phone );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phones"></param>
        public void AddPhones( IEnumerable<String> phones )
        {
            foreach( String phone in phones )
            {
                this.AddPhone( phone );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void InitializePhones()
        {
            this._Phones.Clear();
        } 

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configurationId"></param>
        /// <param name="key"></param>
        /// <param name="alarmTime"></param>
        /// <param name="alarmType"></param>
        /// <param name="alarmGroup"></param>
        public Alarm( Int32 configurationId, String key, DateTime alarmTime, String alarmType, 
                      Int32 alarmGroup, String largeMsg, String shortMsg, Boolean needSend, Int32 clientId )
        {
            this.ConfigurationId = configurationId;
            this.Key = key;
            this.AlarmTime = alarmTime;
            this.AlarmType = alarmType;
            this.AlarmGroup = alarmGroup;

            this.LargeMessage = largeMsg;
            this.ShortMessage = shortMsg;

            this._NeedSend = needSend;

            this.ClientId = clientId;
        }
    }
}
