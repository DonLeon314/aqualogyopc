﻿using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using AqualogyOPCKernel2.Savers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace AqualogyOPCKernel2.Model
{
    public class AlarmDetectorsStorage:
            IAlarmDetectors
    {
        private static Int32 _DefaultTimeDetectAlarm = 10;
        private static Int32 _DefaultTimeActual= 10;

        #region Alarm detectors storage

        private class AlarmDetectorsCollection:
                    KeyedCollection<String, AlarmDetector>
        {
            public AlarmDetectorsCollection() :
                    base( StringComparer.CurrentCultureIgnoreCase )
            {

            }

            protected override String GetKeyForItem( AlarmDetector item )
            {
                return item.ItemKey;
            }
        }

        private AlarmDetectorsCollection _AlarmDetectors = new AlarmDetectorsCollection();

        #endregion

        private TimeSpan _TimeDetectAlarm = TimeSpan.FromSeconds( _DefaultTimeDetectAlarm );
        private TimeSpan _TimeActual = TimeSpan.FromSeconds( _DefaultTimeActual );


        private SQLiteAlarmDetectorsSaver _SQLiteAlarmDetectorsSaver = new SQLiteAlarmDetectorsSaver();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="timeDetect"></param>
        public AlarmDetectorsStorage( Int32 timeDetect, Int32 timeActual )
        {
            this._TimeDetectAlarm = TimeSpan.FromSeconds( ( timeDetect == 0 ? _DefaultTimeDetectAlarm : timeDetect ) );
            this._TimeActual = TimeSpan.FromSeconds( ( timeActual == 0 ? _DefaultTimeActual : timeActual ) );

        }

        #region interface IAlarmDetectors

        public Boolean MarkNeedSend( IEnumerable<String> detectorKeys, Boolean needSend )
        {
            foreach( var detector_key in detectorKeys )
            {
                String key = detector_key.Trim();
                if( this._AlarmDetectors.Contains( key ) )
                {
                    AlarmDetector detector = this._AlarmDetectors[key];
                    if( detector != null )
                    {
                        detector.NeedSend = needSend;
                    }
                }
            }

            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<Alarm> DetectAlarms()
        {
            List<Alarm> result = new List<Alarm>();

            foreach( var detector in this._AlarmDetectors )
            {
                if( detector.IsNewAlarm() )
                {
                    Alarm alarm = new Alarm( detector.ConfigurationId, detector.ItemKey, detector.AlarmTime,
                                             detector.AlarmType, detector.AlarmGroupType.Value,
                                             detector.LargeMessage, detector.ShortMessage, detector.NeedSend, detector.ClientId );
                    
                    alarm.OnChangeProperty += detector.OnChangeAlarm;
                    

                    result.Add( alarm );
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="configurationId"></param>
        public void AddValue( OpcUASubscribeItemLDL item, Boolean state, DateTime timeState, Int32? priority )
        {
            AlarmDetector detector = null;
            if( this._AlarmDetectors.Contains( item.Key ) )
            {
                detector = this._AlarmDetectors[item.Key];
            }
            else
            {
                detector = this._SQLiteAlarmDetectorsSaver.Load( item, this._TimeDetectAlarm );
                
                if( detector != null )
                {
                    TimeSpan delta = DateTime.Now - detector.AlarmTime;

                    if( delta > this._TimeActual ) 
                    {                        
                        this._SQLiteAlarmDetectorsSaver.Delete( detector.ItemKey );
                        detector = null;
                    }
                }

                if( detector == null )
                {
                    detector = new AlarmDetector( item.Key, item.AlarmType, item.ConfigurationId, this._TimeDetectAlarm,
                                                  item.LargeMessage, item.ShortMessage, item.ClientId );
                }                

                this._AlarmDetectors.Add( detector );

                detector.OnChange += OnDetectorChange;
            }

            detector.AddValue( state, timeState );
            if( priority.HasValue )
            {
                detector.AlarmGroupType = priority.Value;
            }
        }

        private void OnDetectorChange( Object o, AlarmDetectorChangeArgs e )
        {
            if( false == this._SQLiteAlarmDetectorsSaver.Save( e.Detector.ItemKey, e.Detector.AlarmTime, e.Detector.LastValue,
                                                             e.Detector.State, e.Detector.NeedSend ) )
            {
                
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetAlarmGroupType( String key, Int32 priority )
        {
            if( this._AlarmDetectors.Contains( key ) )
            {
                this._AlarmDetectors[key].AlarmGroupType = priority;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Boolean DetectorExist( String key )
        {
            return this._AlarmDetectors.Contains( key );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Boolean DetectorTypeGroup( String key )
        {
            key = key.Trim();

            if( this._AlarmDetectors.Contains( key ) )
            {
                return this._AlarmDetectors[key].AlarmGroupType.HasValue;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void DetectorDelete( String key )
        {
            key = key.Trim();

            if( this._AlarmDetectors.Contains( key ) )
            {
                this._AlarmDetectors[key].OnChange -= OnDetectorChange;

                this._AlarmDetectors.Remove( key );

                Debug.WriteLine( String.Format( "Delete detector:{0}", key ) );
            }
        }

        #endregion
    }
}
