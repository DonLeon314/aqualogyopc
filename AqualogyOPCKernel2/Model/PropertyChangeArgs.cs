﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class PropertyChangeArgs:
            EventArgs
    {
        /// <summary>
        /// Property New value
        /// </summary>
        public Object NewValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Property old value
        /// </summary>
        public Object OldValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Property name
        /// </summary>
        public String PropertyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public PropertyChangeArgs( String propertyName, Object newValue, Object oldValue )
        {
            this.PropertyName = propertyName;
            this.NewValue = newValue;
            this.OldValue = oldValue;
        }
    }
}
