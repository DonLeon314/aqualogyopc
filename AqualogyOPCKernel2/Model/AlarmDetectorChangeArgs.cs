﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class AlarmDetectorChangeArgs:
            EventArgs
    {
        /// <summary>
        /// Detector 
        /// </summary>
        public AlarmDetector Detector
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="detector"></param>
        public AlarmDetectorChangeArgs( AlarmDetector detector )
        {
            this.Detector = detector;
        }
    }
}
