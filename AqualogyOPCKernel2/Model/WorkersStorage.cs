﻿using AqualogyOPCKernel2.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCConfigModel2;
using AqualogyOPCKernel2.Interfaces;

namespace AqualogyOPCKernel2.Model
{
    public class WorkersStorage:
            IWorkerStorage
    {
        private WorkersCollection _Workers = new WorkersCollection();
        

        public WorkersStorage()
        {

        }


        public void CreateFromBBDD( OPCConfigContext2 dbContext )
        {
            // Load workers

            this._Workers.Clear();

            foreach( var bbdd_worker in dbContext.Workers.Include( "Configurations" ).Include( "WorkerGroups" ).Include( "WorkTimes" ) )
            {
                this._Workers.Add( OpcWorker.CreateFromBBDD( bbdd_worker ) );
            }
        }

        #region interface IWorkerStorage
        /// <summary>
        /// GetWorker by ID
        /// </summary>
        /// <param name="workerId"></param>
        /// <returns></returns>
        public OpcWorker GetWorkerById( Int32 workerId )
        {
            OpcWorker result = null;

            if( this._Workers.Contains( workerId ) )
            {
                result = this._Workers[workerId];
            }

            return result;
        }
        #endregion

}
}
