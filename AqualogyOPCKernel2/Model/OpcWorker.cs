﻿using AqualogyOPCKernel2.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCConfigModel2.ModelBBDD;

namespace AqualogyOPCKernel2.Model
{
    public class OpcWorker
    {
        private UniqueCollection _Emails = new UniqueCollection();
        
        private UniqueCollection _Phones = new UniqueCollection();

        private List<OpcWorkTime> _WorkTimes = new List<OpcWorkTime>();


        public Int32 Id
        {
            get;
            set;
        }

        public String Alias
        {
            get;
            set;
        }

        public IEnumerable<String> Emails
        {
            get
            {
                return this._Emails;
            }
        }

        public IEnumerable<String> Phones
        {
            get
            {
                return this._Phones;
            }
        }


        public Boolean IsPronesPresent
        {
            get
            {
                return ( this._Phones.Count != 0 );
            }
        }

        public Boolean IsEMailsPresent
        {
            get
            {
                return ( this._Emails.Count != 0 );
            }
        }

        public Worker.eChannel Channel
        {
            get;
            private set;
        }

        public IEnumerable<OpcWorkTime> WorkTimes
        {
            get
            {
                return this._WorkTimes;
            }
        }
    
        /// <summary>
        /// Constructor
        /// </summary>
        private OpcWorker()
        {

        }

        /// <summary>
        /// BBDD creator
        /// </summary>
        /// <param name="bbddWorker"></param>
        /// <returns></returns>
        public static OpcWorker CreateFromBBDD( Worker bbddWorker )
        {
            OpcWorker result = new OpcWorker();

            result.Id = bbddWorker.Id;
            result.Alias = bbddWorker.Alias;
            result.Channel = bbddWorker.Channel;

            // Phones
            var phones = bbddWorker.Phones.Split( new Char[]{';'} );

            foreach( var p in phones.Select( s => s.Trim() ).Where( s => false == String.IsNullOrWhiteSpace( s ) ).ToList() )
            {
                if( result._Phones.Contains( p ) == false )
                {
                    result._Phones.Add( p );
                }
            }

            // Emails
            var emails = bbddWorker.EMails.Split( new Char[] { ';' } );

            foreach( var p in emails.Select( s => s.Trim() ).Where( s => false == String.IsNullOrWhiteSpace( s ) ).ToList() )
            {
                if( result._Emails.Contains( p ) == false )
                {
                    result._Emails.Add( p );
                }
            }

            //TODO: ???    
            // Configurations


            // WorkTimes
            foreach( WorkTime bbdd_worktime in bbddWorker.WorkTimes )
            {
                result._WorkTimes.Add( new OpcWorkTime( bbdd_worktime.Id, bbdd_worktime.WeekDays,
                                                        bbdd_worktime.StartTime, bbdd_worktime.EndTime ) );
            }

            return result;
        }
    }
}
