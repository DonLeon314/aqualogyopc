﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCConfigModel2.ModelBBDD;

namespace AqualogyOPCKernel2.Model
{
    public class WorkerSmallInfo
    {
        private List<Int32> _InConfigurations = new List<Int32>();

        /// <summary>
        /// 
        /// </summary>
        public Int32 WorkerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Work time selector
        /// </summary>
        public AlarmCluster.eWorkTImeSelector WorkTImeSelector
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configurationId"></param>
        /// <returns></returns>
        public Boolean PresentInConfiguration( Int32 configurationId )
        {
            return this._InConfigurations.Contains( configurationId );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workerId"></param>
        /// <param name="inConfigurations"></param>
        public WorkerSmallInfo( Int32 workerId, AlarmCluster.eWorkTImeSelector workTImeSelector,  IEnumerable<Int32> inConfigurations )
        {
            this.WorkerId = workerId;
            this._InConfigurations.AddRange( inConfigurations );
            this.WorkTImeSelector = workTImeSelector;
        }
    }
}
