﻿using AqualogyOPCKernel2.OPCUASubscribeItems;
using OPCUAClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.OPCUASubscribeItems.Visitors;
using OPCUAClient.Interfaces;

namespace AqualogyOPCKernel2.Model
{
    public class RawValuesProcessor
    {        

        private List<OpcUASubscribeBase> _ForSubscribePrioryty = new List<OpcUASubscribeBase>();
         
        private ProcessGroupTypeVisitor _GroupTypeProcessor = null;

        private IOPCUAWatcher _IOPCUAWatcher;

        private AlarmDetectorsStorage _AlarmDetectors = null;

        /// <summary>
        /// 
        /// </summary>
        public IAlarmDetectors AlarmDetectors
        {
            get
            {
                return this._AlarmDetectors;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCUAWatcher"></param>
        public RawValuesProcessor( IOPCUAWatcher iOPCUAWatcher )
        {
            //TODO: change code - alarm detect time from BBDD
            this._AlarmDetectors = new AlarmDetectorsStorage( 10, 10 );

            this._GroupTypeProcessor = new ProcessGroupTypeVisitor( iOPCUAWatcher, this._AlarmDetectors );
            this._IOPCUAWatcher = iOPCUAWatcher;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public IReadOnlyCollection<Alarm> Process( List<OPCItemClientArgs> values )
        {
            if( values != null )
            {
                var normal_values = Normalize( values );

                ProcessGroupType( normal_values );
            }

            // Find Alarmas
            return this._AlarmDetectors.DetectAlarms();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private IEnumerable<NormalNewItem> Normalize( IEnumerable<OPCItemClientArgs> values )
        {
            var normalizeItems = values.Select( i => NormalNewItem.CreateFromRaw( i ) ).Where( i1 => i1 != null );

            return normalizeItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        private void ProcessGroupType( IEnumerable<NormalNewItem> values )
        {
            this._GroupTypeProcessor.Initialize();

            foreach( var item in values )
            {
                this._GroupTypeProcessor.Value = item.LastValue;
                this._GroupTypeProcessor.ValueTime = item.LastTimeValue.ToLocalTime();

                item.OpcUASubscribeBase.Accept( this._GroupTypeProcessor );
            }

            if( this._IOPCUAWatcher != null )
            {
                this._IOPCUAWatcher.Subscribe( this._GroupTypeProcessor.ForSubscription );
            }

            this._GroupTypeProcessor.Initialize();
        }

    }
}
