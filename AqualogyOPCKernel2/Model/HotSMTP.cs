﻿using AqualogyOPCKernel2.Hot;
using AqualogyOPCKernel2.MailSender;
using OPCConfigModel2;
using OPCConfigModel2.ExportClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class HotSMTP
    {
        private static SMTPConfig _SMTPConfig = null;
        
        private static SMTPSender _SMTPSender = null;

        private static HotIndicator _HotIndicator = new HotIndicator("HOT_SMTPCONFIG", "DateTime");

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static SMTPSender GetSMTPSender()
        {
            SMTPSender result = null;            

            try
            {
                using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
                {
                    using( var transaction = dbContext.Database.BeginTransaction() )
                    { 
                        if( _HotIndicator.IsActual(dbContext) == false )
                        {
                            _SMTPSender = null;
                            _SMTPConfig = null;
                        }

                        if( _SMTPSender == null )
                        {
                            if( _SMTPConfig == null )
                            {
                                LoadSMTPConfig(dbContext);
                            }

                            if( _SMTPConfig != null )
                            {
                                _SMTPSender = new SMTPSender(_SMTPConfig);
                                if( _SMTPSender != null )
                                {
                                    _HotIndicator.SetActual();
                                }
                            }
                        }
                    }

                }
                result = _SMTPSender;

            }catch( Exception ex )
            {
                Debug.WriteLine( String.Format( "GetSMTPSender() ERROR:{0}", ex.Message ) );
            }

            return result;
        }

        private static void LoadSMTPConfig( OPCConfigContext2 dbContext )
        {
            SMTPConfig smtp_config = new SMTPConfig(
#if DEBUG
            dbContext.GetParameter("SMTP_INFORMATOR", "Host", "smtp.google.com"),
            dbContext.GetParameter("SMTP_INFORMATOR", "Port", 587),
            dbContext.GetParameter("SMTP_INFORMATOR", "User", "donleon314@gmail.com"),
            dbContext.GetParameter("SMTP_INFORMATOR", "Password", "Elcorazon2015"),
            dbContext.GetParameter("SMTP_INFORMATOR", "From", "donleon314@gmail.com"),
            dbContext.GetParameter("SMTP_INFORMATOR", "EnableSSL", false)
#else
            dbContext.GetParameter( "SMTP_INFORMATOR", "Host", "" ),
            dbContext.GetParameter( "SMTP_INFORMATOR", "Port", 0 ),
            dbContext.GetParameter( "SMTP_INFORMATOR", "User", "" ),
            dbContext.GetParameter( "SMTP_INFORMATOR", "Password", "" ),
            dbContext.GetParameter( "SMTP_INFORMATOR", "From", "" ),
            dbContext.GetParameter( "SMTP_INFORMATOR", "EnableSSL", false )
#endif
            );

            if( false == String.IsNullOrWhiteSpace( smtp_config.Host ) )
            {
                _SMTPConfig = smtp_config;                
            }
        }
    
    }
}
