﻿using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using OPCConfigModel2.ModelBBDD;
using System;
using System.Collections.Generic;
using System.Text;

namespace AqualogyOPCKernel2.Model
{
    public class OpcConfiguration
    {
        #region Child extractor
        private class ChildExtractor:
                    IOPCUASubscribeItemVisitor
        {
            public IReadOnlyCollection<OpcUASubscribeBase> ChildItems = null;

            public Boolean Visit( OpcUASubscribeItemLDL item )
            {
                this.ChildItems = item.ChildItems();

                return true;
            }
            
            public Boolean Visit( OpcUASubscribeItemPriority item )
            {
                return false;
            }
        }
        #endregion 
        private List<OpcUASubscribeBase> _Items = new List<OpcUASubscribeBase>();

        public Int32 Id
        {
            get;
            private set;
        }

        public String Description
        {
            get;
            private set;
        }

        public Configuration.eConfigState ConfigState
        {
            get;
            set;
        }

        public IEnumerable<OpcUASubscribeBase> GetOpcUAItems( Boolean allItems )
        {
            ChildExtractor child_extractor = null;

            foreach( var item in this._Items )
            {
                yield return item;
                if( allItems )
                {
                    if( child_extractor == null )
                    {
                        child_extractor = new ChildExtractor();
                    }

                    if( item.Accept( child_extractor ) )
                    {
                        foreach( var child_item in child_extractor.ChildItems )
                        {
                            yield return child_item;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        private OpcConfiguration()
        {
        }

        /// <summary>
        /// Creator from BBDD
        /// </summary>
        /// <param name="bbddConfiguration"></param>
        /// <returns></returns>
        public static OpcConfiguration CreteFromBBDD( Configuration bbddConfiguration )
        {
            OpcConfiguration result = new OpcConfiguration();

            result.Id = bbddConfiguration.Id;
            result.Description = bbddConfiguration.Description;

            foreach( var bbdd_tag in bbddConfiguration.Tags )
            {
                OpcUASubscribeBase new_item = SubscribeItemCreator.Create( bbdd_tag );
                if( new_item != null )
                {
                    result._Items.Add( new_item );
                }
            }

            return result;
        }
    }
}
