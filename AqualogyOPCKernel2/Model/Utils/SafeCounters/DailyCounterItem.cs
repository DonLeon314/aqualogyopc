﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils.SafeCounters
{
    /// <summary>
    /// Dayly counter item
    /// </summary>
    public class DailyCounterItem
    {
        /// <summary>
        /// Counter value
        /// </summary>
        public Int32 Value
        {
            get;
            set;
        }

        /// <summary>
        /// Key counter
        /// </summary>
        public String Key
        {
            get;
            private set;
        }

        public DateTime LastCounterChanged
        {
            get;
            private set;
        }

        public Int32 MaxValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public DailyCounterItem( String key, Int32 value, DateTime lastCounterChanged, Int32 maxValue )
        {
            this.Key = key;
            this.Value = value;
            this.LastCounterChanged = lastCounterChanged;
            this.MaxValue = maxValue;
        }
    }
}
