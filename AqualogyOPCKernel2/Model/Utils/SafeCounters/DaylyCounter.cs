﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils.SafeCounters
{
    public class DailyCounter
    {
        public Int32 MaxCounter
        {
            get;
            private set;
        }
        public String _CounterName;

        public static IDaylyCounterSaver _Saver = null;
        public static Object _SyncObject = new Object();
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="counterName"></param>
        /// <param name="maxCounter"></param>
        public DailyCounter( String counterName, Int32 maxCounter )
        {
            
            this.MaxCounter = maxCounter;
            this._CounterName = counterName.Trim().ToUpper();

        }

        public Boolean Enable()
        {
            Boolean result = false;

            if (this.MaxCounter == 0)
            {
                result = true;
            }
            else
            {
                DailyCounterItem data = null;
                try
                {
                    lock (_SyncObject)
                    {
                        DateTime current_time = DateTime.Now;

                        if (_Saver == null)
                        {
                            CreateSaver();
                        }

                        data = _Saver.GetCounter(this._CounterName);

                        if (data == null ||
                            (data.LastCounterChanged.Year != current_time.Year ||
                              data.LastCounterChanged.Month != current_time.Month ||
                              data.LastCounterChanged.Day != current_time.Day ) ||
                              data.MaxValue != this.MaxCounter )
                        {
                            data = new DailyCounterItem(this._CounterName, this.MaxCounter, DateTime.Now, this.MaxCounter );
                        }                        

                        result = (data.Value != 0);

                        if( data.Value > 0 )
                        {
                            data.Value = data.Value - 1;
                            _Saver.SaveCounter( this._CounterName, data.Value, this.MaxCounter );
                        }

                    }
                }
                catch (Exception ex)
                {

                }
            }
            return result;
        }

        private void CreateSaver()
        {
            _Saver = new SQLiteCounterSaver();
        }
    }
}
