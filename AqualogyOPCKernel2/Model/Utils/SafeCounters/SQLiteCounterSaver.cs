﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils.SafeCounters
{
    class SQLiteCounterSaver:
            IDaylyCounterSaver
    {
        private SQLiteConnection _Connection = null;

        #region interface IDaylyCounterSaver
        
        public Boolean SaveCounter(String counterName, Int32 counterValue, Int32 maxCounter)
        {
            try
            {
                String sql_query = "SELECT id FROM Counters WHERE CounterName = :pCounterName";
                Int64 id = 0;

                using (SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
                {
                    cmd.Parameters.Add(new SQLiteParameter("pCounterName", counterName));

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if( reader.Read() )
                        {
                            id = reader.GetInt64(0);
                        }
                    }
                }

                if (id == 0)
                {
                    sql_query = "INSERT INTO Counters ( CounterName, LastDate, ValueCounter, MaxCounter ) VALUES(" +
                                            " :pCounterName, :pLastDate, :pValueCounter, :pMaxCounter )";
                }
                else
                {
                    sql_query = "UPDATE Counters SET CounterName = :pCounterName," +
                                                    " LastDate = :pLastDate," +
                                                    " ValueCounter = :pValueCounter" +
                                                    " MaxCounter = :pMaxCounter" +
                                                    " WHERE id = :pID";
                }

                using (SQLiteCommand cmd = new SQLiteCommand(sql_query, GetConnection()))
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "pCounterName", counterName ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pLastDate", DateTime.Now.Ticks ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pValueCounter", counterValue ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pMaxCounter", maxCounter ) );

                    cmd.Parameters.Add( new SQLiteParameter( "pID", id ) );

                    Object execute_result = cmd.ExecuteScalar();
                }

                return true;

            }catch( Exception ex )
            {
                Debug.WriteLine(String.Format("SQLiteCounterSaver.SaveCounter error: {0}", ex.Message) );
            }

            return false;
        }

        public DailyCounterItem GetCounter( String counterName )
        {
            String sql_query = "SELECT LastDate, ValueCounter, MaxCounter" +
                                    " FROM Counters" +
                                    " WHERE CounterName = :pCounterName";

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SQLiteParameter("pCounterName", counterName ) );
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        Int64 bbdd_last_date = reader.GetInt64( 0 );
                        Int32 bbdd_value = reader.GetInt32(1);
                        Int32 bbdd_max_counter = reader.GetInt32( 2 );

                        return new DailyCounterItem( counterName, bbdd_value, new DateTime( bbdd_last_date ), bbdd_max_counter );

                    }
                }
            }

            return null;
        }
        #endregion

        /// <summary>
        /// Get connection
        /// </summary>
        /// <returns></returns>
        private SQLiteConnection GetConnection()
        {
            if( this._Connection == null )
            {
                String fname = "DailyCounters.db3".ToLocalFileName();

                if( false == File.Exists( fname ) )
                {
                    CreateDB( fname );
                }

                OpenDB( fname );

                CorrectDB();
            }

            return this._Connection;
        }

        private void CreateDB( String dbFileName )
        {
            SQLiteConnection.CreateFile(dbFileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFileName"></param>
        private void OpenDB(String dbFileName)
        {
            this._Connection = new SQLiteConnection(String.Format("Data Source={0}; Version=3;", dbFileName));
            this._Connection.Open();
        }

        private void CorrectDB()
        {

            if (SQLiteEx.SQLiteEx.TableExist(this._Connection, "Counters") == false)
            {
                using (SQLiteCommand command = new SQLiteCommand(this._Connection))
                {
                    command.CommandText = String.Format(@"CREATE TABLE [Counters] (" +
                                                                "[id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                                                "[LastDate] NUMERIC NOT NULL, " +
                                                                "[ValueCounter] NUMERIC NOT NULL, " +
                                                                "[MAxCounter] NUMERIC NOT NULL, " +
                                                                "[CounterName] VARCHAR(128) NOT NULL " +
                                                                ")");

                    command.CommandType = CommandType.Text;
                    Int32 result = command.ExecuteNonQuery();
                }
            }
        }

        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (this._Disposed)
                return;

            if (disposing)
            {
                try
                {
                    // Free any other managed objects here. 
                    if (this._Connection != null)
                    {
                        this._Connection.Close();
                    }                    
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(String.Format("SQLLite error: {0}", ex.Message));
                }
                finally
                {
                    this._Connection = null;
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion

    }
}
