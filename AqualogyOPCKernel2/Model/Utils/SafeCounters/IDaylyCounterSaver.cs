﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils.SafeCounters
{
    public interface IDaylyCounterSaver
    {
        Boolean SaveCounter( String counterName, Int32 counterValue, Int32 maxCounter );
        DailyCounterItem GetCounter( String counterName );
    }
}
