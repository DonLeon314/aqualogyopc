﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils
{

    public class OpcClusterEqualityComparer:
                IEqualityComparer<OpcCluster>
    {
        #region IEqualityComparer<OpcCluster>

        public bool Equals( OpcCluster x, OpcCluster y )
        {
            return ( ( x.AlarmGroup == y.AlarmGroup ) & ( String.Compare( x.AlarmType, x.AlarmType, true ) == 0 ) );
        }

        public int GetHashCode( OpcCluster obj )
        {
            string combined = String.Format("{0}-{1}", obj.AlarmGroup, obj.AlarmType.ToUpper() );
            return ( combined.GetHashCode() );
        }

        #endregion
    }

}
