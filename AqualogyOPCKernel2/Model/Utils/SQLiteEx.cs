﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.SQLiteEx
{
    public class SQLiteEx
    {
        /// <summary>
        /// Clear SQLite table
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static Int32 ClearTable( SQLiteConnection connection, String tableName )
        {
            if( false == TableExist( connection, tableName ) )
            {
                return 0;
            }

            String sql_query = String.Format( "DELETE FROM {0}", tableName );
            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, connection ) )
            {
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// SQLite table exist
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static Boolean TableExist( SQLiteConnection connection, String tableName )
        {
            tableName = tableName.Trim();

            if( String.IsNullOrWhiteSpace( tableName ) )
            {
                return false;
            }

            String sql_query = String.Format( "SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{0}'", tableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, connection ) )
            {
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        String value = reader[0].ToString();
                        return ( String.Compare(value, tableName, true ) == 0 );
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Boolean ColumnOnTableExist( SQLiteConnection connection, String tableName, String columnName )
        {
            tableName = tableName.Trim();
            columnName = columnName.Trim();

            if( String.IsNullOrWhiteSpace( tableName ) || String.IsNullOrWhiteSpace( columnName ) )
            {
                return false;
            }

            String sql_query = String.Format( "PRAGMA table_info('{0}')", tableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, connection ) )
            {
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    while( reader.Read() )
                    {
                        String column_name = reader[1].ToString();
                        if( String.IsNullOrWhiteSpace( column_name ) )
                        {
                            continue;
                        }
                        column_name = column_name.Trim();
                        if( String.Compare( columnName, column_name, true ) == 0 )
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
