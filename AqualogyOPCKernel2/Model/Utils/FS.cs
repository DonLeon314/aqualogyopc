﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils
{
    public static class FS
    {
        public static String ToLocalFileName( this String fileName )
        {
            String result = System.AppDomain.CurrentDomain.BaseDirectory;
            fileName = fileName.Trim();

            if( result[result.Length - 1] != '\\' )
            {
                result += @"\";
            }

            result = ( result + fileName );

            return result;
        }
    }
}
