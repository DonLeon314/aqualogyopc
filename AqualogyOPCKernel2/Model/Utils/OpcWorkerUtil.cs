﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model.Utils
{
    public static class OpcWorkerUtil
    {

        public static Boolean ChannelPresent( this OpcWorker _this )
        {
            switch ( _this.Channel )
            {
                case OPCConfigModel2.ModelBBDD.Worker.eChannel.EMail:

                     if( _this.IsEMailsPresent )
                     {
                        return true;
                     }
                     break;

                case OPCConfigModel2.ModelBBDD.Worker.eChannel.Sms:

                    if( _this.IsPronesPresent )
                    {
                        return true;
                    }
                    break;

                case OPCConfigModel2.ModelBBDD.Worker.eChannel.SmsAndEmail:

                    if( _this.IsPronesPresent || _this.IsEMailsPresent )
                    {
                        return true;
                    }

                    break;
            }

            return false;
        }


        public static Boolean IsOnline( this OpcWorker _this, DateTime? dateTime = null )
        {

            if( dateTime.HasValue == false )
            {
                dateTime = DateTime.Now;
            }

            foreach( var wt in _this.WorkTimes )
            {
                if( dateTime.Value.IsWithin2( wt.StartTime, wt.EndTime, wt.WeekDays ) )
                {
                    return true;
                }
            }

            return false;
        }

    }
}
