﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCConfigModel2;
using OPCUAClient.Interfaces;

namespace AqualogyOPCKernel2.Model
{
    internal class ConnectionsStorage:
                IOPCConnectionInfos
    {
        #region Collection defines
        private class ConnectionInfo:
                    IOPCUAConnectionInfo
        {
            public Int32 Id
            {
                get;
                private set;
            }
            public String UrlServer
            {
                get;
                private set;
            }

            public Int32 PublishingInterval
            {
                get;
                private set;
            }

            public Int32 TimeOut
            {
                get;
                private set;
            }

            public Int32 DefaultSamplingInterval
            {
                get;
                private set;
            }

            public Int32 DefaultQueueSize
            {
                get;
                private set;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="urlServer"></param>
            /// <param name="publishingInterval"></param>
            /// <param name="timeOut"></param>
            /// <param name="defaultSamplingInterval"></param>
            /// <param name="defaultQueueSize"></param>
            public ConnectionInfo( Int32 id, String urlServer, Int32 publishingInterval, Int32 timeOut, Int32 defaultSamplingInterval, Int32 defaultQueueSize )
            {
                this.Id = id;
                this.UrlServer = urlServer;
                this.DefaultQueueSize = DefaultQueueSize;
                this.DefaultSamplingInterval = defaultSamplingInterval;
                this.TimeOut = timeOut;
            }
        }

        private class ConnectionsCollection:
                KeyedCollection<Int32, ConnectionInfo>
        {
            public ConnectionsCollection()
            {

            }

            protected override Int32 GetKeyForItem( ConnectionInfo item )
            {
                return item.Id;
            }

        }
        #endregion

        private ConnectionsCollection _Connections = new ConnectionsCollection();


        /// <summary>
        /// DefaultConstructor
        /// </summary>
        public ConnectionsStorage()
        {
        }

        /// <summary>
        /// Create from BBDD
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static ConnectionsStorage CreateFromBBDD( OPCConfigContext2 dbContext )
        {
            ConnectionsStorage result = new ConnectionsStorage();

            result.LoadFromBBDD( dbContext );
        

            return result;
        }

        /// <summary>
        /// Load from BBDD
        /// </summary>
        /// <param name="dbContext"></param>
        public void LoadFromBBDD( OPCConfigContext2 dbContext )
        {
            this._Connections.Clear();

            foreach( var bbdd_server in dbContext.UAServers )
            {                
                this._Connections.Add( new ConnectionInfo( bbdd_server.Id,
                                                           bbdd_server.OPCUAUrl,
                                                           bbdd_server.PublishingInterval,
                                                           bbdd_server.Timeout,
                                                           bbdd_server.SamplingInterval,
                                                           bbdd_server.QueueSize ) );
            }
        }

        #region interface IOPCConnectionInfos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlServer"></param>
        /// <returns></returns>
        public IOPCUAConnectionInfo FindConnectionInfo( Int32 idServer )
        {
            //TODO: Autoload

            if( this._Connections.Contains( idServer ) )
            {
                return this._Connections[idServer];
            }

            return null;   
        }

        #endregion
    }
}
