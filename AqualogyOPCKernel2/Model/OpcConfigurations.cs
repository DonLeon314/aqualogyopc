﻿using OPCConfigModel2;
using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AqualogyOPCKernel2.OPCUASubscribeItems;

namespace AqualogyOPCKernel2.Model
{
    public class OpcConfigurations
    {

    #region Configuration collection
        private class ConfigurationCollection:
                KeyedCollection<Int32, OpcConfiguration>
        {
            public ConfigurationCollection()
            {
            }

            protected override Int32 GetKeyForItem( OpcConfiguration item )
            {
            return item.Id;
            }
        }
        #endregion

        private ConfigurationCollection _Collection = new ConfigurationCollection();
        private IOPCUAWatcher _IOPCUAWatcher = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iOPCUAWatcher"></param>
        public OpcConfigurations( IOPCUAWatcher iOPCUAWatcher )
        {
            this._IOPCUAWatcher = iOPCUAWatcher;
        }


        public void UnsubscribeAll()
        {
            if( this._IOPCUAWatcher != null )
            {
                foreach( var cfg in this._Collection )
                {
                    this._IOPCUAWatcher.UnSubscribe( cfg.GetOpcUAItems( true ) );
                }
            }
        }

        public void SubscribeAll()
        {
            if( this._IOPCUAWatcher != null )
            {
                foreach( var cfg in this._Collection )
                {
                    this._IOPCUAWatcher.Subscribe( cfg.GetOpcUAItems( false ) );
                }
            }
        }

        /// <summary>
        /// Load from BBDD
        /// </summary>
        /// <param name="dbContext"></param>
        public void LoadFromBBDD( OPCConfigContext2 dbContext )
        {
            // Unsubscribe
            UnsubscribeAll();

            this._Collection.Clear();

            foreach( var bbdd_config in dbContext.Configurations.Include("Tags") )
            {
                OpcConfiguration new_cfg = OpcConfiguration.CreteFromBBDD( bbdd_config );

                if( new_cfg != null ) // ????
                {
                    this._Collection.Add( new_cfg );
                }
            }

            // Subscribe
            SubscribeAll();

        }
    }
}
