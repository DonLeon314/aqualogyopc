﻿using AqualogyOPCKernel2.OPCUASubscribeItems;
using Opc.Ua;
using OPCUAClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class NormalNewItem
    {
        public OpcUASubscribeBase OpcUASubscribeBase
        {
            get;
            private set;
        }

        public Object LastValue
        {
            get;
            private set;
        }

        public DateTime LastTimeValue
        {
            get;
            private set;
        }

        private NormalNewItem()
        {

        }

        public static NormalNewItem CreateFromRaw( OPCItemClientArgs rawData )
        {
            NormalNewItem result = new NormalNewItem();

            result.OpcUASubscribeBase = rawData.ConnectionArgs.IPCUASubscribeItem as OpcUASubscribeBase;

            var goods = rawData.ConnectionArgs.Values.ToList().Where( dv => DataValue.IsGood( dv ) ).ToList();

            goods.Sort( ( left, right ) => -1 * left.ServerTimestamp.CompareTo( right.ServerTimestamp ) );

            var first = goods.FirstOrDefault();

            if( first == null )
            {
                return null;
            }

            result.LastValue = first.Value;
            result.LastTimeValue = first.ServerTimestamp;
            
            return result;
        }
    }
}
