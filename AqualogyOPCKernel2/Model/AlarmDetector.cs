﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Model
{
    public class AlarmDetector
    {

        public EventHandler<AlarmDetectorChangeArgs> OnChange = (o, e) => {
                                                                            Debug.WriteLine( "Change detector" );            
                                                                            return;
                                                                          };
        private void PropertyChange()
        {
            this.OnChange( this, new AlarmDetectorChangeArgs( this ) );
        }

        private Boolean _NeedSend;

        public Boolean NeedSend
        {
            get
            {
                return this._NeedSend;
            }
            set
            {
                if( this._NeedSend != value )
                {
                    this._NeedSend = value;
                    PropertyChange();
                }
            }
        }
        private DateTime _AlarmTime;
        public DateTime AlarmTime
        {
            get
            {
                return this._AlarmTime;
            }

            private set
            {
                if( this._AlarmTime != value )
                {
                    this._AlarmTime = value;
                    PropertyChange();
                }
            }
        } 

        private String _AlarmType;

        public String AlarmType
        {
            get
            {
                return this._AlarmType;
            }
            private set
            {
                if( String.Compare( this._AlarmType, value, true ) != 0 )
                {
                    this._AlarmType = value;
                    PropertyChange();
                }
            }
        }

        private String _ItemKey;
        /// <summary>
        /// Id Alarm processor
        /// </summary>
        public String ItemKey
        {
            get
            {
                return this._ItemKey;
            }

            private set
            {
                if( String.Compare( this._ItemKey, value, true ) != 0 )
                {
                    this._ItemKey = value;
                    PropertyChange();
                }
            }
        }

        private Int32 _ConfigurationId;

        public Int32 ConfigurationId
        {
            get
            {
                return this._ConfigurationId;
            }

            private set
            {
                if( this._ConfigurationId != value )
                {
                    this._ConfigurationId = value;
                    PropertyChange();
                }
            }
        }

        public TimeSpan TimeDetectedAlarm
        {
            get;
            private set;
        }

        private Boolean _LastValue;
        public Boolean LastValue
        {
            get
            {
                return this._LastValue;
            }

            private set
            {
                if( this._LastValue != value )
                {
                    this._LastValue = value;
                    PropertyChange();
                }
            }
        }

        private Boolean _State;
        public Boolean State
        {
            get
            {
                return this._State;
            }
            private set
            {
                if( this._State != value )
                {
                    this._State = value;
                    PropertyChange();
                }
            }
        }

        public Int32? AlarmGroupType
        {
            get;
            set;
        }


        public String LargeMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Shorm message
        /// </summary>
        public String ShortMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Client
        /// </summary>
        public Int32 ClientId
        {
            get;
            private set;
        }

        public void OnChangeAlarm( Object o, PropertyChangeArgs a )
        {
            if( String.Compare( a.PropertyName, "NeedSend", true ) == 0 )
            {
                if( a.NewValue is Boolean )
                {
                    this.NeedSend = (Boolean)a.NewValue;
                }
            }
            else
            {
            }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="history"></param>
        public AlarmDetector( String itemKey, String alarmType, Int32 configurationId, 
                              TimeSpan timeDetectedAlarm, String largeMessage, String shortMessage, Int32 clientId )
        {            
            this._ItemKey = itemKey;
            this._AlarmType = alarmType;
            this._ConfigurationId = configurationId;
            
            this.TimeDetectedAlarm = timeDetectedAlarm;
            
            this._State = false;
            this._LastValue = false;
            this._AlarmTime = DateTime.Now;

            this._NeedSend = false;

            this.LargeMessage = largeMessage;
            this.ShortMessage = shortMessage;

            this.ClientId = clientId;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="timeDetectedAlarm"></param>
        /// <param name="state"></param>
        /// <param name="time"></param>
        public AlarmDetector( String itemKey, String alarmType, Int32 configurationId, TimeSpan timeDetectedAlarm, 
                              Boolean lastValue, Boolean state, DateTime time, Boolean needSend )
        {
            this._ItemKey = itemKey;
            this._AlarmType = alarmType;
            this._ConfigurationId = configurationId;

            this.TimeDetectedAlarm = timeDetectedAlarm;
            
            this._State = state;
            this._LastValue = lastValue;
            this._AlarmTime = time;

            this._NeedSend = needSend;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean IsNewAlarm()
        {
            DateTime time = DateTime.Now;

            var dt = time - this.AlarmTime;

            Debug.WriteLine( String.Format( "Item:{0} Current State: {1} CurrentTime:{2}", this.ItemKey, this.State, time ) );

            if( dt >= this.TimeDetectedAlarm )
            {
                // Alarm ON
                if( this.State == false && this.LastValue == true )
                {
                    Debug.WriteLine( String.Format( "Item:{0} Change State: Alarm ON Current time:{1}", this.ItemKey, time ) );
                    this.State = true;
                    this.NeedSend = false;
                    PropertyChange();
                    return true;
                }

                // Alarm OFF
                if( this.State == true && this.LastValue == false )
                {
                    Debug.WriteLine( String.Format( "Item:{0} Change State: Alarm OFF Current time:{1}", this.ItemKey, time ) );
                    this.State = false;
                    this.NeedSend = false;
                    PropertyChange();
                    return false;
                }
            }

            if( this.NeedSend )
            {
                Debug.WriteLine( String.Format( "Item:{0} Need resend", this.ItemKey ) );
            }

            return this.NeedSend;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="time"></param>
        public void AddValue( Boolean newValue, DateTime time )
        {
            if( newValue != this.LastValue )
            {
               // iOPCInfo.Info( String.Format( "Item:{0} Change Last value - State:{1} {2} -> {3} time:{4}", this.Tag.TagId, this.State, this.LastValue, newValue, time ), eInfoType.Debug );
                this._AlarmTime = time;
                this.LastValue = newValue;
                this.NeedSend = false;
                PropertyChange();
            }
            else
            {
                //iOPCInfo.Info( String.Format( "Item:{0} No change Last value - State:{1} Last value:{2} time:{3}", this.Tag.TagId, this.State, this.LastValue, time ), eInfoType.Debug );
            }
        }
    }
}
