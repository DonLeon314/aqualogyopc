﻿using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model;
using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.OPCUASubscribeItems.Visitors
{
    public class ProcessGroupTypeVisitor:
                    IOPCUASubscribeItemVisitor
    {
        private IOPCUAWatcher _IOPCUAWatcher = null;
        private IAlarmDetectors _IAlarmDetectors = null;

        private List<OpcUASubscribeItemPriority> _ForSubscription = new List<OpcUASubscribeItemPriority>();
               

        public Object Value
        {
            get;
            set;
        }

        public DateTime ValueTime
        {
            get;
            set;
        }

        public IEnumerable<OpcUASubscribeItemPriority> ForSubscription
        {
            get
            {
                return this._ForSubscription;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iOPCUAWatcher"></param>
        public ProcessGroupTypeVisitor( IOPCUAWatcher iOPCUAWatcher, IAlarmDetectors iAlarmDetectors )
        {
            this._IOPCUAWatcher = iOPCUAWatcher;
            this._ForSubscription.Clear();
            this._IAlarmDetectors = iAlarmDetectors;
        }


        public void Initialize()
        {
            this._ForSubscription.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean Visit( OpcUASubscribeItemLDL item )
        {
            if( this._IAlarmDetectors != null )
            {
                Int32? priority = null;

                if( this._IAlarmDetectors.DetectorTypeGroup( item.Key ) == false )
                {
                    if( this._IOPCUAWatcher != null )
                    {
                        // Create priority name
                        String tag_priority = item.ItemName.CreatePriority();
                                               
                        // Load priority value
                        Object value = this._IOPCUAWatcher.LoadValue( item.IdServer, tag_priority );

                        if( value != null || value is Int32 )
                        {
                            var child_item = new OpcUASubscribeItemPriority( item, tag_priority );
                            item.AddChildItem( child_item );
                            this._ForSubscription.Add( child_item );                            
                            priority = (Int32)value;
                        }
                    }
                }

#if DEBUG
                Boolean alarm_value = ( (Int32)this.Value < 30 ? true : false );
#else
                Boolean alarm_value = ( (Int32)this.Value > 0 );    
#endif
                this._IAlarmDetectors.AddValue( item, alarm_value, this.ValueTime, priority );

            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean Visit( OpcUASubscribeItemPriority item )
        {
            if( this._IAlarmDetectors != null )
            {
                if( this.Value is Int32 )
                {
                   Int32 priority_value = (Int32)this.Value;
                   this._IAlarmDetectors.SetAlarmGroupType( item.ParentItem.Key, priority_value );
                }
            }
            return true;
        }
    }
}
