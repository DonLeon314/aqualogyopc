﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCUAClient.Interfaces;
using AqualogyOPCKernel2.Interfaces;

namespace AqualogyOPCKernel2.OPCUASubscribeItems.Visitors
{
    public class ResubscribeNormalizator:
            IOPCUASubscribeItemVisitor
    {
        private IAlarmDetectors _IAlarmDetectors = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ResubscribeNormalizator( IAlarmDetectors iAlarmDetectors )
        {

            this._IAlarmDetectors = iAlarmDetectors;

        }

        #region IOPCUASubscribeItemVisitor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean Visit( OpcUASubscribeItemLDL item )
        {
            if( this._IAlarmDetectors != null )
            {

                this._IAlarmDetectors.DetectorDelete( item.Key );
            }

            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean Visit( OpcUASubscribeItemPriority item )
        {
            OpcUASubscribeItemLDL parent = item.ParentItem as OpcUASubscribeItemLDL;
            if( parent != null )
            {
                parent.DeleteChild( item.Key );
            }

            return false;
        }
        #endregion
    }
}
