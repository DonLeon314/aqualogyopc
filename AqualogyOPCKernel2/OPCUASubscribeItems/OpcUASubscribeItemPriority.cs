﻿using AqualogyOPCKernel2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.OPCUASubscribeItems
{
    public class OpcUASubscribeItemPriority:
          OpcUASubscribeBase
    {
        public OpcUASubscribeItemPriority( OpcUASubscribeBase parentItem, String itemName,
                                           Int32 samplingInterval = 30, Int32 queueSize = 10 ):
                          base( String.Format( "{0}-Priority", parentItem.Key ), itemName, parentItem.IdServer,
                                                samplingInterval, queueSize )
        {
            this.ParentItem = parentItem;
        }

        /// <summary>
        /// Parent key
        /// </summary>
        public OpcUASubscribeBase ParentItem
        {
            get;
            private set;
        }

        /// <summary>
        /// Accept
        /// </summary>
        /// <param name="iVisitor"></param>
        /// <returns></returns>
        public override Boolean Accept( IOPCUASubscribeItemVisitor iVisitor )
        {
            return iVisitor.Visit( this );
        }
    }
}
