﻿using AqualogyOPCKernel2.Collections;
using AqualogyOPCKernel2.Interfaces;
using OPCConfigModel2.ModelBBDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.OPCUASubscribeItems
{
    public class OpcUASubscribeItemLDL:
            OpcUASubscribeBase
    {
        #region ChildItems

        private OpcUASubscribeBaseKeyedCollection _ChildItems = new OpcUASubscribeBaseKeyedCollection();               

        #endregion


        private String _AlarmType = String.Empty;

        /// <summary>
        /// 
        /// </summary>
        public Int32 ConfigurationId
        {
            get;
            private set;
        }

        /// <summary>
        /// Client
        /// </summary>
        public Int32 ClientId
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String AlarmType
        {
            get
            {
                if( String.IsNullOrWhiteSpace( this._AlarmType ) )
                {
                    this._AlarmType = this.ItemName.GetAlarmType();
                }

                return _AlarmType;
            }
        }

        /// <summary>
        /// Large message
        /// </summary>
        public String LargeMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Short message
        /// </summary>
        public String ShortMessage
        {
            get;
            private set;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configurationId"></param>
        /// <param name="idTag"></param>
        /// <param name="itemName"></param>
        /// <param name="idServer"></param>
        /// <param name="samplingInterval"></param>
        /// <param name="queueSize"></param>
        private OpcUASubscribeItemLDL( Int32 configurationId, Int32 idTag, String itemName, Int32 idServer,
                           Int32 samplingInterval = 30, Int32 queueSize = 10 ):
                base( String.Format( "config:{0}-item:{1}-LDL", configurationId, idTag ), itemName, idServer,
                           samplingInterval, queueSize )
        {
            this.ConfigurationId = configurationId;
        }


        /// <summary>
        /// Create from BBDD
        /// </summary>
        /// <param name="bbddTag"></param>
        /// <returns></returns>
        public static OpcUASubscribeBase CreateFromBBDD( Tag bbddTag )
        {

            OpcUASubscribeItemLDL result = new OpcUASubscribeItemLDL( bbddTag.ConfigurationId, bbddTag.Id, bbddTag.TagName,
                                                                      bbddTag.UAServerId, 30, 10 );

            result.ConfigurationId = bbddTag.ConfigurationId;
            result.LargeMessage = bbddTag.LargeMessage;
            result.ShortMessage = bbddTag.ShortMessage;

            result.ClientId = bbddTag.TagClientId;

            return result;

        }

        /// <summary>
        /// Child items
        /// </summary>
        public IReadOnlyCollection<OpcUASubscribeBase> ChildItems()
        {
            return this._ChildItems;
        }

        /// <summary>
        /// Add child item
        /// </summary>
        /// <param name="newChild"></param>
        public void AddChildItem( OpcUASubscribeBase newChild )
        {
            if( this._ChildItems.Contains( newChild.Key.Trim() ) == false )
            {
                this._ChildItems.Add( newChild );
            }            
        }

        /// <summary>
        /// Delete child
        /// </summary>
        /// <param name="keyChildItem"></param>
        public void DeleteChild( String keyChildItem)
        {
            keyChildItem = keyChildItem.Trim();

            if( this._ChildItems.Contains( keyChildItem ) )
            {
                this._ChildItems.Remove( keyChildItem );
            }
        }

        /// <summary>
        /// Accept
        /// </summary>
        /// <param name="iVisitor"></param>
        /// <returns></returns>
        public override Boolean Accept( IOPCUASubscribeItemVisitor iVisitor )
        {
            return iVisitor.Visit( this );
        }
    }
}
