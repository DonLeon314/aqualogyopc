﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.OPCUASubscribeItems
{
    class SubscribeItemCreator
    {
        /// <summary>
        /// Create Subscribe item AB AC
        /// </summary>
        /// <param name="bbddTag"></param>
        /// <returns></returns>
        public static OpcUASubscribeBase Create( OPCConfigModel2.ModelBBDD.Tag bbddTag )
        {
            OpcUASubscribeBase result = null;
            
            result = OpcUASubscribeItemLDL.CreateFromBBDD( bbddTag );            

            return result;
        }
    }
}
