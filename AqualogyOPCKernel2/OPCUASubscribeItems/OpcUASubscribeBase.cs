﻿using AqualogyOPCKernel2.Interfaces;
using OPCUAClient.Interfaces;
using System;


namespace AqualogyOPCKernel2.OPCUASubscribeItems
{
    public abstract class OpcUASubscribeBase:
                    IOPCUASubscribeItem
                    
    {
        #region interface IOPCUASubscribeItem
        
        /// <summary>
        /// OPC Item unique key
        /// </summary>
        public String Key
        {
            get;
            private set;
        }

        /// <summary>
        /// OPC Item name
        /// </summary>
        public String ItemName
        {
            get;
            private set;
        }

        public Int32 SamplingInterval
        {
            get;
            private set;
        }

        public Int32 QueueSize
        {
            get;
            private set;
        }

        public Int32 IdServer
        {
            get;
            private set;
        }
        #endregion

        public OpcUASubscribeBase( String key, String itemName, Int32 idServer, 
                           Int32 samplingInterval = 30, Int32 queueSize = 10 )
        {
            this.Key = key;            

            this.IdServer = idServer;
            this.ItemName = itemName;
            this.SamplingInterval = samplingInterval;
            this.QueueSize = queueSize;
        }

        public abstract Boolean Accept( IOPCUASubscribeItemVisitor iVisitor );
    }
}
