﻿using AqualogyOPCKernel2.Hot;
using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model;
using OPCConfigModel2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPCConfigModel2.ModelBBDD;
using System.Diagnostics;

namespace AqualogyOPCKernel2.Savers
{
    class MSSQLSaver:
          IAlarmasSaver
    {

        private List<StorageBase> _Storages = new List<StorageBase>();
        private ITagClientSource _ITagClientSource = null;

        private DateTime? _LastSalto = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public MSSQLSaver( ITagClientSource iTagClientSource)
        {
            this._ITagClientSource = iTagClientSource;
        }

        #region interface IAlarmasSaver
        public Boolean Save( IReadOnlyCollection<Alarm> alarmsForSave )
        {
            Boolean result = false;

            MSSQLSaverCreatorItems rows_creator = new MSSQLSaverCreatorItems( alarmsForSave, this._ITagClientSource );


            ReadOnlyCollection<GreenBoxRow> rowsForSave = rows_creator.GetGreenBoxItems();

            // Save new rows
            Boolean result_save_alarms = SaveNewRows( rowsForSave );
            
            foreach( Alarm alarm in alarmsForSave )
            {
                alarm.NeedSend = ( result_save_alarms == false );
            }

            Boolean salto_need = true;
            if( this._LastSalto.HasValue )
            {
                Int32 delay_salto = HotParameter.HotValue( "SALTO", "SaltoPeriod ", 5 );
                var delta_salto = DateTime.Now - this._LastSalto.Value;

                salto_need = ( delta_salto.Minutes > delay_salto );
            }

            if( salto_need )
            {
                Salto();
            }

            return result;
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        private void Salto()
        {
            ReloadBBDDEventSavers();

            StorageBase prev_storage = null;

            Int32 history_time = HotParameter.HotValue( "SALTO", "HistoryTime", 30 );

            foreach( StorageBase storage in this._Storages )
            {
                storage.ProcessEvents( prev_storage, history_time );
                prev_storage = storage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rowsForSave"></param>
        /// <returns></returns>
        private Boolean SaveNewRows( ReadOnlyCollection<GreenBoxRow> rowsForSave )
        {
            if( rowsForSave.Count == 0 )
            {
                return true;
            }

            if( _Storages.Count == 0 )
            { 
                // Storages empty
                return false;
            }

            return this._Storages[0].SaveRows( rowsForSave );
        }

        private HotIndicator _NeedReloadStorages = new HotIndicator( "SALTOS", "LastModified" );

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Boolean ReloadBBDDEventSavers()
        {
            try
            {
                using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
                {
                    if( false == this._NeedReloadStorages.IsActual( dbContext ) )
                    {
                        return true;
                    }


                    List<MSSQLStorage> savers = new List<MSSQLStorage>();
                    foreach( var cfg in dbContext.EventSaverConfigs )
                    {
                        BBDDEventSaverFinder finder = new BBDDEventSaverFinder( cfg.ConnectionString, cfg.TableName );
                        MSSQLStorage event_saver = savers.Find( finder.ConnectionStringComparer );
                     
                        if( event_saver == null )
                        {
                            savers.Add( MSSQLStorage.BBDDEventSaverCreate( cfg ) );
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                Debug.
            }


        //                {
        //                 this._Storages.Clear();
        //                 using( OPCConfigModel2 dbContext = new OPCConfigModel2() )
        //                 { 
        //                     foreach( var cfg in dbContext.EventSaverConfigs )
        //                     {
        //                         BBDDEventSaverFinder finder = new BBDDEventSaverFinder( cfg.ConnectionString, cfg.TableName );
        //                         BBDDEventSaver event_saver = savers.Find( finder.ConnectionStringComparer );
        // 
        //                         if( event_saver == null )
        //                         {
        //                             savers.Add( BBDDEventSaver.BBDDEventSaverCreate( cfg ) );
        //                         }
        //                     }
        // 
        //                 this._EventSavers.Clear();
        //                 this._EventSavers.AddRange( savers );
        // 
        //                 if( this._EventSavers.Count > 0 )
        //                 {
        //                     this._EventSavers.Add( new BBDDSaverLastDummy() );
        //                 }
        // 
        //                 // Sort storages
        //                 this._EventSavers.Sort( ( saver1, saver2 ) =>
        //                 {
        //                     return saver1.Priority.CompareTo( saver2.Priority );
        //                 } );
        // 
        //                 return true; // OK! :)
        // 
        //             } catch( Exception ex )
        //             {
        //                 //this._IOPCInfo.Info( ex );
        //             }
        // 
        //
                return false; // Error! :(
        }

    }
}
