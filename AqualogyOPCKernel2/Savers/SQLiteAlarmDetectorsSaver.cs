﻿using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model;
using AqualogyOPCKernel2.Model.Utils;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AqualogyOPCKernel2.SQLiteEx;

namespace AqualogyOPCKernel2.Savers
{
    public class SQLiteAlarmDetectorsSaver:
            IAlarmDetectorSaver,
             IDisposable
    {
        private SQLiteConnection _SQLiteConnection = null;

        private String _TableName = "AlarmDetectors";

        /// <summary>
        /// Constructor
        /// </summary>
        public SQLiteAlarmDetectorsSaver()
        {

        }

        #region interface IAlarmDetectorSaver

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public AlarmDetector Load( OpcUASubscribeItemLDL item, TimeSpan alarmDetect )
        {
            AlarmDetector result = null;

            try
            {
                String sql_query = String.Format( " SELECT [ItemKey], [AlarmTime], [State], [LastValue], [NeedSend] " +
                                                        "FROM [{0}] WHERE ItemKey = :pItemKey", this._TableName );

                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
                {
                    cmd.Parameters.Clear();

                    cmd.Parameters.Add( new SQLiteParameter( "pItemKey", item.Key ) );

                    using( SQLiteDataReader reader = cmd.ExecuteReader() )
                    {
                        if( reader.Read() )
                        {
                            String bbdd_key = reader.GetString( 0 );
                            Int64 bbdd_alarm_time_ticks = reader.GetInt64( 1 );
                            Int32 bbdd_state = reader.GetInt32( 2 );
                            Int32 bbdd_last_value = reader.GetInt32( 3 );
                            Int32 bbdd_need_send = reader.GetInt32( 4 );

                            result = new AlarmDetector( bbdd_key, item.AlarmType, item.ConfigurationId, alarmDetect,
                                                        ( bbdd_last_value == 1 ), ( bbdd_state == 1 ),
                                                        new DateTime( bbdd_alarm_time_ticks ), ( bbdd_need_send == 1 ) );
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                Debug.WriteLine( String.Format("Sqlite error:{0}", ex.Message) );
            }

            return result;
        }

        private Int64 GetIdForItemKey( String itemKey )
        {
            Int64 result = 0;
            String sql_query = String.Format( "SELECT [id] FROM [{0}] WHERE ItemKey = :pItemKey", this._TableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add( new SQLiteParameter( "pItemKey", itemKey ) );

                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        result = reader.GetInt64( 0 );
                    }
                }
            }

            return result;            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <param name="alarmTime"></param>
        /// <param name="lastValue"></param>
        /// <param name="state"></param>
        /// <param name="needSend"></param>
        /// <returns></returns>
        public Boolean Save( String itemKey, DateTime alarmTime, Boolean lastValue, Boolean state, Boolean needSend )
        {
            String sql_query;

            try
            {
                Int64 id = GetIdForItemKey( itemKey );

                if( id == 0 )
                {
                    sql_query = String.Format( "INSERT INTO [{0}] ( [ItemKey], [AlarmTime], [State], [LastValue], [NeedSend] )" +
                                                            " VALUES(:pItemKey, :pAlarmTime, :pState, :pLastValue, :pNeedSend )",
                                                            this._TableName );
                }
                else
                {
                    sql_query = String.Format( "UPDATE [{0}] SET [AlarmTime] = :pAlarmTime, " +
                                                                "[State] = :pState, " +
                                                                "[LastValue] = :pLastValue, " +
                                                                "[NeedSend] = :pNeedSend " +
                                                       " WHERE [Id] = :pId ", this._TableName );
                }

                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
                {
                    cmd.Parameters.Clear();

                    cmd.Parameters.Add( new SQLiteParameter( "pId", id ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pItemKey", itemKey ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pAlarmTime", alarmTime.Ticks ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pState", ( state ? 1 : 0 ) ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pLastValue", ( lastValue ? 1 : 0 ) ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pNeedSend", ( needSend ? 1 : 0 ) ) );

                    Object execute_result = cmd.ExecuteScalar();

                    return true;
                }
            }
            catch( Exception ex )
            {
                Debug.WriteLine( String.Format("SQLite error:{0}", ex.Message ) );
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public Boolean Delete( String itemKey )
        {
            try
            {

                String sql_query = String.Format( "DELETE FROM [{0}] WHERE [ItemKey] = :pItemKey", this._TableName );

                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, GetConnection() ) )
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "pItemKey", itemKey ) );

                    Object execute_result = cmd.ExecuteScalar();
                }

                return true;

            }
            catch( Exception ex )
            {
                Debug.WriteLine( String.Format( "SQLite error:{0}", ex.Message ) );
            }

            return false;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private SQLiteConnection GetConnection()
        {
            if( this._SQLiteConnection == null )
            {

                String fname = "AlarmDetectors.db3".ToLocalFileName();

                if( false == File.Exists( fname ) )
                {
                    CreateDB( fname );
                }

                OpenDB( fname );

                CorrectDB();
            }

            return this._SQLiteConnection;
        }

        /// <summary>
        /// 
        /// </summary>
        private void CorrectDB()
        {            

            if( SQLiteEx.SQLiteEx.TableExist( this._SQLiteConnection, this._TableName ) == false )
            {
                using( SQLiteCommand command = new SQLiteCommand( this._SQLiteConnection ) )
                {
                    command.CommandText = String.Format( @"CREATE TABLE [{0}] (" +
                                                                "[id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                                                "[ItemKey] VARCHAR(128) NOT NULL, " +
                                                                "[AlarmTime] NUMERIC NOT NULL, " +
                                                                "[State] INT NOT NULL, " +
                                                                "[LastValue] INT NOT NULL, " +
                                                                "[NeedSend] INT NOT NULL " +
                                                                ")", this._TableName );

                    command.CommandType = CommandType.Text;
                    Int32 result = command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFileName"></param>
        private void CreateDB( String dbFileName )
        {
            SQLiteConnection.CreateFile( dbFileName );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFileName"></param>
        private void OpenDB( String dbFileName )
        {
            this._SQLiteConnection = new SQLiteConnection( String.Format( "Data Source={0}; Version=3;", dbFileName ) );
            this._SQLiteConnection.Open();
        }

        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                try
                {
                    // Free any other managed objects here. 
                    if( this._SQLiteConnection != null )
                    {
                        this._SQLiteConnection.Close();
                    }
                }catch( Exception ex )
                {
                    Debug.WriteLine( String.Format( "SQLLite error: {0}", ex.Message ) );
                }
                finally
                {
                    this._SQLiteConnection = null;
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion
    }
}
