﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Savers
{
    public abstract class StorageBase
    {
        
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Priority
        {
            get;
            set;
        }

        public Boolean LastStorage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iBBDDEventSource"></param>
        /// <param name="iOPCInfo"></param>
        /// <returns></returns>
        public abstract Boolean ProcessEvents( StorageBase storageBase, Int32 historyTime );

        /// <summary>
        /// 
        /// </summary>
        public abstract ReadOnlyCollection<GreenBoxRow> GetRowsForSave( Int32 historyTime );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void BeginTransaction();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void RollbackTransaction();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void CommitTransaction();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleteIds"></param>
        /// <param name="iOPCInfo"></param>
        public abstract void DeleteEvents( IReadOnlyCollection<GreenBoxRow> deleteMessages, Boolean delete, String storageDestination );


        public abstract Boolean SaveRows( ReadOnlyCollection<GreenBoxRow> rowsForSave );
    }
}
