﻿using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Savers
{
    public class MSSQLSaverCreatorItems
    {
        private IReadOnlyCollection<Alarm> _AlarmsForSave = null;
        private ITagClientSource _ITagClientSource = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="alarmsForSave"></param>
        public MSSQLSaverCreatorItems( IReadOnlyCollection<Alarm> alarmsForSave, ITagClientSource iTagClientSource )
        {
            this._AlarmsForSave = alarmsForSave;
            this._ITagClientSource = iTagClientSource;
        }

        /// <summary>
        /// GreenBox items
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<GreenBoxRow> GetGreenBoxItems()
        {
            List<GreenBoxRow> result = new List<GreenBoxRow>();

            DateTime current_date = DateTime.Now;

            foreach( Alarm alarm in this._AlarmsForSave )
            {
                String phone_channel = alarm.PhonesChannel();
                String email_channel = alarm.EMailsChannel();

                if( String.IsNullOrWhiteSpace( phone_channel ) && String.IsNullOrWhiteSpace( email_channel ) )
                {
                    alarm.NeedSend = true;
                    continue;
                }

                if( String.IsNullOrWhiteSpace( email_channel ) == false )
                {
                    result.Add( new GreenBoxRow()
                    {
                        nId = 0,
                        dFechaHora = alarm.AlarmTime,
                        nIdNotificacion = 0,
                        sCliente = ( this._ITagClientSource != null ? this._ITagClientSource.GetClientDescription( alarm.ClientId ) : "" ),
                        sCanal = email_channel,
                        sAsunto = alarm.ShortMessage,
                        sTextoEmail = alarm.LargeMessage,
                        sTextoSms = "",
                        sEstado = "PENDIENTE",
                        dFechaEstado = current_date,
                        bEmail = true,
                        bSms = false,
                        nReintentos = 0                        
                    });
                }

                if( String.IsNullOrWhiteSpace( phone_channel ) == false )
                {
                    result.Add( new GreenBoxRow()
                    {
                        nId = 0,
                        dFechaHora = alarm.AlarmTime,
                        nIdNotificacion = 0,
                        sCliente = ( this._ITagClientSource != null ? this._ITagClientSource.GetClientDescription( alarm.ClientId ) : "" ),
                        sCanal = phone_channel,
                        sAsunto = "",
                        sTextoEmail = "",
                        sTextoSms = alarm.LargeMessage,
                        sEstado = "PENDIENTE",
                        dFechaEstado = current_date,
                        bEmail = false,
                        bSms = true,
                        nReintentos = 0
                    } );
                }
            }

            return result.AsReadOnly();
        }
    }
}
