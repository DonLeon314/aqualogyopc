﻿using AqualogyOPCKernel2.Collections;
using AqualogyOPCKernel2.Interfaces;
using AqualogyOPCKernel2.Model;
using AqualogyOPCKernel2.Model.Utils;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using AqualogyOPCKernel2.OPCUASubscribeItems.Visitors;
using AqualogyOPCKernel2.Savers;
using OPCConfigModel2;
using OPCConfigModel2.ModelBBDD;
using OPCUAClient;
using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using AqualogyOPCKernel2.Model.Utils.SafeCounters;
using AqualogyOPCKernel2.Hot;

namespace AqualogyOPCKernel2
{
    public class OPCKernel2:
                IValuesConsumer
    {
        private OPCUAClient.OPCUAClient _OPCClient = null;        

        private HotOpcWorkTimes _WorkTImes = new HotOpcWorkTimes();

        private Thread _Thread = null;
        private ManualResetEvent _StopEvent = new ManualResetEvent( false );
        private ManualResetEvent _ResumeEvent = new ManualResetEvent( false );
        private OpcConfigurations _Configurations = null;
        private ITagClientSource _ITagClientSource = null;

        private List<OPCItemClientArgs> _Values = null;
        private Object _SyncValues = new Object();

        private Object _SyncProcess = new Object();

        private RawValuesProcessor _RawValuesProcessor = null;

        private WorkersStorage _Workers = new WorkersStorage();

        private ClusterStorage _Clusters = null;

        private IAlarmasSaver _IAlarmasSaver = null;

        private DailyCounter _SmsCounter = null; 

        public OPCKernel2()
        {
            
        }

        #region interface IValuesConsumer
        /// <summary>
        /// 
        /// </summary>
        public void AddValues()
        {

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {

            StartThread();           
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            // Stop subscribe

            // Stop thread
            StopThread();
        }

        Int32 _ProcessDetect = 10;
        private Int32 GetProcessDetect()
        {
            return _ProcessDetect;
        }


        /// <summary>
        /// 
        /// </summary>
        private void StartThread()
        {
            _Thread = new Thread( () =>
            {
                this.ProcessRead();
            } );

            this._Thread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        private void StopThread()
        {
            this._StopEvent.Set();
            this._Thread.Join();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private void LoadConfigurations( OPCConfigContext2 dbContext )
        {

            this._OPCClient = new OPCUAClient.OPCUAClient( ConnectionsStorage.CreateFromBBDD( dbContext ) );

            this._OPCClient.OnChangeItem += OnItemChange;

            this._OPCClient.OnAutoResubscribe += OnAutoResubscribe;

            this._Configurations = new OpcConfigurations( this._OPCClient );

            this._Configurations.LoadFromBBDD( dbContext );
        }

        /// <summary>
        /// On auto reconnect - Resubscribe
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        private void OnAutoResubscribe( Object o, OPCAutoresubscribeArgs e )
        {
            lock( this._SyncProcess )
            {                          
                Debug.WriteLine( String.Format( "OnAutoResubscribe:{0}", e.ConnectionInfo.ServerUrl ) );

                ResubscribeNormalizator visitor = new ResubscribeNormalizator( this._RawValuesProcessor.AlarmDetectors );
                List<IOPCUASubscribeItem> result = new List<IOPCUASubscribeItem>();

                foreach( var item in e.OldItems )
                {
                    OpcUASubscribeBase sib = item as OpcUASubscribeBase;
                    if( sib != null )
                    {
                        if( sib.Accept( visitor ) )
                        {
                            result.Add( sib );
                        }
                    }
                }

                e.NewItems = result;
                Debug.WriteLine( String.Format( "OnAutoResubscribe:{0} newItems:{1}", e.ConnectionInfo.ServerUrl, result.Count ) );
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnItemChange( Object sender, OPCItemClientArgs e )
        {
#if DEBUG
            String msg = String.Format( "Change: {0}\tTime: {1}\tValue: {2}",
                                            e.ConnectionArgs.IPCUASubscribeItem.ItemName,
                                            e.ConnectionArgs.Values.First().ServerTimestamp,
                                            e.ConnectionArgs.Values.First().Value );
            Debug.WriteLine( msg );
#endif
            lock( this._SyncValues )
            {
                if( this._Values == null )
                {
                    this._Values = new List<OPCItemClientArgs>();
                }

                this._Values.Add( e );

                this._ResumeEvent.Set();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void ProcessValues()
        {
            List<OPCItemClientArgs> new_values = null;
            lock( this._SyncValues )
            {
                new_values = this._Values;
                this._Values = null;
            }            

            lock( this._SyncProcess )
            {
                if( this._RawValuesProcessor == null )
                {
                    this._RawValuesProcessor = new RawValuesProcessor( this._OPCClient );
                }

//                 if( this._SSS )
//                 {
//                     new_values.Clear();
//                 }

                IReadOnlyCollection<Alarm> alarms = this._RawValuesProcessor.Process( new_values );


                // Fill workers
                DateTime current_dt = DateTime.Now;
                foreach( var alarm in alarms )
                {
                    var alarm_workers = this._Clusters.FindWorkers( alarm.AlarmType, alarm.AlarmGroup, alarm.ConfigurationId );

                    // Delete workers offline
                    // Delete workers without channels                    
                    alarm_workers = alarm_workers.Where( aw => aw.ChannelPresent() ).ToList();

                    // Get phones and mails

                    alarm.InitializeEMails();
                    alarm.InitializePhones();

                    Int32 sms_per_day = HotParameter.HotValue( "ServiceParams", "SmsLimit", 0 );

                    if ( this._SmsCounter == null || this._SmsCounter.MaxCounter != sms_per_day )
                    {
                        this._SmsCounter = new DailyCounter("SMS_COUNTER", sms_per_day);
                    }

                    foreach( OpcWorker worker in alarm_workers )
                    {                        
                        alarm.AddEMails( worker.Emails);

                        foreach ( String phone in worker.Phones)
                        {
                            if( this._SmsCounter.Enable() )
                            {
                                alarm.AddPhone( phone );
                            }
                        }
                    }
                }

#if DEBUG
                // DEBUG
                Console.WriteLine( String.Format( "--{0}------", DateTime.Now ) );
                foreach( Alarm alarm in alarms )
                {
                    Console.WriteLine( String.Format( "{0}\t-\t{1}", alarm.LargeMessage, alarm.ShortMessage ) );
                    Console.WriteLine( String.Format( "\t\tEMails:{0}", alarm.EMailsChannel() ) );
                    Console.WriteLine( String.Format( "\t\tPhones:{0}\n", alarm.PhonesChannel() ) );                    
                }
#endif

                // Save to BBDD
                SaveToBBDD( alarms );

            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarms"></param>
        private void SaveToBBDD( IReadOnlyCollection<Alarm> alarmsForSave )
        {
            if( alarmsForSave.Count == 0 )
            {
                return;
            }

            if( this._ITagClientSource == null )
            {
                var tag_client_storage = new OpcTagClientStorage();
                tag_client_storage.ReloadFromBBDD();
                this._ITagClientSource = tag_client_storage;
            }

            if( this._IAlarmasSaver == null )
            {
                this._IAlarmasSaver = new MSSQLSaver( this._ITagClientSource );
            }
            Boolean result_save = this._IAlarmasSaver.Save( alarmsForSave );
        }


        /// <summary>
        /// 
        /// </summary>
        public void ProcessRead()
        {
            try
            {
                ManualResetEvent[] events = new ManualResetEvent[] { this._StopEvent, this._ResumeEvent };
                using( OPCConfigContext2 db_context = new OPCConfigContext2() )
                {
                    // Load Configs
                    // Subscribe configurations
                    LoadConfigurations( db_context );

                    /// Laod workers
                    this._Workers.CreateFromBBDD( db_context );

                    /// Load clusters
                    this._Clusters = new ClusterStorage( this._Workers );
                    this._Clusters.LoadFromBBDD( db_context );

                    while( true )
                    {
                       

                        Int32 wait_result = WaitHandle.WaitAny( events, GetProcessDetect() * 1000 );

                        if( wait_result == 0 )
                        {
                            break;
                        }
                        else
                        {
                            this._ResumeEvent.Reset();
                        }
                         ProcessValues();
                        //Debug.WriteLine( "Iteration:" + DateTime.Now.ToString() );
                    }
                }                
            } catch( Exception ex )
            {
            }
            finally
            {
                if( this._Configurations != null )
                {
                    try
                    {
                        this._Configurations.UnsubscribeAll();

                    } catch( Exception ex )
                    {
                       
                    }
                }
            }
        }
    }
}
