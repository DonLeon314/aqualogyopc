﻿using OPCConfigModel2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Hot
{
    public class HotIndicator
    {
        private String _Section;
        private String _Parameter;

        private DateTime? _ActualTime = null;

        public HotIndicator( String section, String parameter )
        {
            this._Section = section;
            this._Parameter = parameter;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetActual()
        {
            this._ActualTime = DateTime.Now;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public Boolean IsActual( OPCConfigContext2 dbContext )
        {
            Boolean result = true;

            if (this._ActualTime.HasValue == false)
            {
                result = false;
            }
            else
            {
                Int64 bbdd_actual = dbContext.GetParameter(this._Section, this._Parameter, 0);
                if (bbdd_actual != 0)
                {
                   DateTime dt = new DateTime(bbdd_actual);
                   if (dt < this._ActualTime.Value)
                   {
                        result = false;
                   }
                }
                else
                {   
                    // TRUE                    
                }
            }

            return result;
        }
    }
}
