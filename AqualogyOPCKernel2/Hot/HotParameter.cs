﻿using OPCConfigModel2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Hot
{
    /// <summary>
    /// 
    /// </summary>
    public class HotParameter
    {
        public static Int32 HotValue(String section, String parameter, Int32 defaultValue)
        {
            try
            {
                using (OPCConfigContext2 dbContext = new OPCConfigContext2())
                {
                    return dbContext.GetParameter(section, parameter, defaultValue);
                }
            }catch( Exception ex )
            {
                Debug.WriteLine(String.Format("HotParameter.HotValue error:{0}", ex.Message));
            }

            return defaultValue;
        }
    }
}
