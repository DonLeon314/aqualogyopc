﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AqualogyOPCKernel2
{
    public class OpcItemPartes
    {
        private static Char[] s_OPCSeparator = new Char[] { ';' };
        private static Char[] s_ParteSeparator = new Char[] { '=' };

        /// <summary>
        /// NS parte OPC UA item
        /// </summary>
        public String NSValue
        {
            get;
            set;
        }

        /// <summary>
        /// S parte OPC UA item
        /// </summary>
        public String SValue
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        private OpcItemPartes()
        {

        }

        /// <summary>
        /// Create OpcItemPartes from string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static OpcItemPartes CreateFromString( String str )
        {
            String[] tokens = str.Split( s_OPCSeparator ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToArray();
            if( tokens.Length == 2 )
            {
                String ns_value;                

                String[] value = tokens[0].Split( s_ParteSeparator ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToArray();
                if( value.Length == 2 )
                {
                    ns_value = value[1];
                    value = tokens[1].Split( s_ParteSeparator ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToArray();
                    if( value.Length == 2 )
                    {
                        return new OpcItemPartes()
                        { 
                            SValue = value[1],
                            NSValue = ns_value 
                        };
                    }
                }
            }    

            return null;
        }

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format( "ns={0};s={1}", this.NSValue, this.SValue );
        }

    }


    /// <summary>
    /// 
    /// </summary>
    public static class OPCItemUtils
    {
        private static Char[] s_TokenSeparators = new Char[] { '.' };
        private static Char[] s_WorrdSeparators = new Char[] { '_' };        


        /// <summary>
        /// Get alarm type
        /// </summary>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static String GetAlarmType( this String _this )
        {
            if( String.IsNullOrWhiteSpace( _this ) == false )
            {
                String[] tokens = _this.Split( s_TokenSeparators ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToArray();

                if( tokens.Length > 3 )
                {
                    String[] words = tokens[2].Split( s_WorrdSeparators ).Where( w => String.IsNullOrWhiteSpace( w ) == false ).ToArray();

                    if( words.Length > 1 && words[0].Length > 1 )
                    {
                        return words[0].Substring( words[0].Length - 2, 2 );                                    
                    }

                }
            }

           return String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static String CreatePriority( this String _this )
        {
            String result = String.Empty;

            OpcItemPartes partes = OpcItemPartes.CreateFromString( _this );
            if( partes != null )
            {
                String[] tokens = partes.SValue.Split( s_TokenSeparators ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToArray();
                if( tokens != null && tokens.Length > 1 )
                {
                    tokens[tokens.Length - 1] = "Priority";
                    partes.SValue = String.Join( ".", tokens );

                    return partes.ToString();
                }

            }

            return result;
        }
    }
}
