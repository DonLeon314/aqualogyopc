﻿using AqualogyOPCKernel.MailSender;
using OPCConfigModel2.ExportClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;


namespace AqualogyOPCKernel2.MailSender
{
    /// <summary>
    /// 
    /// </summary>
    public class SMTPSender
    {

        private SMTPConfig _SmtpConfig = null;

        public SMTPSender( SMTPConfig smtpConfig )
        {
            this._SmtpConfig = smtpConfig;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emails">emails string (separator ';')</param>
        /// <param name="subjText">Subj text</param>
        /// <param name="bobyText">Body text</param>
        public void SendMessage( String emails, String subjText, String bodyText, IReadOnlyCollection<SMTPSenderAttachmentsInfo> attachments = null  )
        {
            if( this._SmtpConfig == null )
            {
                throw new Exception( "SMTPSender: without config" );
            }

            List<String> addresses = emails.Split( new Char[] { ';' } ).
                                            Where( a => String.IsNullOrWhiteSpace( a ) == false ).
                                            ToList();

            if( addresses.Count == 0 )
            {
                return;
            }

            var client = new SmtpClient( this._SmtpConfig.Host, this._SmtpConfig.Port );

            client.EnableSsl = this._SmtpConfig.EnableSSL;
            client.UseDefaultCredentials = false; 
            client.Credentials = new NetworkCredential( this._SmtpConfig.User, this._SmtpConfig.Password );

            MailMessage message = new MailMessage()
            {
                From = new System.Net.Mail.MailAddress( this._SmtpConfig.From ),
                Subject = subjText,
                Body = bodyText
            };

            message.IsBodyHtml = false;

            foreach( String address in addresses )
            {
                message.To.Add( new System.Net.Mail.MailAddress( address ) );
            }

            if( attachments != null && attachments.Count > 0 )
            {
                foreach( var attachment in attachments )
                {
                    message.Attachments.Add( new Attachment( attachment.FileData, attachment.FileName, attachment.Content ) );
                }
            }

            client.Send( message );
        }
    }
}
