﻿using System;
using System.IO;


namespace AqualogyOPCKernel.MailSender
{
    public class SMTPSenderAttachmentsInfo
    {
        public Stream FileData
        {
            get;
            private set;
        }

        public String FileName
        {
            get;
            private set;
        }

        public String Content
        {
            get;
            private set;
        }


        public SMTPSenderAttachmentsInfo( Stream stream, String fileName, String content )
        {
            this.FileData = stream;
            this.FileName = fileName;
            this.Content = content;
        }
    }
}
