﻿using AqualogyOPCKernel2.Model;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using System;
using System.Collections.Generic;

namespace AqualogyOPCKernel2.Interfaces
{
    public interface IAlarmDetectors
    {
        IReadOnlyCollection<Alarm> DetectAlarms();
        void AddValue( OpcUASubscribeItemLDL item, Boolean state, DateTime timeState, Int32? priority );

        void SetAlarmGroupType( String key, Int32 value );
        Boolean DetectorExist( String key );
        void DetectorDelete( String key );
        Boolean DetectorTypeGroup( String key );

        Boolean MarkNeedSend( IEnumerable<String> detectorKeys, Boolean needSend );
    }
}
