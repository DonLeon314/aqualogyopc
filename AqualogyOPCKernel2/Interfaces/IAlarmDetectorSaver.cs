﻿using AqualogyOPCKernel2.Model;
using AqualogyOPCKernel2.OPCUASubscribeItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Interfaces
{
    public interface IAlarmDetectorSaver
    {
        AlarmDetector Load( OpcUASubscribeItemLDL item, TimeSpan detectAlarm );
        Boolean Save( String key, DateTime alarmTime, Boolean lastValue, Boolean state, Boolean needSend );
        Boolean Delete( String itemKey );
    }
}
