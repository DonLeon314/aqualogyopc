﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Interfaces
{
    /// <summary>
    /// Tag client description storage
    /// </summary>
    public interface ITagClientSource
    {
        String GetClientDescription( Int32 idTagClient );
    }
}
