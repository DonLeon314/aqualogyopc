﻿using AqualogyOPCKernel2.OPCUASubscribeItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Interfaces
{
    public interface IOPCUASubscribeItemVisitor
    {
        Boolean Visit( OpcUASubscribeItemLDL item );        
        Boolean Visit( OpcUASubscribeItemPriority item );
    }
}
