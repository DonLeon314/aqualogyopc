﻿using AqualogyOPCKernel2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Interfaces
{
    public interface IAlarmasSaver
    {
        Boolean Save( IReadOnlyCollection<Alarm> alarmsForSave );
    }
}
