﻿using AqualogyOPCKernel2.Model;
using System;

namespace AqualogyOPCKernel2.Interfaces
{
    public interface IWorkerStorage
    {
        OpcWorker GetWorkerById( Int32 workerId );
    }
}
