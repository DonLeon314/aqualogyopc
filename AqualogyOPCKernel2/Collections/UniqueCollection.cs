﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Collections
{
    public class UniqueCollection:
            KeyedCollection<String,String>
    {
        public UniqueCollection() :
                base( StringComparer.CurrentCultureIgnoreCase )
        {

        }

        protected override String GetKeyForItem( String item )
        {
            return item;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValues"></param>
        public void AddIfNotExist( IEnumerable<String> newValues )
        {
            foreach( String new_value in newValues )
            {
                if( this.Contains(new_value) == false )
                {
                    this.Add( new_value );
                }
            }
        }
    }
}
