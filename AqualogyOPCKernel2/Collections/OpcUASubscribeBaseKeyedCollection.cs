﻿using AqualogyOPCKernel2.OPCUASubscribeItems;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Collections
{
    /// <summary>
    /// 
    /// </summary>
    class OpcUASubscribeBaseKeyedCollection:
            KeyedCollection<String, OpcUASubscribeBase>
    {

        /// <summary>
        /// 
        /// </summary>
        public OpcUASubscribeBaseKeyedCollection():
                base(StringComparer.CurrentCultureIgnoreCase)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override String GetKeyForItem( OpcUASubscribeBase item )
        {
            return item.Key;
        }
    }
}
