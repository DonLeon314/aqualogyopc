﻿using AqualogyOPCKernel2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel2.Collections
{
    public class WorkersCollection:
            KeyedCollection<Int32,OpcWorker>
    {
        public WorkersCollection()
        {

        }

        protected override Int32 GetKeyForItem( OpcWorker item )
        {
            return item.Id;
        }
    }
}
