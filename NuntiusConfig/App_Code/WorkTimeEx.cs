﻿using OPCConfigModel.Model;
using System;
using System.Web.Mvc;

namespace NuntiusConfig.App_Code
{
    /// <summary>
    /// BBDD model WorkTime ASP MVC extension
    /// </summary>
    public static class WorkTimeEx
    {
        /// <summary>
        /// To ASP MVC  SelectListItem
        /// </summary>
        /// <param name="_this">WorkTime BBDD entity</param>
        /// <param name="selected"> select (default false )</param>
        /// <returns></returns>
        public static SelectListItem ToSelectListItem( this WorkTime _this, Boolean selected = false)
        {
            return new SelectListItem()
            {
                Value = _this.Id.ToString(),
                Text = _this.Name,
                Selected = selected
            };
        }
    }
}