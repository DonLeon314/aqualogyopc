﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.App_Code
{
    public static class ExceptionEx
    {


        public static String ToWebMessage( this Exception _this )
        {

            if( _this.InnerException != null )
            {
                return String.Format( "Error:{0} InnerException:{1} Stack:{2} Source:{3}",
                                     _this.Message, _this.InnerException.Message, _this.StackTrace, _this.Source );
            }
            else
            {
                return String.Format( "Error:{0} Stack:{1} Source:{2}",
                            _this.Message, _this.StackTrace, _this.Source );
            }
        }
    }
}