﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace NuntiusConfig.App_Code.Helpers
{
    public class ButtonHelper
    {
        private class AqualogyOPCWCFServiceCtrlInfo
        {
            public String Title
            {
                get;
                private set;
            }

            public String UrlImage
            {
                get;
                private set;
            }

            public String StyleImage
            {
                get;
                private set;
            }

            public String Id
            {
                get;
                private set;
            }

            public String StyleCtrl
            {
                get;
                private set;
            }

            public String OnClick
            {
                get;
                private set;
            }

            private AqualogyOPCWCFServiceCtrlInfo()
            {

            }

            public static AqualogyOPCWCFServiceCtrlInfo CreateFromCSHTML( Object parameters )
            {
                AqualogyOPCWCFServiceCtrlInfo result = new AqualogyOPCWCFServiceCtrlInfo();

                Type type_params = parameters.GetType();
                PropertyInfo[] properties_params = type_params.GetProperties();

                Type type_result = result.GetType();
                PropertyInfo[] properties_result = type_result.GetProperties();

                foreach( var prop_result in properties_result )
                {
                    var prop_paarams = properties_params.Where( pp => String.Compare( pp.Name, prop_result.Name, false ) == 0 ).FirstOrDefault();
                    if( prop_paarams == null )
                    {
                        continue;
                    }

                    prop_result.SetValue( result, prop_paarams.GetValue( parameters ) );
                }

                return result;
            }

        }
        public static string AqualogyOPCWCFServiceCtrl( String text, Object parameters )
        {
            AqualogyOPCWCFServiceCtrlInfo info = null;

            try
            {
                info = AqualogyOPCWCFServiceCtrlInfo.CreateFromCSHTML( parameters );

            }catch( Exception ex )
            {
                
            }
            if( info == null )
            {
                return "";
            }

            StringBuilder result = new StringBuilder();

            result.Append( "<button" );

            if( String.IsNullOrWhiteSpace( info.Title ) == false )
            {
                result.AppendFormat( " title = \"{0}\" ", info.Title );
            }

            if( String.IsNullOrWhiteSpace( info.OnClick ) == false )
            {
                result.AppendFormat( " onclick=\"{0}\"", info.OnClick );
            }

            if( String.IsNullOrWhiteSpace( info.StyleCtrl ) == false )
            {
                result.AppendFormat( " style=\"{0}\"", info.StyleCtrl );
            }

            if( String.IsNullOrWhiteSpace( info.Id ) == false )
            {
                result.AppendFormat( " id=\"{0}\"", info.Id );
            }

            result.Append( ">" );

            if( String.IsNullOrWhiteSpace( info.UrlImage ) == false )
            {
                result.AppendFormat( " <img src=\"{0}\"", info.UrlImage );
                if( String.IsNullOrWhiteSpace( info.StyleImage ) == false )
                {
                    result.AppendFormat( " style=\"{0}\"", info.StyleImage );
                }

                result.Append( ">" );
            }

            if( String.IsNullOrWhiteSpace( text ) == false )
            {
                result.Append( text );
            }

            result.Append( "</button>" );

            return result.ToString();
        }
    }
}