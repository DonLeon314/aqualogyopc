﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.App_Code
{

    /// <summary>
    /// BBDD Config extension
    /// </summary>
    public static class ConfigEx
    {
        /// <summary>
        /// BBDD Config to SelectListItem
        /// </summary>
        /// <param name="_this"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static SelectListItem ToSelectListItem( this Config _this, Boolean selected )
        {
            SelectListItem result = new SelectListItem()
            {
                Selected = selected,
                Text = _this.Name,
                Value = _this.Id.ToString()
            };

            return result;
        }
    }
}