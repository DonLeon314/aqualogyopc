﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.App_Code
{
    /// <summary>
    /// BBDD Group ASP MVC extension
    /// </summary>
    public static class GroupEx
    {
        /// <summary>
        /// BBDD Group to SelectListItem
        /// </summary>
        /// <param name="_this"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static SelectListItem ToSelectListItem( this Group _this, Boolean selected = false )
        {
            if( _this == null )
            {
                return null;
            }

            return new SelectListItem()
            {
                Text = _this.Name,
                Value = _this.Id.ToString(),
                Selected = selected
            };
        }
    }
}