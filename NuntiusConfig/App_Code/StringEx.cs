﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.App_Code
{
    /// <summary>
    /// 
    /// </summary>
    public class StringEx
    {
        /// <summary>
        /// 
        /// </summary>
        private static char[] s_Separators = new char[] { ';' };
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_this"></param>
        /// <param name="separators"></param>
        /// <returns></returns>
        public static IReadOnlyCollection<String> SplitAndNormalize( String str, char[] separators = null )
        {            
            List<String> tokens = new List<String>();

            if( false == String.IsNullOrWhiteSpace( str ) )
            {
                if( separators == null || separators.Length == 0 )
                {
                    separators = s_Separators;
                }

                List<String> raw_tokens = str.Split( separators ).Where( t => String.IsNullOrWhiteSpace( t ) == false ).ToList();

                foreach( String token in raw_tokens )
                {
                    if( false == tokens.Contains( token, StringComparer.OrdinalIgnoreCase ) )
                    {
                        tokens.Add( token );
                    }
                }
            }

            return tokens;
        }
    }
}