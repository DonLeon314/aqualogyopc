﻿using OPCConfigModel.Model;
using System;
using System.Web.Mvc;

namespace NuntiusConfig.App_Code
{
    /// <summary>
    /// BBDD extensions
    /// </summary>
    public static class UserEx
    {
        /// <summary>
        /// Convert User to SelectListItem
        /// </summary>
        /// <param name="_this"></param>
        /// <param name="selected">SelectListItem.Selected value</param>
        /// <returns></returns>
        public static SelectListItem ToSelectListItem( this User _this, Boolean selected )
        {
            SelectListItem result = new SelectListItem();

            result.Value = _this.Id.ToString();
            result.Selected = selected;
            result.Text = "";

            if( false == String.IsNullOrWhiteSpace( _this.FirstName ) )
            {
                result.Text = _this.FirstName;
            }

            if( false == String.IsNullOrWhiteSpace( _this.SecondName ) )
            {
                if( result.Text.Length > 0 )
                {
                    result.Text += " ";
                }

                result.Text += _this.SecondName;
            }

            if( false == String.IsNullOrWhiteSpace( _this.Alias ) )
            {
                if( result.Text.Length > 0 )
                {
                    result.Text += " ";
                }

                result.Text += "( " + _this.Alias + " )";
            }

            return result;
        }
    }
}