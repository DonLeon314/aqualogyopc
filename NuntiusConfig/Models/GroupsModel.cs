﻿using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{
    /// <summary>
    /// Groups model
    /// </summary>
    public class GroupsModel
    {

        /// <summary>
        /// Constructor
        /// </summary>
        private GroupsModel()
        {            
        }

        public static GroupsModel Create( Int32 id = 0 )
        {
            
            GroupsModel result = new GroupsModel();

            result.SelectedConfigurationId = id;

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                // Load cofigurations
                List<SelectListItem> configurations = new List<SelectListItem>();

                Config current_config = null;

                foreach( var item in db.OPCConfigs.Include( "Groups" ).ToList() )
                {
                    SelectListItem config_item = new SelectListItem()
                    {
                        Text = item.Name,
                        Value = item.Id.ToString(),
                        Selected = ( item.Id == id )
                    };

                    if (configurations.Count == 0 && id == 0)
                    {
                        result.SelectedConfigurationId = item.Id;
                        config_item.Selected = true;
                    }
                    else
                    {
                        if (id != 0 && item.Id == id)
                        {
                            config_item.Selected = true;
                        }
                    }


                    if( config_item.Selected )
                    {
                        current_config = item;
                    }

                    configurations.Add( config_item );
                }

                result.Configurations = configurations;

                // Load groups
                List<GroupMVC> groups = new List<GroupMVC>();

                if( result.SelectedConfigurationId != 0 && configurations.Count > 0 && current_config != null )
                {
                    foreach( Group group in current_config.Groups )
                    {
                        groups.Add( new GroupMVC()
                        {
                            Id = group.Id,
                            IdConfig = group.OPCConfigId,
                            Name = group.Name
                        } );
                    };
                }

                result.SelectedConfigGroups = groups;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 SelectedConfigurationId
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SelectListItem> Configurations
        {
            get;
            private set;
        }

        public IEnumerable<GroupMVC> SelectedConfigGroups
        {
            get;
            private set;
        }
    }
}