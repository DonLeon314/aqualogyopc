﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.Models
{
    public class ErrorModel
    {
        public String Message
        {
            get;
            set;
        }

        public String Stack
        {
            get;
            set;
        }

        public String Source
        {
            get;
            set;
        }

        public String InnerException
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        public ErrorModel( Exception exception )
        {        
            InitDefault();

            this.Message = exception.Message;
            this.InnerException = ( exception.InnerException != null ? exception.InnerException.Message : "" );
            this.Source = exception.Source;
            this.Stack = exception.StackTrace;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ErrorModel( String message )
        {
            InitDefault();

            if( String.IsNullOrWhiteSpace( message ) )
            {
                this.Message = message;
            }
        }

        /// <summary>
        /// Initialize default
        /// </summary>
        private void InitDefault()
        {
            this.Message = "UNKNOWN ERROR!";
            this.InnerException = "";
            this.Source = "";
            this.Stack = "";
        }


}    
}