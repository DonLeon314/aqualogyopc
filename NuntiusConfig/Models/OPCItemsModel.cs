﻿using NuntiusConfig.App_Code;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{
    public class OPCItemsModel
    {
        /// <summary>
        /// Configurations
        /// </summary>
        public SelectList Configurations
        {
            get;
            set;
        }

        public String CurrentConfig
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public OPCItemsModel()
        {
        }

        public HttpPostedFileBase Files
        {
            get; set;
        }

        /// <summary>
        /// Create model
        /// </summary>
        /// <returns></returns>
        public static OPCItemsModel CreateFromBBDD()
        {
            OPCItemsModel result = new OPCItemsModel();

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                result.Configurations = new SelectList( db.OPCConfigs.ToList().Select( c => c.ToSelectListItem( false ) ).ToList(), "Value", "Text" );                
            }

            return result;
        }


        public void FileToBBDD()
        {

            Int32 config_id = Int32.Parse( this.CurrentConfig );
            if( config_id == 0 )
            {
                throw new ArgumentOutOfRangeException();
            }

            Config config = null;

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                using( DbContextTransaction trasaction = db.Database.BeginTransaction() )
                {
                    config = db.OPCConfigs.Include( "Groups" ).Where( c => c.Id == config_id ).FirstOrDefault();
                    if( config == null )
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    DeleteItemsForGroups( db, config.Groups );

                    using( StreamReader reader = new StreamReader( this.Files.InputStream, Encoding.UTF8 ) )
                    {
                        //var r = reader.Read();
                        String str = "";
                        Boolean first = true;

                        try
                        {

                            while( ( str = reader.ReadLine() ) != null )
                            {
                                if( first )
                                {
                                    first = false;
                                    continue;
                                }

                                AddItemToBBDD( ParseRawString( str ), config, db );
                                db.SaveChanges();
                            }

                            trasaction.Commit();

                        }
                        catch( Exception ex )
                        {
                            trasaction.Rollback();

                            throw ex;
                        }


                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="groups"></param>
        private void DeleteItemsForGroups( OPCConfigContext dbContext, IEnumerable<Group> groups )
        {
            Boolean change = false;

            foreach( var group in groups )
            {
                Group bbdd_group = dbContext.Groups.Include( "OPCTags" ).Where( g => g.Id == group.Id ).FirstOrDefault();
                if( bbdd_group != null )
                {
                    if( bbdd_group.OPCTags.Count > 0 )
                    {
                        foreach( var tag in bbdd_group.OPCTags.ToList() )
                        {
                            var bbdd_tag = dbContext.Tags.Include( "Groups" ).Where( t => t.Id == tag.Id ).FirstOrDefault();
                            if( bbdd_tag != null && bbdd_tag.Groups.Count == 1 && bbdd_tag.Groups.First().Id == bbdd_group.Id )
                            {
                                change = true;
                                dbContext.Tags.Remove( bbdd_tag );// .RemoveRange( bbdd_group.OPCTags );
                            }
                        }
                    }
                }
            }

            if( change )
            {
                dbContext.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="config"></param>
        /// <param name="dbContext"></param>
        private void AddItemToBBDD( String[] rawData, Config config, OPCConfigContext dbContext )
        {
            if( rawData.Length < 6 || rawData.Length > 7 )
            {
                throw new ArgumentOutOfRangeException("Columns count < 6 or > 7");
            }

            String server = rawData[0];
            String station = rawData[1];
            String ns = rawData[2];
            String opc_item = rawData[3];
            String raw_groups = rawData[4];
            String long_message = rawData[5];
            String short_message = ( rawData.Length > 6 ? rawData[6] : "" );

            String tag_id = "ns=" + ns.Trim() + ";s=" + opc_item.Trim();

            // Parse groups
            var groups = ParseRawGroups( raw_groups );
            if( groups.Count == 0 )
            {
                throw new ArgumentOutOfRangeException( "Groups empty");
            }

            // Check groups
            foreach( String group_name in groups )
            {
                var group = config.Groups.Where( g => String.Compare( group_name, g.Name.Trim(), true ) == 0 ).FirstOrDefault();
                if( group == null )
                {
                    throw new ArgumentOutOfRangeException( String.Format("Group:{0} not found", group_name ) );
                }

                TagName tag = dbContext.Tags.Include("Groups").Where( t => String.Compare( t.TagId, tag_id, true ) == 0 &&
                                                                           String.Compare( t.Client, station, true ) == 0 &&
                                                                           String.Compare( t.Server, server, true ) == 0 ).FirstOrDefault();
                Boolean add = true;
                if( tag == null )
                {
                    tag = new TagName()
                    {
                        Server = server,
                        Client = station,
                        TagId = tag_id,
                        LargeMessage = long_message,
                        ShortMessage = short_message
                    };
                }
                else
                {
                    if( null == tag.Groups.Where( g => g.Id == group.Id ).FirstOrDefault() )
                    {
                        add = false;
                    }
                }

                if( add )
                {
                    group.OPCTags.Add( tag );
                }
            }
        }

        private IReadOnlyCollection<String> ParseRawGroups( String rawGroups )
        {

            var raw_groups = rawGroups.Split( new char[] { ' ' } );

            List<String> groups = new List<String>();
            foreach( String g in raw_groups )
            {
                if( false == String.IsNullOrWhiteSpace( g ) )
                {
                    groups.Add( g.Trim() );
                }
            }

            return groups;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private String[] ParseRawString( String line )
        {
            String[] tokens = line.Split( new Char[] { ';' } );
            
            List<String> raw_data = new List<String>();

            foreach( String cell in tokens )
            {
                if( false == String.IsNullOrWhiteSpace( cell ) )
                {
                        raw_data.Add( cell.Trim() );
                }
            }

            return raw_data.ToArray();                
        }
    }
}