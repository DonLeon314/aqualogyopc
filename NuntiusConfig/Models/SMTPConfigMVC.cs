﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using OPCConfigModel;
using OPCConfigModel.ExportClasses;

namespace NuntiusConfig.Models
{
    public class SMTPConfigMVC
    {
        public String Host
        {
            get;
            set;
        }

        public String User
        {
            get;
            set;
        }

        public String Password
        {
            get;
            set;
        }
        
        public String From
        {
            get;
            set;
        }

        public Boolean EnableSSL
        {
            get;
            set;
        }

        [Range( 1, 65535, ErrorMessage = "Can only be between 1 .. 65535" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 Port
        {
            get;
            set;
        }

        public SMTPConfigMVC()
        {

        }

        public static SMTPConfigMVC LoadFromBBDD()
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                SMTPConfig cfg = SMTPConfig.CreateFromBBDD( dbContext );
                return new SMTPConfigMVC()
                {
                    Host = cfg.Host,
                    Port = cfg.Port,
                    User = cfg.User,
                    Password = cfg.Password,
                    From = cfg.From,
                    EnableSSL = cfg.EnableSSL
                };
            }
        }

        public void SaveToBBDD()
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                SMTPConfig cfg = new SMTPConfig( this.Host, this.Port, this.User, this.Password, this.From, this.EnableSSL );

                cfg.SaveToBBDD( dbContext );
            }
        }

        /// <summary>
        /// Test
        /// </summary>
        public void TestConfig()
        {
            var client = new SmtpClient( this.Host, this.Port );

            client.EnableSsl = this.EnableSSL;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential( this.User, this.Password );

            //
            // TODO: ? Client
            //
            String subj_text = "AqualogyOPC WEB GUI test SMTP config";
            String body_text = subj_text;

            MailMessage message = new MailMessage()
            {
                From = new System.Net.Mail.MailAddress( this.From ),
                Subject = subj_text,
                Body = body_text
            };

            message.IsBodyHtml = false;
            message.To.Add( new System.Net.Mail.MailAddress( this.From ) );            

            client.Send( message );
        }
    }
}