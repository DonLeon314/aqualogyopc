﻿using OPCConfigModel;
using System.Collections.Generic;
using System.Linq;


namespace NuntiusConfig.Models
{
    public class WorkTimesModel
    {

        public List<WorkTimeMVC> WorkTimes
        {
            get
            {
                List<WorkTimeMVC> result = new List<WorkTimeMVC>();

                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    foreach( var wt in db.WorkTimes.ToList() )
                    {
                        result.Add( new WorkTimeMVC( wt ) );
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkTimesModel()
        {
            
        }
    }
}
