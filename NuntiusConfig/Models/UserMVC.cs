﻿using NuntiusConfig.App_Code;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OPCConfigModel.Model.User;

namespace NuntiusConfig.Models
{
    public class UserMVC
    {       

        public Int32 Id
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 64 )]
        public String Alias
        {
            get;
            set;
        }

        [StringLength( 64 )]
        public String FirstName
        {
            get;
            set;
        }

        [StringLength( 64 )]
        public String SecondName
        {
            get;
            set;
        }

        public eChannel Channel
        {
            get;
            set;
        } 

        public String Phones
        {
            get;
            set;
        }

        public String EMails
        {
            get;
            set;
        }

        public ListBoxCheckedModel WorkTimes
        {
            get;
            set;
        }

        public eTimeEvent TimeOfEvents
        {
            get;
            set;
        }
     

        /// <summary>
        /// Constructor
        /// </summary>
        public UserMVC()
        {
            this.WorkTimes = new ListBoxCheckedModel();
        }

        public void UpdateToBBDD()
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                User row;
                List<SelectListItem> all_work_times = new List<SelectListItem>();

                if( this.Id == 0 )
                {
                    row = new User();
                    row = dbContext.Users.Add( row );
                }
                else
                {
                    row = dbContext.Users.Include( "Phones" ).Include( "EMails" ).Include( "WorkTimes" ).Where( u => u.Id == this.Id ).FirstOrDefault();
                }

                if( row != null )
                {
                    row.Alias = this.Alias;
                    row.FirstName = this.FirstName;
                    row.SecondName = this.SecondName;
                    row.Channel = this.Channel;

                    row.TimeOfEvents = this.TimeOfEvents;

                    // Save Work times

                    row.WorkTimes.Clear();

                    if( this.WorkTimes != null && this.WorkTimes.Posted != null )
                    {
                        foreach( String value in this.WorkTimes.Posted )
                        {
                            Int32 id = 0;
                            if( Int32.TryParse( value, out id ) && id != 0 )
                            {
                                var worktime_bbdd = dbContext.WorkTimes.Where( wt => wt.Id == id ).FirstOrDefault();
                                if( worktime_bbdd != null )
                                {
                                    row.WorkTimes.Add( worktime_bbdd );
                                }
                            }
                        }
                    }

                    dbContext.Phones.RemoveRange( row.Phones );

                    // refresh phones
                    if( row.Phones != null || row.Phones.Count > 0 )
                    {
                        dbContext.Phones.RemoveRange( row.Phones );
                    }
                    foreach( var new_phone in StringEx.SplitAndNormalize( this.Phones ).
                                            Select( item => new Phone() { Id = 0, UserId = row.Id, Value = item } ).ToList() )
                    {
                        row.Phones.Add( new_phone );
                    }

                    // refresh e-mails
                    if( row.EMails != null || row.EMails.Count > 0 )
                    {
                        dbContext.EMails.RemoveRange( row.EMails );
                    }
                    foreach( var new_email in StringEx.SplitAndNormalize( this.EMails ).
                                                   Select( item => new EMail() { Id = 0, UserId = row.Id, Value = item } ).ToList() )
                    {
                        row.EMails.Add( new_email );
                    }

                    dbContext.SaveChanges();
                    dbContext.SaveBBDDTime( "USERS_GROUPS", "LastModified" );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public static void DeleteById( Int32 id )
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                var row = new User()
                {
                    Id = id
                };

                dbContext.Users.Attach( row );
                dbContext.Users.Remove( row );

                dbContext.SaveChanges();
                dbContext.SaveBBDDTime( "USERS_GROUPS", "LastModified" );
            }
        }


        public static UserMVC Create( Int32 id )
        {
            UserMVC result = null;

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                User bbdd_user = null;

                List<SelectListItem> all_work_times = db.WorkTimes.ToList().Select( wt => wt.ToSelectListItem( false ) ).ToList();


                if( id != 0 )
                {
                    // BBDD user data
                    bbdd_user = db.Users.Include( "Phones" ).Include( "EMails" ).Include( "WorkTimes" ).Where( u => u.Id == id ).FirstOrDefault();
                }


                if( bbdd_user != null )
                {

                    List<SelectListItem> selected_work_times = bbdd_user.WorkTimes.Select( wt => wt.ToSelectListItem( false ) ).ToList();

                    result = new UserMVC()
                    {
                        Id = bbdd_user.Id,
                        FirstName = bbdd_user.FirstName,
                        SecondName = bbdd_user.SecondName,
                        Alias = bbdd_user.Alias,

                        WorkTimes = new ListBoxCheckedModel( new SelectList( all_work_times, "Value", "Text" ),
                                                             new SelectList( selected_work_times, "Value", "Text" ) ),
                        Channel = bbdd_user.Channel,

                        Phones = ( ( bbdd_user.Phones == null || bbdd_user.Phones.Count == 0 ) ?
                                     "" :
                                     String.Join( ";", bbdd_user.Phones.Select( p => p.Value ).ToArray(), 0, bbdd_user.Phones.Count ) ),
                        EMails = ( ( bbdd_user.EMails == null || bbdd_user.EMails.Count == 0 ) ?
                                     "" :
                                     String.Join( ";", bbdd_user.EMails.Select( p => p.Value ).ToArray(), 0, bbdd_user.EMails.Count ) ),

                        TimeOfEvents = bbdd_user.TimeOfEvents
                    };
                }
                else
                {
                    result = new UserMVC()
                    {
                        Channel = OPCConfigModel.Model.User.eChannel.sms_mail,
                        WorkTimes = new ListBoxCheckedModel( new SelectList( all_work_times, "Value", "Text" ),
                                                             new SelectList( new List<SelectListItem>(), "Value", "Text" ) ),
                        TimeOfEvents = OPCConfigModel.Model.User.TimeOfEvent_DefaultValue
                    };
                }
            }

            return result;
        }
    }
}