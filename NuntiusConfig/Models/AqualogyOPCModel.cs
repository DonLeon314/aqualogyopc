﻿using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.Models
{
    public class AqualogyOPCModel
    {
        private OPCConfigContext _Context = new OPCConfigContext();

        public List<EventSaverConfig> getEventSavers()
        {            
            return this._Context.EventSaverConfigs.ToList();
        }

        public EventSaverConfig getEventSaver( Int32 id )
        {
            return this._Context.EventSaverConfigs.Where( row => row.Id == id).FirstOrDefault();
        }
    }
}