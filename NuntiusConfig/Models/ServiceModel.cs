﻿using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NuntiusConfig.Models
{
    // Service model
    public class ServiceModel
    {
        /// <summary>
        /// Process detected time
        /// </summary>
        [Range( 1, 50, ErrorMessage = "Can only be between 1 .. 50" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 ProcessDetected
        {
            get;
            set;
        }

        /// <summary>
        /// SMS limit
        /// </summary>
        [Range( 0, 5000, ErrorMessage = "Can only be between 0 .. 5000" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number 0, 1, 2....5000" )]
        public Int32 SmsLimit
        {
            get;
            set;
        }

        /// <summary>
        /// end-point WCF service
        /// </summary>
        public String UrlService
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Range( 1, 500, ErrorMessage = "Can only be between 1 .. 500" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 ActualState
        {
            get;
            set;
        }

        /// <summary>
        /// save errors to log file
        /// </summary>
        [Display( Name = "Errors" )]
        public Boolean LogError
        {
            get;
            set;
        }

        /// <summary>
        /// save debugs to log file
        /// </summary>
        [Display( Name = "Debugs" )]
        public Boolean LogDebug
        {
            get;
            set;
        }

        /// <summary>
        /// save messages to log file
        /// </summary>
        [Display( Name = "Messages" )]
        public Boolean LogMessage
        {
            get;
            set;
        }

        [Display( Name = "Max log file(Mb)" )]
        [Range( 1, 50, ErrorMessage = "Can only be between 1 .. 50" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 MaxLog
        {
            get;
            set;
        }

        [Display( Name = "Process SALTO (min)" )]
        [Range( 1, 1440, ErrorMessage = "Can only be between 1 .. 1440" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 ProcessSalto
        {
            get;
            set;
        }

        [Display( Name = "SALTO History (min)" )]
        [Range( 1, 1440, ErrorMessage = "Can only be between 1 .. 1440" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 ProcessSaltoHistory
        {
            get;
            set;
        }


        /// <summary>
        /// 
        /// </summary>
        public ServiceModel()
        {
            InitDefault();
        }

        /// <summary>
        /// Load from BBDD
        /// </summary>
        public void LoadFromBBDD()
        {
            String section = "ServiceParams";

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                this.ProcessDetected = db.GetParameter( section, "ProcessDetected", 10 );
                this.SmsLimit = db.GetParameter( section, "SmsLimit", 1000 );
                this.UrlService = db.GetParameter( section, "UrlService", @"//localhost:9001" );
                this.ActualState = db.GetParameter( section, "ActualState", 10 );

                this.LogDebug = db.GetParameter( section, "LogDebug", true );
                this.LogError = db.GetParameter( section, "LogError", true);
                this.LogMessage = db.GetParameter( section, "LogMessage", true );

                this.MaxLog = db.GetParameter( section, "MaxLog", 20 );


                // SALTO
                this.ProcessSalto = db.GetParameter( "SALTO", "SaltoPeriod", 5 );
                this.ProcessSaltoHistory = db.GetParameter( "SALTO", "HistoryTime", 20 );
            }
        }

        /// <summary>
        /// Save to BBDD
        /// </summary>
        public void SaveToBBDD()
        {
            String section = "ServiceParams";

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                db.SetParameter( section, "ProcessDetected", this.ProcessDetected );
                db.SetParameter( section, "SmsLimit", this.SmsLimit );
                db.SetParameter( section, "UrlService", this.UrlService );
                db.SetParameter( section, "ActualState", this.ActualState );

                db.SetParameter( section, "LogDebug", this.LogDebug );
                db.SetParameter( section, "LogError", this.LogError );
                db.SetParameter( section, "LogMessage", this.LogMessage );

                db.SetParameter( section, "MaxLog", this.MaxLog );

                // SALTO
                db.SetParameter( "SALTO", "SaltoPeriod", this.ProcessSalto );
                db.SetParameter( "SALTO", "HistoryTime", this.ProcessSaltoHistory );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void InitDefault()
        {
            this.ActualState = 10;
            this.SmsLimit = 10000;
            this.ProcessDetected = 1;
            this.UrlService = @"//localhost:9001";
            
            this.LogDebug = true;
            this.LogError = true;
            this.LogMessage = true;

            this.MaxLog = 20;
        }
    }
}