﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{
    public class ListBoxCheckedModel
    {   
        private List<SelectListItem> _AllItems = new List<SelectListItem>();
        private List<SelectListItem> _SelectedItems = new List<SelectListItem>();

        public IEnumerable<SelectListItem> AllItems
        {       
            get
            {
                return this._AllItems;
            }
        }

        public String[] Posted
        {
            get;
            set;
        }

        public IEnumerable<SelectListItem> Selected
        {
            get
            {
                return this._SelectedItems;
            }
        }

        public ListBoxCheckedModel()
        {
        }

        public ListBoxCheckedModel( IEnumerable<SelectListItem> allItems, IEnumerable<SelectListItem> selectedTems )
        {
            this._AllItems.AddRange( allItems );
            this._SelectedItems.AddRange( selectedTems );
        }
    }
}