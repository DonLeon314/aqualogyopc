﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Models.ListBoxCheckedModels
{
    /// <summary>
    /// SelectedListIten's storage
    /// </summary>
    internal class SelectListItemsKeyedCollection:
                KeyedCollection<String,SelectListItem>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="comparer"></param>
        public SelectListItemsKeyedCollection( IEqualityComparer<String> comparer ):
                base( comparer )
        {

        }

        /// <summary>
        /// Constructor default
        /// </summary>
        /// <param name="comparer"></param>
        public SelectListItemsKeyedCollection()
        {
        }

        /// <summary>
        /// Get key
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override String GetKeyForItem( SelectListItem item )
        {
            return item.Value;
        }
    }
}