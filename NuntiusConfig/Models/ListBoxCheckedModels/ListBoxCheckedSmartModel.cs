﻿
using NuntiusConfig.Models.ListBoxCheckedModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ListBoxCheckedSmartModel:
                IListBoxCheckedModel
    {
        /// <summary>
        /// SelectListItem's case insensitive
        /// </summary>
        private SelectListItemsKeyedCollection _Items = new SelectListItemsKeyedCollection( StringComparer.CurrentCultureIgnoreCase );


        public String[] Posted
        {
            get;
            set;
        }

        /// <summary>
        /// All items
        /// </summary>
        public IEnumerable<SelectListItem> AllItems
        {
            get
            {
                return this._Items;
            }
        }

        /// <summary>
        /// Selected items
        /// </summary>
        public IEnumerable<SelectListItem> Selected
        {
            get
            {
                foreach( var item in this._Items )
                {
                    if( item.Selected )
                    {
                        yield return item;
                    }
                }
            }
        }

        /// <summary>
        /// Add new Item
        /// </summary>
        /// <param name="text">Item text</param>
        /// <param name="value">Item value. if item with value exist - refresh 'Text' and 'Select' </param>
        /// <param name="selected"> :Q </param>
        public void AddItem( String text, String value, Boolean selected = false )
        {
            String key = value.Trim();
            if( String.IsNullOrWhiteSpace( key ) )
            {
                throw new ArgumentException( String.Format( "Argument value:'{0}' incorrect!", value ) );
            }

            if( this._Items.Contains( key ) )
            {
                var item = this._Items[key];
                item.Text = text;
                item.Selected = selected;
            }
            else
            {
                this._Items.Add( new SelectListItem() { Value = value, Text = text, Selected = selected } );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public Boolean TryChangeItemState( String value, Boolean state )
        {
            String key = value.Trim();

            if( this._Items.Contains( key ) )
            {
                this._Items[key].Selected = state;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ListBoxCheckedSmartModel()
        {
            
        }
    }
}