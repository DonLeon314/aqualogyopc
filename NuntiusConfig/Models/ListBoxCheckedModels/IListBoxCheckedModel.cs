﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NuntiusConfig.Models.ListBoxCheckedModels
{
    public interface IListBoxCheckedModel
    {

        IEnumerable<SelectListItem> AllItems
        {
            get;
        }

        String[] Posted
        {
            get;
            set;
        }

        IEnumerable<SelectListItem> Selected
        {
            get;
        }

    }
}
