﻿using System;
using NuntiusConfig.AqualogyOPCWCFService;
using NuntiusConfig.Models.ListBoxCheckedModels;

namespace NuntiusConfig.Models
{
    public class ConfigurationsStatesModel
    {

        /// <summary>
        /// Configurations state Info
        /// </summary>
        public IListBoxCheckedModel Configurations
        {
            get
            {
                ListBoxCheckedSmartModel result = new ListBoxCheckedSmartModel();

                try
                {
                    using( AqualogyOPCRestServiceClient client = new AqualogyOPCRestServiceClient( "WSHttpBinding_IAqualogyOPCRestService" ) )
                    {
                        foreach( var item in client.GetConfigurations() )
                        {
                            result.AddItem( item.Name, item.Id.ToString(), item.State );
                        }
                    }

                } catch( Exception ex )
                {

                }


                return result;
            }
        }        

        /// <summary>
        /// 
        /// </summary>
        public ConfigurationsStatesModel()
        {

        }

        public enum eCommand
        {
            Start,
            Stop,
            Restart
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SendCommand( eCommand command )
        {

            using( AqualogyOPCRestServiceClient client = new AqualogyOPCRestServiceClient( "WSHttpBinding_IAqualogyOPCRestService" ) )
            {
                switch( command )
                {
                    case eCommand.Start:
                        {
                            client.StartProcessing();

                            break;
                        }
                    case eCommand.Stop:
                        {
                            client.StopProcessing();

                            break;
                        }
                    case eCommand.Restart:
                        {
                            client.RestartProcessing();

                            break;
                        }
                }
            }
        }
    }
}