﻿using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace NuntiusConfig.Models
{
    public class WorkTimeMVC
    {
        public Int32 Id
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 64 )]        
        public String Name
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [RegularExpression( @"^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$", ErrorMessage = "must be time HH:MM:SS" )]
        public String StartTime
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [RegularExpression( @"^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$", ErrorMessage = "must be time HH:MM:SS" )]
        public String EndTime
        {
            get;
            set;
        }
                
        public WeekDayModel WeekDays
        {
            get;
            set;
        }

        public WorkTimeMVC()
        {
            this.WeekDays = new WeekDayModel();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bbddWorkTime"></param>
        public WorkTimeMVC( WorkTime bbddWorkTime = null )
        {
            if( bbddWorkTime == null )
            {
                this.StartTime = new TimeSpan(8, 0, 0).ToString();
                this.EndTime = new TimeSpan(17,0,0).ToString();
                this.Name = String.Format( "{0}-{1}", this.StartTime.ToString(), this.EndTime.ToString( ) );
                this.WeekDays = new WeekDayModel( WorkTime.WeekDays_DefaultValue );
            }
            else
            {
                this.Id = bbddWorkTime.Id;
                this.StartTime = bbddWorkTime.StartTime.ToString();
                this.EndTime = bbddWorkTime.EndTime.ToString();
                this.Name = bbddWorkTime.Name;
                this.WeekDays = new WeekDayModel( bbddWorkTime.WeekDays );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workTimeId"></param>
        public static void DeleteWorkTimeById( Int32 workTimeId )
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                var row = new WorkTime()
                {
                    Id = workTimeId
                };

                dbContext.WorkTimes.Attach( row );
                dbContext.WorkTimes.Remove( row );

                dbContext.SaveChanges();

                dbContext.SaveBBDDTime( "USERS_GROUPS", "LastModified" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workTimeId"></param>
        /// <returns></returns>
        public static WorkTimeMVC CreateFromBBDD( Int32 workTimeId )
        {
            WorkTimeMVC result = null;

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                var work_time_bbdd = db.WorkTimes.Where( wt => wt.Id == workTimeId ).FirstOrDefault();
                if( work_time_bbdd != null )
                {
                    result = new WorkTimeMVC( work_time_bbdd );
                    return result;
                }
                else
                {
                    throw new Exception( String.Format( "WorkTime with id:{0} not found", workTimeId ) );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SaveToBBDD()
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                if( this.Id == 0 )
                {
                    WorkTime new_wt = new WorkTime()
                    {
                        Name = this.Name,
                        StartTime = TimeSpan.Parse(this.StartTime),
                        EndTime = TimeSpan.Parse(this.EndTime),
                        WeekDays = this.WeekDays.StringPostedDays
                    };

                    dbContext.WorkTimes.Add( new_wt );
                    dbContext.SaveChanges();

                    dbContext.SaveBBDDTime( "USERS_GROUPS", "LastModified" );

                }
                else
                {
                    var wt_bbdd = dbContext.WorkTimes.Where( wt => wt.Id == this.Id ).FirstOrDefault();
                    if( wt_bbdd != null )
                    {
                        wt_bbdd.Name = this.Name;
                        wt_bbdd.StartTime = TimeSpan.Parse( this.StartTime );
                        wt_bbdd.EndTime = TimeSpan.Parse( this.EndTime );
                        wt_bbdd.WeekDays = this.WeekDays.StringPostedDays;

                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new Exception( String.Format( "WorkTime: entyty with id:{0} not found", this.Id ) );
                    }
                }
            }
        }

    }
}