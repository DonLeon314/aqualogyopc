﻿using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UsersModel
    {

        public IEnumerable<UserMVC> Users
        {
            get
            {
                List<UserMVC> result = new List<UserMVC>();

                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    foreach( var bbdd_user in db.Users.ToList() )
                    {
                        UserMVC user = new UserMVC()
                        {
                            Id = bbdd_user.Id,
                            FirstName = bbdd_user.FirstName,
                            SecondName = bbdd_user.SecondName,
                            Alias = bbdd_user.Alias
                        };

                        result.Add( user );
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public UsersModel()
        {
        }
    }
}