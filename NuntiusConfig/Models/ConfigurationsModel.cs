﻿using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NuntiusConfig.Models
{
    public class ConfigurationsModel
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ConfigMVC> Configurations
        {
            get
            {
                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    return db.OPCConfigs.ToList().Select( c => new ConfigMVC( c ) ).ToList();
                }
            }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigurationsModel()
        {
            
        }
    }
}