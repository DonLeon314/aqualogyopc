﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using OPCConfigModel;
using NuntiusConfig.App_Code;

namespace NuntiusConfig.Models
{
    public class ConfigMVC
    {        
        /// <summary>
        /// BBDD id
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Config name
        /// </summary>
        [Required( ErrorMessage = "Required." )]
        [StringLength( 64 )]
        public String Name
        {
            get;
            set;
        }

        public Boolean Active
        {
            get;
            set;
        }

        public Boolean Visible
        {
            get;
            set;
        }

        [Range( 1, 600, ErrorMessage = "Can only be between 1 .. 600" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 Timeout
        {
            get;
            set;
        }

        [Range( 1, 3600, ErrorMessage = "Can only be between 1 .. 3600" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 AlarmDetected
        {
            get;
            set;
        }

        [Range( 1, 500, ErrorMessage = "Can only be between 1 .. 500" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 PublishingInterval
        {
            get;
            set;
        }

        [Range( 1, 500, ErrorMessage = "Can only be between 1 .. 500" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 SamplingInterval
        {
            get;
            set;
        }

        [Range( 1, 20, ErrorMessage = "Can only be between 1 .. 20" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 QueueSize
        {
            get;
            set;
        }

        public ListBoxCheckedModel Groups
        {
            get;
            set;
        }

        public ConfigMVC()
        {
            Initialize( new Config() );
        }

        public ConfigMVC( Config bbddConfig, IEnumerable<SelectListItem> allGroups = null )
        {
            
            if( bbddConfig == null )
            {
                bbddConfig = new Config();
            }

            Initialize( bbddConfig, allGroups );        
        }

        private void Initialize( Config bbddConfig, IEnumerable<SelectListItem> allGroups = null )
        {
            this.Id = bbddConfig.Id;
            this.Active = bbddConfig.Active;
            this.Visible = bbddConfig.Visible;
            this.Name = bbddConfig.Name;
            this.Timeout = bbddConfig.Timeout;
            this.AlarmDetected = bbddConfig.AlarmDetected;
            this.PublishingInterval = bbddConfig.PublishingInterval;
            this.SamplingInterval = bbddConfig.SamplingInterval;
            this.QueueSize = bbddConfig.QueueSize;

            if( allGroups != null )
            {                
                this.Groups = new ListBoxCheckedModel( allGroups, ( ( bbddConfig.Groups == null ) ? new List<SelectListItem>() :
                                                                    bbddConfig.Groups.Select( g => g.ToSelectListItem() ).ToList()
                                                                  ) 
                                                      );      
            }
            else
            {
                this.Groups = new ListBoxCheckedModel();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configId"></param>
        /// <returns></returns>
        public static ConfigMVC Create( Int32 configId )
        {
            ConfigMVC result;

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                Config config_bbdd = null;
                if( configId != 0 )
                {
                    config_bbdd = db.OPCConfigs.Include( "Groups" ).Where( cfg => cfg.Id == configId ).FirstOrDefault();
                }
                else
                {
                    config_bbdd = new Config();
                }

                result = new ConfigMVC( config_bbdd, db.Groups.ToList().Select( g => g.ToSelectListItem() ) );
            }
                        
            return result;
        }


        public void SaveToBBDD()
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                Config row;

                if( this.Id == 0 )
                {
                    row = new Config();
                    row = dbContext.OPCConfigs.Add( row );
                }
                else
                {
                    row = dbContext.OPCConfigs.Include( "Groups" ).Where( cfg => cfg.Id == this.Id ).FirstOrDefault();
                }

                if( row != null )
                {             
                    row.Name = this.Name;
                    row.Active = this.Active;
                    row.AlarmDetected = this.AlarmDetected;
                    row.PublishingInterval = this.PublishingInterval;
                    row.QueueSize = this.QueueSize;
                    row.SamplingInterval = this.SamplingInterval;
                    row.Timeout = this.Timeout;
                    row.Visible = this.Visible;

                    dbContext.SaveChanges();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configId"></param>
        public static void DeleteById( Int32 configId )
        {
            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                var row = new Config()
                {
                    Id = configId
                };

                dbContext.OPCConfigs.Attach( row );
                dbContext.OPCConfigs.Remove( row );

                dbContext.SaveChanges();
            }
        }
    }
}