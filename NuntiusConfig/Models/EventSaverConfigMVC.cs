﻿using OPCConfigModel.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace NuntiusConfig.Models
{
    /// <summary>
    /// Event saver config for MVC
    /// </summary>
    public class EventSaverConfigMVC
    {
        public Int32 Id
        {
            get;
            set;
        }
        
        public Int32 Priority
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 256 )]
        public String Description
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 256 )]
        public String ConnectionString
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 64 )]
        public String TableName
        {
            get;
            set;
        }

//         [Required( ErrorMessage = "Required." )]
//         [Range( 1, 100, ErrorMessage = "Can only be between 1 .. 100" )]
//         [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
//         public Int32 Repeats
//         {
//             get;
//             set;
//         }

        [Required( ErrorMessage = "Required." )]
        [Range( 1, 600, ErrorMessage = "Can only be between 1 .. 600" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32? TimeOutProcess
        {
            get;
            set;
        }

        /// <summary>
        /// EMail administrator
        /// </summary>
        [EmailAddress]
        public String EMailAdministrator
        {
            get;
            set;
        }

        /// <summary>
        /// OPC item for reboot GreenBox
        /// </summary>
        public String OPCItem
        {
            get;
            set;
        }

        public String OPCUAUrlServer
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Required( ErrorMessage = "Required." )]
        [Range( 1, 1440, ErrorMessage = "Can only be between 1 .. 1440" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 HistoryTime
        {
            get;
            set;
        }

        /// <summary>
        /// Error percent
        /// </summary>
        [Required( ErrorMessage = "Required." )]
        [Range( 1, 100, ErrorMessage = "Can only be between 1 .. 100" )]
        [RegularExpression( "^[0-9]*$", ErrorMessage = "Count must be a natural number" )]
        public Int32 ErrorPercent
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public EventSaverConfigMVC()
        {
            Initialize( new EventSaverConfig() );
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bbddEventSaverConfig"></param>
        public EventSaverConfigMVC( EventSaverConfig bbddEventSaverConfig )
        {
            if( bbddEventSaverConfig == null )
            {
                bbddEventSaverConfig = new EventSaverConfig();
            }

            Initialize( bbddEventSaverConfig );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bbddEventSaverConfig"></param>
        private void Initialize( EventSaverConfig bbddEventSaverConfig )
        {
            this.Id = bbddEventSaverConfig.Id;
            this.ConnectionString = bbddEventSaverConfig.ConnectionString;
            this.Description = bbddEventSaverConfig.Description;
            this.EMailAdministrator = bbddEventSaverConfig.EMailAdministrator;
            this.OPCItem = bbddEventSaverConfig.OPCItem;
            this.OPCUAUrlServer = bbddEventSaverConfig.OPCUAUrlServer;
            this.Priority = bbddEventSaverConfig.Priority;
            this.TableName = bbddEventSaverConfig.TableName;

        }



    }
}