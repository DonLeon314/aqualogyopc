﻿using OPCConfigModel;
using System.Collections.Generic;
using System.Linq;

namespace NuntiusConfig.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class EventSaverConfigModel
    {

        /// <summary>
        /// Event savers
        /// </summary>
        public List<EventSaverConfigMVC> EventSaverConfigs
        {
            get
            {
                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    return db.EventSaverConfigs.ToList().
                                                Select( es2 => new EventSaverConfigMVC( es2 ) ).
                                                OrderBy( esmvc => esmvc.Priority ).
                                                ToList();
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public EventSaverConfigModel()
        {

        }
    }
}