﻿using NuntiusConfig.App_Code;
using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{
    public class GroupMVC
    {
        public Int32 Id
        {
            get;
            set;
        }

        [Required( ErrorMessage = "Required." )]
        [StringLength( 64 )]
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 IdConfig
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ListBoxCheckedModel Users
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bbddGroup"></param>
        public GroupMVC()
        {
            this.Id = 0;
            this.IdConfig = 0;
            this.Name = String.Empty;
            this.Users = new ListBoxCheckedModel();
        }


        /// <summary>
        /// Create GrpupMVC model
        /// </summary>
        /// <param name="idGroup"></param>
        /// <param name="idConfiguration"></param>
        /// <returns></returns>
        public static GroupMVC Create( Int32 idGroup, Int32 idConfiguration )
        {
            GroupMVC result = null;

            OPCConfigModel.Model.Group group = null;

            List<SelectListItem> all_users = new List<SelectListItem>();

            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                all_users = dbContext.Users.ToList().Select( u => u.ToSelectListItem( false ) ).ToList();

                if( idGroup != 0 )
                {
                        group = dbContext.Groups.Include( "Users" ).Where( g => g.Id == idGroup ).First();
                }                
            }

            if( group == null && idGroup != 0 )
            {
                throw new Exception( String.Format( "Group with id={0} not found!", idGroup ) );
            }

            if( idGroup == 0 )
            {
                result = new GroupMVC()
                {
                    IdConfig = idConfiguration,
                    Name = "New Group",
                    Users = new ListBoxCheckedModel( all_users, new List<SelectListItem>() )
                };
            }
            else
            {
                result = new GroupMVC()
                {
                    Id = group.Id,
                    IdConfig = group.OPCConfigId,
                    Name = group.Name,
                    Users = new ListBoxCheckedModel( all_users, ( ( group.Users != null && group.Users.Count > 0 ) ?
                                                           group.Users.Select( g => g.ToSelectListItem( true ) ).ToList() :
                                                           new List<SelectListItem>() ) )
                };
            }

            return result;
        }


        public static void DeleteById( Int32 idGroup )
        {
            using( OPCConfigContext dbConfig = new OPCConfigContext() )
            {
                OPCConfigModel.Model.Group group = new OPCConfigModel.Model.Group() { Id = idGroup };

                dbConfig.Groups.Attach( group );
                dbConfig.Groups.Remove( group );

                dbConfig.SaveChanges();
                dbConfig.SaveBBDDTime( "USERS_GROUPS", "LastModified" );
            }
        }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="dbConfig"></param>
            /// <param name="idConfiguration"></param>
            /// <param name="groupName"></param>
            /// <returns></returns>
            private OPCConfigModel.Model.Group FindGroupByConfigIdAndName( OPCConfigContext dbConfig, Int32 idConfiguration, String groupName )
        {

            return dbConfig.Groups.Where( g => ( g.OPCConfigId == idConfiguration && String.Compare( g.Name, groupName, true ) == 0 ) )
                                           .FirstOrDefault();
        }

        /// <summary>
        /// Save to BBDD
        /// </summary>
        public void SaveToBBDD()
        {
            using( OPCConfigContext dbConfig = new OPCConfigContext() )
            {
                OPCConfigModel.Model.Group bbdd_group = null;

                if( this.Id != 0 )
                {
                    // Check group name
                    bbdd_group = FindGroupByConfigIdAndName( dbConfig, this.IdConfig, this.Name.Trim() );
                    if( bbdd_group != null && this.Id != bbdd_group.Id )
                    {
                        throw new Exception( String.Format( "Unable update - group: '{0}' exist ", this.Name ) );
                    }

                    // load
                    bbdd_group = dbConfig.Groups.Include( "Users" ).Where( g => g.Id == this.Id ).FirstOrDefault();
                }
                else
                {
                    // Check hroup name
                    if( null != FindGroupByConfigIdAndName( dbConfig, this.IdConfig, this.Name.Trim() ) )
                    {
                        throw new Exception( String.Format( "Unable update - group: '{0}' exist", this.Name ) );
                    }

                    // create
                    bbdd_group = new OPCConfigModel.Model.Group();
                    dbConfig.Groups.Add( bbdd_group );
                }

                if( bbdd_group != null )
                {
                    bbdd_group.Name = this.Name.Trim();
                    bbdd_group.OPCConfigId = this.IdConfig;
                    bbdd_group.Users.Clear();
                    if( this.Users.Posted != null )
                    {
                        foreach( var str_user_id in this.Users.Posted )
                        {
                            Int32 user_id = 0;
                            if( Int32.TryParse( str_user_id, out user_id ) )
                            {
                                OPCConfigModel.Model.User bbdd_user = dbConfig.Users.Where( u => u.Id == user_id ).FirstOrDefault();
                                if( bbdd_user != null )
                                {
                                    bbdd_group.Users.Add( bbdd_user );
                                }
                            }
                        }
                    }
                    dbConfig.SaveChanges();
                    dbConfig.SaveBBDDTime( "USERS_GROUPS", "LastModified" );
                }
            }
        }
    }
}