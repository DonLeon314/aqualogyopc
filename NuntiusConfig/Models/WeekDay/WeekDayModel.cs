﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Models
{

    public class WeekDayModel
    {
        private static List<SelectListItem> s_DaysRepository = new List<SelectListItem>()
        {
            new SelectListItem() {Value="L", Text="Lunes" },
            new SelectListItem() {Value="M", Text="Martes" },
            new SelectListItem() {Value="X", Text="Miercoles" },
            new SelectListItem() {Value="J", Text="Jueves" },
            new SelectListItem() {Value="V", Text="Viernes" },
            new SelectListItem() {Value="S", Text="Sabado" },
            new SelectListItem() {Value="D", Text="Domingo" }
        };

        private List<SelectListItem> _SelectedDays = new List<SelectListItem>();
        
        public WeekDayModel( String weekDays = null )
        {
            this.StringSelectedDays = weekDays;
        }

        public String StringPostedDays
        {
            get
            {
                StringBuilder result = new StringBuilder();
                if (this.PostedDays != null && this.PostedDays.Length > 0)
                {
                    foreach (String day_id in this.PostedDays)
                    {
                        SelectListItem day_info = s_DaysRepository.Where(d => d.Value == day_id).FirstOrDefault();
                        if (day_info != null)
                        {
                            result.Append( day_id );
                        }
                    }
                }

                return result.ToString();
            }
        }

        public String StringSelectedDays
        {
            get
            {
                StringBuilder result = new StringBuilder();

                if( this.SelectedDays != null && this.SelectedDays.Count > 0 )
                {
                    foreach( SelectListItem day in this.SelectedDays )
                    {
                        result.Append( day.Value );                        
                    }
                }

                return result.ToString();
            }

            set
            {
                this._SelectedDays.Clear();                 

                if( String.IsNullOrWhiteSpace( value ) == false )
                {   
                    value = value.Trim().ToUpper();
                    for(Int32 idx = 0; idx < value.Length; idx++ )
                    {
                        Char id = value[idx];
                        SelectListItem day_info = s_DaysRepository.Where( di => di.Value[0] == id ).FirstOrDefault();
                        if( day_info != null )
                        {
                            this._SelectedDays.Add( new SelectListItem() { Text = day_info.Text, Value = day_info.Value } );
                        }
                    }
                }
            }
        }

        public String[] PostedDays
        {
            get;
            set;
        }

        public IList<SelectListItem> SelectedDays
        {
            get
            {
                return this._SelectedDays;
            }
        }

        public IList<SelectListItem> AllDays
        {
            get
            {
                return s_DaysRepository.ToList();
            }
        }


    }
}