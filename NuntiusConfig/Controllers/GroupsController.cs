﻿using NuntiusConfig.Models;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace NuntiusConfig.Controllers
{
    public class GroupsController: Controller
    {        
        /// <summary>
        /// GET: Groups
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {

                return View( "Index", GroupsModel.Create( 0 ) );


            }catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex);
                return RedirectToAction( "Index", "Error" );
            }
        }

        public ActionResult IndexById( Int32 id )
        {
            try
            {

                return View( "Index", GroupsModel.Create( id ) );


            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult UpdateGroup( GroupMVC obj )
        {
            try
            {
                if( ModelState.IsValid )
                {

                    obj.SaveToBBDD();

                    Session["NeedRestartService"] = true;

                    Response.StatusCode = 200;
                    return Json( new { ConfigurationId = obj.IdConfig } );
                }

                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, "Invalid model!" );

            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details( Int32 groupId, Int32 configurationId )
        {
            try
            {

                return PartialView( GroupMVC.Create( groupId, configurationId ) );
                
            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idGroup"></param>
        /// <param name="idConfig"></param>
        /// <returns></returns>
        public HttpStatusCodeResult DeleteById( Int32 idGroup, Int32 idConfig )
        {
            try
            {
                GroupMVC.DeleteById( idGroup );

                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult( HttpStatusCode.OK, idConfig.ToString() );

            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }
    }
}