﻿using NuntiusConfig.Models;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace NuntiusConfig.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            return View( new UsersModel() );
        }

        public ActionResult IndexRefresh()
        {
            return View( new UsersModel() );
        }

        /// <summary>
        /// Get Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details( Int32 id )
        {
            try
            {

                return PartialView( UserMVC.Create( id ) );


            } catch( Exception ex )
            {

                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );

            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbCintext"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private WorkTime FindWorkTime( OPCConfigContext dbCintext, String value )
        {
            WorkTime result = null;

            Int32 id = 0;
            if( Int32.TryParse( value, out id ) && id != 0 )
            {
                result = dbCintext.WorkTimes.Where( wt => wt.Id == id ).FirstOrDefault();
            }

            return result;
        }


        /// <summary>
        /// Add or Update user
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public HttpStatusCodeResult UpdateUser( UserMVC obj )
        {
            try
            {

                obj.UpdateToBBDD();
                
                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult( HttpStatusCode.OK );

            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpStatusCodeResult DeleteUserById( Int32 id )
        {
            try
            {
                UserMVC.DeleteById( id );

                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult( HttpStatusCode.OK );
            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }
    }
}