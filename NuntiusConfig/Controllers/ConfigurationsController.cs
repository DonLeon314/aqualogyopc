﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NuntiusConfig.Models;

namespace NuntiusConfig.Controllers
{
    public class ConfigurationsController : Controller
    {
        // GET: Configurations
        public ActionResult Index()
        {            

            return View( new ConfigurationsModel() );         
        }

        /// <summary>
        /// Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details( Int32 id )
        {
            try
            {
                var model = ConfigMVC.Create( id );

                return PartialView( model );

            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public HttpStatusCodeResult UpdateConfiguration( ConfigMVC obj )
        {
            try
            {
                obj.SaveToBBDD();
                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpStatusCodeResult DeleteConfigById( Int32 id )
        {
            try
            {

                ConfigMVC.DeleteById( id );

                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult(HttpStatusCode.OK);

            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}