﻿using NuntiusConfig.Models;
using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Controllers
{
    public class OPCItemsController : Controller
    {
        // GET: OPCItems
        public ActionResult Index()
        {
            return View( "Index", OPCItemsModel.CreateFromBBDD() );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public  ActionResult LoadItems( OPCItemsModel obj )
        {
            OPCItemsModel m =(OPCItemsModel)obj;

            try
            {
                obj.FileToBBDD();
                
                Session["NeedRestartService"] = true;

                return View( "Index", OPCItemsModel.CreateFromBBDD() );
                //return new HttpStatusCodeResult( HttpStatusCode.OK );
            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex);
                return RedirectToAction( "Index", "Error" );
               // return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }            
        }        

    }
}