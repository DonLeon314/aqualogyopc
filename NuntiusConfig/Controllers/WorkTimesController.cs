﻿using NuntiusConfig.Models;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Controllers
{
    public class WorkTimesController: Controller
    {
        // GET: WorkTimes
        public ActionResult Index()
        {
            return View( new WorkTimesModel() );
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details( Int32 id )
        {
            try
            {

                WorkTimeMVC work_time = null;

                if( id != 0 )
                {
                    work_time = WorkTimeMVC.CreateFromBBDD( id );
                }
                else
                {
                    work_time = new WorkTimeMVC()
                    {
                        WeekDays = new WeekDayModel(WorkTime.WeekDays_DefaultValue)
                    };
                }

                return PartialView( work_time );
            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="workTime"></param>
        /// <returns></returns>
        public HttpStatusCodeResult UpdateWorkTime( WorkTimeMVC workTime )
        {
            try
            {

                workTime.SaveToBBDD();

                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult(HttpStatusCode.OK); 
            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message ); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpStatusCodeResult DeleteWorkTimeById( Int32 id )
        {
            try
            {
                WorkTimeMVC.DeleteWorkTimeById( id );

                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}