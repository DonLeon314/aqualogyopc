﻿using NuntiusConfig.AqualogyOPCWCFService;
using NuntiusConfig.Models;
using System;
using System.Net;
using System.ServiceModel;
using System.Web.Mvc;

namespace NuntiusConfig.Controllers
{
    public class WCFServiceController : Controller
    {
        // GET: WCFService
        public ActionResult Index()
        {
            return View( new ConfigurationsStatesModel() );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpStatusCodeResult Start()
        {
            try
            {

                ConfigurationsStatesModel.SendCommand( ConfigurationsStatesModel.eCommand.Start );

                Session["NeedRestartService"] = false;

                return new HttpStatusCodeResult( HttpStatusCode.OK );                

            } catch( FaultException<AqualogyOPCRestServiceFault> faultEx )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, faultEx.Detail.CustomError );

            }catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpStatusCodeResult Stop()
        {
            try
            {

                ConfigurationsStatesModel.SendCommand( ConfigurationsStatesModel.eCommand.Stop );
                
                Session["NeedRestartService"] = false;

                return new HttpStatusCodeResult( HttpStatusCode.OK );

            }
            catch( FaultException<AqualogyOPCRestServiceFault> faultEx )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, faultEx.Detail.CustomError );

            } catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpStatusCodeResult Restart()
        {
            try
            {

                ConfigurationsStatesModel.SendCommand( ConfigurationsStatesModel.eCommand.Restart );

                Session["NeedRestartService"] = false;

                return new HttpStatusCodeResult( HttpStatusCode.OK );

            }catch( FaultException<AqualogyOPCRestServiceFault> faultEx )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, faultEx.Detail.CustomError );

            }catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }
    }
}