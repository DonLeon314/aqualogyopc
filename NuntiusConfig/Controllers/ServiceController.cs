﻿using NuntiusConfig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Controllers
{
    public class ServiceController : Controller
    {
        // GET: Service
        public ActionResult Index()
        {
            try
            {

                ServiceModel model = new ServiceModel();

                model.LoadFromBBDD();
            
                return View( model );
                
            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex);
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult UpdateService( ServiceModel obj )
        {
            try
            {
                obj.SaveToBBDD();
                
                Session["NeedRestartService"] = true;

                return View( "Index" );
            }
            catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public HttpStatusCodeResult UpdateSMTP( SMTPConfigMVC obj )
        {
            try
            {
                obj.SaveToBBDD();
                
                Session["NeedRestartService"] = true;

                return new HttpStatusCodeResult( HttpStatusCode.OK );

            } catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult DetailsSMTP()
        {
            try
            {
                return PartialView( SMTPConfigMVC.LoadFromBBDD() );


            } catch( Exception ex )
            {
                TempData["ErrorModel"] = new ErrorModel( ex );
                return RedirectToAction( "Index", "Error" );
            }
        }

        /// <summary>
        /// Self test
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public HttpStatusCodeResult SMTPTest( SMTPConfigMVC obj )
        {
            try
            {
                obj.TestConfig();

                return new HttpStatusCodeResult( HttpStatusCode.OK );

            } catch( Exception ex )
            {
                StringBuilder sb = new StringBuilder();

                sb.Append( ex.Message );

                if( ex.InnerException != null )
                {
                    sb.Append( ":" );
                    sb.Append( ex.InnerException.Message );
                }

                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, sb.ToString() );
            }
        }
    }
}