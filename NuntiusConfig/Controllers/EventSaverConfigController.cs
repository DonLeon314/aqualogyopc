﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using OPCConfigModel;
using OPCConfigModel.Model;
using NuntiusConfig.Models;

namespace NuntiusConfig.Controllers
{
    public class EventSaverConfigController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View( new EventSaverConfigModel() );            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details( Int32 id )
        {
            EventSaverConfigMVC result = null;

            if( id != 0 )
            {
                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    var result_bbdd = db.EventSaverConfigs.Where( esc => esc.Id == id ).FirstOrDefault();

                    if( result_bbdd != null )
                    {
                        result = new EventSaverConfigMVC( result_bbdd );
                    }

                }
            }
            else
            {
                result = new EventSaverConfigMVC();
            }

            if( result != null )
                return PartialView( result );

            return HttpNotFound();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        //public ActionResult UpdateEventSaverConfig( EventSaverConfig obj )
        public HttpStatusCodeResult UpdateEventSaverConfig( EventSaverConfigMVC obj )         
        {
            try
            {
                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    EventSaverConfig row;

                    var transaction = db.Database.BeginTransaction();

                    try
                    {

                        if( obj.Id == 0 )
                        {
                            row = new EventSaverConfig();
                            row = db.EventSaverConfigs.Add( row );
                        }
                        else
                        {
                            row = db.EventSaverConfigs.Where( esc => esc.Id == obj.Id ).FirstOrDefault();
                        }

                        if( row != null )
                        {
                            row.Description = obj.Description;
                            row.ConnectionString = obj.ConnectionString;
                            row.Priority = obj.Priority;
                            //row.Repeats = obj.Repeats;
                            row.TableName = obj.TableName;
                            row.OPCItem = obj.OPCItem;
                            row.OPCUAUrlServer = obj.OPCUAUrlServer;
                            row.EMailAdministrator = obj.EMailAdministrator;


                            db.SaveBBDDTime( "SALTOS", "LastModified" );

                            db.SaveChanges();
                        }

                        transaction.Commit();

                        return new HttpStatusCodeResult( HttpStatusCode.OK );

                    } catch( Exception ex )
                    {

                        transaction.Rollback();

                        throw ex;
                    }
                }
            }
            catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpStatusCodeResult DeleteSaverById( Int32 id )
        {
            try
            {
                using( OPCConfigContext db = new OPCConfigContext() )
                {
                    var row = new EventSaverConfig()
                    {
                        Id = id
                    };

                    db.EventSaverConfigs.Attach( row );
                    db.EventSaverConfigs.Remove( row );

                    db.SaveChanges();
                    db.SaveBBDDTime( "SALTOS", "LastModified" );

                    Session["NeedRestartService"] = true;

                    return new HttpStatusCodeResult( HttpStatusCode.OK );
                }
            }catch( Exception ex )
            {
                return new HttpStatusCodeResult( HttpStatusCode.InternalServerError, ex.Message );
            }
        }
    }
}