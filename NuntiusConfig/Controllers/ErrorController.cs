﻿using NuntiusConfig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NuntiusConfig.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            ErrorModel model = (ErrorModel)TempData["ErrorModel"];

            if( model == null )
            {
                model = new ErrorModel( "Unknown error!" ); // :(
            }

            return View( model );
        }
    }
}