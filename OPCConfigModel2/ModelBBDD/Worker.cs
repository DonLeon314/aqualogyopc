﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("Workers")]
    public class Worker
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [MaxLength(128)]
        public String Alias
        {
            get;
            set;
        }

        [MaxLength(64)]
        public String FirstName
        {
            get;
            set;
        }

        [MaxLength(64)]
        public String SecondName
        {
            get;
            set;
        }

        /// <summary>
        /// EMails
        /// </summary>
        [MaxLength(1024)]
        public String EMails
        {
            get;
            set;
        }

        [MaxLength(1024)]
        public String Phones
        {
            get;
            set;
        }

        /// <summary>
        /// Channel types
        /// </summary>
        public enum eChannel
        {
            Sms,
            EMail,
            SmsAndEmail
        }

        /// <summary>
        /// Channel
        /// </summary>
        [Required]
        public eChannel Channel
        {
            get;
            set;
        }

        /// <summary>
        /// Configurations
        /// </summary>
        public ICollection<Configuration> Configurations
        {
            get;
            set;
        }

        /// <summary>
        /// Worker groups
        /// </summary>
        public ICollection<WorkerGroup> WorkerGroups
        {
            get;
            set;
        }

        public ICollection<WorkTime> WorkTimes
        {
            get;
            set;
        }

        public ICollection<AlarmCluster> AlarmClusters
        {
            get;
            set;
        }

        public Worker()
        {
            DefaultInit();
        }

        private void DefaultInit()
        {
            this.Configurations = new List<Configuration>();
            this.WorkerGroups = new List<WorkerGroup>();
            this.WorkTimes = new List<WorkTime>();
            this.AlarmClusters = new List<AlarmCluster>();
        }
    }
}
