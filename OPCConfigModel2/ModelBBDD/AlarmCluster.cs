﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel2.ModelBBDD
{
    [Table( "AlarmClisters" )]
    public class AlarmCluster
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Index( "IX_AlarmCluster_AT_AG_W", 1, IsUnique = true )]
        public String AlarmTypeId
        {
            get;
            set;
        }

        public AlarmType AlarmType
        {
            get;
            set;
        }

        [Index( "IX_AlarmCluster_AT_AG_W", 2, IsUnique = true )]
        public Int32 AlarmGroupId
        {
            get;
            set;
        }

        public AlarmGroup AlarmGroup
        {
            get;
            set;
        }

        public enum eWorkTImeSelector 
        {
            eAll,
            eInWorkTIme,
            eOutWorkTime
        }
        
        public eWorkTImeSelector WorkTImeSelector
        {
            get;
            set;
        }

        [Index( "IX_AlarmCluster_AT_AG_W", 3, IsUnique = true )]
        public Int32 WorkerId
        {
            get;
            set;
        }

        public Worker Worker
        {
            get;
            set;
        }

        public Boolean Active
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public AlarmCluster()
        {

        }
    }
}
