﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("Configurations")]
    public class Configuration
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [MaxLength(64)]
        public String Description
        {
            get;
            set;
        }

        public enum eConfigState
        {
            Active,
            NoActive,
            Invisible
        }

        [Required]
        public eConfigState State
        {
            get;
            set;
        }


        /// <summary>
        /// Workers
        /// </summary>
        public ICollection<Worker> Workers
        {
            get;
            set;
        }

        public ICollection<WorkerGroup> WorkerGroups
        {
            get;
            set;
        }

        public ICollection<Tag> Tags
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Configuration()
        {
            DefaultInit();
        }

        /// <summary>
        /// Initialize default
        /// </summary>
        private void DefaultInit()
        {
            this.Workers = new List<Worker>();
            this.WorkerGroups = new List<WorkerGroup>();
            this.Tags = new List<Tag>();
        }
    }
}
