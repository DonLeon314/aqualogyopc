﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel2.ModelBBDD
{
    [Table( "TagClients" )]
    public class TagClient
    {
        /// <summary>
        /// BBDD id
        /// </summary>
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Tag client description
        /// </summary>
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public TagClient()
        {
        }
    }
}
