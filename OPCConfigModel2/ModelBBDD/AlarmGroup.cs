﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//
namespace OPCConfigModel2.ModelBBDD
{
    [Table("AlarmGroups")]
    public class AlarmGroup
    {
        /// <summary>
        /// AlarmGroup BBDD id
        /// </summary>
        [Key]
        [DatabaseGenerated( DatabaseGeneratedOption.None )]
        public Int32 Id
        {
            get;
            set;
        }


        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get;
            set;
        }

        public AlarmGroup()
        {
            DefaultInit();
        }

        private void DefaultInit()
        {
        }
    }
}
