﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("OPCUAServers")]
    public class UAServer
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [MaxLength(256)]
        public String OPCUAUrl
        {
            get;
            set;
        }

        [Required]
        public Int32 Timeout
        {
            get;
            set;
        }

        [Required]
        public Int32 AlarmDetacted
        {
            get;
            set;
        }

        [Required]
        public Int32 PublishingInterval
        {
            get;
            set;
        }

        [Required]
        public Int32 SamplingInterval
        {
            get;
            set;
        }

        [Required]
        public Int32 QueueSize
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public UAServer()
        {
            DefaultInit();
        }

        /// <summary>
        /// Default initialize
        /// </summary>
        private void DefaultInit()
        {
            this.Timeout = 66;
            this.AlarmDetacted = 300;
            this.PublishingInterval = 30;
            this.SamplingInterval = 30;
            this.QueueSize = 10;
        }
    }
}
