﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    /// <summary>
    /// Work time
    /// </summary>
    [Table("WorkTimes")]
    public class WorkTime
    {
        /// <summary>
        /// BBDD id
        /// </summary>
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Description work time
        /// </summary>
        [MaxLength(64)]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// start time
        /// </summary>
        [Required]
        public TimeSpan StartTime
        {
            get;
            set;
        }

        /// <summary>
        /// end time
        /// </summary>
        [Required]
        public TimeSpan EndTime
        {
            get;
            set;
        }

        /// <summary>
        /// Week days
        /// </summary>
        [Required]
        public String WeekDays
        {
            get;
            set;
        }
        
        public ICollection<Worker> Workers
        {
            get;
            set;
        }

        public ICollection<WorkerGroup> WorkerGroups
        {
            get;
            set;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public WorkTime()
        {
            DefaultInit();
        }

        /// <summary>
        /// Initialize default
        /// </summary>
        private void DefaultInit()
        {
            this.Workers = new List<Worker>();
            this.WorkerGroups = new List<WorkerGroup>();
        }
    }
}
