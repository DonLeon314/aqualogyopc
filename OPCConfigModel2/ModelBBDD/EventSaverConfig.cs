﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel2.ModelBBDD
{
    public class EventSaverConfig
    {
        public Int32 Id
        {
            get;
            set;
        }

        [Index( IsUnique = true )]
        public Int32 Priority
        {
            get;
            set;
        }

        [MaxLength( 256 )]
        public String Description
        {
            get;
            set;
        }

        [Required, MaxLength( 256 )]
        public String ConnectionString
        {
            get;
            set;
        }

        [Required, MaxLength( 64 )]
        public String TableName
        {
            get;
            set;
        }
   

        /// <summary>
        /// EMail administrator
        /// </summary>
        public String EMailAdministrator
        {
            get;
            set;
        }

        /// <summary>
        /// OPC item for reboot GreenBox
        /// </summary>
        public String OPCItem
        {
            get;
            set;
        }

        public String OPCUAUrlServer
        {
            get;
            set;
        }
                

        /// <summary>
        /// Constructor
        /// </summary>
        public EventSaverConfig()
        {
    
        }        
    }
}
