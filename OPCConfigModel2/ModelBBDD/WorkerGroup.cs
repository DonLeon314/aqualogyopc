﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("WorkerGroups")]
    public class WorkerGroup
    {
        /// <summary>
        /// User group BBDD id
        /// </summary>
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// USer group description
        /// </summary>
        [Required]
        [MaxLength(64)]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Workers
        /// </summary>
        public ICollection<Worker> Workers
        {
            get;
            set;
        }

//         public ICollection<AlarmCluster> AlarmClusters
//         {
//             get;
//             set;
//         }

        public Int32 ConfigurationId
        {
            get;
            set;
        }

        public Configuration Configuration
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkerGroup()
        {
            DefaultInit();
        }

        /// <summary>
        /// Default initialize
        /// </summary>
        private void DefaultInit()
        {
            this.Workers = new List<Worker>();
           // this.AlarmClusters = new List<AlarmCluster>();
        }
    }
}
