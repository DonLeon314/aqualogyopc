﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("Tags")]
    public class Tag
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        public String TagName
        {
            get;
            set;
        }

        /// <summary>
        /// Large message
        /// </summary>
        public String LargeMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Short message
        /// </summary>
        public String ShortMessage
        {
            get;
            set;
        }

        /// <summary>
        /// OPC UA server id
        /// </summary>
        public Int32 UAServerId
        {
            get;
            set;
        }

        /// <summary>
        /// OPC UA server
        /// </summary>
        public UAServer UAServer
        {
            get;
            set;
        }

        /// <summary>
        /// Configuration BBDD id
        /// </summary>
        public Int32 ConfigurationId
        {
            get;
            set;
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public Configuration Configuration
        {
            get;
            set;
        }

        /// <summary>
        /// Tag client ID
        /// </summary>
        public Int32 TagClientId
        {
            get;
            set;
        }

        /// <summary>
        /// Tag client
        /// </summary>
        public TagClient Client
        {
            get;
            set;
        }

        public Tag()
        {
            DefaultInit();
        }

        private void DefaultInit()
        {

        }
    }
}
