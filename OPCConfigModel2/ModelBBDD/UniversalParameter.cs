﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel2.ModelBBDD
{
    public class UniversalParameter
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [MaxLength( 64 )]
        [Required]
        [Index( "IX_SectionParameter", 1, IsUnique = true )]
        public String SectionName
        {
            get;
            set;
        }

        [MaxLength( 64 )]
        [Required]
        [Index( "IX_SectionParameter", 2, IsUnique = true )]
        public String ParameterName
        {
            get;
            set;
        }

        public String Value
        {
            get;
            set;
        }
    }
}
