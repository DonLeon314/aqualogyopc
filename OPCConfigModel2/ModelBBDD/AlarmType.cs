﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel2.ModelBBDD
{
    [Table("AlarmTypes")]
    public class AlarmType
    {
        [Key]
        [MaxLength(2)]
        public String Id
        {
            get;
            set;
        }

        [MaxLength(256)]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public AlarmType()
        {
            DefaultInit();
        }

        /// <summary>
        /// Default initialize
        /// </summary>
        private void DefaultInit()
        {
            
        }
    }
}
