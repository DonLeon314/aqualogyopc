﻿using System;


namespace OPCConfigModel2.ExportClasses
{
    public class SMTPConfig
    {
        /// <summary>
        /// Host name 
        /// </summary>
        public String Host
        {
            get;
            private set;
        }

        /// <summary>
        /// Porn number
        /// </summary>
        public Int32 Port
        {
            get;
            private set;
        }

        /// <summary>
        /// User name
        /// </summary>
        public String User
        {
            get;
            private set;
        }

        /// <summary>
        /// Email From
        /// </summary>
        public String From
        {
            get;
            private set;
        }

        /// <summary>
        /// Enable SSL
        /// </summary>
        public Boolean EnableSSL
        {
            get;
            private set;
        }

        /// <summary>
        /// User password
        /// </summary>
        public String Password
        {
            get;
            private set;

        }



        /// <summary>
        /// Constructor
        /// </summary>
        private SMTPConfig()
        {

        }

        public SMTPConfig( String host, Int32 port, String user, String password, String from, Boolean enableSSL )
        {
            this.Host = host;
            this.Port = port;
            this.User = user;
            this.Password = password;
            this.From = from;
            this.EnableSSL = enableSSL;
        }


        /// <summary>
        /// Create from BBDD 
        /// </summary>
        /// <returns></returns>
        public static SMTPConfig CreateFromBBDD()
        {
            using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
            {
                return CreateFromBBDD( dbContext );
            }
        }

        /// <summary>
        /// Is valid configuration
        /// </summary>
        /// <returns></returns>
        public Boolean IsValid()
        {
            return (
                        String.IsNullOrWhiteSpace( this.Host ) == false 
                   );
        }

        /// <summary>
        /// Create from BBDD
        /// </summary>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static SMTPConfig CreateFromBBDD( OPCConfigContext2 dbContext )
        {
            return new SMTPConfig()
            {
#if DEBUG
                Host = dbContext.GetParameter("SMTP_INFORMATOR", "Host", "smtp.google.com" ),
                Port = dbContext.GetParameter( "SMTP_INFORMATOR", "Port", 587 ),
                User = dbContext.GetParameter( "SMTP_INFORMATOR", "User", "donleon314@gmail.com" ),
                Password = dbContext.GetParameter( "SMTP_INFORMATOR", "Password", "Elcorazon2015" ),
                From = dbContext.GetParameter( "SMTP_INFORMATOR", "From", "donleon314@gmail.com" ),
                EnableSSL = dbContext.GetParameter( "SMTP_INFORMATOR", "EnableSSL", false )
#else
                Host = dbContext.GetParameter( "SMTP_INFORMATOR", "Host", "" ),
                Port = dbContext.GetParameter( "SMTP_INFORMATOR", "Port", 0 ),
                User = dbContext.GetParameter( "SMTP_INFORMATOR", "User", "" ),
                Password = dbContext.GetParameter( "SMTP_INFORMATOR", "Password", "" ),
                From = dbContext.GetParameter( "SMTP_INFORMATOR", "From", "" ),
                EnableSSL = dbContext.GetParameter( "SMTP_INFORMATOR", "EnableSSL", false )
#endif
            };
        }

        /// <summary>
        /// Save to BBDD Context
        /// </summary>
        public void SaveToBBDD()
        {
            using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
            {
                SaveToBBDD( dbContext );
            }
        }

        /// <summary>
        /// Save to BBDD context
        /// </summary>
        /// <param name="dbContext">EF BBDD context</param>
        public void SaveToBBDD( OPCConfigContext2 dbContext )
        {
            dbContext.SetParameter( "SMTP_INFORMATOR", "Host", this.Host );
            dbContext.SetParameter( "SMTP_INFORMATOR", "Port", this.Port );
            dbContext.SetParameter( "SMTP_INFORMATOR", "User", this.User );
            dbContext.SetParameter( "SMTP_INFORMATOR", "Password", this.Password );
            dbContext.SetParameter( "SMTP_INFORMATOR", "From", this.From );
            dbContext.SetParameter( "SMTP_INFORMATOR", "EnableSSL", this.EnableSSL );
        }
    }
}
