﻿using OPCConfigModel2.ModelBBDD;
using System;
using System.Data.Entity;
using System.Linq;

namespace OPCConfigModel2
{
    public class OPCConfigContext2:
             DbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OPCConfigContext2()
            : base( "DbConnection" )
        {
            //Database.SetInitializer( new MigrateDatabaseToLatestVersion<OPCConfigContext2, OPCConfigModel2.Migrations.Configuration>() );
        }

        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            base.OnModelCreating( modelBuilder );
        }

        /// <summary>
        /// Configurations
        /// </summary>
        public DbSet<Configuration> Configurations
        {
            get;
            set;
        }

        /// <summary>
        /// Workers
        /// </summary>
        public DbSet<Worker> Workers
        {
            get;
            set;
        }

        /// <summary>
        /// Work times
        /// </summary>
        public DbSet<WorkTime> WorkTimes
        {
            get;
            set;
        }

        /// <summary>
        /// Workers groups
        /// </summary>
        public DbSet<WorkerGroup> WorkerGroups
        {
            get;
            set;
        }

        /// <summary>
        /// OPC UA server
        /// </summary>
        public DbSet<UAServer> UAServers
        { 
            get;
            set;
        }

        /// <summary>
        /// Alarm types
        /// </summary>
        public DbSet<AlarmType> AlarmTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Alarm groups
        /// </summary>
        public DbSet<AlarmGroup> AlarmGroups
        {
            get;
            set;
        }

        /// <summary>
        /// OPC tags
        /// </summary>
        public DbSet<Tag> Tags
        {
            get;
            set;
        }

        /// <summary>
        /// Clusters
        /// </summary>
        public DbSet<AlarmCluster> AlarmClusters
        {
            get;
            set;
        }


        /// <summary>
        /// Parameters
        /// </summary>
        public DbSet<UniversalParameter> Parameters
        {
            get;
            set;
        }

        /// <summary>
        /// Event Savers
        /// </summary>
        public DbSet<EventSaverConfig> EventSaverConfigs
        {
            get;
            set;
        }

        /// <summary>
        /// Tag Clients
        /// </summary>
        public DbSet<TagClient> TagClients
        {
            get;
            set;
        }

        #region WORK WITH PARAMETERS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Int32 GetParameter( String nameSection, String nameParameter, Int32 defaultValue )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();

            if( row != null )
            {
                Int32 value = defaultValue;
                if( Int32.TryParse( row.Value, out value ) )
                {
                    return value;
                }
            }

            return defaultValue;
        }

        public Int64 GetParameter( String nameSection, String nameParameter, Int64 defaultValue )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();

            if( row != null )
            {
                Int64 value = defaultValue;
                if( Int64.TryParse( row.Value, out value ) )
                {
                    return value;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public String GetParameter( String nameSection, String nameParameter, String defaultValue = null )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row != null )
            {
                return row.Value;
            }

            return defaultValue;
        }

        public Boolean GetParameter( String nameSection, String nameParameter, Boolean defaultValue = false )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row != null )
            {
                Boolean r = defaultValue;
                if( Boolean.TryParse( row.Value, out r ) )
                {
                    return r;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Set parameter
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, String value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value;

            this.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, Int32 value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }

        public void SetParameter( String nameSection, String nameParameter, Int64 value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }

        /// <summary>
        /// Set parameter
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, Boolean value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }


        /// <summary>
        /// Check parameters
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        private void CheckParameters( String nameSection, String nameParameter )
        {
            if( String.IsNullOrWhiteSpace( nameSection ) || String.IsNullOrWhiteSpace( nameParameter ) )
            {
                throw new ArgumentException( String.Format( "Incorrect parameters Section:'{0}' Name:'{1}'", nameSection, nameParameter ) );
            }
        }

        #endregion

    }
}
