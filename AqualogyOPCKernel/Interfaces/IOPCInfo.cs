﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    public enum eInfoType
    {
        Error,
        Message,
        Debug,
        God
    };

    public interface IOPCInfo
    {
        void Info( String message, eInfoType infoType = eInfoType.Message );
        void Info( Exception exception );
    }
}
