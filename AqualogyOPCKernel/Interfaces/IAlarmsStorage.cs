﻿using AqualogyOPCKernel.Model;
using AqualogyOPCKernel.OPCUAClient;
using System.Collections.Generic;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IAlarmsStorage
    {
        void ProcessNewValues( IEnumerable<TagDataValue> newValues );
        IReadOnlyCollection<TagAlarmInfo> DetectAlarms();
    }
}
