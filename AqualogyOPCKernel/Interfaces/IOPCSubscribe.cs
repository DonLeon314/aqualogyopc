﻿using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IOPCSubscribe
    {
        void Subscribe( IOPCUAServerInfo serverInfo, IEnumerable<OPCTag> tag );
    }
}
