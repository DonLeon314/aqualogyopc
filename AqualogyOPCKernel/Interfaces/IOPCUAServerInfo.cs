﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IOPCUAServerInfo
    {
        String HostUrl
        {
            get;
        }

        uint Timeout
        {
            get;
        }

        Int32 AlarmDetacted
        {
            get;
        }

        Int32 PublishingInterval
        {
            get;
        }

        Int32 SamplingInterval
        {
            get;
        }

        uint QueueSize
        {
            get;
        }
    }
}
