﻿using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IOPCUnsubscribe
    {
        void Unsubscribe( IOPCUAServerInfo serverInfo, IEnumerable<OPCTag> tag );
    }
}
