﻿using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IAlarmSaver
    {
        Boolean Save( IEnumerable<TagAlarmInfo> alarmtags, IMarkNeedSendAlarms iMarkNeedSendAlarms );
    }
}
