﻿using AqualogyOPCKernel.Model;
using System;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IMarkNeedSendAlarms
    {
        void MarkNeedSend( OPCTag tag, Boolean needSend );
    }
}
