﻿using AqualogyOPCKernel.Model;
using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface ITags
    {
        OPCTag FindTag( OPCTagUniqueKey tagKey );
    }
}
