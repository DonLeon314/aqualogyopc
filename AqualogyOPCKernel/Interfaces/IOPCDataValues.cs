﻿using AqualogyOPCKernel.OPCUAClient;
using System.Collections.Generic;


namespace AqualogyOPCKernel.Interfaces
{
    internal interface IOPCDataValues
    {
        void AddDataValues( IEnumerable<TagDataValue> values );
    }
}
