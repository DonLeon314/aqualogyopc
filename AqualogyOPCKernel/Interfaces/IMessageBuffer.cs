﻿using AqualogyOPCKernel.MessageBuffer;
using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;

namespace AqualogyOPCKernel.Interfaces
{
    internal interface IMessageBuffer
    {
        /// <summary>
        /// Buffer empty?
        /// </summary>
        Boolean IsEmpty
        {
            get;
        }

        /// <summary>
        /// Save messages to buffer
        /// </summary>
        /// <param name="newMessages"></param>
        void SaveToBuffer( IEnumerable<TagAlarmInfo> newMessages );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iTags"></param>
        /// <returns></returns>
        IEnumerable<TagAlarmInfo> Messages( ITags iTags );

        /// <summary>
        /// Clear buffer
        /// </summary>
        void Clear();
    }
}
