﻿using AqualogyOPCKernel.Model;
using OPCConfigModel;
using System;


namespace AqualogyOPCKernel.Interfaces
{
    //TODO: Future
    public interface IStorageInfos
    {
        OPCUser GetUser( OPCUserKey keyUser, OPCConfigContext contextDB );

    }
}
