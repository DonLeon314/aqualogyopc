﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Interfaces
{
    public interface IOPCInfoProcess
    {
        Boolean ProcessInfo( eInfoType type );
        Int32 MaxLogFile();
    }
}
