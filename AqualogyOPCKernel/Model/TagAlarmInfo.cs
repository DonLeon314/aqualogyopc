﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class TagAlarmInfo
    {
        public OPCTag Tag
        {
            get;
            private set;
        }

        public DateTime AlarmTime
        {
            get;
            private set;
        }

        public DateTime TmeCheckWorkTime
        {
            get;
            private set;
        }

        public TagAlarmInfo( OPCTag tag, DateTime alarmTime, DateTime tmeCheckWorkTime )
        {
            this.AlarmTime = alarmTime;
            this.Tag = tag;
            this.TmeCheckWorkTime = tmeCheckWorkTime;
        }
    }
}
