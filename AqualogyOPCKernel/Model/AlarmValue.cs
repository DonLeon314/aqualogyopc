﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class AlarmValue
    {
        public Boolean Status
        {
            get;
            private set;
        }

        public DateTime Time
        {
            get;
            private set;
        }

        public AlarmValue( Boolean status, DateTime time )
        {
            this.Status = status;
            this.Time = time;
        }
    }
}
