﻿using AqualogyOPCKernel.Model.Collections;
using System;
using System.Collections.Generic;
using System.Linq;


namespace AqualogyOPCKernel.Model
{
    public class OPCHotGroup
    {

        private BBDDIdsCollection _UserIds = new BBDDIdsCollection();

        public Int32 Id
        {
            get;
            private set;
        }

        public String Description
        {
            get;
            private set;
        }

        public IReadOnlyCollection<Int32> UserIds
        {
            get
            {
                return this._UserIds.Ids;   
            }
        }

        private OPCHotGroup()
        {
        }

        public static OPCHotGroup CreateFromBBDD( OPCConfigModel.Model.Group bbddGroup )
        {
            OPCHotGroup result = new OPCHotGroup()
            {
                Id = bbddGroup.Id,
                Description = bbddGroup.Name,
            };

            result._UserIds.AddRange( bbddGroup.Users.Select( u => u.Id ) );

            return result;
        }
    }
}
