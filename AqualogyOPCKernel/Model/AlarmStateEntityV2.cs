﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    /// <summary>
    /// Alarm state entity
    /// </summary>
    internal class AlarmStateEntityV2
    {
        /// <summary>
        /// Alarm time
        /// </summary>
        public DateTime AlarmTime
        {
            get;
            private set;
        }

        /// <summary>
        /// id entity
        /// </summary>
        public Int64 id
        {
            get;
            private set;
        }

        /// <summary>
        /// Alarm sate
        /// </summary>
        public Boolean AlarmState
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean LastValue
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean NeedSend
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configurationId"></param>
        /// <param name="tagId"></param>
        /// <param name="alarmTime"></param>
        /// <param name="alarmState"></param>
        public AlarmStateEntityV2( Int64 id, DateTime alarmTime, Boolean lastValue, Boolean alarmState, Boolean needSend )
        {
            this.id = id;
            this.AlarmTime = alarmTime;
            this.AlarmState = alarmState;
            this.LastValue = lastValue;
            this.NeedSend = needSend;
        }
    }
}
