﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model.Utils
{
    /// <summary>
    /// OPC Tag unique key
    /// </summary>
    public class OPCTagUniqueKey
    {
        /// <summary>
        /// Tag ID
        /// </summary>
        public Int32 Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Configuration ID
        /// </summary>
        public Int32 ConfigId
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Tag ID</param>
        /// <param name="configId">Configuration ID</param>
        public OPCTagUniqueKey( Int32 id, Int32 configId )
        {
            this.Id = id;
            this.ConfigId = configId;
        }
    }
}
