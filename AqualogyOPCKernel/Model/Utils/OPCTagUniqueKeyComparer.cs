﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model.Utils
{
    public class OPCTagUniqueKeyComparer:
                IEqualityComparer<OPCTagUniqueKey>
    {
        /// <summary>
        /// Intervace IEqualityComparer<OPCTagUniqueKey>
        /// </summary>
        /// <param name="leftItem"></param>
        /// <param name="rightItem"></param>
        /// <returns></returns>
        public Boolean Equals( OPCTagUniqueKey leftItem, OPCTagUniqueKey rightItem )
        {
            if( leftItem == null && rightItem == null )
            {
                return true;
            }
            else
            {
                if( leftItem == null || rightItem == null )
                {
                    return false;
                }
                else
                {
                    if( leftItem.Id == rightItem.Id && leftItem.ConfigId == rightItem.ConfigId )
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// IEqualityComparer<OPCTagUniqueKey>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int GetHashCode( OPCTagUniqueKey item )
        {
            
            Int32 hCode = item.Id ^ item.ConfigId;

            return hCode.GetHashCode();
        }
    }
}
