﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class OPCWorkTime
    {
        public String Name
        {
            get;
            private set;
        }

        public TimeSpan StartTime
        {
            get;
            private set;
        }

        public TimeSpan EndTime
        {
            get;
            private set;
        }

        public Int32 Id
        {
            get;
            private set;
        }

        public String WeekDays
        {
            get;
            private set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public OPCWorkTime()
        {

        }

        /// <summary>
        /// Create from BBDD
        /// </summary>
        /// <param name="workTime">BBDD entity</param>
        /// <returns></returns>
        public static OPCWorkTime CreateFromBBDD( WorkTime workTime )
        {
            
            if( workTime != null )
            {
                return new OPCWorkTime()
                {
                    Id = workTime.Id,
                    Name = workTime.Name,
                    StartTime = workTime.StartTime,
                    EndTime = workTime.EndTime,
                    WeekDays = workTime.WeekDays
                };
            }
            
            return null;
        }
    }
}
