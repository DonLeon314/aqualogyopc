﻿using AqualogyOPCKernel.Model.Collections;
using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class OPCTag
    {
        /// <summary>
        /// 
        /// </summary>
        private OPCUserKeyColection _Users = new OPCUserKeyColection();
        
        private BBDDIdsCollection _GroupIds = new BBDDIdsCollection();

        public IEnumerable<OPCUser> Users1
        {
            get
            {
                return this._Users;
            }
        }

        public OPCTagUniqueKey UniqueKey
        {
            get;
            private set;
        }

        public String Client
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String TagId
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String LargeMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ShortMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tagId"></param>
        public OPCTag( Int32 id, Int32 configId, String tagId, String largeMessage, String shortMessage, String client )
        {
            this.UniqueKey = new OPCTagUniqueKey( id, configId );            
            this.TagId = tagId;

            this.LargeMessage = largeMessage;
            this.ShortMessage = shortMessage;

            this.Client = client;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configId"></param>
        /// <param name="tagId"></param>
        /// <param name="largeMessage"></param>
        /// <param name="shortMessage"></param>
        /// <param name="client"></param>
        public OPCTag( OPCTagUniqueKey uniqueKey, String tagId, String largeMessage, String shortMessage, String client )
        {
            this.UniqueKey = uniqueKey;
            this.TagId = tagId;

            this.LargeMessage = largeMessage;
            this.ShortMessage = shortMessage;

            this.Client = client;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Boolean ContainsUser( Int32 userId )
        {
            return ( this._Users.Contains( userId ) );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newUser"></param>
        public void AddUser1( OPCUser newUser )
        {
            if( false == this._Users.Contains( newUser.Id ) )
            {
                this._Users.Add( newUser );
            }
        }

        public void AddGroupId( Int32 idGroup )
        {
            this._GroupIds.Add( idGroup );
        }

        public IEnumerable<Int32> GroupIds
        {
            get
            {
                return this._GroupIds.Ids;
            }
        }
    }
}
