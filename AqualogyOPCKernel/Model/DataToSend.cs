﻿using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class DataToSend
    {
        private HashSet<String> _Phones = new HashSet<String>();
        private HashSet<String> _EMails = new HashSet<String>();

        public Boolean SMSChannelEnable
        {
            get;
            set;
        }

        public OPCTag Tag
        {
            get;
            private set;
        }

        public DateTime AlarmTime
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String LargeMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ShortMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<String> Phones
        {
            get
            {
                return this._Phones;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<String> EMails
        {
            get
            {
                return this._EMails;
            }
        }

        public String Client
        {
            get;
            private set;
        }

        public DataToSend( String largeMessage, String shortMessage, String client, DateTime time, OPCTag tag )
        {
            this.LargeMessage = largeMessage;
            this.ShortMessage = shortMessage;
            this.Client = client;
            this.AlarmTime = time;
            this.Tag = tag;
            this.SMSChannelEnable = true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        public void AddPhone( String phone )
        {           
            if( String.IsNullOrWhiteSpace( phone ) )
            {
                return;
            }
            
            phone = phone.Trim();

            if( false == this._Phones.Contains( phone ) )
            {
                this._Phones.Add( phone );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phones"></param>
        public void AddPhones( IEnumerable<String> phones )
        {
            foreach( String phone in phones )
            {
                if( String.IsNullOrWhiteSpace( phone ) )
                {
                    continue;
                }

                String add_phone = phone.Trim();
                if( this._Phones.Contains( add_phone ) == false )
                {
                    this._Phones.Add( add_phone );
                }
            }
        }

        public void AddEMails( IEnumerable<String> emails )
        {
            foreach( String email in emails )
            {
                if( String.IsNullOrWhiteSpace( email ) )
                {
                    continue;
                }

                String add_email= email.Trim();
                if( this._EMails.Contains( add_email ) == false )
                {
                    this._EMails.Add( add_email );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        public void AddEMail( String email )
        {
            if( String.IsNullOrWhiteSpace( email ) )
            {
                return;
            }

            email = email.Trim();

            if( this._EMails.Contains( email ) == false )
            {
                this._EMails.Add( email );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        public String GetPhones( Char separator = ';' ) 
        {
            StringBuilder sb = new StringBuilder();
            
            foreach( String phone in this._Phones )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( separator );
                } 
                sb.Append( phone );
            }

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        public String GetEMails( Char separator = ';' )
        {
            StringBuilder sb = new StringBuilder();

            foreach( String email in this._EMails )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( separator );
                }
                sb.Append( email );
            }

            return sb.ToString();
        }

    } 
}
