﻿using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.OPCUAClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    /// <summary>
    /// Alarm processor
    /// </summary>
    class AlarmProcessor
    {
        public Boolean NeedSend
        {
            get;
            set;
        }

        public DateTime AlarmTime
        {
            get;
            private set;
        }

        public Int64 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Id Alarm processor
        /// </summary>
        public OPCTag Tag
        {
            get;
            private set;
        }

        public TimeSpan TimeDetectedAlarm
        {
            get;
            private set;
        }

        public Boolean LastValue
        {
            get;
            private set;
        }

        public Boolean State
        {
            get;
            private set;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="history"></param>
        public AlarmProcessor( Int64 id, OPCTag tag, TimeSpan timeDetectedAlarm )
        {
            this.Id = id;
            this.Tag = tag;
            this.TimeDetectedAlarm = timeDetectedAlarm;
                        

            this.State = false;
            this.LastValue = false;
            this.AlarmTime = DateTime.Now;

            this.NeedSend = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="timeDetectedAlarm"></param>
        /// <param name="state"></param>
        /// <param name="time"></param>
        public AlarmProcessor( Int64 id, OPCTag tag, TimeSpan timeDetectedAlarm, Boolean lastValue, Boolean state, DateTime time, Boolean needSend )
        {
            this.Id = id;
            this.Tag = tag;
            this.TimeDetectedAlarm = timeDetectedAlarm;


            this.State = state;
            this.LastValue = lastValue;
            this.AlarmTime = time;

            this.NeedSend = needSend;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean IsNewAlarm( IOPCInfo iOPCInfo )
        {
            DateTime time = DateTime.Now;

            var dt = time - this.AlarmTime;
            
            iOPCInfo.Info( String.Format( "Item:{0} State: {1}", this.Tag.TagId, this.State ), eInfoType.Debug );

            if( dt >= this.TimeDetectedAlarm )
            {
                // Alarm ON
                if( this.State == false && this.LastValue == true )
                {
                    iOPCInfo.Info( String.Format("Item:{0} State: Alarm ON {1}", this.Tag.TagId, DateTime.Now ), eInfoType.Debug );
                    this.State = true;
                    this.NeedSend = false;
                    return true;
                }

                // Alarm OFF
                if( this.State == true && this.LastValue == false )
                {
                    iOPCInfo.Info( String.Format( "Item:{0} State: Alarm OFF {1}", this.Tag.TagId, DateTime.Now ), eInfoType.Debug );
                    this.State = false;
                    this.NeedSend = false;
                    return false;
                }
            }

            if( this.NeedSend )
            {
                iOPCInfo.Info( String.Format( "Item:{0} Need resend", this.Tag.TagId ), eInfoType.Debug );
            }

            return this.NeedSend;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="time"></param>
        public void AddValue( Boolean newValue, DateTime time, IOPCInfo iOPCInfo )
        {            
            if( newValue != this.LastValue )
            {
                iOPCInfo.Info( String.Format( "Item:{0} Change Last value - State:{1} Last Value:{2} -> New Value:{3} time:{4}", this.Tag.TagId, this.State, this.LastValue, newValue, time ), eInfoType.Debug );
                this.AlarmTime = time;
                this.LastValue = newValue;
                this.NeedSend = false;
            }
            else
            {
                iOPCInfo.Info( String.Format( "Item:{0} No change Last value - State:{1} Last value:{2} time:{3}", this.Tag.TagId, this.State, this.LastValue, time ), eInfoType.Debug );
            }
        }
    }
}
