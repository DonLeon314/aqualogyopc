﻿using Opc.Ua;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class NodeIdEx:
        NodeId
    {
        public Int32 IdTag
        {
            get;
            private set;
        }
        public NodeIdEx( Int32 id, string text ) :
                base( text )
        {
            this.IdTag = id;
        }
    }
}
