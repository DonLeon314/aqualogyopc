﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    public class OPCUser
    {

        public Int32 Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Alias 
        /// </summary>
        public String Alias
        {
            get;
            private set;
        }

        /// <summary>
        /// First name
        /// </summary>
        public String FirstName
        {
            get;
            private set;
        }

        /// <summary>
        /// Second name
        /// </summary>
        public String SecondName
        {
            get;
            private set;
        }

        /// <summary>
        /// Phones
        /// </summary>
        public IReadOnlyCollection<String> Phones
        {
            get;
            private set;
        }

        /// <summary>
        /// EMails
        /// </summary>
        public IReadOnlyCollection<String> EMails
        {
            get;
            private set;
        }

        public User.eChannel Channel
        {
            get;
            private set;
        }


        private User.eTimeEvent _TimeOfEvents = User.eTimeEvent.eAllTime;

        private OPCWorkTimeKeyCollection _WorkTimes = new OPCWorkTimeKeyCollection();

        /// <summary>
        /// Need send 
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public Boolean NeedSend( DateTime time )
        {
            // Always
//             if( this._TimeOfEvents == User.eTimeEvent.eAllTime )
//             {
//                 return true;
//             }

            Boolean time_inside = false;

            foreach( var wt in this._WorkTimes )
            {
                if( time.IsWithin2( wt.StartTime, wt.EndTime, wt.WeekDays ) )
                {
                    time_inside = true;
                    break;
                }
            }

//             if( this._TimeOfEvents == User.eTimeEvent.eInside )
//             {
//                 return time_inside;
//             }

//            // User.eTimeEvent.eOutside 
            return time_inside;            
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        private OPCUser()
        {            
        }

        /// <summary>
        /// Create OPCUser from BBDD
        /// </summary>
        /// <param name="bbddUser"> BBDD User entity</param>
        /// <returns></returns>
        public static OPCUser CreateFromBBDD(User bbddUser)
        {
            if( bbddUser == null )
            {
                throw new Exception("CreateFromBBDD invalid parameter");
            }

            OPCUser result = new OPCUser();

            result.Id = bbddUser.Id;
            result.Alias = bbddUser.Alias;
            result.FirstName = bbddUser.FirstName;
            result.SecondName = bbddUser.SecondName;
            result.Channel = bbddUser.Channel;

            result.Phones =  bbddUser.Phones.Select( item => item.Value ).ToList();
            result.EMails = bbddUser.EMails.Select( item => item.Value ).ToList();

            // Work time
            result._TimeOfEvents = bbddUser.TimeOfEvents;

            foreach( WorkTime work_time in bbddUser.WorkTimes )
            {
                var add_value = OPCWorkTime.CreateFromBBDD( work_time );

                if( add_value != null )
                {
                    result._WorkTimes.Add( add_value );
                }
            }


            return result;
        }

    }
}
