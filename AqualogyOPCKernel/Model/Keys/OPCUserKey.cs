﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    //TODO: Future

    /// <summary>
    /// User key
    /// </summary>
    public class OPCUserKey:
            IComparable<OPCUserKey>
    {
        /// <summary>
        /// BBDD id entity
        /// </summary>
        public Int32 Id
        {
            get;
            private set;
        }

        int IComparable<OPCUserKey>.CompareTo( OPCUserKey obj )
        {            
            return this.Id.CompareTo( obj.Id );            
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bbddId"></param>
        public OPCUserKey( Int32 id )
        {
            this.Id = id;
        }
    }
}
