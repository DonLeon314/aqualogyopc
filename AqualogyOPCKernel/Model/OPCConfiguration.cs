﻿using AqualogyOPCKernel.Config;
using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Model.Utils;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class OPCConfiguration
    {
        private OPCServerKeyCollection _Servers = null;

        /// <summary>
        /// Configuration name
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        public Boolean Active
        {
            get;
            set;
        }

        public Int32 Id
        {
            get;
            private set;
        }

        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        private OPCConfiguration( String name )
        {
            this.Name = name;
        }

        public OPCTag FindTag( OPCTagUniqueKey tagKey )
        {
            foreach( var server in this._Servers )
            {
                OPCTag tag = server.FindTag( tagKey );
                if( tag != null )
                {
                    return tag;
                }
            }
            return null;
        }

        public static OPCConfiguration CreateFromBBDD( OPCConfigContext context, OPCConfigModel.Model.Config configEntity,
                                                       IOPCInfo iOPCInfo, Object syncObject )
        {
            OPCConfiguration config = null;             

            OPCUserKeyColection users = LoadUsers( context, iOPCInfo );
            OPCServerKeyCollection servers = new OPCServerKeyCollection();

            foreach( Group group in configEntity.Groups.ToList() )
            {
                Group g = context.Groups.Include( "OPCTags" ).Include("Users").Where( _g => _g.Id == group.Id ).FirstOrDefault();
                
                foreach( TagName tag in g.OPCTags.ToList() )
                {
                    OPCServer server = null;
                    if( servers.Contains( tag.Server.Trim() ) )
                    {
                        server = servers[tag.Server.Trim()];
                    }
                    else
                    {
                        //TODO: sizeTagPortion
                        server = new OPCServer( tag.Server.Trim(), (uint)configEntity.Timeout, configEntity.AlarmDetected,
                                                configEntity.PublishingInterval, configEntity.SamplingInterval, 
                                                (uint)configEntity.QueueSize );
                        servers.Add( server );
                    }

                    OPCTagUniqueKey tag_unique_key = new OPCTagUniqueKey( tag.Id, configEntity.Id );

                    OPCTag opc_tag = server.FindTag( tag_unique_key );
                    if( opc_tag == null )
                    {
                        opc_tag = new OPCTag( tag_unique_key, tag.TagId.Trim(), tag.LargeMessage, tag.ShortMessage, tag.Client );
                        server.AddTag( opc_tag );
                    }

                    opc_tag.AddGroupId( group.Id );

                    // Add user to tag                    
                    foreach( User user in g.Users.ToList() )
                    {
                        if( false == opc_tag.ContainsUser( user.Id ) )
                        {
                            OPCUser add_user = null;
                            if( users.Contains( user.Id ) )
                            {
                                add_user = users[user.Id];
                            }
                            else
                            {

                            }
                            if( add_user != null )
                            {
                                //opc_tag.AddUser( add_user );
                            }
                        }
                    }
                }
            }

            config = new OPCConfiguration( configEntity.Name ) { _Servers = servers, Active = configEntity.Active, Id = configEntity.Id };

            return config;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static OPCUserKeyColection LoadUsers( OPCConfigContext context, IOPCInfo iOPCInfo )
        {
            OPCUserKeyColection result = new OPCUserKeyColection();

            foreach( User user in context.Users.Include( "Phones" ).Include( "EMails" ).Include("WorkTimes").ToList() )
            {
                OPCUser add_value = OPCUser.CreateFromBBDD( user );
                if( add_value != null )
                {
                    result.Add( add_value );

                    iOPCInfo.Info(String.Format("Load user - Alias:{0} Channel:{1}", user.Alias, user.Channel), eInfoType.Debug);
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Subscribe( IOPCSubscribe iOPCSubscribe )
        {            
            foreach( OPCServer server in this._Servers )
            {
                server.Subscribe( iOPCSubscribe );
            }
        }

        public void Unsubscribe( IOPCUnsubscribe iOPCUnsubscribe )
        {
            foreach( OPCServer server in this._Servers )
            {
                server.Unsubscribe( iOPCUnsubscribe );
            }
        }
    }
}
