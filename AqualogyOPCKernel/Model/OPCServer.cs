﻿using AqualogyOPCKernel.Interfaces;
using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Configuration;
using AqualogyOPCKernel.Model.Utils;

namespace AqualogyOPCKernel.Model
{
    internal class OPCServer:
                IOPCUAServerInfo
    {
        private OPCTagKeyCollection _Tags = new OPCTagKeyCollection();

        
        /// <summary>
        /// 
        /// </summary>
        public String Host
        {
            get;
            private set;
        }


        private Int32 _AlarmDetacted = 0;
        private Int32 _PublishingInterval = 0;
        private Int32 _SamplingInterval = 0;
        private uint _QueueSize = 1;
        private uint _Timeout = 60;

        #region interface IOPCUAServerInfo
        uint IOPCUAServerInfo.Timeout
        {
            get
            {
                return this._Timeout;
            }
        }

        String IOPCUAServerInfo.HostUrl
        {
            get
            {
                return this.Host;
            }
        }

        Int32 IOPCUAServerInfo.AlarmDetacted
        {
            get
            {
                return this._AlarmDetacted;
            }
        }

        Int32 IOPCUAServerInfo.PublishingInterval
        {
            get
            {
                return this._PublishingInterval;
            }
        }

        Int32 IOPCUAServerInfo.SamplingInterval
        {
            get
            {
                return this._SamplingInterval;
            }
        }

        uint IOPCUAServerInfo.QueueSize
        {
            get
            {
                return this._QueueSize;
            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        public OPCServer( String host, uint timeout, Int32 alarmDetacted, Int32 publishingInterval, Int32 samplingInterval, uint queueSize )
        {
            this.Host = host;

            this._Timeout = timeout;
            this._AlarmDetacted = alarmDetacted;
            this._PublishingInterval = publishingInterval;
            this._SamplingInterval = samplingInterval;
            this._QueueSize = queueSize;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Subscribe( IOPCSubscribe iOPCSubscribe )
        {
            iOPCSubscribe.Subscribe( this, this._Tags );
        }
                
        /// <summary>
        /// 
        /// </summary>
        public void Unsubscribe( IOPCUnsubscribe iOPCUnsubscribe )
        {
            iOPCUnsubscribe.Unsubscribe( this, this._Tags );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public OPCTag FindTag( OPCTagUniqueKey tagUniqueKey )
        {
            if( this._Tags.Contains( tagUniqueKey ) )
            {
                return this._Tags[tagUniqueKey];
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newTag"></param>
        public void AddTag( OPCTag newTag )
        {
            if( this._Tags.Contains( newTag.UniqueKey ) == false )
            {
                this._Tags.Add( newTag );
            }
        }  
    }
}
