﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    /// <summary>
    /// Alarm state entity
    /// </summary>
    internal class AlarmStateEntity
    {
        /// <summary>
        /// Alarm time
        /// </summary>
        public DateTime AlarmTime
        {
            get;
            private set;
        }

        /// <summary>
        /// id entity
        /// </summary>
        public Int64 id
        {
            get;
            private set;
        }

        /// <summary>
        /// Configuration id
        /// </summary>
        public Int32 ConfigurationId
        {
            get;
            private set;
        }

        /// <summary>
        /// Tag id
        /// </summary>
        public Int32 TagId
        {
            get;
            private set;
        }

        /// <summary>
        /// Alarm sate
        /// </summary>
        public Boolean AlarmState
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean LastValue
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configurationId"></param>
        /// <param name="tagId"></param>
        /// <param name="alarmTime"></param>
        /// <param name="alarmState"></param>
        public AlarmStateEntity( Int64 id, Int32 configurationId, Int32 tagId, DateTime alarmTime, Boolean lastValue, Boolean alarmState )
        {
            this.id = id;
            this.ConfigurationId = configurationId;
            this.TagId = tagId;
            this.AlarmTime = alarmTime;
            this.AlarmState = alarmState;
            this.LastValue = lastValue;
        }
    }
}
