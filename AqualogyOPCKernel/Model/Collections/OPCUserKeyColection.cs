﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class OPCUserKeyColection:
        KeyedCollection<Int32,OPCUser>
    {
        /// <summary>
        /// 
        /// </summary>
        public OPCUserKeyColection()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Int32 GetKeyForItem( OPCUser item )
        {
            return item.Id;
        }
    }
}
