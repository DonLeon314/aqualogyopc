﻿using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Model.Utils;
using AqualogyOPCKernel.OPCUAClient;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace AqualogyOPCKernel.Model.Collections
{
    internal class AlarmProcessorsStorage
    {
        private AlarmProcessorKeyColection _Collection = new AlarmProcessorKeyColection();
        private SQLiteConnection _SQLiteConnection = null;
        private DateTime _TimeStart = DateTime.Now;
        private static String _AlarmStatesTableName = "AlarmStates";
        private Int32 _ActualState = 10;
        private IOPCInfo _IOPCInfo = null;

        private Boolean _CheckTable = true;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public AlarmProcessorsStorage( SQLiteConnection connection, Int32 actualState, IOPCInfo iOPCInfo )
        {
            this._SQLiteConnection = connection;
            this._ActualState = actualState;
            this._IOPCInfo = iOPCInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="detectedTime"></param>
        /// <returns></returns>
        private AlarmProcessor GetProcessor( OPCTag tag, TimeSpan detectedTime )
        {
            AlarmProcessor result = null;

            if( this._Collection.Contains( tag.UniqueKey ) )
            {
                result = this._Collection[tag.UniqueKey];
                this._IOPCInfo.Info( String.Format( "Item:{0} State:{1} - found in memory", tag.TagId, result.State ), eInfoType.Debug );
                return result;
            }

            // Find en SQLite
            result = FindInSQLiteBBDD( tag, detectedTime );

            if( result == null )
            {
                this._IOPCInfo.Info( String.Format( "Item:{0} State:false - create new", tag.TagId ), eInfoType.Debug );
                result = new AlarmProcessor( 0, tag, detectedTime );
            }

            if( result != null )
            {
                this._Collection.Add( result );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="detectedTime"></param>
        /// <returns></returns>
        private AlarmProcessor FindInSQLiteBBDD( OPCTag tag, TimeSpan detectedTime )
        {
            AlarmProcessor result = null;

            if( SQLiteEx.SQLiteEx.TableExist( _SQLiteConnection, _AlarmStatesTableName ) )
            {
                Int64 id = FindIDAlarmState( tag.UniqueKey );
                
                if( id > 0 )
                {  // state existe                    
                    AlarmStateEntity ase = Load( id );
                    if( ase != null )
                    {
                        TimeSpan delta = DateTime.Now - ase.AlarmTime;
                        if( delta.Minutes < this._ActualState )
                        {
                            this._IOPCInfo.Info( String.Format( "Item:{0} State:{1} - Restore from db3", tag.TagId, ase.AlarmState ), eInfoType.Debug );
                            result = new AlarmProcessor( id, tag, detectedTime, ase.LastValue, ase.AlarmState, ase.AlarmTime, false ); //TODO
                        }
                        else
                        {
                            this._IOPCInfo.Info( String.Format( "Item:{0} State:false - Out of time, create new", tag.TagId ), eInfoType.Debug );
                            result = new AlarmProcessor( id, tag, detectedTime );
                        }                    
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Load Alarm state
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private AlarmStateEntity Load( Int64 id )
        {
            AlarmStateEntity result = null;

            if( id == 0 || id < 1 )
            {
                return result;
            }                                      //     0         1          2           3                 4
            String sql_query = String.Format( "SELECT [ConfigId], [TagId], [AlarmTime], [AlarmState], [AlarmLastValue] FROM [{0}]" +
                                                      "WHERE [id] = :pID ", _AlarmStatesTableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                cmd.Parameters.Add( new SQLiteParameter( "pID", (Object) id ) );
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        Int32 config_id = decimal.ToInt32( reader.GetDecimal( 0 ) );
                        Int32 tag_id = decimal.ToInt32( reader.GetDecimal( 1 ) );
                        DateTime alarm_time = reader.GetDateTime( 2 );

                        // state
                        Int32 value = decimal.ToInt32( reader.GetDecimal( 3 ) );
                        Boolean alarm_state = ( value != 0 );
                        
                        // last value
                        value = decimal.ToInt32( reader.GetDecimal( 4 ) );
                        Boolean alarm_last_value = ( value != 0 );

                        result = new AlarmStateEntity( id, config_id, tag_id, alarm_time, alarm_last_value, alarm_state );
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Create table
        /// </summary>
        private void CreateTableAlarmStates()
        {
            String sql_query = String.Format( @"CREATE TABLE [{0}] (" +
                                                    "[id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                                    "[AlarmTime] [datetime] NOT NULL, " +
                                                    "[AlarmState] [int] NOT NULL, " +
                                                    "[AlarmLastValue] [int] NOT NULL, " +
                                                    "[TagId] [int] NOT NULL, " +
                                                    "[ConfigId] [int] NOT NULL )", _AlarmStatesTableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                Int32 exec_result = cmd.ExecuteNonQuery();
            }
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagUniqueKey"></param>
        /// <returns></returns>
        private Int64 FindIDAlarmState( OPCTagUniqueKey tagUniqueKey )
        {
            return FindIDAlarmState( tagUniqueKey.ConfigId, tagUniqueKey.Id );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Int64 FindIDAlarmState( Int32 configId, Int32 id )
        {
            String sql_query = String.Format( "SELECT [id] FROM [{0}] WHERE [ConfigId] = :pConfigId AND [TagId] = :pTagId", _AlarmStatesTableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                cmd.Parameters.Add( new SQLiteParameter("pConfigId", (Object)configId ) );
                cmd.Parameters.Add( new SQLiteParameter( "pTagId", (Object) id ) );
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        return decimal.ToInt64( reader.GetDecimal( 0 ) );
                    }
                }
            }

            return 0;
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<TagAlarmInfo> DetectAlarmas()
        {
            List<TagAlarmInfo> result = null;

            foreach( AlarmProcessor ap in this._Collection )
            {
                if( ap.IsNewAlarm( this._IOPCInfo ) )
                {                    
                    if( result == null )
                    {
                        result = new List<TagAlarmInfo>();
                    }

                    result.Add( new TagAlarmInfo( ap.Tag, ap.AlarmTime, ap.AlarmTime ) );
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarmProcessor"></param>
        private void Save( AlarmProcessor alarmProcessor )
        {

            if( this._CheckTable )
            {
                if( false == SQLiteEx.SQLiteEx.TableExist( this._SQLiteConnection, _AlarmStatesTableName ) )
                {
                    CreateTableAlarmStates();
                }
                this._CheckTable = false;
            }

            String sql_query;
            if( alarmProcessor.Id == 0 )
            {
                sql_query = String.Format( "INSERT INTO [{0}] ([AlarmTime], [AlarmState], [AlarmLastValue], [TagId], [ConfigId]) "+
                                                    " VALUES ( :pAlarmTime, :pAlarmState, :pAlarmLastValue, :pTagId, :pConfigId ); "+
                                                    "SELECT last_insert_rowid();", _AlarmStatesTableName );
            }
            else
            {
                sql_query = String.Format( "UPDATE [{0}] SET [AlarmTime] = :pAlarmTime, [AlarmState] =  :pAlarmState, "+
                                                            "[AlarmLastValue] = :pAlarmLastValue, [TagId] = :pTagId, "+
                                                            "[ConfigId] = :pConfigId WHERE [Id] = :pId",
                                           _AlarmStatesTableName);
            }

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                cmd.Parameters.Add( new SQLiteParameter( "pAlarmTime", (Object)alarmProcessor.AlarmTime ) );
                cmd.Parameters.Add( new SQLiteParameter( "pAlarmState", (Object) (alarmProcessor.State? 1 : 0 ) ) );
                cmd.Parameters.Add( new SQLiteParameter( "pAlarmLastValue", (Object) ( alarmProcessor.LastValue ? 1 : 0 ) ) );
                cmd.Parameters.Add( new SQLiteParameter( "pTagId", (Object)alarmProcessor.Tag.UniqueKey.Id ) );
                cmd.Parameters.Add( new SQLiteParameter( "pConfigId", (Object) alarmProcessor.Tag.UniqueKey.ConfigId ) );
                if( alarmProcessor.Id != 0 )
                {
                    cmd.Parameters.Add( new SQLiteParameter( "pId", (Object) alarmProcessor.Id ) );
                }

                Object execute_result = cmd.ExecuteScalar();

                if( alarmProcessor.Id == 0 && execute_result is Int64 )
                {
                    Int64 id = (Int64) execute_result;
                    alarmProcessor.Id = id;
                }
            }
        }

        /// <summary>
        /// Process new values
        /// </summary>
        /// <param name="newValues"></param>
        public void ProcessNewValues( IEnumerable<TagDataValue> newValues )
        {
            //newValues = null;

            foreach( TagDataValue tdv in newValues )
            {
                AlarmProcessor processor = null;
                processor = GetProcessor( tdv.Tag, TimeSpan.FromSeconds( tdv.AlarmDetectedTime ) );

                if( processor == null )
                {
                    processor = new AlarmProcessor( 0, tdv.Tag, TimeSpan.FromSeconds( tdv.AlarmDetectedTime ) );
                    this._Collection.Add( processor );
                }

                //TODO::!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if DEBUG
                Int32 raw_v = (Int32) tdv.Value.Value;
                Boolean value = ( raw_v < 30 );
#endif

                    if(
#if DEBUG
                        true   // warning CS0162: Unreachable code detected - OK!!!!!!!!!!!!!!!!!!!!!!!           
#else
                        tdv.Value.Value is Boolean
#endif
                       )
                    {
                        processor.AddValue(
#if DEBUG
                                                value,
#else                                                
                                                (Boolean) tdv.Value.Value,
#endif
                                                tdv.Value.ServerTimestamp.ToLocalTime(), this._IOPCInfo );
                        Save( processor );
                    }
                    else
                    {
                         throw new Exception( String.Format( "Item:{0} value no Boolean Value{1} Type{2}",
                                                        tdv.Tag.TagId, tdv.Value.Value.ToString(),
                                                        ( tdv.Value.Value != null ? tdv.Value.Value.GetType().ToString() : "UNKNOWN(null)" ) ) );
                    }                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TagAlarmInfo> DetectAlarms()
        {
            List<TagAlarmInfo> result = null;

            foreach( AlarmProcessor ap in this._Collection )
            {
                if( ap.IsNewAlarm( this._IOPCInfo ) )
                {
                    this._IOPCInfo.Info( String.Format("Tag:{0} - ALarm!", ap.Tag.TagId ), eInfoType.Debug );
                    if( result == null )
                    {
                        result = new List<TagAlarmInfo>();
                    }
                    result.Add( new TagAlarmInfo( ap.Tag, ap.AlarmTime, ap.AlarmTime ) );
                }
                else
                {
                    this._IOPCInfo.Info( String.Format( "Tag:{0} - No aLarm", ap.Tag.TagId ), eInfoType.Debug );
                }
            }

            return result;
        }
    }
}
