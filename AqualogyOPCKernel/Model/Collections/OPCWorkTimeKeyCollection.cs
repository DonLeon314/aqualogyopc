﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class OPCWorkTimeKeyCollection:
            KeyedCollection<Int32,OPCWorkTime>
    {

        public OPCWorkTimeKeyCollection()
        {

        }

        protected override Int32 GetKeyForItem( OPCWorkTime item )
        {
            return item.Id;
        }
    }
}
