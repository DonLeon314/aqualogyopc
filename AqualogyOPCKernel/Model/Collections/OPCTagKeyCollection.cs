﻿using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.ObjectModel;
 
namespace AqualogyOPCKernel.Model
{
    /// <summary>
    /// 
    /// </summary>
    class OPCTagKeyCollection:
            KeyedCollection<OPCTagUniqueKey, OPCTag>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OPCTagKeyCollection():
               base( new OPCTagUniqueKeyComparer() ) 
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override OPCTagUniqueKey GetKeyForItem( OPCTag item )
        {
            return item.UniqueKey;
        }
    }
}
