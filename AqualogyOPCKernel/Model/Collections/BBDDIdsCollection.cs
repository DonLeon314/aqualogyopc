﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model.Collections
{
    /// <summary>
    /// Collection for BBDD ID
    /// </summary>
    public class BBDDIdsCollection
    {
        /// <summary>
        /// Collection
        /// </summary>
        protected class BBDDIdsKeyedCollection:
                KeyedCollection<Int32,Int32>
        {
            public BBDDIdsKeyedCollection()
            {
            }

            protected override Int32 GetKeyForItem( Int32 item )
            {
                return item;
            }
        }

        protected BBDDIdsKeyedCollection _Collection = new BBDDIdsKeyedCollection();

        /// <summary>
        /// Default constructor
        /// </summary>
        public BBDDIdsCollection()
        {

        }

        /// <summary>
        /// Add new id 
        /// </summary>
        /// <param name="newId"></param>
        public void Add( Int32 newId )
        {
            if( this._Collection.Contains( newId ) == false )
            {
                this._Collection.Add( newId );
            }
        }

        /// <summary>
        /// Add news ids
        /// </summary>
        /// <param name="newValues"></param>
        public void AddRange( IEnumerable<Int32> newValues )
        {
            foreach( var item in newValues )
            {
                Add( item );
            }
        }

        /// <summary>
        /// Id contains
        /// </summary>
        /// <param name="findId"></param>
        /// <returns>true or false</returns>
        public Boolean Contains( Int32 findId )
        {
            return this._Collection.Contains( findId );
        }

        /// <summary>
        /// Ids
        /// </summary>
        public IReadOnlyCollection<Int32> Ids
        {
            get
            {
                return this._Collection;
            }
        }
    }
}
