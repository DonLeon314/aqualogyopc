﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    internal class OPCServerKeyCollection:
        KeyedCollection<String, OPCServer>
    {
        public OPCServerKeyCollection()
        {

        }

        protected override String GetKeyForItem( OPCServer item )
        {
            return item.Host;
        }               
    }
}
