﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Model
{
    class OPCConfigurationKeyCollection:
        KeyedCollection<String,OPCConfiguration>
    {
        public OPCConfigurationKeyCollection() :
                base( StringComparer.CurrentCultureIgnoreCase )
        {

        } 
        protected override String GetKeyForItem( OPCConfiguration item )
        {
            return item.Name;
        }
    }
}
