﻿using AqualogyOPCKernel.Model.Utils;
using System.Collections.ObjectModel;


namespace AqualogyOPCKernel.Model
{
    internal class AlarmProcessorKeyColection:
        KeyedCollection<OPCTagUniqueKey, AlarmProcessor>
    {
        public AlarmProcessorKeyColection()
        {

        }

        protected override OPCTagUniqueKey GetKeyForItem( AlarmProcessor item )
        {
            return item.Tag.UniqueKey;
        }
    }
}
