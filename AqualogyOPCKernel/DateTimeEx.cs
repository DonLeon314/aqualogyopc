﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel
{
    /// <summary>
    /// DateTime extension
    /// </summary>
    public static class DateTimeEx
    {
        /// <summary>
        ///  DayOfWeek to Espanol letra
        /// </summary>
        private static Dictionary<DayOfWeek, String> _EspanolDayOfWeekLetras = new Dictionary<DayOfWeek, String>()
        {
            {  DayOfWeek.Sunday, "D" },
            {  DayOfWeek.Monday, "L" },
            {  DayOfWeek.Tuesday, "M" },
            {  DayOfWeek.Wednesday, "X" },
            {  DayOfWeek.Thursday, "J" },
            {  DayOfWeek.Friday, "V" },
            {  DayOfWeek.Saturday, "S" }
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static String EspanolDayOfWeekLetra( this DateTime obj )
        {
            return _EspanolDayOfWeekLetras[obj.DayOfWeek];
        }

        private static Boolean Between( this DateTime _this, DateTime start, DateTime end, String weekDays )
        {
            String wd = start.EspanolDayOfWeekLetra();

            if( false == weekDays.Contains( wd ) )
            {
                return false;
            }

            return ( start <= _this && end >= _this );
        }

        public static Boolean IsWithin2( this DateTime _this, TimeSpan startTime, TimeSpan endTime, String weekDays )
        {
            weekDays = weekDays.ToUpper();

            DateTime swt = DateTime.Now;
            DateTime ewt = DateTime.Now;

            if( startTime < endTime )
            {
                swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );

                return _this.Between( swt, ewt, weekDays );
            }
            else
            {
                if( startTime == endTime )
                {
                    // -day
                    swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                    ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );
                    if( _this.TimeOfDay <= endTime )
                    {
                        swt = swt.AddDays( -1 );
                        return _this.Between( swt, ewt, weekDays );
                    }
                    else
                    {
                        ewt = ewt.AddDays( 1 );
                        return _this.Between( swt, ewt, weekDays );
                    }
                }
                else
                {   // startTime > endTime
                    swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                    ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );
                    if( _this.TimeOfDay < ewt.TimeOfDay )
                    {
                        swt = swt.AddDays( -1 );
                        return _this.Between( swt, ewt, weekDays );
                    }
                    else
                    {
                        ewt = ewt.AddDays( 1 );
                        return _this.Between( swt, ewt, weekDays );
                    }
                }
            }
        }

        public static Boolean IsWithin( this DateTime _this, TimeSpan startTime, TimeSpan endTime, String weekDays )
        {
            weekDays = weekDays.ToUpper();

            DateTime swt = DateTime.Now;
            DateTime ewt = DateTime.Now;

            if( startTime < endTime)
            {
                swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );
            }

            if( startTime > endTime )
            {
                if (_this.TimeOfDay >= startTime)
                {
                    swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                    ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );
                    swt = ewt.AddDays( 1 );
                }
                if (_this.TimeOfDay <= endTime)
                {
                    swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                    ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );
                    swt = swt.AddDays( -1 );
                }
            }

            if (startTime == endTime)
            {
                swt = new DateTime( _this.Year, _this.Month, _this.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
                ewt = new DateTime( _this.Year, _this.Month, _this.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );

                if (_this.TimeOfDay >= endTime)
                {
                    return weekDays.Contains( _this.EspanolDayOfWeekLetra() );
                }
                else
                {
                   swt = swt.AddDays( -1 );
                }
            }

            String wd = swt.EspanolDayOfWeekLetra();

            if (false == weekDays.Contains( wd ))
            {
                return false;
            }

            return ( swt <= _this && ewt >= _this );
        }

        public static Tuple<DateTime, DateTime> CalcWorkTime( this DateTime t, TimeSpan startTime, TimeSpan endTime )
        {
            DateTime start_wt = new DateTime( t.Year, t.Month, t.Day, startTime.Hours, startTime.Minutes, startTime.Seconds );
            DateTime end_wt = new DateTime( t.Year, t.Month, t.Day, endTime.Hours, endTime.Minutes, endTime.Seconds );

            if (endTime < startTime)
            {
                if (t.TimeOfDay <= endTime)
                {
                    start_wt = start_wt.AddHours( -24.0 );
                }
                else
                {
                    if (t.TimeOfDay >= endTime)
                    {
                        end_wt = end_wt.AddHours( 24.0 );
                    }
                }
            }
            else
            {
                if (endTime == startTime)
                {
                    end_wt = end_wt.AddHours( 24.0 );
                }
            }

            return new Tuple<DateTime,DateTime>( start_wt, end_wt );
        }
    }
}
