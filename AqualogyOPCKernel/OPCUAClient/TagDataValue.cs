﻿using AqualogyOPCKernel.Model;
using Opc.Ua;
using System;


namespace AqualogyOPCKernel.OPCUAClient
{
    internal class TagDataValue
     {
        public OPCTag Tag
        {
            get;
            private set;
        }

        public DataValue Value
        {
            get;
            private set;
        }

        public Int32 AlarmDetectedTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="value"></param>
        public TagDataValue( OPCTag tag, DataValue value, Int32 alarmDetectedTime )
        {
            this.Tag = tag;
            this.Value = value;
            this.AlarmDetectedTime = alarmDetectedTime;
        }
    }
}
