﻿using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Model;
using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace AqualogyOPCKernel.OPCUAClient
{
    internal class OPCUAClient   // Shell
    {
        private Session _Session = null;
        private ApplicationConfiguration _Configuration = null;
        private Subscription _Subscription = null;
        private IOPCDataValues _IOPCDataValues = null;
        private MonitorStorage _Monitors = new MonitorStorage();
        private Object _SyncObject = new Object();
        /// <summary>
        /// 
        /// </summary>
        public Int32 SamplingInterval
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint QueueSize
        {
            get;
            private set;
        }
        /// <summary>
        /// Publishing interval
        /// </summary>
        public Int32 PublishingInterval
        {
            get;
            private set;
        }

        /// <summary>
        /// Url server
        /// </summary>
        public String UrlServer
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Timeout
        {
            get;
            private set;
        }

        private IOPCInfo _IOPCInfo = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="urlServer"></param>
        internal OPCUAClient( String urlServer, IOPCDataValues iOPCDataValues, uint timeout, Int32 publishingInterval,
                                Int32 samplingInterval, uint queuesize, IOPCInfo iOPCInfo )
        {
            this.UrlServer = urlServer;

            this._IOPCInfo = iOPCInfo;


            this.Timeout = ( timeout < 60 ? 60 : timeout );

            this.PublishingInterval = publishingInterval;

            this._IOPCDataValues = iOPCDataValues;

            this.SamplingInterval = samplingInterval;
            this.QueueSize = ( queuesize == 0 ? 1 : queuesize );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public Boolean UnwatchTags( IEnumerable<OPCTag> tags )
        {
            lock (this._SyncObject)
            {

                if (this._Subscription != null )
                {
                    List<MonitoredItemEx> monitors = new List<MonitoredItemEx>();
                    foreach( OPCTag tag in tags )
                    {
                        MonitoredItemEx monitor = this._Monitors.Find( tag.UniqueKey );
                        if( monitor != null )
                        {
                            monitors.Add( monitor );
                        }
                    }

                    if( monitors.Count > 0 )
                    {
                        this._Subscription.RemoveItems( monitors );
                        this._Subscription.ApplyChanges();
                        this._Monitors.Remove( monitors );
                    }

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="monitors"></param>
        private void PrepareMonitors( IEnumerable<MonitoredItemEx> monitors )
        {

            foreach( MonitoredItemEx monitor in monitors )
            {
                NodeId node = monitor.Tag.TagId;
                monitor.StartNodeId = node;
                monitor.MonitoringMode = MonitoringMode.Reporting;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tag"></param>
        public void WatchMonitors( List<MonitoredItemEx> monitors, Boolean first = true )
        {
            lock (this._SyncObject)
            {
                if (this._Session == null)
                {                    
                    try
                    {
                        Open();             

                    } catch (Exception ex)
                    {
                        this._IOPCInfo.Info( ex );

                        PrepareMonitors( monitors );
                        
                        this._Monitors.SmartAdd( monitors );

                        if( this._ReconnectTimer == null )
                        {
                            this._ReconnectTimer = new Timer( this.Reconnect, "TimerReconnect", 10000, 10000 );
                        }
                        return;
                    }
                }

                if (this._Subscription == null)
                {
                    if (false == CreateSurscribtion())
                    {
                        throw new Exception( "Create subscription error" );
                    }
                }

                PrepareMonitors( monitors );

                if( first )
                {
                    foreach( MonitoredItemEx monitor in monitors )
                    {
                        monitor.Notification += ( item, e ) =>
                        {

                           // Debug.WriteLine( String.Format( "@@@ OnChange Thread:{0}", Thread.CurrentThread.ManagedThreadId ) );


                            List<TagDataValue> result = new List<TagDataValue>();

                           // this._IOPCInfo.Info( String.Format( "-start--------------------------" ), eInfoType.Debug ); 

                            foreach( var v in item.DequeueValues() )
                            {
#if DEBUG
                               // Thread.Sleep( 500 );
#endif
                                MonitoredItemEx monitor_ex = item as MonitoredItemEx;
                                if( monitor_ex != null )
                                {
                                    result.Add( new TagDataValue( monitor_ex.Tag, v, monitor.AlarmDetectedTime ) );

                                    String msg = String.Format("(! raw data!)\tTag:'{0}'\tServerTS:{1}\tSourceTS:{2} ",
                                                    monitor_ex.Tag.TagId, v.ServerTimestamp, 
                                                    v.SourceTimestamp );

                                    this._IOPCInfo.Info(msg, eInfoType.Debug);
#if DEBUG
                                    Int32 raw_v = (Int32) v.Value;
                                    Boolean value = ( raw_v < 30 );
#else
                                    Boolean value = (Boolean)v.Value;
#endif
                                    this._IOPCInfo.Info( String.Format( "New value => ItemId:{0} Time:{1} Value:{2}",
                                                                        monitor_ex.Tag.TagId,
                                                                        v.ServerTimestamp,                                                                        
                                                                        value ),
                                                          eInfoType.Debug );

                                }
                            }

                           // this._IOPCInfo.Info( String.Format( "-end--------------------------" ), eInfoType.Debug );

                            if( this._IOPCDataValues != null && result.Count > 0 )
                            {
                                this._IOPCDataValues.AddDataValues( result );
                            }
                            return;
                        };
                    };
                };

                this._Monitors.SmartAdd( monitors );

                List<MonitoredItemEx> ms = new List<MonitoredItemEx>();
                Int32 portion = 0;
                Int32 cnt = 0;
                foreach( var monitor in monitors )
                {
                    cnt++;
                    portion++;
                    ms.Add( monitor );

                    if( portion > 100 || portion == monitors.Count )
                    {
                        this._Subscription.AddItems( ms ); // monitors );
                        this._Subscription.ApplyChanges();
                        portion = 0;
                        ms.Clear();
                    }                    
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Boolean CreateSurscribtion()
        {
            this._Subscription = new Subscription( this._Session.DefaultSubscription )
            {
                PublishingInterval = this.PublishingInterval,
                TimestampsToReturn = TimestampsToReturn.Server
            };
            Boolean result = this._Session.AddSubscription( this._Subscription );
            if( result == false )
            {
                this._Subscription = null;
                return false;
            }

            this._Subscription.Create();
            return true;
        }

        public void Close()
        {
            this._Session.Close();
            this._Session.Dispose();
            this._Session = null;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Open()
        {
            if( this._Configuration == null )
            {
                this._Configuration = new ApplicationConfiguration
                {
                    ApplicationType = ApplicationType.Client,
                    CertificateValidator = new CertificateValidator(),
                    ClientConfiguration = new ClientConfiguration()
                };
            }
            String host = "opc.tcp:" + this.UrlServer;
            EndpointDescription endpointDescription = SelectEndpoint( host, false );

            EndpointConfiguration endpointConfiguration = EndpointConfiguration.Create( null );
            ConfiguredEndpoint endpoint = new ConfiguredEndpoint( null, endpointDescription, endpointConfiguration );
            this._Session = Session.Create( this._Configuration, endpoint, false, true, "", this.Timeout * 1000, null, null );

            this._Session.Notification += _Session_Notification;
            this._Session.PublishError += _Session_PublishError;
            this._Session.SessionClosing += _Session_SessionClosing;
            this._Session.KeepAlive += _Session_KeepAlive;
            this._Session.KeepAliveInterval = 10000;            
        }

        private void _Session_KeepAlive( Session session, KeepAliveEventArgs e )
        {
#if DEBUG
  //          Debug.WriteLine( String.Format("Thread:{0}", Thread.CurrentThread.ManagedThreadId ) );
  //          Debug.WriteLine( String.Format( "KeepAlive State:{0} Time{1} ", e.CurrentState.ToString(), DateTime.Now.ToLocalTime() ) );
#endif
            if (e.CurrentState == ServerState.Unknown || e.CurrentState == ServerState.Shutdown )
            {
                if (this._ReconnectTimer == null)
                {
                    this._Session.KeepAlive -= _Session_KeepAlive;
                    this._ReconnectTimer = new Timer( this.Reconnect, "TimerReconnect", 10000, 10000 );
                }
            }
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void Reconnect( Object obj )
        {            
            this._ReconnectCount++;

            lock (this._SyncObject)
            {
                try
                {
                    this._IOPCInfo.Info( "Reconnect to OPC server....", eInfoType.Message );
                    if (this._Session != null)
                    {
                        this._Session.Close();
                        this._Session = null;
                    }
                    this._Subscription = null;
                    Open();

                    if( this._ReconnectTimer != null )
                    {
                        this._ReconnectTimer.Dispose();
                        this._ReconnectTimer = null;
                    }

                    this._IOPCInfo.Info( "Reconnect to OPC server - OK!", eInfoType.Message );

                } catch (Exception ex)
                {
                    this._IOPCInfo.Info( ex );
                    return;
                }

                MonitorStorage oldmonitores = null;
                try
                {
                    this._IOPCInfo.Info( "Resubscribe items....", eInfoType.Message );
                    oldmonitores = this._Monitors;
                    this._Monitors = new MonitorStorage();

                    WatchMonitors( new List<MonitoredItemEx>( oldmonitores.NewItems ), true );

                    this._IOPCInfo.Info( "Resubscribe items - OK!", eInfoType.Message );

                }catch (Exception ex)
                {
                    this._IOPCInfo.Info( ex );
                    if (oldmonitores != null)
                    {
                        this._Monitors = oldmonitores;
                    }
                }
            }
        }

        private System.Threading.Timer _ReconnectTimer = null;
        private Int32 _ReconnectCount = 0;

        private void _Session_SessionClosing( object sender, EventArgs e )
        {
            //Debug.WriteLine( "Session_SessionClosing" );
            return;
        }

        private void _Session_PublishError( Session session, PublishErrorEventArgs e )
        {
            //Debug.WriteLine( "Session_PublishError" );
            return;
        }

        private void _Session_Notification( Session session, NotificationEventArgs e )
        {
            //Debug.WriteLine( "Session_Notification" );
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discoveryUrl"></param>
        /// <param name="useSecurity"></param>
        /// <returns></returns>
        private EndpointDescription SelectEndpoint( string discoveryUrl, bool useSecurity )
        {
            Uri uri = new Uri( discoveryUrl );
            EndpointDescription selectedEndpoint = null;

            using( DiscoveryClient client = DiscoveryClient.Create( uri ) )
            {
                EndpointDescriptionCollection endpoints = client.GetEndpoints( null );

                for( int i = 0; i < endpoints.Count; i++ )
                {
                    EndpointDescription endpoint = endpoints[i];

                    if( endpoint.EndpointUrl.StartsWith( uri.Scheme ) )
                    {
                        if( useSecurity )
                        {
                            if( endpoint.SecurityMode == MessageSecurityMode.None )
                                continue;
                        }
                        else
                        {
                            if( endpoint.SecurityMode != MessageSecurityMode.None )
                                continue;
                        }

                        if( selectedEndpoint == null )
                            selectedEndpoint = endpoint;

                        if( endpoint.SecurityLevel > selectedEndpoint.SecurityLevel )
                            selectedEndpoint = endpoint;
                    }
                }

                if( selectedEndpoint == null && endpoints.Count > 0 )
                    selectedEndpoint = endpoints[0];
                return selectedEndpoint;
            }
        }
    }
}
