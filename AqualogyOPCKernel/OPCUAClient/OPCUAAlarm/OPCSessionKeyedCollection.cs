﻿using System;
using System.Collections.ObjectModel;

namespace AqualogyOPCKernel.OPCUAClient.OPCUAAlarm
{
    /// <summary>
    /// 
    /// </summary>
    class OPCSessionKeyedCollection:
        KeyedCollection<String, OPCSession>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OPCSessionKeyedCollection()
        {

        }

        /// <summary>
        /// Get key for item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override String GetKeyForItem( OPCSession item )
        {
            return item.UrlServer;
        }
    }
}
