﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Opc.Ua.Client;

namespace AqualogyOPCKernel.OPCUAClient.OPCUAAlarm
{
    internal class OPCSessionStorage:
                IDisposable
    {
        private OPCSessionKeyedCollection _Sessions = new OPCSessionKeyedCollection();

        public OPCSessionStorage()
        {

        }

        public OPCSession GetOPCUASession( String urlServer )
        {
            if( this._Sessions.Contains( urlServer ) )
            {
                return this._Sessions[urlServer];
            }

            OPCSession session = new OPCSession();
            session.Open( urlServer );

            this._Sessions.Add( session );

            return session;
        }

        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                // Free any other managed objects here. 
                if( this._Sessions.Count > 0 )
                {                    

                    List<String> for_del = new List<String>();
                    foreach( var item in this._Sessions )
                    {
                        if( item!= null )
                        {
                            try
                            {
                                item.Dispose();                                
                                for_del.Add( item.UrlServer );
                            }
                            catch (System.Exception ex)
                            {
                                Debug.WriteLine( "Error in Dispose(...)" + ex.Message );	
                            }                            
                        }
                    }

                    foreach( String key_for_del in for_del )
                    {
                        this._Sessions.Remove( key_for_del );
                    }
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion

    }
}
