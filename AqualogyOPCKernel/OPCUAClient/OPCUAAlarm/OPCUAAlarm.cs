﻿using Opc.Ua;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.OPCUAClient.OPCUAAlarm
{
    public class OPCUAAlarm:
                IDisposable
    {

        private OPCSessionStorage _Sessions = new OPCSessionStorage();

        public OPCUAAlarm()
        {

        }

        public Boolean SetAlarm( String urlServer, String tagName )
        {
            OPCSession session = this._Sessions.GetOPCUASession( urlServer );

            if( session != null )
            {
                WriteValueCollection writeValues = new WriteValueCollection();
                // Int32 Item
                WriteValue intWriteVal = new WriteValue();
                intWriteVal.NodeId = new NodeId( tagName );
                intWriteVal.AttributeId = Attributes.Value;
                Object obj = new Object();
                obj = 1;
                intWriteVal.Value.Value = obj; // new DataValue( 111 );
                intWriteVal.Value.StatusCode = StatusCodes.Good;
                intWriteVal.Value.ServerTimestamp = DateTime.MinValue;
                intWriteVal.Value.SourceTimestamp = DateTime.MinValue;

                writeValues.Add( intWriteVal );
                StatusCodeCollection results = new StatusCodeCollection();
                DiagnosticInfoCollection diagnosticInfos = new DiagnosticInfoCollection();

                ResponseHeader responseHeader = session.Session.Write( null, writeValues, out results, out diagnosticInfos );
                
                return StatusCode.IsGood( results[0] );
            }

            return false;
        }

        private void WriteValue()
        {
        }


        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                // Free any other managed objects here. 
                if( this._Sessions != null )
                {
                    this._Sessions.Dispose();
                    this._Sessions = null;
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion
    }
}
