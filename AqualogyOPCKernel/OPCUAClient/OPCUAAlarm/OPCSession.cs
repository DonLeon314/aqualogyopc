﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.OPCUAClient.OPCUAAlarm
{
    public class OPCSession:
            IDisposable
    {
        private ApplicationConfiguration _Configuration = null;
        private Int32 _Timeout = 10;
        
        public Session Session
        {
            get;
            private set;
        }

        public String UrlServer
        {
            get;
            private set;
        }


        public void Open( String urlServer )
        {
            if( this._Configuration == null )
            {
                this._Configuration = new ApplicationConfiguration
                {
                    ApplicationType = ApplicationType.Client,
                    CertificateValidator = new CertificateValidator(),
                    ClientConfiguration = new ClientConfiguration()
                };
            }

            this.UrlServer = urlServer;
            String host = "opc.tcp:" + urlServer;
            EndpointDescription endpointDescription = SelectEndpoint( host, false );

            EndpointConfiguration endpointConfiguration = EndpointConfiguration.Create( null );
            ConfiguredEndpoint endpoint = new ConfiguredEndpoint( null, endpointDescription, endpointConfiguration );
            this.Session = Session.Create( this._Configuration, endpoint, false, true, "", (uint)this._Timeout * 1000, null, null );

        }

        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            if( this.Session != null )
            {
                this.Session.Close();
                this.Session.Dispose();
                this.Session = null;
            }

            if( this._Configuration != null )
            {
                this._Configuration = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discoveryUrl"></param>
        /// <param name="useSecurity"></param>
        /// <returns></returns>
        private EndpointDescription SelectEndpoint( string discoveryUrl, bool useSecurity )
        {
            Uri uri = new Uri( discoveryUrl );
            EndpointDescription selectedEndpoint = null;

            using( DiscoveryClient client = DiscoveryClient.Create( uri ) )
            {
                EndpointDescriptionCollection endpoints = client.GetEndpoints( null );

                for( int i = 0; i < endpoints.Count; i++ )
                {
                    EndpointDescription endpoint = endpoints[i];

                    if( endpoint.EndpointUrl.StartsWith( uri.Scheme ) )
                    {
                        if( useSecurity )
                        {
                            if( endpoint.SecurityMode == MessageSecurityMode.None )
                                continue;
                        }
                        else
                        {
                            if( endpoint.SecurityMode != MessageSecurityMode.None )
                                continue;
                        }

                        if( selectedEndpoint == null )
                            selectedEndpoint = endpoint;

                        if( endpoint.SecurityLevel > selectedEndpoint.SecurityLevel )
                            selectedEndpoint = endpoint;
                    }
                }

                if( selectedEndpoint == null && endpoints.Count > 0 )
                    selectedEndpoint = endpoints[0];
                return selectedEndpoint;
            }
        }
        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                Close();
                this._Disposed = true;
            }
            // Free any unmanaged objects here.
            //this._Disposed = true;
        }
        #endregion
    }
}
