﻿using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.OPCUAClient
{
    internal class MonitorStorage
    {
        private MonitoredItemExKeyedCollection _Collection = new MonitoredItemExKeyedCollection();

        /// <summary>
        /// Constructor
        /// </summary>
        public MonitorStorage()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<MonitoredItemEx> Monitors
        {
            get
            {
                return this._Collection;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newMonitors"></param>
        public void SmartAdd( IEnumerable<MonitoredItemEx> newMonitors )
        {
            foreach( MonitoredItemEx monitor in newMonitors )
            {
                if( false == this._Collection.Contains( monitor.Tag.UniqueKey ) )
                {
                    this._Collection.Add( monitor );
                }
            }
        }

        public void Remove( IEnumerable<MonitoredItemEx> forRemove )
        {
            foreach( MonitoredItemEx monitor in forRemove )
            {
                if( this._Collection.Contains( monitor.Tag.UniqueKey ) )
                {
                    this._Collection.Remove( monitor.Tag.UniqueKey );
                }
            }
        }


        public IEnumerable<MonitoredItemEx> NewItems
        {
            get
            {
                List<MonitoredItemEx> result = new List<MonitoredItemEx>();

                foreach( MonitoredItemEx monitor in this._Collection )
                {
                    MonitoredItemEx new_monitor = new MonitoredItemEx( monitor.Tag, monitor.AlarmDetectedTime )
                    {
                        DisplayName = monitor.Tag.TagId,
                        SamplingInterval =  monitor.SamplingInterval,
                        QueueSize = monitor.QueueSize
                    };

                    result.Add( new_monitor );
                }

                return result;
            }
        }

        /// <summary>
        /// Find monitor
        /// </summary>
        /// <param name="uniqueKey">Unique key tag</param>
        /// <returns></returns>
        public MonitoredItemEx Find( OPCTagUniqueKey uniqueKey )
        {
            
            if( this._Collection.Contains( uniqueKey ) )
            {
                return this._Collection[uniqueKey];
            };

            return null;
        }
    }
}
