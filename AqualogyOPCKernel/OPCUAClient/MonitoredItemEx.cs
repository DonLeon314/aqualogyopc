﻿using AqualogyOPCKernel.Model;
using Opc.Ua.Client;
using System;


namespace AqualogyOPCKernel.OPCUAClient
{
    internal class MonitoredItemEx:
            MonitoredItem
    {
        public OPCTag Tag
        {
            get;
            private set;
        }

        public Int32 AlarmDetectedTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="tag"></param>
        public MonitoredItemEx( MonitoredItem subscription, OPCTag tag, Int32 alarmDetectedTime ):
                base( subscription )
        {
            this.Tag = tag;
            this.AlarmDetectedTime = alarmDetectedTime;
        }

        public MonitoredItemEx( OPCTag tag, Int32 alarmDetectedTime )                
        {
            this.Tag = tag;
            this.AlarmDetectedTime = alarmDetectedTime;
        }        
    }
}
