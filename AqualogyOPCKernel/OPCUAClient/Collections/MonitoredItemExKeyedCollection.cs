﻿using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.ObjectModel;

namespace AqualogyOPCKernel.OPCUAClient
{
    internal class MonitoredItemExKeyedCollection:
            KeyedCollection<OPCTagUniqueKey, MonitoredItemEx>
    {
        public MonitoredItemExKeyedCollection()
        {

        }

        protected override OPCTagUniqueKey GetKeyForItem( MonitoredItemEx item )
        {
            return item.Tag.UniqueKey;
        }
    }
}
