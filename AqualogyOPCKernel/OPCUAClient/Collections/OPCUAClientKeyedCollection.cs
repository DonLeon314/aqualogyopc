﻿using System;
using System.Collections.ObjectModel;


namespace AqualogyOPCKernel.OPCUAClient
{
    internal class OPCUAClientKeyedCollection:
            KeyedCollection<String, OPCUAClient>
    {
        public OPCUAClientKeyedCollection() :
                base( StringComparer.InvariantCultureIgnoreCase )
        {

        }

        protected override string GetKeyForItem( OPCUAClient item )
        {
            return item.UrlServer;
        }
    }
}
