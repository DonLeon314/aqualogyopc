﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Statistic
{
    public interface IEventStatistic
    {
        void Save( Int32 sendCount, Int32 deleteCount, Int32 idStorage, DateTime currentTime );
        EventStatisticResult CalcStatistics( Int32 idStorage, DateTime startTime, DateTime endTime );
        void ResetStatistics( Int32 idStorage );
    }
}
