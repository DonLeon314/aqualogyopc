﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Statistic
{
    public class EventStatisticResult
    {
        /// <summary>
        /// All send event
        /// </summary>
        public Int32 SendCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Error count
        /// </summary>
        public Int32 ErrorCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="all"></param>
        /// <param name="persentErrors"></param>
        public EventStatisticResult( Int32 sendCount, Int32 errorCount )
        {
            this.SendCount = sendCount;
            this.ErrorCount = errorCount;
        }
    }
}
