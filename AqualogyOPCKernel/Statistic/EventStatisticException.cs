﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Statistic
{
    /// <summary>
    /// Statistic exception
    /// </summary>
    public class EventStatisticException:
        Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        public EventStatisticException( String message ):
                base( message )
        {
        }
    }
}
