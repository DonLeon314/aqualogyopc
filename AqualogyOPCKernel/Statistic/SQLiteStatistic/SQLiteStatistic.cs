﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Statistic.SQLiteStatistic
{
    public class SQLiteStatistic:
                IEventStatistic,
                IDisposable
    {
        private String _TableName = "StatSend";
        /// <summary>
        /// SQLite connection
        /// </summary>
        private SQLiteConnection _Connection;

        /// <summary>
        /// Constructor
        /// </summary>
        public SQLiteStatistic()
        {
        }

        /// <summary>
        /// Get SQLite connection
        /// </summary>
        private SQLiteConnection Connection
        {
            get
            {
                if( this._Connection == null )
                {
                    Initialize();
                }

                return this._Connection;
            }
        }

        #region IEventStatistic

        public void Save( Int32 sendCount, Int32 deleteCount, Int32 idStorage, DateTime currentTime )
        {
            try
            {
                // Save 
                String sql_query = "INSERT INTO [" + this._TableName + "] " +
                                        " ( [Time], [SendCount], [DeleteCount], [StorageId] ) " +
                                        " VALUES( :p_Time, :p_SendCount, :p_DeleteCount, :p_StorageId )";
                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this.Connection ) )
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "p_Time", (Object) currentTime.Ticks ) );
                    cmd.Parameters.Add( new SQLiteParameter( "p_SendCount", (Object) sendCount ) );
                    cmd.Parameters.Add( new SQLiteParameter( "p_DeleteCount", (Object) deleteCount ) );
                    cmd.Parameters.Add( new SQLiteParameter( "p_StorageId", (Object) idStorage ) );

                    Int32 result_execute = cmd.ExecuteNonQuery();
                }                
            }
            catch( Exception ex )
            {
                // :(
                throw new EventStatisticException( ex.Message );
            }
        }

        public void ResetStatistics( Int32 idStorage )
        {
            try
            {
                String sql_query = "DELETE FROM [" + this._TableName + "] WHERE [StorageId] = :p_StorageId";
                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this.Connection ) )
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "p_StorageId", (Object) idStorage ) );
                    Int32 result_query = cmd.ExecuteNonQuery();
                }
            }
            catch( Exception ex )
            {
                // :(
                throw new EventStatisticException( ex.Message );
            }
        } 
      

        public EventStatisticResult CalcStatistics( Int32 idStorage, DateTime startTime, DateTime endTime )
        {
            try
            {
                // Save 
                String sql_query = "SELECT COUNT(*) AS cnt, Sum( [SendCount] ) AS [SendSum], " +
                                           "Sum( [DeleteCount] ) AS [DeleteSum] " +
                                      "FROM [" + this._TableName + "] " +
                                      "WHERE [StorageId] = :p_StorageId AND " +
                                            " [Time] BETWEEN :p_StartTime AND :p_EndTime ";
                                    

                using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this.Connection ) )
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "p_StorageId", (Object) idStorage ) );
                    cmd.Parameters.Add( new SQLiteParameter( "p_StartTime", (Object) startTime.Ticks ) );
                    cmd.Parameters.Add( new SQLiteParameter( "p_EndTime", (Object) endTime.Ticks ) );

                    using( SQLiteDataReader reader = cmd.ExecuteReader() )
                    {
                        if( reader.Read() )
                        {
                            if( 0 != decimal.ToInt32( reader.GetDecimal( 0 ) ) )
                            {
                                Int32 send_sum = decimal.ToInt32( reader.GetDecimal( 1 ) );
                                Int32 errors_sum = decimal.ToInt32( reader.GetDecimal( 2 ) );

                                // return statistics
                                return new EventStatisticResult( send_sum, errors_sum );
                            }
                        }
                    }
                }

                // get empty statistics
                return new EventStatisticResult( 0, 0 );
            }
            catch( Exception ex )
            {
                // :(
                throw new EventStatisticException( ex.Message );
            }
        }

        #endregion

        private String SQLiteDBFileName( String nameDB )
        {
            String db_file = System.AppDomain.CurrentDomain.BaseDirectory;
            db_file = db_file.Trim();
            if( db_file[db_file.Length - 1] != '\\' )
            {
                db_file += @"\";
            }

            db_file = ( db_file + ( String.IsNullOrWhiteSpace( nameDB ) ? "AqualogyOPCStatistisc.db3" : nameDB ) );

            return db_file;
        }

        /// <summary>
        /// Initialize database
        /// </summary>
        private void Initialize()
        {

            String fname = SQLiteDBFileName( null );

            if( false == File.Exists( fname ) )
            {
                SQLiteCreateDB( fname );
            }            

            SQLiteOpenDB( fname );

            CheckDatabase();
        }

        /// <summary>
        /// Check SQLite database
        /// </summary>
        private void CheckDatabase()
        {
            if( SQLiteEx.SQLiteEx.TableExist( this._Connection, this._TableName ) == false )
            {
                CreateTable();
            }
        }

        /// <summary>
        /// Create table
        /// </summary>
        private void CreateTable()
        {
            String sql_query = "CREATE TABLE [" + this._TableName + "] ( " +
                                        " [Id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                        " [Time] integer NOT NULL, " +
                                        " [SendCount] integer NOT NULL, " +
                                        " [DeleteCount] integer NOT NULL, " +
                                        " [StorageId] integer NOT NULL )";
            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._Connection ) )
            {
                Int32 result_execute = cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create SQLite database
        /// </summary>
        /// <param name="dbFileName"></param>
        private void SQLiteCreateDB( String dbFileName )
        {
            SQLiteConnection.CreateFile( dbFileName );
        }

        /// <summary>
        /// Open SQLite database
        /// </summary>
        /// <param name="dbFileName"></param>
        private void SQLiteOpenDB( String dbFileName )
        {
            this._Connection = new SQLiteConnection( String.Format( "Data Source={0}; Version=3;", dbFileName ) );
            this._Connection.Open();
        }


        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                // Free any other managed objects here.                
                if( this._Connection != null )
                {
                    try
                    {
                        this._Connection.Close();
                        this._Connection.Dispose();
                        this._Connection = null;
                    }
                    catch( Exception ex )
                    {
                        Debug.WriteLine( String.Format( "!!! Error:{0}", ex.Message ) );
                    }
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion

    }
}
