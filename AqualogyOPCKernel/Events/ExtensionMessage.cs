﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Events
{
    public class ExtensionMessage
    {
        public String Message
        {
            get;
            private set;
        }

        public ExtensionMessage( String message )
        {
            this.Message = message;
        }
    }
}
