﻿using AqualogyOPCKernel.HotConfig;
using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.MailSender;
using AqualogyOPCKernel.OPCUAClient.OPCUAAlarm;
using AqualogyOPCKernel.Statistic;
using OPCConfigModel;
using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace AqualogyOPCKernel.Events
{
    public class BBDDEventSaver:
            EventSaverBase
    {          
        public String Description
        {
            get;
            private set;
        }

        public String ConnectionString
        {
            get;
            private set;
        }

        public String TableName
        {
            get;
            private set;
        }


        /// <summary>
        /// EMail administrator
        /// </summary>
        public String EMailAdministrator
        {
            get;
            private set;
        }

        /// <summary>
        /// OPC item for reboot GreenBox
        /// </summary>
        public String OPCItem
        {
            get;
            private set;
        }

        public String OPCUrlServer
        {
            get;
            private set;
        }


        private SqlConnection _Connection = null;
        private SqlTransaction _Transaction = null;

        /// <summary>
        /// Event saver creator
        /// </summary>
        /// <param name="bbddEntyty"></param>
        /// <returns></returns>
        public static BBDDEventSaver BBDDEventSaverCreate( EventSaverConfig bbddEntity )
        {
            BBDDEventSaver result = new BBDDEventSaver()
            {
                ConnectionString = bbddEntity.ConnectionString,
                Priority = bbddEntity.Priority,
                Description = bbddEntity.Description,
                TableName = bbddEntity.TableName,
                Id = bbddEntity.Id,
                EMailAdministrator = bbddEntity.EMailAdministrator,
                OPCItem = bbddEntity.OPCItem,
                OPCUrlServer = bbddEntity.OPCUAUrlServer,
                LastStorage = false,
            };
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private BBDDEventSaver()
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="saveEvents"></param>
        /// <returns></returns>
        public override Boolean ProcessEvents( EventSaverBase eventSaverBase, IOPCInfo iOPCInfo, IHotConfig iHotConfig, Int32 hitoryTime )
        {       
                try
                {
                    eventSaverBase.BeginTransaction( iOPCInfo );

                    
                    var for_save =  eventSaverBase.GetEventForSave( hitoryTime );
                                        

                    if( for_save.Count > 0 )
                    {

                        this.BeginTransaction( iOPCInfo );

                        SaveEvents( for_save );

                        this.CommitTransaction( iOPCInfo );

                        if( iOPCInfo != null )
                        {
                            iOPCInfo.Info( String.Format( "Save {0} events to:'{1}'", for_save.Count, this.Description ) );
                        }

                        eventSaverBase.DeleteEvents( for_save, iOPCInfo, true, iHotConfig, this.Description );
                    };

                    eventSaverBase.CommitTransaction( iOPCInfo );
                    
                }catch( Exception ex )
                {
                    eventSaverBase.RollbackTransaction( iOPCInfo );
                    this.RollbackTransaction( iOPCInfo );

                    if( iOPCInfo != null )
                    {
                        String msg = String.Format( "Rollback:{0}", ex.Message );
                        iOPCInfo.Info( msg );
                    }

                    return false;
                }                
            
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void BeginTransaction( IOPCInfo iOPCInfo )
        {
            this._Connection = new SqlConnection( this.ConnectionString );
            this._Connection.Open();

            this._Transaction = this._Connection.BeginTransaction();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        private void DisposeConnection( IOPCInfo iOPCInfo )
        {
            try
            {
                if( this._Connection != null )
                {
                    this._Connection.Dispose();
                }

            } catch( Exception ex )
            {
                if( iOPCInfo != null )
                {
                    iOPCInfo.Info( ex );
                }
            }
            finally
            {
                this._Connection = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        private void DisposeTransaction( IOPCInfo iOPCInfo )
        {
            try
            {
                if( this._Transaction != null )
                {
                    this._Transaction.Dispose();
                }

            } catch( Exception ex )
            {
                if( iOPCInfo != null )
                {
                    iOPCInfo.Info( ex );
                }
            }
            finally
            {
                this._Transaction = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void RollbackTransaction( IOPCInfo iOPCInfo )
        {
            try
            {
                if( this._Transaction != null )
                {
                    this._Transaction.Rollback();
                }

            } catch( Exception ex )
            {
                if( iOPCInfo != null )
                {
                    iOPCInfo.Info( ex );
                }
                DisposeTransaction( iOPCInfo );
            }
            finally
            {
                DisposeConnection( iOPCInfo );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void CommitTransaction( IOPCInfo iOPCInfo )
        {
            try
            {
                if( this._Transaction != null )
                {
                    this._Transaction.Commit();
                }

            } catch( Exception ex )
            {
                if( iOPCInfo != null )
                {
                    iOPCInfo.Info( ex );

                }

                DisposeTransaction( iOPCInfo );

                throw ex;
                
            }
            finally
            {
                DisposeConnection( iOPCInfo );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        private void AlarmON( IOPCInfo iOPCInfo )
        {
            try
            {
                using( OPCUAAlarm alarm = new OPCUAAlarm() )
                {
                    // OPC ALARM - ON!
                    if( String.IsNullOrWhiteSpace( this.OPCItem ) == false &&
                        String.IsNullOrWhiteSpace( this.OPCUrlServer ) == false )
                    {
                        alarm.SetAlarm( this.OPCUrlServer, this.OPCItem );
                    }
                }
            } catch( Exception ex )
            {
                if( iOPCInfo != null )
                {
                    iOPCInfo.Info( ex );
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="events"></param>
        protected void SaveEvents( IReadOnlyCollection<EventMessage> events )
        {
            
            String sql_query = String.Format( "INSERT INTO [{0}] ( dFechaHora, nIdNotificacion, sCliente, sCanal, sAsunto, sTextoEmail, " +
                                                                   "sTextoSms, sEstado, dFechaEstado, bEmail, bSms, nReintentos ) " +
                                                  " VALUES ( @p_dFechaHora, @p_nIdNotificacion, @p_sCliente, @p_sCanal, @p_sAsunto, @p_sTextoEmail, @p_sTextoSms, @p_sEstado, " +
                                                            "@p_dFechaEstado, @p_bEmail, @p_bSms, @p_nReintentos )", this.TableName );

            using( SqlCommand cmd = ( this._Transaction == null ? new SqlCommand( sql_query, this._Connection ) :
                                        new SqlCommand( sql_query, this._Connection, this._Transaction )
                                    ) )
            {
                foreach( EventMessage item in events )
                {
                    cmd.Parameters.Clear();

                    cmd.Parameters.Add( new SqlParameter( "@p_dFechaHora", item.dFechaHora ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_nIdNotificacion", item.nIdNotificacion ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sCliente", item.sCliente ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sCanal", item.sCanal ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sAsunto", item.sAsunto ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sTextoEmail", item.sTextoEmail ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sTextoSms", item.sTextoSms ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_sEstado", "PENDIENTE" ) ); // item.sEstado ) );

                    cmd.Parameters.Add( new SqlParameter( "@p_dFechaEstado", DateTime.Now ) );                    

                    cmd.Parameters.Add( new SqlParameter( "@p_bEmail", item.bEmail ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_bSms", item.bSms ) );
                    cmd.Parameters.Add( new SqlParameter( "@p_nReintentos", item.nReintentos ) );

                    cmd.ExecuteScalar();
                }
            }
        }


        /// <summary>
        /// Get timeoutEvents and error events
        /// </summary>
        public override IReadOnlyCollection<EventMessage> GetEventForSave( Int32 historyTime )
        {
            List<EventMessage> result = new List<EventMessage>( historyTime );

            DateTime current_time = DateTime.Now;
            DateTime history_time = current_time.AddMinutes( -historyTime );


                          //  0       1           2               3        4       5         6
            String fields = "nId, dFechaHora, nIdNotificacion, sCliente, sCanal, sAsunto, sTextoEmail, "+
                       //     7         8           9         10     11       12          
                          "sTextoSms, sEstado, dFechaEstado, bEmail, bSms, nReintentos ";


            String sql_query = String.Format( "SELECT {0} FROM {1}"+
                                                    " WHERE UPPER( sEstado ) = 'ERROR' AND dFEchaEstado BETWEEN @p_HistoryTime AND @p_CurrentTime",
                                             fields, this.TableName );

            using( SqlCommand cmd = ( this._Transaction == null ? new SqlCommand( sql_query, this._Connection ) :
                                      new SqlCommand( sql_query, this._Connection, this._Transaction ) ) )
            {
                cmd.Parameters.Add( new SqlParameter( "@p_HistoryTime", history_time ) );
                cmd.Parameters.Add( new SqlParameter( "@p_CurrentTime", current_time ) );

                using( SqlDataReader reader = cmd.ExecuteReader() )
                {
                    while( reader.Read() )
                    {
                        var new_msg = new EventMessage()
                        {
                            Id = reader.GetInt32( 0 ),
                            dFechaHora = reader.GetDateTime( 1 ),
                            nIdNotificacion = ( reader.IsDBNull( 2 ) ? null : (Int32?)reader.GetInt32( 2 ) ),
                            sCliente = reader.GetString( 3 ),
                            sCanal = reader.GetString( 4 ),
                            sAsunto = reader.GetString( 5 ),
                            sTextoEmail = reader.GetString( 6 ),
                            sTextoSms = reader.GetString( 7 ),
                            sEstado = reader.GetString( 8 ),
                            dFechaEstado = ( reader.IsDBNull(9) ? null : new DateTime?( reader.GetDateTime( 9 ) ) )
                        };

                        if( reader.IsDBNull( 10 ) )
                        {
                            new_msg.bEmail = null;
                        }
                        else
                        {
                            new_msg.bEmail = reader.GetBoolean( 10 );
                        }

                        if( reader.IsDBNull( 11 ) )
                        {
                            new_msg.bSms = null;
                        }
                        else
                        {
                            new_msg.bSms = reader.GetBoolean( 11 );
                        }

                        if( reader.IsDBNull( 12 ) )
                        {
                            new_msg.nReintentos = null;
                        }
                        else
                        {
                            new_msg.nReintentos = reader.GetInt32( 12 );
                        }

                        result.Add( new_msg );
                    }                    
                }
            }

            return result.AsReadOnly();
        }


        /// <summary>
        /// Delete events
        /// </summary>
        /// <param name="deleteIds"></param>
        public override void DeleteEvents( IReadOnlyCollection<EventMessage> deleteMessages, IOPCInfo iOPCInfo, Boolean delete, 
                                           IHotConfig iHotConfig, String storageDestination )
        {
            if( deleteMessages.Count > 0 )
            {
                if( delete )
                {
                    String sql_query = String.Format( "DELETE FROM {0} WHERE [{0}].[nId] = @p_id", this.TableName );
                    using( SqlCommand cmd = ( this._Transaction == null ? new SqlCommand( sql_query, this._Connection ) :
                                             new SqlCommand( sql_query, this._Connection, this._Transaction ) ) )
                    {
                        foreach( EventMessage message in deleteMessages )
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add( new SqlParameter( "@p_id", message.Id ) );
                            Int32 result_execute = cmd.ExecuteNonQuery();
                        }
                    }

                    //TODO: Create CSV and send


                    if( iOPCInfo != null )
                    {
                        iOPCInfo.Info( String.Format( "Delete {0} lines in '{1}'", deleteMessages.Count, this.Description ), eInfoType.Message );
                    }
                }

                /// Send Messave to Admin
                StringBuilder sb = new StringBuilder();
                foreach( EventMessage message in deleteMessages )
                {
                    if( sb.Length == 0 )
                    {
                        sb.AppendLine( EventMessage.HeaderCSV );
                    }

                    sb.AppendLine( message.LineCSV() );
                }

                if( iHotConfig != null && sb.Length > 0)
                {
                    var smtp_sender = iHotConfig.SMTPSender;
                    if( smtp_sender != null )
                    {
                        using( MemoryStream memstream = new MemoryStream( System.Text.Encoding.ASCII.GetBytes( sb.ToString() ) ) )
                        {
                            String subj = ( String.IsNullOrWhiteSpace( storageDestination ) ?
                                                String.Format( "!Storage '{0}' has errors!", this.Description ) :
                                                String.Format( "!Storage '{0}' has errors and they are moved to '{1}'!", this.Description, storageDestination )
                                           );
                            smtp_sender.SendMessage( this.EMailAdministrator,
                                                     subj,
                                                     //"Information in attachment CSV file",
                                                     sb.ToString(),
                                                     //new List<SMTPSenderAttachmentsInfo>() { new SMTPSenderAttachmentsInfo( memstream, @"lines.csv", @"text/csv" ) }
                                                     null
                                                   );
                        }
                    }

                    // Send Alarm to OPC UA
                    AlarmON( iOPCInfo );

                }
            }
        }
    }
}
