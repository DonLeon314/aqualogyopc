﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AqualogyOPCKernel.HotConfig;
using AqualogyOPCKernel.Interfaces;

namespace AqualogyOPCKernel.Events
{
    public class BBDDSaverDummy:
            EventSaverBase
    {
        private IReadOnlyCollection<EventMessage> _Collection;

        /// <summary>
        /// Constructor
        /// </summary>
        public BBDDSaverDummy( ReadOnlyCollection<EventMessage> events )
        {
            this._Collection = events;
        }

        public override IReadOnlyCollection<EventMessage> GetEventForSave( Int32 historyTime )
        {
            return this._Collection;
        }

        public override void DeleteEvents( IReadOnlyCollection<EventMessage> deleteMessages, IOPCInfo iOPCInfo, Boolean delete,
                                           IHotConfig iHotConfig, String storageDestination )
        {

        }

        public override Boolean ProcessEvents( EventSaverBase eventSaverBase, IOPCInfo iOPCInfo, IHotConfig iHotConfig, Int32 hitoryTime )
        {
            return true;
        }

        public override void BeginTransaction( IOPCInfo iOPCInfo )
        {

        }

        public override void RollbackTransaction( IOPCInfo iOPCInfo )
        {

        }


        public override void CommitTransaction( IOPCInfo iOPCInfo )
        {

        }
    }
}
