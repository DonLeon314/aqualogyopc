﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Events
{
    public class EventMessage
    {
        public Int32 Id
        {
            get;
            set;
        }

        public DateTime dFechaHora
        {
            get;
            set;
        }

        /// <summary>
        /// NULL
        /// </summary>
        public Int32? nIdNotificacion
        {
            get;
            set;
        }

        public String sCliente
        {
            get;
            set;
        }


        /// <summary>
        /// NULL
        /// </summary>
        public String sCanal
        {
            get;
            set;
        }

        /// <summary>
        /// NULL
        /// </summary>
        public String sAsunto
        {
            get;
            set;
        }


        /// <summary>
        /// NULL
        /// </summary>
        public String sTextoEmail
        {
            get;
            set;
        }
        
        /// <summary>
        /// NULL
        /// </summary>
        public String sTextoSms
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String sEstado
        {
            get;
            set;
        }


        /// <summary>
        /// NULL
        /// </summary>
        public DateTime? dFechaEstado
        {
            get;
            set;
        }
        
        /// <summary>
        /// NULL
        /// </summary>
        public Boolean? bEmail
        {
            get;
            set;
        }

        /// <summary>
        /// NULL
        /// </summary>
        public Boolean? bSms
        {
            get;
            set;
        }

        /// <summary>
        /// NULL;
        /// </summary>
        public Int32? nReintentos
        {
            get;
            set;
        }

        /// <summary>
        /// Private default constructor
        /// </summary>
        public EventMessage()
        {

        }


        /// <summary>
        /// CSV header
        /// </summary>
        public static String HeaderCSV
        {
            get
            {         //     0             1           2      3      4          5         6        7        8           9     10      11
                return "dFEchaHora;nIdNotificacion;sCliente;sCanal;sAsunto;sTextoEmail;sTextoSms;sEstado;dFechaEstado;bEmail;bSms;nReintentos";
            }
        }

        public String LineCSV()
        {
            String result = String.Format( @"{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11}",
                                          this.dFechaHora.ToString( @"yyyy/dd/MM HH:mm:ss.fff" ),
                                          ( this.nIdNotificacion.HasValue ? this.nIdNotificacion.Value.ToString() : "NULL" ),
                                          this.sCliente,
                                          ( this.sCanal != null ? this.sCanal : "NULL" ),
                                          ( this.sAsunto != null ? this.sAsunto : "NULL" ),
                                          ( this.sTextoEmail != null ? this.sTextoEmail : "NULL" ),
                                          ( this.sTextoSms != null ? this.sTextoSms : "NULL" ),
                                          this.sEstado,
                                          ( this.dFechaEstado.HasValue ? this.dFechaEstado.Value.ToString( @"yyyy/dd/MM HH:mm:ss.fff" ) : "NULL" ),
                                          ( this.bEmail.HasValue ? ( this.bEmail.Value ? "1" : "0" ) : "NULL " ),
                                          ( this.bSms.HasValue ? ( this.bSms.Value ? "1" : "0" ) : "NULL " ),
                                          ( this.nReintentos.HasValue ? this.nReintentos.Value.ToString() : "NULL" )
                                          );
            return result;
        }
    }
}
