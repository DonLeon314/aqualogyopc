﻿using AqualogyOPCKernel.HotConfig;
using AqualogyOPCKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Events
{
    class BBDDSaverLastDummy:
            EventSaverBase
    {
        /// <summary>
        /// 
        /// </summary>
        public BBDDSaverLastDummy()
        {
            this.Id = Int32.MaxValue;
            this.Priority = Int32.MaxValue;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventSaverBase"></param>
        /// <param name="iOPCInfo"></param>
        /// <returns></returns>
        public override Boolean ProcessEvents( EventSaverBase eventSaverBase, IOPCInfo iOPCInfo, IHotConfig iHotConfig, Int32 historyTime )
        {

            try
            {
                eventSaverBase.BeginTransaction( iOPCInfo );

                var for_save = eventSaverBase.GetEventForSave( historyTime );
                if( for_save.Count > 0 )
                {                                        
                    eventSaverBase.DeleteEvents( for_save, iOPCInfo, false, iHotConfig, null );
                };
                
                eventSaverBase.CommitTransaction( iOPCInfo );

            } catch( Exception ex )
            {
                eventSaverBase.RollbackTransaction( iOPCInfo );

                if( iOPCInfo != null )
                {
                    String msg = String.Format( "Rollback:{0}", ex.Message );
                    iOPCInfo.Info( msg );
                }
                return false;
            }
            return true;
        }                       

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override IReadOnlyCollection<EventMessage> GetEventForSave( Int32 historyTime ) 
        {
            List<EventMessage> result = new List<EventMessage>();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void BeginTransaction( IOPCInfo iOPCInfo )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void RollbackTransaction( IOPCInfo iOPCInfo )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public override void CommitTransaction( IOPCInfo iOPCInfo )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleteIds"></param>
        /// <param name="iOPCInfo"></param>
        /// <param name="markDelete"></param>
        public override void DeleteEvents( IReadOnlyCollection<EventMessage> deleteIds, IOPCInfo iOPCInfo, 
                                           Boolean delete, IHotConfig iHotConfig, String storageDestination )
        {

        }

    }
}
