﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Events
{
    /// <summary>
    /// 
    /// </summary>
    public class BBDDEventSaverFinder
    {
        /// <summary>
        /// 
        /// </summary>
        private String ConnectionString
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private String TableName
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="tableName"></param>
        public BBDDEventSaverFinder( String connectionString, String tableName )
        {
            this.ConnectionString = connectionString;
            this.TableName = tableName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="saver"></param>
        /// <returns></returns>
        public Boolean ConnectionStringComparer( BBDDEventSaver saver )
        {
            SqlConnectionStringBuilder ics = new SqlConnectionStringBuilder( saver.ConnectionString );
            SqlConnectionStringBuilder c = new SqlConnectionStringBuilder( this.ConnectionString );

            return (
                        String.Compare( ics.DataSource.Trim(), c.DataSource.Trim(), true ) == 0 &&
                        String.Compare( ics.InitialCatalog.Trim(), c.InitialCatalog.Trim(), true ) == 0 &&
                        String.Compare( saver.TableName.Trim(), this.TableName, true ) == 0
                   );
        }
    }
}
