﻿using AqualogyOPCKernel.HotConfig;
using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Statistic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;

namespace AqualogyOPCKernel.Events
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class EventSaverBase                
    {

        public List<ExtensionMessage> _ExtensionMessages = new List<ExtensionMessage>();        

        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 Priority
        {
            get;
            set;
        }

        public Boolean LastStorage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iBBDDEventSource"></param>
        /// <param name="iOPCInfo"></param>
        /// <returns></returns>
        public abstract Boolean ProcessEvents( EventSaverBase eventSaverBase, IOPCInfo iOPCInfo, IHotConfig iHotConfig, Int32 hitoryTime );

        /// <summary>
        /// 
        /// </summary>
        public abstract IReadOnlyCollection<EventMessage> GetEventForSave( Int32 historyTime );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void BeginTransaction( IOPCInfo iOPCInfo );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void RollbackTransaction( IOPCInfo iOPCInfo );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        public abstract void CommitTransaction( IOPCInfo iOPCInfo );


        /// <summary>
        /// 
        /// </summary>
        /// <param name="deleteIds"></param>
        /// <param name="iOPCInfo"></param>
        public abstract void DeleteEvents( IReadOnlyCollection<EventMessage> deleteMessages,
                                           IOPCInfo iOPCInfo, Boolean delete, IHotConfig iHotConfig, 
                                           String storageDestination );


    }
}
