﻿using AqualogyOPCKernel.Config;
using AqualogyOPCKernel.Events;
using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.MessageBuffer;
using AqualogyOPCKernel.Model;
using AqualogyOPCKernel.OPCUAClient.OPCUAAlarm;
using AqualogyOPCKernel.Statistic;
using AqualogyOPCKernel.Statistic.SQLiteStatistic;
using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using AqualogyOPCKernel.MailSender;
using OPCConfigModel.ExportClasses;
using AqualogyOPCKernel.HotConfig;

namespace AqualogyOPCKernel.AlarmSaver
{
    internal class MSSQLAlarmSaver:
            IAlarmSaver,
            IDisposable
    {        
        
        private IOPCInfo _IOPCInfo = null;                  
        private SMSLimitCounter _SMSCounter = null;
        private SQLiteConnection _SQLiteConnection;       
        private IEventStatistic _IEventStatistic = new SQLiteStatistic(); //TODO: implement plugin

        private IHotConfig _IHotConfig = null;
        private DateTime? _LastSalto = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="iOPCInfo"></param>
        /// <param name="smsLimit"></param>
        /// <param name="sqliteCollection"></param>
        public MSSQLAlarmSaver( String connectionString, IOPCInfo iOPCInfo, Int32 smsLimit, SQLiteConnection sqliteCollection )
        {
            //this._ConnectionString = connectionString;
            this._SQLiteConnection = sqliteCollection;
            
            this._IOPCInfo = iOPCInfo;            
            this._SMSCounter = new SMSLimitCounter( smsLimit, sqliteCollection );

            this._IHotConfig = new HotConfigStorage( iOPCInfo );
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iOPCInfo"></param>
        /// <param name="smsLimit"></param>
        /// <param name="sqliteCollection"></param>
        public MSSQLAlarmSaver( IOPCInfo iOPCInfo, Int32 smsLimit, SQLiteConnection sqliteCollection )
        {            
            this._IOPCInfo = iOPCInfo;
            this._SMSCounter = new SMSLimitCounter( smsLimit, sqliteCollection );

            this._IHotConfig = new HotConfigStorage( iOPCInfo );
        }

        #region interface IAlarmSaver
        public Boolean Save( IEnumerable<TagAlarmInfo> alarmsinfo, IMarkNeedSendAlarms iMarkNeedSendAlarms )
        {

//             List<Int32> gs = new List<Int32>();
//             gs.Add( 1 );
//             gs.Add( 2 );
//             foreach( var u in this._IHotConfig.GetUsers( gs ) )
//             {
//                 Debug.WriteLine( u.Alias );
//             }


            try
            {
                List<DataToSend> datatosend = new List<DataToSend>();
                if (alarmsinfo != null)
                {
                    this._SMSCounter.RestoreCounter();
                    
                    DateTime time = DateTime.Now;                    
                    foreach (TagAlarmInfo alarmInfo in alarmsinfo)
                    {
                        DataToSend data = new DataToSend( alarmInfo.Tag.LargeMessage, alarmInfo.Tag.ShortMessage, alarmInfo.Tag.Client, alarmInfo.AlarmTime, alarmInfo.Tag );
                        
                        foreach (OPCUser user in  this._IHotConfig.GetUsers( alarmInfo.Tag.GroupIds ) ) //alarmInfo.Tag.Users)
                        {
                            if (user.NeedSend( DateTime.Now )) //alarmInfo.TmeCheckWorkTime ) )
                            {
                                this._IOPCInfo.Info( String.Format( "User:{0} ( {1} {2} ) receives",
                                                                    user.Alias, user.FirstName, user.SecondName ),
                                                     eInfoType.Debug );

                                if (user.Channel == OPCConfigModel.Model.User.eChannel.mail ||
                                    user.Channel == OPCConfigModel.Model.User.eChannel.sms_mail)
                                {
                                    data.AddEMails( user.EMails );
                                }

                                if( user.Channel == OPCConfigModel.Model.User.eChannel.sms ||
                                    user.Channel == OPCConfigModel.Model.User.eChannel.sms_mail )
                                {
                                    data.SMSChannelEnable = this._SMSCounter.DecrementSms();
                                    data.AddPhones( user.Phones );
                                }
                            }
                            else
                            {
                                this._IOPCInfo.Info( String.Format( "User:{0} ( {1} {2} ) no  receives (Alarm time:{3})",
                                                                    user.Alias, user.FirstName, user.SecondName,
                                                                    alarmInfo.TmeCheckWorkTime ),
                                                     eInfoType.Debug );                               
                            }
                        }

                        datatosend.Add( data );
                    }
                }
                Boolean save_result = SaveToDataBase( datatosend, iMarkNeedSendAlarms );

                if( save_result )
                {
                    this._SMSCounter.SaveCounter();
                }

                return save_result;
            }
            catch( Exception ex )
            {
                this._IOPCInfo.Info( ex );
            }

            return false;
        }
        #endregion


        /// <summary>
        /// Detect heed SALTO
        /// </summary>
        /// <returns></returns>
        private Boolean NeedSalto()
        {
            DateTime current_time = DateTime.Now;

            if( this._LastSalto.HasValue )
            {
                var diff = current_time - this._LastSalto.Value;

                if( diff.Minutes < this._IHotConfig.SaltoPeriod )
                {
                    return false;   
                }
            }

            this._LastSalto = current_time;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datatosend"></param>
        private Boolean SaveToDataBase( IEnumerable<DataToSend> datatosend, IMarkNeedSendAlarms iMarkNeedSendAlarms )
        {

            List<OPCTag> need_resend = new List<OPCTag>();
            List<OPCTag> save_items = new List<OPCTag>();
            Int32 reintentosDefault = 0;

            List<EventMessage> events = new List<EventMessage>();

            foreach( DataToSend item in datatosend )
            {
                String channel_mail = item.GetEMails();
                String channel_sms = item.GetPhones();

                // mark NEED SEND
                if( String.IsNullOrWhiteSpace( channel_mail ) && String.IsNullOrWhiteSpace( channel_sms ) )
                {
                    //iMarkNeedSendAlarms.MarkNeedSend( item.Tag );
                    need_resend.Add( item.Tag );
                    continue;
                }

                save_items.Add( item.Tag );

                // EMail                                
                if( String.IsNullOrWhiteSpace( channel_mail ) == false )
                {
                    events.Add( new EventMessage()
                    {
                        dFechaHora = item.AlarmTime,
                        nIdNotificacion = 1,
                        sCliente = item.Client,
                        sCanal = channel_mail,
                        sAsunto = item.ShortMessage,
                        sTextoEmail = item.LargeMessage,
                        sTextoSms = "",
                        sEstado = "PENDIENTE",
                        dFechaEstado = null,
                        bEmail = true,
                        bSms = false,
                        nReintentos = reintentosDefault
                    } );
                }

                // SMS                                
                if( String.IsNullOrWhiteSpace( channel_sms ) == false )
                {
                    events.Add( new EventMessage()
                    {
                        dFechaHora = item.AlarmTime,
                        nIdNotificacion = 1,
                        sCliente = item.Client,
                        sCanal = channel_sms,
                        sAsunto = "",
                        sTextoEmail = "",
                        sTextoSms = item.LargeMessage,
                        sEstado = item.SMSChannelEnable ? "PENDIENTE" : "ERROR",
                        dFechaEstado = null,
                        bEmail = false,
                        bSms = true,
                        nReintentos = reintentosDefault
                    } );
                }
            }

            BBDDSaverDummy dummy = new BBDDSaverDummy( events.AsReadOnly() );

            Boolean result = true;

            var savers = this._IHotConfig.Savers;

            if( savers.Count > 0 )
            {
                EventSaverBase bbdd_source = null;
                Int32 ht = this._IHotConfig.SaltoHistoryTime;
                Boolean need_salto = NeedSalto();

                foreach( EventSaverBase saver in savers )
                {

                    if( bbdd_source == null )
                    {
                        result = saver.ProcessEvents( dummy, this._IOPCInfo, this._IHotConfig, ht );
                        if( result == false )
                        {   // ERROR - add to mark
                            need_resend.Clear();
                            foreach( var item in datatosend )
                            {
                                need_resend.Add( item.Tag );
                            }
                            break;
                        }
                        else
                        {
                            // OK - unmark
                            foreach( var item in save_items )
                            {
                               iMarkNeedSendAlarms.MarkNeedSend( item, false );
                            }
                        }
                    }
                    else
                    {
                        if( need_salto == false )
                        {
                            break;
                        }
                        result = saver.ProcessEvents( bbdd_source, this._IOPCInfo, this._IHotConfig, ht );
                    }

                    if( result )
                    {   // if error skip saver
                        bbdd_source = saver;
                    }
                }
            }
            else
            {
                this._IOPCInfo.Info( String.Format( "Table [EventSaverConfigs] is empty! " ), eInfoType.Error );            
            }

            foreach( var tag_need_send in need_resend )
            {
                iMarkNeedSendAlarms.MarkNeedSend( tag_need_send, true );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datatosend"></param>
        /// <param name="iMarkNeedSendAlarms"></param>
        /// <returns></returns>
        private Boolean SaveToDataBaseOld( IEnumerable<DataToSend> datatosend, IMarkNeedSendAlarms iMarkNeedSendAlarms )
        {
//             try
//             {
//                 using( SqlConnection connection = new SqlConnection( this._ConnectionString ) )
//                 {
//                     SqlTransaction transaction = null;
//                     connection.Open();
//                     try
//                     {
//                         transaction = connection.BeginTransaction( "AqualogyOPCServiceSaveEvents" );
//                         List<OPCTag> need_resend = new List<OPCTag>();
// 
//                         String sql_query = "INSERT INTO T_AL_ENVIOS ( dFechaHora, nIdNotificacion, sCliente, sCanal, sAsunto, sTextoEmail, sTextoSms, sEstado, dFechaEstado, bEmail, bSms, nReintentos ) " +
//                                                  " VALUES ( @p_dFechaHora, @p_nIdNotificacion, @p_sCliente, @p_sCanal, @p_sAsunto, @p_sTextoEmail, @p_sTextoSms, @p_sEstado, " +
//                                                            "@p_dFechaEstado, @p_bEmail, @p_bSms, @p_nReintentos )";
// 
//                         Int32 reintentosDefault = 0;
// 
//                         using( SqlCommand cmd = new SqlCommand( sql_query, connection, transaction ) )
//                         {
//                             foreach( DataToSend item in datatosend )
//                             {
//                                 String channel_mail = item.GetEMails();
//                                 String channel_sms = item.GetPhones();
// 
//                                 // mark NEED SEND
//                                 if( String.IsNullOrWhiteSpace( channel_mail ) && String.IsNullOrWhiteSpace( channel_sms ) )
//                                 {
//                                     //iMarkNeedSendAlarms.MarkNeedSend( item.Tag );
//                                     need_resend.Add( item.Tag );
//                                 }
// 
//                                 // EMail                                
//                                 if( String.IsNullOrWhiteSpace( channel_mail ) == false )
//                                 {
//                                     cmd.Parameters.Clear();
// 
//                                     cmd.Parameters.Add( new SqlParameter( "@p_dFechaHora", item.AlarmTime ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_nIdNotificacion", 1 ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sCliente", item.Client ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sCanal", channel_mail ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sAsunto", item.ShortMessage ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sTextoEmail", item.LargeMessage ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sTextoSms", "" ) ); // (Object) DBNull.Value ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sEstado", "PENDIENTE" ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_dFechaEstado", DBNull.Value ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_bEmail", true ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_bSms", false ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_nReintentos", reintentosDefault ) );
//                                     cmd.ExecuteScalar();
//                                 }
// 
//                                 // SMS                                
//                                 if( String.IsNullOrWhiteSpace( channel_sms ) == false )
//                                 {
//                                     cmd.Parameters.Clear();
// 
//                                     cmd.Parameters.Add( new SqlParameter( "@p_dFechaHora", item.AlarmTime ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_nIdNotificacion", 1 ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sCliente", item.Client ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sCanal", channel_sms ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sAsunto", "" ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sTextoEmail", "" ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sTextoSms", item.LargeMessage ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_sEstado", ( item.SMSChannelEnable ? "PENDIENTE" : "ERROR" ) ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_dFechaEstado", DBNull.Value ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_bEmail", false ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_bSms", true ) );
//                                     cmd.Parameters.Add( new SqlParameter( "@p_nReintentos", reintentosDefault ) );
//                                     cmd.ExecuteScalar();
//                                 }
//                             }
//                         }
// 
//                         transaction.Commit();
// 
//                         foreach( var tag_need_send in need_resend )
//                         {
//                             iMarkNeedSendAlarms.MarkNeedSend( tag_need_send );
//                         }
// 
//                         return true;
// 
//                     }
//                     catch( Exception ex )
//                     {
//                         try
//                         {
//                             transaction.Rollback();
//                         }
//                         catch( Exception ex_rollback )
//                         {
//                             this._IOPCInfo.Info( ex_rollback );
//                         }
//                         this._IOPCInfo.Info( ex );
//                     }
//                 }
//             }
//             catch( Exception ex )
//             {
//                 this._IOPCInfo.Info( ex );
//             }

            return false;
        }

        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                // Free any other managed objects here. 
                if( this._IEventStatistic != null )
                {
                    IDisposable idisp = this._IEventStatistic as IDisposable;
                    if( idisp != null )
                    {
                        idisp.Dispose();
                    }
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion


    }
}
