﻿using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Model;
using AqualogyOPCKernel.Model.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;

namespace AqualogyOPCKernel.MessageBuffer
{
    class SQLiteMessageBuffer:
            IMessageBuffer
    {        
        SQLiteConnection _SQLiteConnection = null;

        public SQLiteMessageBuffer( SQLiteConnection connection )
        {
            this._SQLiteConnection = connection;            
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateTable()
        {
            using( SQLiteCommand command = new SQLiteCommand( this._SQLiteConnection ) )
            {
                    command.CommandText = @"CREATE TABLE [Messages] ("+
                                                "[id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, "+
                                                "[SaveDate] datetime NOT NULL, " +
                                                "[dFechaHora] datetime NOT NULL, " +
                                                "[TagId] [int] NOT NULL, "+
	                                            "[ConfigId] [int] NOT NULL )";

                    command.CommandType = CommandType.Text;
                    Int32 result = command.ExecuteNonQuery();
            }
        }

#region interface IMessageBuffer

        /// <summary>
        /// 
        /// </summary>
        public Boolean IsEmpty
        {
            get
            {
                return ( GetBufferSize() == 0 );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newMessages"></param>
        public void SaveToBuffer( IEnumerable<TagAlarmInfo> newAlarms )
        {
            String sql_query = "INSERT INTO [Messages] ( [SaveDate], [dFechaHora], [TagId], [ConfigId] ) VALUES( :pSaveDate, :pFechaHora, :pTagId, :pConfigId )";
            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                foreach( var alarm in newAlarms )
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add( new SQLiteParameter( "pSaveDate", (Object) DateTime.Now ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pFechaHora", (Object) alarm.AlarmTime ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pTagId", (Object)alarm.Tag.UniqueKey.Id ) );
                    cmd.Parameters.Add( new SQLiteParameter( "pConfigId", (Object)alarm.Tag.UniqueKey.ConfigId ) );

                    Int32 r = cmd.ExecuteNonQuery();
                }
            }

        }

        /// <summary>
        /// Clear buffer
        /// </summary>
        public void Clear()
        {
            if( SQLiteEx.SQLiteEx.TableExist( _SQLiteConnection, "Messages" ) )
            {
                SQLiteEx.SQLiteEx.ClearTable( _SQLiteConnection, "Messages" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<TagAlarmInfo> Messages( ITags iTags )
        {
            if( false == SQLiteEx.SQLiteEx.TableExist( _SQLiteConnection, "Messages" ) )
            {
                CreateTable();
            }

            String sql_query = "SELECT [id], [SaveDate], [dFechaHora], [TagId], [ConfigId] FROM [Messages]";
            
            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    while( reader.Read() )
                    {
                        Int32 tag_id = decimal.ToInt32( reader.GetDecimal( 3 ) );
                        Int32 config_id = decimal.ToInt32( reader.GetDecimal( 4 ) );
                        OPCTag tag = iTags.FindTag( new OPCTagUniqueKey( tag_id, config_id ) );
                        if( tag != null )
                        {
                            DateTime time = reader.GetDateTime( 2 );
                            yield return new TagAlarmInfo( tag, time, DateTime.Now );                        
                        }
                    }
                }
            }
        }
        #endregion

        private Int32 GetBufferSize()
        {
            Int32 result = 0;

            if( false == SQLiteEx.SQLiteEx.TableExist( _SQLiteConnection, "Messages" ) )
            {
                CreateTable();
                return 0;
            }


            using( SQLiteCommand command = new SQLiteCommand( this._SQLiteConnection ) )
            {
                command.CommandText = @"SELECT Count(*) FROM [Messages]";

                command.CommandType = CommandType.Text;
                using( SQLiteDataReader reader = command.ExecuteReader() )
                {
                    result = decimal.ToInt32( reader.GetDecimal( 0 ) );                    
                }
            }

            return result;
        }

    }
}
