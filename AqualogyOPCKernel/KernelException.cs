﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel
{
    public class KernelException:
        Exception
    {
        public KernelException( String message ) :
                base( message )
        {

        }
    }
}
