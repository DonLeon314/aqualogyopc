﻿using AqualogyOPCKernel.Events;
using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.MailSender;
using AqualogyOPCKernel.Model;
using OPCConfigModel;
using OPCConfigModel.ExportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.HotConfig
{
    /// <summary>
    /// Hot Config Storage
    /// </summary>
    public class HotConfigStorage:
              IHotConfig
    {
        private OPCHotGroupsCollection _HotGroups = new OPCHotGroupsCollection();
        private OPCHotUserCollection _HotUsers = new OPCHotUserCollection();

        private SMTPSender _SMTPSender = null;


        private DateTime? _SaversActual = null;
        private DateTime? _UsersGroupsActual = null;

        private List<EventSaverBase> _EventSavers = new List<EventSaverBase>();

        private IOPCInfo _IOPCInfo = null;

        public HotConfigStorage( IOPCInfo iOPCInfo )
        {
            this._IOPCInfo = iOPCInfo;
        }

        #region IHotConfig


        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        public IEnumerable<OPCUser> GetUsers( IEnumerable<Int32> groupIds )
        {
            System.Diagnostics.Debug.WriteLine( "GetUSers" );

            ActualizeGroupsAndUsers();

            foreach( Int32 group_id in groupIds )
            {
                if( this._HotGroups.Contains( group_id ) )
                {
                    OPCHotGroup group = this._HotGroups[group_id];
                    foreach( Int32 user_id in group.UserIds )
                    {
                        if( this._HotUsers.Contains( user_id ) )
                        {
                            yield return this._HotUsers[user_id];
                        }
                        else
                        {
                            if( this._IOPCInfo != null )
                            {
                                this._IOPCInfo.Info( String.Format( "User with ID:{0} not found!", user_id ) );
                            }
                        }
                    }
                }
                else
                {
                    if( this._IOPCInfo != null )
                    {
                        this._IOPCInfo.Info( String.Format( "Group with ID:{0} not found!", group_id ) );
                    }
                }
            }
        }


        /// <summary>
        /// Salto history time
        /// </summary>
        public Int32 SaltoHistoryTime
        {
            get
            {
                Int32 result = 20;
                try
                {
                    using( OPCConfigContext dbContext = new OPCConfigContext() )
                    {
                        result = dbContext.GetParameter("SALTO", "HistoryTime", result );
                    }


                } catch( Exception ex )
                {
                    //TODO: message                    
                }

                return result;
            }
        }

        /// <summary>
        /// SALTO period
        /// </summary>
        public Int32 SaltoPeriod
        {
            get
            {
                Int32 result = 5;

                try
                {
                    using( OPCConfigContext dbContext = new OPCConfigContext() )
                    {
                        result = dbContext.GetParameter( "SALTO", "SaltoPeriod", result );
                    }

                } catch( Exception ex )
                {
                   //TODO: message                         
                }

                return result;
            }
        }


        public SMTPSender SMTPSender
        {
            //TODO: optimize
            get
            {
                SMTPConfig smtp_cfg = SMTPConfig.CreateFromBBDD();
                               

                this._SMTPSender = new SMTPSender( smtp_cfg );


                return this._SMTPSender;

            }
        }

        /// <summary>
        /// Hot Savers config 
        /// </summary>
        public IReadOnlyCollection<EventSaverBase> Savers
        {
            get
            {
                Boolean load = false;
                try
                {
                    using( OPCConfigContext dbContext = new OPCConfigContext() )
                    {
                        var transaction = dbContext.Database.BeginTransaction();
                        try
                        {
                            DateTime? bbdd_time = LoadTimeMark( dbContext, "SALTOS", "LastModified" );

                            if( this._SaversActual.HasValue )
                            {
                                if( bbdd_time.HasValue )
                                {
                                    load = ( this._SaversActual.Value < bbdd_time.Value );
                                }
                            }
                            else
                            {
                                load = true;
                            }

                            if( load )
                            {
                                if( this._IOPCInfo != null )
                                {
                                    this._IOPCInfo.Info( String.Format( "Load SALTO storages info (BBDD last save:{0})", 
                                                                        ( bbdd_time.HasValue ? bbdd_time.ToString() : "NULL" ) ),
                                                         eInfoType.Message );
                                }
                                ReloadBBDDEventSavers( dbContext );
                                this._SaversActual = ( bbdd_time.HasValue ? bbdd_time : DateTime.Now );
                            }

                            transaction.Commit();

                        }catch( Exception ex )
                        {
                            transaction.Rollback();
                            if( this._IOPCInfo != null )
                            {
                                this._IOPCInfo.Info( ex );
                            }
                        }
                    }
                } catch( Exception ex )
                {
                    if( this._IOPCInfo != null )
                    {
                        this._IOPCInfo.Info( ex );
                    }
                }

                return this._EventSavers;
            }
        }

        #endregion

        private DateTime? LoadTimeMark( OPCConfigContext dbContext, String section, String parameterName )
        {
            DateTime? result = null;

             // get ticks actual bbdd
             Int64 bbdd_ticks = dbContext.GetParameter( section, parameterName, (long)0 );

             if( bbdd_ticks != 0 )
             {   // Compare                        
                 result = new DateTime( bbdd_ticks );                   
             }

            return result;
        }

        private Boolean ReloadBBDDEventSavers( OPCConfigContext dbContext )
        {
            
            try
            {
                 List<BBDDEventSaver> savers = new List<BBDDEventSaver>();
                 foreach( var cfg in dbContext.EventSaverConfigs )
                 {
                     BBDDEventSaverFinder finder = new BBDDEventSaverFinder( cfg.ConnectionString, cfg.TableName );
                     BBDDEventSaver event_saver = savers.Find( finder.ConnectionStringComparer );
 
                     if( event_saver == null )
                     {
                         savers.Add( BBDDEventSaver.BBDDEventSaverCreate( cfg ) );
                     }
                 }

                this._EventSavers.Clear();
                 this._EventSavers.AddRange( savers );
 
                 if( this._EventSavers.Count > 0 )
                 {
                     this._EventSavers.Add( new BBDDSaverLastDummy() );
                 }

                 // Sort storages
                 this._EventSavers.Sort( ( saver1, saver2 ) =>
                 {
                     return saver1.Priority.CompareTo( saver2.Priority );
                 } );
 
                 return true; // OK! :)

            }catch( Exception ex )
            {
                //this._IOPCInfo.Info( ex );
            }
 
            return false; // Error! :(
        }


        /// <summary>
        /// Check User and Groups actual
        /// </summary>
        public void ActualizeGroupsAndUsers()
        {
            Boolean load = false;
            try
            {
                using( OPCConfigContext dbContext = new OPCConfigContext() )
                {
                    var transaction = dbContext.Database.BeginTransaction();
                    try
                    {
                        DateTime? bbdd_time = LoadTimeMark( dbContext, "USERS_GROUPS", "LastModified" );

                        if( this._UsersGroupsActual.HasValue )
                        {
                            if( bbdd_time.HasValue )
                            {
                                load = ( this._UsersGroupsActual.Value < bbdd_time.Value );
                            }
                        }
                        else
                        {
                            load = true;
                        }

                        if( load )
                        {
                            if( this._IOPCInfo != null )
                            {
                                this._IOPCInfo.Info( String.Format( "Load Users and Groups (BBDD last save:{0})",
                                                                    ( bbdd_time.HasValue ? bbdd_time.ToString() : "NULL" ) ),
                                                         eInfoType.Message );
                            }
                            ReloadUsersAndGroups( dbContext );
                            this._UsersGroupsActual = ( bbdd_time.HasValue ? bbdd_time : DateTime.Now );
                        }

                        transaction.Commit();

                    } catch( Exception ex )
                    {
                        transaction.Rollback();
                        if( this._IOPCInfo != null )
                        {
                            this._IOPCInfo.Info( ex );
                        }
                    }
                }
            } catch( Exception ex )
            {
                if( this._IOPCInfo != null )
                {
                    this._IOPCInfo.Info( ex );
                }
            }
        }            

        /// <summary>
        /// Reload Groups and users
        /// </summary>
        /// <param name="dbContext"></param>
        private void ReloadUsersAndGroups( OPCConfigContext dbContext )
        {
            // Reload group
            this._HotGroups.Clear();

            foreach( OPCConfigModel.Model.Group group in dbContext.Groups.Include( "Users" ).ToList() )
            {
                OPCHotGroup new_group = OPCHotGroup.CreateFromBBDD( group );
                if( new_group != null )
                {
                    this._HotGroups.Add( new_group );
                }
            }

            // Reload users
            this._HotUsers.Clear();

            foreach( OPCConfigModel.Model.User user in dbContext.Users.Include("Phones").Include("EMails").Include("WorkTimes").ToList() )
            {
                OPCUser new_user = OPCUser.CreateFromBBDD( user );
                if( new_user != null )
                {
                    this._HotUsers.Add( new_user );   
                }
            }
        }
    }
}
