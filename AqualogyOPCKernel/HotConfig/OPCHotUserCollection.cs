﻿using AqualogyOPCKernel.Model;
using System;
using System.Collections.ObjectModel;

namespace AqualogyOPCKernel.HotConfig
{
    /// <summary>
    /// Collection for users
    /// </summary>
    class OPCHotUserCollection:
          KeyedCollection<Int32,OPCUser>  
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OPCHotUserCollection()
        {

        }

        /// <summary>
        /// Get user key
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Int32 GetKeyForItem( OPCUser item )
        {
            return item.Id;
        }
    }
}
