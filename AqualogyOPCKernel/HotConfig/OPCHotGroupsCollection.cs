﻿using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.HotConfig
{
    /// <summary>
    /// Collection ffor groups
    /// </summary>
    public class OPCHotGroupsCollection:
            KeyedCollection<Int32, OPCHotGroup>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OPCHotGroupsCollection()
        {

        }

        /// <summary>
        /// Get item key
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override Int32 GetKeyForItem( OPCHotGroup item )
        {
            return item.Id;
        }
    }
}
