﻿using AqualogyOPCKernel.Events;
using AqualogyOPCKernel.MailSender;
using AqualogyOPCKernel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.HotConfig
{
    public interface IHotConfig
    {
        Int32 SaltoHistoryTime
        {
            get;
        }

        Int32 SaltoPeriod
        {
            get;
        }

        SMTPSender SMTPSender
        {
            get;
        }

        IReadOnlyCollection<EventSaverBase> Savers
        {
            get;
        }

        IEnumerable<OPCUser> GetUsers( IEnumerable<Int32> groupIds );
    }
}
