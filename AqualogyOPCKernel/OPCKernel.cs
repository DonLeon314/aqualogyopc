﻿using AqualogyOPCKernel.Interfaces;
using AqualogyOPCKernel.Model;
using AqualogyOPCKernel.OPCUAClient;
using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using AqualogyOPCKernel.Config;
using System.ServiceModel;
using System.Linq;
using AqualogyOPCKernel.MessageBuffer;
using AqualogyOPCKernel.Model.Utils;
using AqualogyOPCKernel.Model.Collections;
using AqualogyOPCRestService;

namespace AqualogyOPCKernel
{
    [ServiceBehavior( InstanceContextMode = InstanceContextMode.Single )]
    public class OPCKernel:
            IOPCUnsubscribe,
            IOPCSubscribe,
            IOPCDataValues,            
            ITags
    {
        private IOPCInfo _IOPCInfo = null;
        private OPCConfigurationKeyCollection _Configurations = new OPCConfigurationKeyCollection();

        private OPCUAClientKeyedCollection _Clientes = new OPCUAClientKeyedCollection();

        private Thread _Thread = null;
        private ManualResetEvent _StopEvent = new ManualResetEvent( false );
        private ManualResetEvent _ResumeEvent = new ManualResetEvent( false );

        private List<TagDataValue> _NewValues = new List<TagDataValue>();
        private Object _SyncObject = new Object();

        private Int32 _ProcessDetect = 0;
        private Int32 _SmsLimit = 0;
        private Int32 _ActualState = 10; // 10 min


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOPCInfo"></param>
        /// <param name="processDetect"></param>
        private OPCKernel( IOPCInfo iOPCInfo, Int32 processDetect, Int32 smsLimit, Int32 actualState )
        {
            this._IOPCInfo = iOPCInfo;
            this._ProcessDetect = processDetect;
            this._SmsLimit = smsLimit;
            this._ActualState = actualState;
        }


        #region IAqualogyOPCRestService

        public AqualogyOPCConfigorationInfo[] GetConfigurations()
        {
            Dictionary<Int32,AqualogyOPCConfigorationInfo> result = new Dictionary<Int32, AqualogyOPCConfigorationInfo>();

            try
            {

                // Load all configs
                using( OPCConfigContext dbContext = new OPCConfigContext() )
                {
                    foreach( var cfg in dbContext.OPCConfigs.Where( c => c.Visible ).ToList() )
                    {
                        if( result.ContainsKey( cfg.Id ) == false ) // 8)
                        {
                                result.Add( cfg.Id, new AqualogyOPCConfigorationInfo( cfg.Id, cfg.Name, false ) );
                        }
                    }
                }

                // Set active
                foreach( var cfg in this._Configurations )
                {
                    if( result.ContainsKey( cfg.Id ) )
                    {
                        result[cfg.Id].State = true;
                    }
                    else
                    {
                        result.Add( cfg.Id, new AqualogyOPCConfigorationInfo( cfg.Id, cfg.Name, true ) );
                    }
                }

                return result.Values.ToArray();

            } catch( Exception ex )
            {   
                //TODO: return error
                this._IOPCInfo.Info( ex );
            }

            return null;
        }

        /// <summary>
        /// Stop OPC processing
        /// </summary>
        /// <returns></returns>
        public Boolean StopProcessing()
        {         
            foreach( var cfg in this._Configurations )
            {
                if( cfg.Active )
                {
                    this._IOPCInfo.Info( String.Format( "Configuration:'{0}' unsubscribe...", cfg.Name ) );
                    cfg.Unsubscribe( this );
                    this._IOPCInfo.Info( String.Format( "Configuration:'{0}' unsubscribe - OK", cfg.Name ) );
                }
            }

            this._NewValues.Clear();

            this._StopEvent.Set();

            this._Thread.Join();
          
            return true;
        }

        /// <summary>
        /// Start OPC processing
        /// </summary>
        /// <returns></returns>
        public Boolean StartProcessing()
        {
            //TODO: WARNING!
            this._NewValues.Clear();
            this._Clientes.Clear();

            StartSubscribe();

            StartThread();

            return true;
        }


        #endregion

        #region interface ITags
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagKey"></param>
        /// <returns></returns>
        public OPCTag FindTag( OPCTagUniqueKey tagKey )
        {
            foreach( var config in this._Configurations )
            {
                if( config.Id == tagKey.ConfigId )
                {
                    return config.FindTag( tagKey );
                }
            }
            return null;
        }
        #endregion


        #region interface IOPCDataValues

        void IOPCDataValues.AddDataValues( IEnumerable<TagDataValue> values )
        {
            lock( this._SyncObject )
            {
                this._NewValues.AddRange( values );
            }

            //this._IOPCInfo.Info( "ResumeEvent Set", eInfoType.Debug );
            this._ResumeEvent.Set();
        }

        #endregion

        #region interface IOPCSubscribe
        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlServer"></param>
        /// <param name="tag"></param>
        void IOPCSubscribe.Subscribe( IOPCUAServerInfo serverInfo, IEnumerable<OPCTag> tags )
        {
            String server_url = serverInfo.HostUrl.Trim();
            
            AqualogyOPCKernel.OPCUAClient.OPCUAClient client = null;

            if( this._Clientes.Contains( server_url ) )
            {
                client = this._Clientes[server_url];
            }
            else
            {
                client = new OPCUAClient.OPCUAClient(server_url, this, 
                                                     serverInfo.Timeout, 
                                                     serverInfo.PublishingInterval, 
                                                     serverInfo.SamplingInterval, serverInfo.QueueSize, this._IOPCInfo );
                this._Clientes.Add( client );
            }

            List<MonitoredItemEx> monitors = new List<MonitoredItemEx>();

            foreach( var tag in tags )
            {
                MonitoredItemEx monitor = new MonitoredItemEx( tag, serverInfo.AlarmDetacted )
                {
                    DisplayName = tag.TagId,                    
                    SamplingInterval = serverInfo.SamplingInterval,
                    QueueSize = serverInfo.QueueSize                    
                };
                monitors.Add( monitor );
            }

            client.WatchMonitors( monitors );
        }
        #endregion

        #region interface IOPCUnsubscribe
        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlServer"></param>
        /// <param name="tag"></param>
        void IOPCUnsubscribe.Unsubscribe( IOPCUAServerInfo serverInfo, IEnumerable<OPCTag> tags )
        {
            String server_url = serverInfo.HostUrl.Trim();

            if( this._Clientes.Contains( server_url ) )
            {
                AqualogyOPCKernel.OPCUAClient.OPCUAClient client = this._Clientes[server_url];
                client.UnwatchTags( tags );
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private void StartThread()
        {
            _Thread = new Thread( () =>
            {
                this.ProcessRead();
            } );

            this._Thread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ProcessRead()
        {
            try
            {
                this._IOPCInfo.Info( "Start OPC messages processor...", eInfoType.Debug );
                ManualResetEvent[] events = new ManualResetEvent[] { this._StopEvent, this._ResumeEvent };

                using( LocalDBConfig stateCfg = new LocalDBConfig( "AqualogyOPCServiceState.db3" ) )
                {

                    stateCfg.Initialize();

                    using( LocalDBConfig localCfg = new LocalDBConfig( "AqualogyOPCServiceBuffer.db3" ) )
                    {
                        localCfg.Initialize();

                        using( AqualogyOPCKernel.AlarmSaver.MSSQLAlarmSaver saver = new AqualogyOPCKernel.AlarmSaver.MSSQLAlarmSaver(
                                                                                        this._IOPCInfo, this._SmsLimit, stateCfg.SQLiteConnection ) )
                        {
                            SQLiteMessageBuffer buffer = new SQLiteMessageBuffer( localCfg.SQLiteConnection );

                            AlarmProcessorsStorageV2 processors = new AlarmProcessorsStorageV2( stateCfg.SQLiteConnection, this._ActualState, this._IOPCInfo );

                            while( true )
                            {
                                List<TagAlarmInfo> all_alarms = null;
                                IEnumerable<TagAlarmInfo> alarms = null;

                                try
                                {

                                    processors.ProcessNewValues( ProcessNewValues() );

                                    all_alarms = new List<TagAlarmInfo>();

                                    //all_alarms.AddRange( buffer.Messages( this ) );

                                    alarms = processors.DetectAlarmas();

                                }
                                catch( Exception ex )
                                {
                                    this._IOPCInfo.Info( ex );
                                }

                                if( alarms != null || all_alarms != null )
                                {

                                    if( alarms != null )
                                    {
                                        all_alarms.AddRange( alarms );
                                    }

                                    Boolean save_result = saver.Save( all_alarms, processors );

//                                     if( false == save_result && alarms != null )
//                                     {
//                                         buffer.SaveToBuffer( alarms );
//                                     }

                                    if( save_result )
                                    {
                                        buffer.Clear();
                                    }
                                }
                                // this._IOPCInfo.Info( String.Format( "OPCMessage processor wait {0} sec. ...{1}", this._ProcessDetect, DateTime.Now ),
                                //             eInfoType.Debug );
                                Int32 wait_result = WaitHandle.WaitAny( events, this._ProcessDetect * 1000 );

                              ///  Debug.WriteLine( String.Format( "--->>{0}", wait_result ) );

                                if( wait_result == 0 )
                                {
                                    //this._IOPCInfo.Info( "OPCMessage processor exit...", eInfoType.Debug );
                                    break;
                                }
                                else
                                {
                                    //this._IOPCInfo.Info( String.Format("OPCMessage processor resume Time:{0}", DateTime.Now ), eInfoType.Debug );
                                    this._ResumeEvent.Reset();
                                }
                            };
                        }
                    }
                }                    
            }
            catch( Exception ex )
            {
                this._IOPCInfo.Info( ex );
            }

            this._IOPCInfo.Info( "OPCMessage processor stop", eInfoType.Message );
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<TagDataValue> ProcessNewValues()
        {
            List<TagDataValue> result = new List<TagDataValue>();

            lock( this._SyncObject )
            {
                result.AddRange( this._NewValues );
                this._NewValues.Clear();
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private void StartSubscribe()
        {

            foreach( var cfg in this._Configurations )
            {   
                if( cfg.Active )
                {
                    this._IOPCInfo.Info( String.Format( "Configuration:'{0}' subscribe...", cfg.Name ) );
                    cfg.Subscribe( this );
                    this._IOPCInfo.Info( String.Format( "Configuration:'{0}' subscribe - OK", cfg.Name ) );
                }
            }
        }

        private void StopSubscribe( Int32 configId )
        {
            foreach( var cfg in this._Configurations )
            {
                if( cfg.Id == configId )
                {
                    if( cfg.Active )
                    {                        
                        cfg.Unsubscribe( this );
                        cfg.Active = false;
                        SaveConfigState( configId, false );
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configId"></param>
        /// <param name="activeState"></param>
        private void SaveConfigState( Int32 configId, Boolean activeState )
        {
            using( OPCConfigContext context = new OPCConfigContext() )
            {
                OPCConfigModel.Model.Config bbdd_config = context.OPCConfigs.Where( c => c.Id == configId ).FirstOrDefault();
                if( bbdd_config != null )
                {
                    bbdd_config.Active = activeState;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configId"></param>
        private void StartSubscribe( Int32 configId )
        {
            foreach( var cfg in this._Configurations )
            {
                if( cfg.Id == configId )
                {
                    if( cfg.Active == false )
                    {                        
                        cfg.Subscribe( this );
                        cfg.Active = true;
                        SaveConfigState( configId, true );
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CloseClientes()
        {
            foreach( var client in this._Clientes )
            {
                client.Close();                
            }

            this._Clientes.Clear();            
        }

        /// <summary>
        /// Add new Configuration
        /// </summary>
        /// <param name="newConfiguration"></param>
        private void AddConfiguration( OPCConfiguration newConfiguration )
        {
            if( this._Configurations.Contains( newConfiguration.Name ) )
            {
                throw new KernelException( String.Format( "Configuration '{0}' exist in OPCKernel", newConfiguration.Name ) );
            }

            this._Configurations.Add( newConfiguration );

        }

        /// <summary>
        /// Kernel creator
        /// </summary>
        /// <param name="xmlFileConfig"></param>
        /// <param name="iOPCInfo"></param>
        /// <returns></returns>
        public static OPCKernel CreateKernelFromBBDD( AqualogyOPCServiceConfig config, IOPCInfo iOPCInfo )
        {           

            OPCKernel kernel = new OPCKernel( iOPCInfo, config.ProcessAlarmDetect, config.SmsLimit, config.ActualState );

            Object sync_obj = new object();

            using( OPCConfigContext context = new OPCConfigContext() )
            {
                foreach( OPCConfigModel.Model.Config config_entity in context.OPCConfigs.Include("Groups").ToList() )
                {
                    if( config_entity.Visible == false || config_entity.Active == false )
                    {
                        continue;
                    }

                    OPCConfiguration opc_config = OPCConfiguration.CreateFromBBDD( context, config_entity, iOPCInfo, sync_obj );

                    if( opc_config != null )
                    {
                        kernel.AddConfiguration( opc_config );
                    }
                }
            }

            return kernel;
        }

        /// <summary>
        /// Get all available configurations
        /// </summary>
        /// <returns></returns>
        public static AqualogyOPCConfigorationInfo[] AvaulableConfigs()
        {
            List<AqualogyOPCConfigorationInfo> result = new List<AqualogyOPCConfigorationInfo>();

            using( OPCConfigContext dbContext = new OPCConfigContext() )
            {
                foreach( var cfg in dbContext.OPCConfigs.Where( c => c.Visible ).ToList() )
                {
                    result.Add( new AqualogyOPCConfigorationInfo( cfg.Id, cfg.Name, false ) ); 
                }
            }

            return result.ToArray();
        }
    }
}
