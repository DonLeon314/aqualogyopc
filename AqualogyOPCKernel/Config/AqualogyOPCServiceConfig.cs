﻿using AqualogyOPCKernel.Interfaces;
using OPCConfigModel;
using System;
using System.Collections.Generic;
using System.Xml;

namespace AqualogyOPCKernel.Config
{
    public class AqualogyOPCServiceConfig:
                IOPCInfoProcess
    {        
        private static Int32 DefaultProcessAlarmDetect = 1;

        private HashSet<eInfoType> _Events = new HashSet<eInfoType>();


        /// <summary>
        /// time wait process alarm detect
        /// </summary>
        public Int32 ProcessAlarmDetect
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 SmsLimit
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 ActualState
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String HttpServicePoint
        {
            get;
            private set;
        }


        public Boolean AutoStartProcessing
        {
            get;
            private set;
        }

        public Int32 MaxLog
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        private AqualogyOPCServiceConfig()
        {
        
            this.ProcessAlarmDetect = DefaultProcessAlarmDetect;

            this._Events.Add( eInfoType.Debug );
            this._Events.Add( eInfoType.Error );
            this._Events.Add( eInfoType.Message );

            this.AutoStartProcessing = true;
        }

        #region interface IOPCInfoProcess
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Boolean ProcessInfo( eInfoType type )
        {
            return ( this._Events.Contains( type ) );
        }

        public Int32 MaxLogFile()
        {
            return this.MaxLog;
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public static AqualogyOPCServiceConfig Create()
        {

            AqualogyOPCServiceConfig cfg = new AqualogyOPCServiceConfig();

            using( OPCConfigContext db = new OPCConfigContext() )
            {
                cfg.LoadEvents( db );
                cfg.LoadOPCClient( db );                
            }            
            
            
            return cfg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void LoadOPCClient( OPCConfigContext dbContext )
        {

            String section = "ServiceParams";

            this.ProcessAlarmDetect = dbContext.GetParameter( section, "ProcessDetected", 10 );

            this.SmsLimit = dbContext.GetParameter( section, "SmsLimit", 5000 );

            if( this.SmsLimit == 0 )
            {
                this.SmsLimit = -1;
            }


            this.HttpServicePoint = dbContext.GetParameter( section, "UrlService", @"//localhost:9001" );

            this.ActualState = dbContext.GetParameter( section, "ActualState", 10 );

            this.MaxLog = dbContext.GetParameter( section, "MaxLog", 20 );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        private void LoadEvents( OPCConfigContext dbContext )
        {
            String section = "ServiceParams";

            Boolean debugs = dbContext.GetParameter( section, "LogDebug", true );
            Boolean errors = dbContext.GetParameter( section, "LogError", true );
            Boolean messages = dbContext.GetParameter( section, "LogMessage", true );
            Boolean gods = dbContext.GetParameter( section, "Gods", false );

            HashSet<eInfoType> events = new HashSet<eInfoType>();

            if( debugs )
            {
                events.Add( eInfoType.Debug );
            }

            if( errors )
            {
                events.Add( eInfoType.Error );
            }

            if( messages )
            {
                events.Add( eInfoType.Message );
            }

            if( messages )
            {
                events.Add( eInfoType.God );
            }                       

            if( events.Count > 0 )
            {
                this._Events = events;
            }
        } 

    }
}
