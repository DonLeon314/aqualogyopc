﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Config
{
    internal class SMSLimitCounter
    {
        private Int32 _SmsCounter = -1;
        private Int32 _SmsLimit = -1;
        private String _SmsLimitTableName = "SmsLimit";
        private SQLiteConnection _SQLiteConnection = null;


        public SMSLimitCounter( Int32 smsLimit, SQLiteConnection sqliteConnection )
        {
            this._SmsLimit = ( smsLimit == 0 ? -1 : smsLimit );
            this._SQLiteConnection = sqliteConnection;
        }

        private void CreateTable_SmsLimit()
        {
            using( SQLiteCommand command = new SQLiteCommand( this._SQLiteConnection ) )
            {
                command.CommandText = String.Format( @"CREATE TABLE [{0}] (" +
                                                            "[id] integer PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                                                            "[SmsCounter] int NOT NULL, " +
                                                            "[CounterDate] datetime NOT NULL )", this._SmsLimitTableName );


                command.CommandType = CommandType.Text;
                Int32 result = command.ExecuteNonQuery();
            }
        }

        public void RestoreCounter()
        {
            if( this._SmsLimit == -1 )
            {
                this._SmsCounter = -1;
                return;
            }

            if( false == SQLiteEx.SQLiteEx.TableExist( this._SQLiteConnection, this._SmsLimitTableName ) )
            {
                CreateTable_SmsLimit();
            }

            Tuple<DateTime, Int32> current = LoadSmsLimit();

            if( current == null )
            {
                this._SmsCounter = this._SmsLimit;
            }
            else
            {
                if( current.Item1.Date != DateTime.Now.Date )
                {
                    this._SmsCounter = this._SmsLimit;
                }
                else
                {
                    this._SmsCounter = current.Item2;
                }
            }
        }

        public void SaveCounter()
        {
            Save( DateTime.Now, this._SmsCounter );            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean DecrementSms()
        {

            if( this._SmsLimit == -1 )
            {
                return true;
            }

            Boolean result = ( this._SmsCounter > 0 );
            if( this._SmsCounter > 0 )
            {
                this._SmsCounter--;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="result"></param>
        private void Save( DateTime date, Int32 counter )
        {
            SQLiteEx.SQLiteEx.ClearTable( this._SQLiteConnection, this._SmsLimitTableName );

            String sql_query = String.Format( @"INSERT INTO {0} (SmsCounter,CounterDate) VALUES( :pSmsCounter, :pCounterDate )", this._SmsLimitTableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                cmd.Parameters.Add( new SQLiteParameter( "pSmsCounter", (Object) counter ) );
                cmd.Parameters.Add( new SQLiteParameter( "pCounterDate", (Object) date ) );

                Int32 r = cmd.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Tuple<DateTime, Int32> LoadSmsLimit()
        {
            String sql_query = String.Format( @"SELECT SmsCounter, CounterDate FROM {0}", this._SmsLimitTableName );

            using( SQLiteCommand cmd = new SQLiteCommand( sql_query, this._SQLiteConnection ) )
            {
                using( SQLiteDataReader reader = cmd.ExecuteReader() )
                {
                    if( reader.Read() )
                    {
                        Int32 counter = decimal.ToInt32( reader.GetDecimal( 0 ) );
                        DateTime date = reader.GetDateTime( 1 );

                        return new Tuple<DateTime, Int32>( date, counter );
                    }
                }
            }

            return null;
        }
    }
}
