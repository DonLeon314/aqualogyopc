﻿using System;

namespace AqualogyOPCKernel.Config
{
    public class ConfigurationConfig
    {
        public String Name
        {
            get;
            private set;
        }

        public uint Timeout
        {
            get;
            private set;
        }

        public Int32 AlarmDetected
        {
            get;
            private set;
        }

        public Int32 PublishingInterval
        {
            get;
            private set;
        }

        public Int32 SamplingInterval
        {
            get;
            private set;
        }

        public uint QueueSize
        {
            get;
            private set;
        }

        public ConfigurationConfig( String name, uint timeout, Int32 alarmDetected, Int32 publishingInterval, Int32 samplingInterval, uint queueSize )
        {
            this.Name = name;
            this.AlarmDetected = ( alarmDetected <= 3 ? 3 : alarmDetected );
            this.PublishingInterval = ( publishingInterval <= 2 ? 2 : publishingInterval );
            this.SamplingInterval = ( samplingInterval <= 2 ? 2 : samplingInterval );
            this.QueueSize = ( queueSize < 1 ? 1 : queueSize );
            this.Timeout = ( timeout < 60 ? 60 : timeout );
        }
    }
}
