﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCKernel.Config
{
    internal class LocalDBConfig:
            IDisposable
    {           
        private String _DBFileName = String.Empty;

        public SQLiteConnection SQLiteConnection
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbFileName"></param>
        public LocalDBConfig( String dbFileName = null )
        {
            this._DBFileName = dbFileName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqliteConnection"></param>
        public void Initialize()
        {
            
            String fname = SQLiteDBFileName( this._DBFileName );

            if( false == File.Exists( fname ) )
            {
                SQLiteCreateDB( fname );
            }

            SQLiteOpenDB( fname );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameDB"></param>
        /// <returns></returns>
        private String SQLiteDBFileName( String nameDB )
        {
            String db_file = System.AppDomain.CurrentDomain.BaseDirectory;
            db_file = db_file.Trim();
            if( db_file[db_file.Length - 1] != '\\' )
            {
                db_file += @"\";
            }

            db_file = ( db_file + ( String.IsNullOrWhiteSpace( nameDB ) ? "AqualogyOPCServiceBuffer.db3" : nameDB ) );

            return db_file;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SQLiteCreateDB( String dbFileName )
        {
            SQLiteConnection.CreateFile( dbFileName );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        private void SQLiteOpenDB( String dbFileName )
        {
            this.SQLiteConnection = new SQLiteConnection( String.Format( "Data Source={0}; Version=3;", dbFileName ) );
            this.SQLiteConnection.Open();
        }


        #region IDisposable
        /// <summary>
        /// Flag: Has Dispose already been called? 
        /// </summary>
        protected Boolean _Disposed = false;

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            Dispose( true );

            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Protected implementation of Dispose pattern. 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose( bool disposing )
        {
            if( this._Disposed )
                return;

            if( disposing )
            {
                // Free any other managed objects here.                
                if( this.SQLiteConnection != null )
                {                    
                    this.SQLiteConnection.Close();
                    this.SQLiteConnection.Dispose();
                    this.SQLiteConnection = null;
                }
            }
            // Free any unmanaged objects here.
            this._Disposed = true;
        }
        #endregion
    }
}
