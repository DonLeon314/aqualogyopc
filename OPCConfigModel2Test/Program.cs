﻿using AqualogyOPCKernel2;
using OPCConfigModel2;
using OPCConfigModel2.ModelBBDD;
using System;
using System.Linq;

namespace OPCConfigModel2Test
{
    class Program
    {
        static OPCKernel2 _Kernel = null;

        static void Main( string[] args )
        {
            try
            {
                if( false )
                {
                    Create();
                }
                else
                {
                    Start();

                    Console.WriteLine( "Stop...." );
                    Console.ReadKey();

                    Stop();

                    Console.WriteLine( "Exit...." );
                    Console.ReadKey();
                }

            } catch( Exception ex )
            {
                Console.WriteLine( ex.Message );
            }

             return;
        }

        private static void Start()
        {
            _Kernel = new OPCKernel2();

            _Kernel.Start();

            Console.WriteLine( "Start-OK!" );

        }

        private static void Stop()
        {
            
            _Kernel.Stop();

            Console.WriteLine( "Stop-OK!" );

        }

        private static void Create()
        {
            using( OPCConfigContext2 dbContext = new OPCConfigContext2() )
            {
                // Add configurations
                AddConfigs( dbContext );

                AddAlarmTypes( dbContext );

                AddWorkTImes( dbContext );

                AddWorkers( dbContext );

                AddWorkerGroups( dbContext );

                AddAlarmGroups( dbContext );

                AddOPCItems( dbContext );

                AddClusters( dbContext );
            }
        }


        private static void AddClusters( OPCConfigContext2 dbContext )
        {
            AlarmType at_ab = dbContext.AlarmTypes.Where( at => String.Compare( "aB", at.Id, true ) == 0 ).ToList().FirstOrDefault();
            AlarmType at_ac = dbContext.AlarmTypes.Where( at => String.Compare( "Ac", at.Id, true ) == 0 ).ToList().FirstOrDefault();

            AlarmGroup ag_200 = dbContext.AlarmGroups.Where( ag => ag.Id == 200 ).ToList().FirstOrDefault();
            AlarmGroup ag_400 = dbContext.AlarmGroups.Where( ag => ag.Id == 400 ).ToList().FirstOrDefault();
            AlarmGroup ag_600 = dbContext.AlarmGroups.Where( ag => ag.Id == 600 ).ToList().FirstOrDefault();

            WorkerGroup g_capataz = dbContext.WorkerGroups.Where( wg => String.Compare( "Capataz", wg.Description, true ) == 0 ).ToList().FirstOrDefault();
            WorkerGroup g_jefe = dbContext.WorkerGroups.Where( wg => String.Compare( "Jefe", wg.Description, true ) == 0 ).ToList().FirstOrDefault();

            Worker leo = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Leo", true ) == 0 ).FirstOrDefault();
            Worker vladimir = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Vladimir", true ) == 0 ).FirstOrDefault();
            Worker donald = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Donald", true ) == 0 ).FirstOrDefault();


            // AC
            AlarmCluster cluster1 = new AlarmCluster();
            cluster1.AlarmType = at_ac;
            cluster1.AlarmGroup = ag_200;            
            cluster1.Worker = leo;
            cluster1.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eInWorkTIme;
            cluster1.Active = true;
            dbContext.AlarmClusters.Add( cluster1 );            
            dbContext.SaveChanges();


            AlarmCluster cluster2 = new AlarmCluster();
            cluster2.AlarmType = at_ac;
            cluster2.AlarmGroup = ag_400;            
            cluster2.Worker = donald;
            cluster2.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eAll;
            cluster2.Active = true;
            dbContext.AlarmClusters.Add( cluster2 );
            dbContext.SaveChanges();

            AlarmCluster cluster3 = new AlarmCluster();
            cluster3.AlarmType = at_ac;
            cluster3.AlarmGroup = ag_600;            
            cluster3.Worker = vladimir;
            cluster3.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eInWorkTIme;
            cluster3.Active = true;
            dbContext.AlarmClusters.Add( cluster3 );
            dbContext.SaveChanges();


            AlarmCluster cluster31 = new AlarmCluster();
            cluster31.AlarmType = at_ac;
            cluster31.AlarmGroup = ag_600;
            cluster31.Worker = leo;
            cluster31.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eInWorkTIme;
            cluster31.Active = true;
            dbContext.AlarmClusters.Add( cluster31 );
            dbContext.SaveChanges();
            

            // AB
            AlarmCluster cluster4 = new AlarmCluster();
            cluster4.AlarmType = at_ab;
            cluster4.AlarmGroup = ag_200;            
            cluster4.Worker = leo;
            cluster4.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eAll;
            cluster4.Active = true;
            dbContext.AlarmClusters.Add( cluster4 );
            dbContext.SaveChanges();

            AlarmCluster cluster5 = new AlarmCluster();
            cluster5.AlarmType = at_ab;
            cluster5.AlarmGroup = ag_400;
            cluster5.Worker = donald;
            cluster5.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eOutWorkTime;
            cluster5.Active = true;
            dbContext.AlarmClusters.Add( cluster5 );
            dbContext.SaveChanges();

            AlarmCluster cluster6 = new AlarmCluster();
            cluster6.AlarmType = at_ab;
            cluster6.AlarmGroup = ag_600;
            cluster6.WorkTImeSelector = AlarmCluster.eWorkTImeSelector.eInWorkTIme;
            cluster6.Worker = vladimir;
            cluster6.Active = true;
            dbContext.AlarmClusters.Add( cluster6 );
            dbContext.SaveChanges();
        }

        private static void AddOPCItems( OPCConfigContext2 dbContext )
        {
            // Add servers
            UAServer opc_server = new UAServer()
            {
                OPCUAUrl = "//Leon:53530/OPCUA/SimulationServer"
            };

            dbContext.UAServers.Add( opc_server );
            dbContext.SaveChanges();

            var config = dbContext.Configurations.ToList().FirstOrDefault();

            String name1 = "Alarmas.DES08.SFG010001AB_CA01_CLPF01.E_AVER.InAlarm";
            String name2 = "Alarmas.DES08.SFG010001AB_CA01_CLPF01.V_CLOR.LoLo.InAlarm";
            String name3 = "Alarmas.DES08.SFG020002AC_GN01_COBA01.V_NSMS.LoLo.InAlarm";

            TagClient tc1 = new TagClient();
            tc1.Description = "E_AVER.InAlarm";
            dbContext.TagClients.Add( tc1 );

            TagClient tc2 = new TagClient();
            tc2.Description = "V_CLOR.LoLo.InAlarm";
            dbContext.TagClients.Add( tc1 );

            TagClient tc3 = new TagClient();
            tc3.Description = "V_NSMS.LoLo.InAlarm";
            dbContext.TagClients.Add( tc3 );

            String tag_name1 = "ns=5;s=" + name1;
            String tag_name2 = "ns=5;s=" + name2;
            String tag_name3 = "ns=5;s=" + name3;

            Tag tag1 = new Tag();
            tag1.Configuration = config;
            tag1.TagName = tag_name1;
            tag1.UAServer = opc_server;
            tag1.LargeMessage = "LM: " + name1;
            tag1.ShortMessage = "SM: " + name1;
            tag1.Client = tc1;

            dbContext.Tags.Add( tag1 );

            Tag tag2 = new Tag();
            tag2.Configuration = config;
            tag2.TagName = tag_name2;
            tag2.UAServer = opc_server;
            tag2.LargeMessage = "LM: " + name2;
            tag2.ShortMessage = "SM: " + name2;
            tag2.Client = tc2;

            dbContext.Tags.Add( tag2 );


            Tag tag3 = new Tag();
            tag3.Configuration = config;
            tag3.TagName = tag_name3;
            tag3.UAServer = opc_server;
            tag3.LargeMessage = "LM: " + name3;
            tag3.ShortMessage = "SM: " + name3;
            tag3.Client = tc3;

            dbContext.Tags.Add( tag3 );

            dbContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddAlarmGroups( OPCConfigContext2 dbContext )
        {
            AlarmGroup ag_200 = new AlarmGroup()
            {
                Id = 200,
                Description = "Alarma"
            };

            AlarmGroup ag_400 = new AlarmGroup()
            {
                Id = 400,
                Description = "Aviso"
            };

            AlarmGroup ag_600 = new AlarmGroup()
            {
                Id = 600,
                Description = "type 600"
            };

            dbContext.AlarmGroups.Add( ag_200 );
            dbContext.AlarmGroups.Add( ag_400 );
            dbContext.AlarmGroups.Add( ag_600 );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddWorkerGroups( OPCConfigContext2 dbContext )
        {
            var config = dbContext.Configurations.ToList().FirstOrDefault();

            WorkerGroup wg_capataz = new WorkerGroup()
            {
              Description = "Capataz"
            };
            wg_capataz.Configuration = config;

            dbContext.WorkerGroups.Add( wg_capataz );

            WorkerGroup wg_jefe = new WorkerGroup()
            {
                Description = "Jefe"
            };

            wg_jefe.Configuration = config;

            dbContext.WorkerGroups.Add( wg_jefe );

            Worker leo = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Leo", true ) == 0 ).FirstOrDefault();
            Worker vladimir = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Vladimir", true ) == 0 ).FirstOrDefault();
            Worker donald = dbContext.Workers.Where( w => String.Compare( w.FirstName, "Donald", true ) == 0 ).FirstOrDefault();


            wg_capataz.Workers.Add( leo );
            wg_capataz.Workers.Add( vladimir );
            wg_capataz.Workers.Add( donald );

            wg_jefe.Workers.Add( vladimir );
            wg_jefe.Workers.Add( donald );

            dbContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddWorkers( OPCConfigContext2 dbContext )
        {
            var config = dbContext.Configurations.ToList().FirstOrDefault();

            var wt1 = dbContext.WorkTimes.ToList().FirstOrDefault();
            var wt2 = dbContext.WorkTimes.ToList().LastOrDefault();

            Worker w = new Worker()
            {
                Alias = "donleon314@gmail.com",
                FirstName = "Leo",
                SecondName = "Grigorev",
                EMails = "donleon314@gmail.com",
                Phones = "+34600262398",
                Channel = Worker.eChannel.SmsAndEmail
            };

            w.Configurations.Add( config );
            w.WorkTimes.Add( wt1 );

            dbContext.Workers.Add( w );

            w = new Worker()
            {
                Alias = "D.Trump@gmail.com",
                FirstName = "Donald",
                SecondName = "Trump",
                EMails = "D.Trump@gmail.com",
                Phones = "001",
                Channel = Worker.eChannel.SmsAndEmail
            };

            w.Configurations.Add( config );
            w.WorkTimes.Add( wt1 );

            dbContext.Workers.Add( w );

            w = new Worker()
            {
                Alias = "V.Putin.com",
                FirstName = "Vladimir",
                SecondName = "Putin",
                EMails = "V.Putin@gmail.com",
                Phones = "+7 001",
                Channel = Worker.eChannel.SmsAndEmail
            };

            w.Configurations.Add( config );
            w.WorkTimes.Add( wt1 );

            dbContext.Workers.Add( w );

            dbContext.SaveChanges();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddWorkTImes( OPCConfigContext2 dbContext )
        {
            WorkTime wt = new WorkTime()
            {
                Description = "WorkTime #1",
                StartTime = new TimeSpan( 8, 0, 0 ),
                EndTime = new TimeSpan( 17, 0, 0 ),
                WeekDays = "LMXJV"
            };

            dbContext.WorkTimes.Add( wt );

            wt = new WorkTime()
            {
                Description = "WorkTime #2",
                StartTime = new TimeSpan( 12, 0, 0 ),
                EndTime = new TimeSpan( 0, 0, 0 ),
                WeekDays = "SD"
            };

            dbContext.WorkTimes.Add( wt );

            wt = new WorkTime()
            {
                Description = "WorkTime #3",
                StartTime = new TimeSpan( 17, 0, 0 ),
                EndTime = new TimeSpan( 8, 0, 0 ),
                WeekDays = "SD"
            };

            dbContext.WorkTimes.Add( wt );


            dbContext.SaveChanges();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddAlarmTypes( OPCConfigContext2 dbContext )
        {
            AlarmType at = new AlarmType() 
            { 
                Id="AB",
                Description = "Abastecimiento"
            };

            dbContext.AlarmTypes.Add( at );

            at = new AlarmType()
            {
                Id = "AC",
                Description = "Saneamiento"
            };

            dbContext.AlarmTypes.Add( at );

            dbContext.SaveChanges();


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        private static void AddConfigs( OPCConfigContext2 dbContext )
        {
            Configuration cfg = new Configuration()
            {
                Description = "Simulation #1"
            };

            dbContext.Configurations.Add( cfg );


            cfg = new Configuration()
            {
                Description = "Simulation #2"
            };

            dbContext.Configurations.Add( cfg );

            dbContext.SaveChanges();


        }
    }
}
