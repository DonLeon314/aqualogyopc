﻿using OPCUAClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Opc.Ua.Client;
using System.Diagnostics;
using OPCUAClient.Interfaces;

namespace OPCUAClientTest
{

    class OPCItem:
            IOPCUASubscribeItem
    {
        public String Key
        {
            get;
            set;
        }
        
        public Int32 IdServer
        {
            get;
            set;
        }

        public String ItemName
        {
            get;
            set;
        }

        public Int32 SamplingInterval
        {
            get;
            set;
        }

        public Int32 QueueSize
        {
            get;
            set;
        }

        public OPCItem( String key, String itemName, Int32 idServer, Int32 samplingInterval = 5, Int32 queueSize = 10 )
        {
            this.Key = key;
            this.ItemName = itemName;
            this.SamplingInterval = samplingInterval;
            this.QueueSize = queueSize;
            this.IdServer = idServer;
        }
    }

    class Server:
            IOPCUAConnectionInfo
    {
        public Int32 Id
        {
            get;
            private set;
        }

        public String UrlServer
        {
            get;
            private set;
        }

        public Int32 PublishingInterval
        {
            get;
            private set;
        }

        public Int32 TimeOut
        {
            get;
            private set;
        }

        public Int32 DefaultSamplingInterval
        {
            get;
            private set;
        }

        public Int32 DefaultQueueSize
        {
            get;
            private set;
        }

        public Server( Int32 id, String urlServer, Int32 publishingInterval = 5,
                       Int32 timeOut = 100, Int32 defaultSamplingInterval = 5, Int32 defaultQueueSize = 10 )
        {
            this.Id = id;
            this.UrlServer = urlServer;
            this.PublishingInterval = publishingInterval;
            this.TimeOut = timeOut;
            this.DefaultSamplingInterval = defaultSamplingInterval;
            this.DefaultQueueSize = defaultQueueSize;
        }
    }

    class ServersStorage:
            IOPCConnectionInfos
    {
        List<IOPCUAConnectionInfo> _Collection = new List<IOPCUAConnectionInfo>();

        public ServersStorage()
        {

        }

        public IOPCUAConnectionInfo FindConnectionInfo( Int32 idServer )
        {
            foreach( var item in this._Collection )
            {
                if( item.Id == idServer )
                {
                    return item;
                }
            }

            return null;
        }

        public void Add( IOPCUAConnectionInfo connection )
        {
            if( this._Collection.Where( c => c.Id == connection.Id ).FirstOrDefault() == null )
            {
                this._Collection.Add( connection );
            }

        }
    }

    class Program
    {
        static void Main( string[] args )
        {
            ServersStorage servers = new ServersStorage();

            servers.Add( new Server( 1, @"//localhost:53530/OPCUA/SimulationServer" ));
            servers.Add( new Server( 2, @"//127.0.0.1:53530/OPCUA/SimulationServer"));

            List<IOPCUASubscribeItem> items1 = new List<IOPCUASubscribeItem>();
            items1.Add( new OPCItem( "1-1", "ns=5;s=Sensor_1_1", 1 ) );
            items1.Add( new OPCItem( "1-2", "ns=5;s=Sensor_1_2", 1 ) );

            List<IOPCUASubscribeItem> items2 = new List<IOPCUASubscribeItem>();
            items2.Add( new OPCItem( "2-1", "ns=5;s=Sensor_2_1", 2 ) );
            items2.Add( new OPCItem( "2-2", "ns=5;s=Sensor_2_2", 2 ) );

            OPCUAClient.OPCUAClient client = new OPCUAClient.OPCUAClient( servers );

            client.OnChangeItem += OnChangeItem;

        
            client.Subscribe( items1 );
            client.Subscribe( items2 );


            Console.ReadKey();

            // unsubscribe

            List<IOPCUASubscribeItem> items3 = new List<IOPCUASubscribeItem>() { items1[0], items1[1], items2[0] };
            //List<IOPCUASubscribeItem> items4 = new List<IOPCUASubscribeItem>() { items2[0] };

            client.StopWatching( items3 );
            //client.StopWatching( items4 );

            Console.ReadKey();            


        }

        private static Object _Sync = new Object();

        private static void OnChangeItem( Object sender, OPCItemClientArgs e )
        {
            lock( _Sync )
            {
                MonitoredItemNotificationEventArgs e_server = e.ConnectionArgs.EventArgs;

                Debug.WriteLine( "-------------------" );
                Debug.WriteLine( String.Format( "On item:'{0}' values:", e.ConnectionArgs.IPCUASubscribeItem.ItemName ) );
                foreach( var v in e.ConnectionArgs.Values )
                {
                    
                    Debug.WriteLine( String.Format( "                     {0} : {1} : {2}", v.Value.ToString(),
                                                    v.ServerTimestamp.ToString(), v.StatusCode.ToString() ) );
                }
            }
        }
    }
}
