// <auto-generated />
namespace OPCConfigModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SALTOrelease : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SALTOrelease));
        
        string IMigrationMetadata.Id
        {
            get { return "201809171204248_SALTO release"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
