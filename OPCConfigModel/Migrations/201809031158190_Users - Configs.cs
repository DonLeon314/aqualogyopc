namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsersConfigs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserConfigs",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Config_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Config_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Configs", t => t.Config_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Config_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserConfigs", "Config_Id", "dbo.Configs");
            DropForeignKey("dbo.UserConfigs", "User_Id", "dbo.Users");
            DropIndex("dbo.UserConfigs", new[] { "Config_Id" });
            DropIndex("dbo.UserConfigs", new[] { "User_Id" });
            DropTable("dbo.UserConfigs");
        }
    }
}
