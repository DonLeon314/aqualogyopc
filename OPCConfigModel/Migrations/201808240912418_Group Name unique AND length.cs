namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupNameuniqueANDlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Groups", "Name", c => c.String(nullable: false, maxLength: 64));
            CreateIndex("dbo.Groups", "Name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Groups", new[] { "Name" });
            AlterColumn("dbo.Groups", "Name", c => c.String());
        }
    }
}
