namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADD_WEEK_DAYS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkTimes", "WeekDays", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkTimes", "WeekDays");
        }
    }
}
