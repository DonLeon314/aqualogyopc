namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBBDDEventConfig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventSaverConfigs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Priority = c.Int(nullable: false),
                        Description = c.String(maxLength: 256),
                        ConnectionString = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Priority, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.EventSaverConfigs", new[] { "Priority" });
            DropTable("dbo.EventSaverConfigs");
        }
    }
}
