namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Worktimechange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "WorkTime_Id", "dbo.WorkTimes");
            DropIndex("dbo.Users", new[] { "WorkTime_Id" });
            CreateTable(
                "dbo.WorkTimeUsers",
                c => new
                    {
                        WorkTime_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkTime_Id, t.User_Id })
                .ForeignKey("dbo.WorkTimes", t => t.WorkTime_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.WorkTime_Id)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.Users", "TimeOfEvents", c => c.Int(nullable: false));
            AddColumn("dbo.WorkTimes", "WeekDays", c => c.String(nullable: false, maxLength: 10));
            DropColumn("dbo.Users", "WeekDays");
            DropColumn("dbo.Users", "WorkTime_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "WorkTime_Id", c => c.Int());
            AddColumn("dbo.Users", "WeekDays", c => c.String());
            DropForeignKey("dbo.WorkTimeUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.WorkTimeUsers", "WorkTime_Id", "dbo.WorkTimes");
            DropIndex("dbo.WorkTimeUsers", new[] { "User_Id" });
            DropIndex("dbo.WorkTimeUsers", new[] { "WorkTime_Id" });
            DropColumn("dbo.WorkTimes", "WeekDays");
            DropColumn("dbo.Users", "TimeOfEvents");
            DropTable("dbo.WorkTimeUsers");
            CreateIndex("dbo.Users", "WorkTime_Id");
            AddForeignKey("dbo.Users", "WorkTime_Id", "dbo.WorkTimes", "Id");
        }
    }
}
