namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOPCitemtype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TagNames", "ItemType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TagNames", "ItemType");
        }
    }
}
