namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs");
            DropForeignKey("dbo.Users", "OPCConfigId", "dbo.OPCConfigs");
            DropIndex("dbo.Groups", new[] { "OPCConfig_Id" });
            DropIndex("dbo.Users", new[] { "OPCConfigId" });
            RenameColumn(table: "dbo.Groups", name: "OPCConfig_Id", newName: "OPCConfigId");
            RenameColumn(table: "dbo.Users", name: "OPCConfigId", newName: "OPCConfig_Id");
            AlterColumn("dbo.Groups", "OPCConfigId", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "OPCConfig_Id", c => c.Int());
            CreateIndex("dbo.Groups", "OPCConfigId");
            CreateIndex("dbo.Users", "OPCConfig_Id");
            AddForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs");
            DropForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs");
            DropIndex("dbo.Users", new[] { "OPCConfig_Id" });
            DropIndex("dbo.Groups", new[] { "OPCConfigId" });
            AlterColumn("dbo.Users", "OPCConfig_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Groups", "OPCConfigId", c => c.Int());
            RenameColumn(table: "dbo.Users", name: "OPCConfig_Id", newName: "OPCConfigId");
            RenameColumn(table: "dbo.Groups", name: "OPCConfigId", newName: "OPCConfig_Id");
            CreateIndex("dbo.Users", "OPCConfigId");
            CreateIndex("dbo.Groups", "OPCConfig_Id");
            AddForeignKey("dbo.Users", "OPCConfigId", "dbo.OPCConfigs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
        }
    }
}
