namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change15addClient : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TagNames", "Client", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TagNames", "Client");
        }
    }
}
