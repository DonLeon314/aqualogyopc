namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OPCConfigs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OPCConfig_Id = c.Int(),
                        OPCTag_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OPCConfigs", t => t.OPCConfig_Id)
                .ForeignKey("dbo.OPCTags", t => t.OPCTag_Id)
                .Index(t => t.OPCConfig_Id)
                .Index(t => t.OPCTag_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        OPCConfig_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OPCConfigs", t => t.OPCConfig_Id)
                .Index(t => t.OPCConfig_Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OPCServers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Host = c.String(),
                        Note = c.String(),
                        OPCConfig_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OPCConfigs", t => t.OPCConfig_Id)
                .Index(t => t.OPCConfig_Id);
            
            CreateTable(
                "dbo.OPCTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagId = c.String(),
                        OPCServerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OPCServers", t => t.OPCServerId, cascadeDelete: true)
                .Index(t => t.OPCServerId);
            
            CreateTable(
                "dbo.UserGroups",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Group_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Group_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Group_Id);
            
            CreateTable(
                "dbo.PhoneUsers",
                c => new
                    {
                        Phone_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Phone_Id, t.User_Id })
                .ForeignKey("dbo.Phones", t => t.Phone_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.Phone_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs");
            DropForeignKey("dbo.OPCServers", "OPCConfig_Id", "dbo.OPCConfigs");
            DropForeignKey("dbo.OPCTags", "OPCServerId", "dbo.OPCServers");
            DropForeignKey("dbo.Groups", "OPCTag_Id", "dbo.OPCTags");
            DropForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs");
            DropForeignKey("dbo.PhoneUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.PhoneUsers", "Phone_Id", "dbo.Phones");
            DropForeignKey("dbo.UserGroups", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.UserGroups", "User_Id", "dbo.Users");
            DropIndex("dbo.PhoneUsers", new[] { "User_Id" });
            DropIndex("dbo.PhoneUsers", new[] { "Phone_Id" });
            DropIndex("dbo.UserGroups", new[] { "Group_Id" });
            DropIndex("dbo.UserGroups", new[] { "User_Id" });
            DropIndex("dbo.OPCTags", new[] { "OPCServerId" });
            DropIndex("dbo.OPCServers", new[] { "OPCConfig_Id" });
            DropIndex("dbo.Users", new[] { "OPCConfig_Id" });
            DropIndex("dbo.Groups", new[] { "OPCTag_Id" });
            DropIndex("dbo.Groups", new[] { "OPCConfig_Id" });
            DropTable("dbo.PhoneUsers");
            DropTable("dbo.UserGroups");
            DropTable("dbo.OPCTags");
            DropTable("dbo.OPCServers");
            DropTable("dbo.Phones");
            DropTable("dbo.Users");
            DropTable("dbo.Groups");
            DropTable("dbo.OPCConfigs");
        }
    }
}
