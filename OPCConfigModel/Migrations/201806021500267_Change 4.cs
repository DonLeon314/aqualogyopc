namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs");
            DropIndex("dbo.Users", new[] { "OPCConfig_Id" });
            DropColumn("dbo.Users", "OPCConfig_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "OPCConfig_Id", c => c.Int());
            CreateIndex("dbo.Users", "OPCConfig_Id");
            AddForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
        }
    }
}
