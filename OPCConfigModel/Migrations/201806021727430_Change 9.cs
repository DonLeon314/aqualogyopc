namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change9 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Users", "WorkTime_Id", c => c.Int());
            CreateIndex("dbo.Users", "WorkTime_Id");
            AddForeignKey("dbo.Users", "WorkTime_Id", "dbo.WorkTimes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "WorkTime_Id", "dbo.WorkTimes");
            DropIndex("dbo.Users", new[] { "WorkTime_Id" });
            DropColumn("dbo.Users", "WorkTime_Id");
            DropTable("dbo.WorkTimes");
        }
    }
}
