namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EMails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EMails", "UserId", "dbo.Users");
            DropIndex("dbo.EMails", new[] { "UserId" });
            DropTable("dbo.EMails");
        }
    }
}
