namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OPCConfigModel.OPCConfigContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true; //TODO: TEST MIGRATION -false;
            
            AutomaticMigrationDataLossAllowed = true;

            ContextKey = "OPCConfigModel.OPCConfigContext";
        }

        protected override void Seed(OPCConfigModel.OPCConfigContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            return;
        }
    }
}
