namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change14 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhoneUsers", "Phone_Id", "dbo.Phones");
            DropForeignKey("dbo.PhoneUsers", "User_Id", "dbo.Users");
            DropIndex("dbo.PhoneUsers", new[] { "Phone_Id" });
            DropIndex("dbo.PhoneUsers", new[] { "User_Id" });
            CreateIndex("dbo.Phones", "UserId");
            AddForeignKey("dbo.Phones", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropTable("dbo.PhoneUsers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PhoneUsers",
                c => new
                    {
                        Phone_Id = c.Int(nullable: false),
                        User_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Phone_Id, t.User_Id });
            
            DropForeignKey("dbo.Phones", "UserId", "dbo.Users");
            DropIndex("dbo.Phones", new[] { "UserId" });
            CreateIndex("dbo.PhoneUsers", "User_Id");
            CreateIndex("dbo.PhoneUsers", "Phone_Id");
            AddForeignKey("dbo.PhoneUsers", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PhoneUsers", "Phone_Id", "dbo.Phones", "Id", cascadeDelete: true);
        }
    }
}
