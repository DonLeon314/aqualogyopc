namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupNameuniqueANDchangeAddConfigId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Groups", new[] { "Name" });
            DropIndex("dbo.Groups", new[] { "OPCConfigId" });
            CreateIndex("dbo.Groups", new[] { "Name", "OPCConfigId" }, unique: true, name: "IX_Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Groups", "IX_Name");
            CreateIndex("dbo.Groups", "OPCConfigId");
            CreateIndex("dbo.Groups", "Name", unique: true);
        }
    }
}
