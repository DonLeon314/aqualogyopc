namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeGroup : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Groups", "OPCTag_Id", "dbo.OPCTags");
            DropIndex("dbo.Groups", new[] { "OPCTag_Id" });
            CreateTable(
                "dbo.OPCTagGroups",
                c => new
                    {
                        OPCTag_Id = c.Int(nullable: false),
                        Group_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OPCTag_Id, t.Group_Id })
                .ForeignKey("dbo.OPCTags", t => t.OPCTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .Index(t => t.OPCTag_Id)
                .Index(t => t.Group_Id);
            
            DropColumn("dbo.Groups", "OPCTag_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Groups", "OPCTag_Id", c => c.Int());
            DropForeignKey("dbo.OPCTagGroups", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.OPCTagGroups", "OPCTag_Id", "dbo.OPCTags");
            DropIndex("dbo.OPCTagGroups", new[] { "Group_Id" });
            DropIndex("dbo.OPCTagGroups", new[] { "OPCTag_Id" });
            DropTable("dbo.OPCTagGroups");
            CreateIndex("dbo.Groups", "OPCTag_Id");
            AddForeignKey("dbo.Groups", "OPCTag_Id", "dbo.OPCTags", "Id");
        }
    }
}
