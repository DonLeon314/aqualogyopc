namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change13 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.OPCConfigs", newName: "Configs");
            RenameTable(name: "dbo.OPCTags", newName: "TagNames");
            RenameTable(name: "dbo.OPCTagGroups", newName: "TagNameGroups");
            RenameColumn(table: "dbo.TagNameGroups", name: "OPCTag_Id", newName: "TagName_Id");
            RenameIndex(table: "dbo.TagNameGroups", name: "IX_OPCTag_Id", newName: "IX_TagName_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.TagNameGroups", name: "IX_TagName_Id", newName: "IX_OPCTag_Id");
            RenameColumn(table: "dbo.TagNameGroups", name: "TagName_Id", newName: "OPCTag_Id");
            RenameTable(name: "dbo.TagNameGroups", newName: "OPCTagGroups");
            RenameTable(name: "dbo.TagNames", newName: "OPCTags");
            RenameTable(name: "dbo.Configs", newName: "OPCConfigs");
        }
    }
}
