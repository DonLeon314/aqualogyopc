// <auto-generated />
namespace OPCConfigModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ADD_WEEK_DAYS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ADD_WEEK_DAYS));
        
        string IMigrationMetadata.Id
        {
            get { return "201806131009560_ADD_WEEK_DAYS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
