namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OPCTags", "OPCServerId", "dbo.OPCServers");
            DropForeignKey("dbo.OPCServers", "OPCConfig_Id", "dbo.OPCConfigs");
            DropIndex("dbo.OPCServers", new[] { "OPCConfig_Id" });
            DropIndex("dbo.OPCTags", new[] { "OPCServerId" });
            AddColumn("dbo.OPCTags", "Server", c => c.String());
            DropTable("dbo.OPCServers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OPCServers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Host = c.String(),
                        Note = c.String(),
                        OPCConfig_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.OPCTags", "Server");
            CreateIndex("dbo.OPCTags", "OPCServerId");
            CreateIndex("dbo.OPCServers", "OPCConfig_Id");
            AddForeignKey("dbo.OPCServers", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
            AddForeignKey("dbo.OPCTags", "OPCServerId", "dbo.OPCServers", "Id", cascadeDelete: true);
        }
    }
}
