// <auto-generated />
namespace OPCConfigModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addopcuaurlserver : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addopcuaurlserver));
        
        string IMigrationMetadata.Id
        {
            get { return "201808200940214_add opc ua url server"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
