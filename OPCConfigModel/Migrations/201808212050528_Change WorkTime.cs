namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeWorkTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WorkTimes", "Name", c => c.String(nullable: false, maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.WorkTimes", "Name", c => c.String());
        }
    }
}
