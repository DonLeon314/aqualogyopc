namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddlengthforfieldAliasonclassUser : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Alias", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Alias", c => c.String());
        }
    }
}
