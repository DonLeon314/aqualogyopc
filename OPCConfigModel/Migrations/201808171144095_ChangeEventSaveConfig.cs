namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeEventSaveConfig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventSaverConfigs", "Repeats", c => c.Int(nullable: false));
            AddColumn("dbo.EventSaverConfigs", "EMailAdministrator", c => c.String());
            AddColumn("dbo.EventSaverConfigs", "OPCItem", c => c.String());
            AddColumn("dbo.EventSaverConfigs", "HistoryTime", c => c.Int(nullable: false));
            AddColumn("dbo.EventSaverConfigs", "ErrorPercent", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventSaverConfigs", "ErrorPercent");
            DropColumn("dbo.EventSaverConfigs", "HistoryTime");
            DropColumn("dbo.EventSaverConfigs", "OPCItem");
            DropColumn("dbo.EventSaverConfigs", "EMailAdministrator");
            DropColumn("dbo.EventSaverConfigs", "Repeats");
        }
    }
}
