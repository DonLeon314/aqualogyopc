namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeGroup2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs");
            DropIndex("dbo.Groups", new[] { "OPCConfig_Id" });
            RenameColumn(table: "dbo.Groups", name: "OPCConfig_Id", newName: "OPCConfigId");
            AlterColumn("dbo.Groups", "OPCConfigId", c => c.Int(nullable: false));
            CreateIndex("dbo.Groups", "OPCConfigId");
            AddForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs");
            DropIndex("dbo.Groups", new[] { "OPCConfigId" });
            AlterColumn("dbo.Groups", "OPCConfigId", c => c.Int());
            RenameColumn(table: "dbo.Groups", name: "OPCConfigId", newName: "OPCConfig_Id");
            CreateIndex("dbo.Groups", "OPCConfig_Id");
            AddForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
        }
    }
}
