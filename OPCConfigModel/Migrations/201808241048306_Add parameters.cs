namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addparameters : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UniversalParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SectionName = c.String(nullable: false, maxLength: 64),
                        ParameterName = c.String(nullable: false, maxLength: 64),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.SectionName, t.ParameterName }, unique: true, name: "IX_SectionParameter");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UniversalParameters", "IX_SectionParameter");
            DropTable("dbo.UniversalParameters");
        }
    }
}
