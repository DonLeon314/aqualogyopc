namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OPCTags", "LargeMessage", c => c.String());
            AddColumn("dbo.OPCTags", "ShortMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OPCTags", "ShortMessage");
            DropColumn("dbo.OPCTags", "LargeMessage");
        }
    }
}
