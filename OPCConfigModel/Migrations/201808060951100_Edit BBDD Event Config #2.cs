namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditBBDDEventConfig2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventSaverConfigs", "TableName", c => c.String(nullable: false, maxLength: 64));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventSaverConfigs", "TableName");
        }
    }
}
