namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SALTOrelease : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EventSaverConfigs", "TimeOutProcess");
            DropColumn("dbo.EventSaverConfigs", "HistoryTime");
            DropColumn("dbo.EventSaverConfigs", "ErrorPercent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventSaverConfigs", "ErrorPercent", c => c.Int(nullable: false));
            AddColumn("dbo.EventSaverConfigs", "HistoryTime", c => c.Int(nullable: false));
            AddColumn("dbo.EventSaverConfigs", "TimeOutProcess", c => c.Int());
        }
    }
}
