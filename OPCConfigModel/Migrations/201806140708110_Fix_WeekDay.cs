namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_WeekDay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "WeekDays", c => c.String());
            DropColumn("dbo.WorkTimes", "WeekDays");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkTimes", "WeekDays", c => c.String());
            DropColumn("dbo.Users", "WeekDays");
        }
    }
}
