namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditBBDDEventConfig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EventSaverConfigs", "ConnectionString", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EventSaverConfigs", "ConnectionString", c => c.String(maxLength: 256));
        }
    }
}
