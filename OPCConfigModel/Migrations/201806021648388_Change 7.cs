namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change7 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.OPCTags", "OPCServerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OPCTags", "OPCServerId", c => c.Int(nullable: false));
        }
    }
}
