namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeUsers : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Alias", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Users", "FirstName", c => c.String(maxLength: 64));
            AlterColumn("dbo.Users", "SecondName", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "SecondName", c => c.String());
            AlterColumn("dbo.Users", "FirstName", c => c.String());
            AlterColumn("dbo.Users", "Alias", c => c.String(maxLength: 64));
        }
    }
}
