namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs");
            DropForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs");
            DropIndex("dbo.Groups", new[] { "OPCConfigId" });
            DropIndex("dbo.Users", new[] { "OPCConfig_Id" });
            RenameColumn(table: "dbo.Groups", name: "OPCConfigId", newName: "OPCConfig_Id");
            RenameColumn(table: "dbo.Users", name: "OPCConfig_Id", newName: "OPCConfigId");
            AlterColumn("dbo.Groups", "OPCConfig_Id", c => c.Int());
            AlterColumn("dbo.Users", "OPCConfigId", c => c.Int(nullable: false));
            CreateIndex("dbo.Groups", "OPCConfig_Id");
            CreateIndex("dbo.Users", "OPCConfigId");
            AddForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
            AddForeignKey("dbo.Users", "OPCConfigId", "dbo.OPCConfigs", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "OPCConfigId", "dbo.OPCConfigs");
            DropForeignKey("dbo.Groups", "OPCConfig_Id", "dbo.OPCConfigs");
            DropIndex("dbo.Users", new[] { "OPCConfigId" });
            DropIndex("dbo.Groups", new[] { "OPCConfig_Id" });
            AlterColumn("dbo.Users", "OPCConfigId", c => c.Int());
            AlterColumn("dbo.Groups", "OPCConfig_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Users", name: "OPCConfigId", newName: "OPCConfig_Id");
            RenameColumn(table: "dbo.Groups", name: "OPCConfig_Id", newName: "OPCConfigId");
            CreateIndex("dbo.Users", "OPCConfig_Id");
            CreateIndex("dbo.Groups", "OPCConfigId");
            AddForeignKey("dbo.Users", "OPCConfig_Id", "dbo.OPCConfigs", "Id");
            AddForeignKey("dbo.Groups", "OPCConfigId", "dbo.OPCConfigs", "Id", cascadeDelete: true);
        }
    }
}
