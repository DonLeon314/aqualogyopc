namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Alias", c => c.String());
            AddColumn("dbo.Users", "Channel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Channel");
            DropColumn("dbo.Users", "Alias");
        }
    }
}
