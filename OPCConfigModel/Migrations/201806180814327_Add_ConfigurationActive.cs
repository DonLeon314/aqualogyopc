namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ConfigurationActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Configs", "Active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Configs", "Active");
        }
    }
}
