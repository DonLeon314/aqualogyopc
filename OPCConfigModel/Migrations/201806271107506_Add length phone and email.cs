namespace OPCConfigModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addlengthphoneandemail : DbMigration
    {

        public Addlengthphoneandemail()
        {
            return;
        }

        public override void Up()
        {
            AlterColumn("dbo.EMails", "Value", c => c.String(maxLength: 256));
            AlterColumn("dbo.Phones", "Value", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Phones", "Value", c => c.String());
            AlterColumn("dbo.EMails", "Value", c => c.String());
        }
    }
}
