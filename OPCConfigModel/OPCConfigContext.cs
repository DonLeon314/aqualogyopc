﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel
{
    public class OPCConfigContext:
                DbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OPCConfigContext()
            : base( "DbConnection" )
        {
            Database.SetInitializer( new MigrateDatabaseToLatestVersion<OPCConfigContext, OPCConfigModel.Migrations.Configuration>() );
        }

        /// <summary>
        /// Configs
        /// </summary>
        public DbSet<Config> OPCConfigs
        {
            get; set;
        }

        /// <summary>
        /// Groups
        /// </summary>
        public DbSet<Group> Groups
        {
            get;
            set;
        }

        public DbSet<User> Users
        {
            get;
            set;
        }

        public DbSet<TagName> Tags
        {
            get;
            set;
        }

        public DbSet<EMail> EMails
        {
            get;
            set;
        }

        public DbSet<Phone> Phones
        {
            get;
            set;
        }

        public DbSet<WorkTime> WorkTimes
        {
            get;
            set;
        }

        public DbSet<EventSaverConfig> EventSaverConfigs
        {
            get;
            set;
        }

        /// <summary>
        /// Parameters
        /// </summary>
        public DbSet<UniversalParameter> Parameters
        {
            get;
            set;
        }

#region WORK WITH PARAMETERS
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public Int32 GetParameter( String nameSection, String nameParameter, Int32 defaultValue )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );
            
            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();

            if( row != null )
            {
                Int32 value = defaultValue;
                if( Int32.TryParse( row.Value, out value ) )
                {
                    return value;
                }
            }

            return defaultValue;
        } 
       
        public Int64 GetParameter( String nameSection, String nameParameter, Int64 defaultValue )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();

            if( row != null )
            {
                Int64 value = defaultValue;
                if( Int64.TryParse( row.Value, out value ) )
                {
                    return value;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public String GetParameter( String nameSection, String nameParameter, String defaultValue = null )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row != null )
            {
                return row.Value;                
            }

            return defaultValue;
        }

        public Boolean GetParameter( String nameSection, String nameParameter, Boolean defaultValue = false )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            var row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                                  String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row != null )
            {
                Boolean r = defaultValue;
                if( Boolean.TryParse( row.Value, out r ) )
                {
                    return r;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Set parameter
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, String value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value;

            this.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, Int32 value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }

        public void SetParameter( String nameSection, String nameParameter, Int64 value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }

        /// <summary>
        /// Set parameter
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="value"></param>
        public void SetParameter( String nameSection, String nameParameter, Boolean value )
        {
            nameSection = nameSection.Trim();
            nameParameter = nameParameter.Trim();

            // Check parameters
            CheckParameters( nameSection, nameParameter );

            UniversalParameter row = null;

            row = this.Parameters.Where( p => String.Compare( p.SectionName, nameSection, true ) == 0 &&
                                             String.Compare( p.ParameterName, nameParameter, true ) == 0
                                           ).FirstOrDefault();
            if( row == null )
            {
                row = new UniversalParameter()
                {
                    SectionName = nameSection,
                    ParameterName = nameParameter
                };

                this.Parameters.Add( row );
            }

            row.Value = value.ToString();

            this.SaveChanges();
        }        


        /// <summary>
        /// Check parameters
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        private void CheckParameters( String nameSection, String nameParameter )
        {
            if( String.IsNullOrWhiteSpace( nameSection ) || String.IsNullOrWhiteSpace( nameParameter ) )
            {
                throw new ArgumentException( String.Format( "Incorrect parameters Section:'{0}' Name:'{1}'", nameSection, nameParameter ) );
            }
        }

        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {        
            base.OnModelCreating( modelBuilder );
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSection"></param>
        /// <param name="nameParameter"></param>
        /// <param name="timeBBDD"></param>
        public void SaveBBDDTime( String nameSection, String nameParameter, DateTime? timeBBDD = null )
        {            

            SetParameter( nameSection, nameParameter, ( timeBBDD.HasValue ? timeBBDD.Value.Ticks : DateTime.Now.Ticks ) );
        }
    }
}
