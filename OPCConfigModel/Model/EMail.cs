﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel.Model
{
    public class EMail
    {
        public Int32 Id
        {
            get;
            set;
        }

        [MaxLength( 256 )]
        public String Value
        {
            get;
            set;
        }

        public Int32 UserId
        {
            get;
            set;
        }
    }
}
