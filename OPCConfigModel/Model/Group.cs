﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace OPCConfigModel.Model
{
    public class Group
    {

        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [MaxLength( 64 )]
        [Required]
        [Index( "IX_Name", 1, IsUnique = true )]
        public String Name
        {
            get;
            set;
        }

        public ICollection<User> Users
        {
            get;
            set;
        }

        public ICollection<TagName> OPCTags
        {
            get;
            set;
        }

        [Index( "IX_Name", 2, IsUnique = true )]
        public Int32 OPCConfigId
        {
            get;
            set;
        }
        public Config OPCConfig
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Group()
        {
            this.Users = new List<User>();
            this.OPCTags = new List<TagName>();
        }
    }
}
