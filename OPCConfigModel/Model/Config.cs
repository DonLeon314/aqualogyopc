﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel.Model
{
    /// <summary>
    /// Configuration Entity
    /// </summary>
    public class Config
    {
        /// <summary>
        /// BBDD id
        /// </summary>
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Active configuration
        /// </summary>
        [DefaultValue(true)]
        public Boolean Active
        {
            get;
            set;
        }

        /// <summary>
        /// Visible configuration
        /// </summary>
        [DefaultValue(true)]
        public Boolean Visible
        {
            get;
            set;
        }

        /// <summary>
        /// Configuration name
        /// </summary>
        [MaxLength(64)]
        [Required]                
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Groups
        /// </summary>
        public ICollection<Group> Groups
        {
            get;
            set;
        }


        public ICollection<User> Users
        {
            get;
            set;
        }

        /// <summary>
        /// Timeout (sec)
        /// </summary>
        [DefaultValue(66)]
        public Int32 Timeout
        {
            get;
            set;
        } 
        
        /// <summary>
        /// Alarm detected (sec)
        /// </summary>
        [DefaultValue(20)]
        public Int32 AlarmDetected
        {
            get;
            set;
        } 
        
        /// <summary>
        /// Publishing interval (sec)
        /// </summary>
        [DefaultValue(10)]
        public Int32 PublishingInterval
        {
            get;
            set;
        }
        
        /// <summary>
        /// Sampling interval (sec)
        /// </summary>
        [DefaultValue(2)]
        public Int32 SamplingInterval
        {
            get;
            set;
        } 
        
        /// <summary>
        /// Queue size
        /// </summary>
        [DefaultValue(5)]
        public Int32 QueueSize
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Config()
        { 
            DefaultInit();
        }

        /// <summary>
        /// Default initialize
        /// </summary>
        public void DefaultInit()
        {
            this.Groups = new List<Group>();
            this.Users = new List<User>();


            this.Active = true;
            this.Visible = true;
            this.AlarmDetected = 300;
            this.Name = "Config";
            this.PublishingInterval = 30;
            this.SamplingInterval = 30;
            this.QueueSize = 10;
            this.Timeout = 66;
        }
    }
}
