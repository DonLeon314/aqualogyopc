﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OPCConfigModel.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkTime
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [MaxLength( 64 )]
        [Required]
        public String Name
        {
            get;
            set;
        }

        public TimeSpan StartTime
        {
            get;
            set;
        }

        public TimeSpan EndTime
        {
            get;
            set;
        }

        public static String WeekDays_DefaultValue = "LMXJV";

        [MaxLength(10)]
        [Required]        
        public String WeekDays
        {
            get;
            set;
        }

        public ICollection<User> Users
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkTime()
        {
            this.Users = new List<User>();

            this.WeekDays = WeekDays_DefaultValue;
        }

    }
}
