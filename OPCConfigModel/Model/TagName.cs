﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel.Model
{
    public class TagName
    {
        public Int32 Id
        {
            get;
            set;
        }


        public enum eOPCItemType
        {
            eAlarm = 0
        }

        public eOPCItemType ItemType
        {
            get;
            set;
        }

        public String Server
        {
            get;
            set;
        }

        public String Client
        {
            get;
            set;
        }

        public String TagId
        {
            get;
            set;
        }

        public String LargeMessage
        {
            get;
            set;
        }

        public String ShortMessage
        {
            get;
            set;
        }

        public ICollection<Group> Groups
        {
            get;
            set;
        }

        /// </summary>

        public TagName()
        {
            this.Groups = new List<Group>();
        }
    }
}
