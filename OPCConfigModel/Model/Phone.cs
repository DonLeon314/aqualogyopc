﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Phone
    {
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength( 256 )]
        public String Value
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 UserId
        {
            get;
            set;
        }
    }
}
