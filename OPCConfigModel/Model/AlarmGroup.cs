﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OPCConfigModel.Model
{
    /// <summary>
    /// Alarm group
    /// </summary>
    public class AlarmGroup
    {
        /// <summary>
        /// Id alarm group
        /// </summary>
        [Required, Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// Alarm group description
        /// </summary>
        [Required]
        [MaxLength(64)]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public AlarmGroup()
        {
        }
    }
}
