﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace OPCConfigModel.Model
{
    public class ATGTWT
    {
        [Key]
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [Index( "IX_ATGTWT_U", 1, IsUnique = true )]
        public String AlarmTypeId
        {
            get;
            set;
        }

        public AlarmType AlarmType
        {
            get;
            set;
        }

        [Required]        
        [Index( "IX_ATGTWT_U", 2, IsUnique = true )]
        public Int32 UserId
        {
            get;
            set;
        }

        public User User
        {
            get;
            set;
        }



        /// <summary>
        /// Default constructor
        /// </summary>
        public ATGTWT()
        {
            this.WorkTimes = new List<WorkTime>();
        }
    }
}
