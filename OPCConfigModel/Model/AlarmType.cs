﻿using OPCConfigModel.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OPCConfigModel.Model
{
    /// <summary>
    /// Alarm type
    /// </summary>
    public class AlarmType
    {
        private String _Id;

        /// <summary>
        /// Id alarp type
        /// </summary>
        [Key]
        [Required]
        [MaxLength( 16 )]
        public String Id
        {
            get
            {
                return this._Id.Trim();
            }

            set
            {
                this._Id = value.Trim().ToUpper();
            }
        }

        /// <summary>
        /// Type name
        /// </summary>
        [Required]
        [MaxLength(64)]
        public String Description
        {
            get;
            set;
        }

        /// <summary>
        /// Users
        /// </summary>
        public ICollection<User> Users
        {
            get;
            set;
        }

        public AlarmType()
        {
            this.Users = new List<User>();
        }
    }
}
