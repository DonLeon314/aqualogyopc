﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCConfigModel.Model
{
    public class User
    {
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [MaxLength( 64 )]
        public String Alias
        {
            get;
            set;
        }
                
        [MaxLength( 64 )]
        public String FirstName
        {
            get;
            set;
        }

        [MaxLength( 64 )]
        public String SecondName
        {
            get;
            set;
        }

        public enum eChannel
        {
            unknown = 0,
            sms = 1,
            mail = 2,
            sms_mail = 3,
        }

        public eChannel Channel
        {
            get;
            set;
        }

        public ICollection<Group> Groups
        {
            get;
            set;
        }

        public ICollection<Phone> Phones
        {
            get;
            set;
        }

        public ICollection<EMail> EMails
        {
            get;
            set;
        }

        public ICollection<Config> Configs
        {
            get;
            set;
        }

        /// <summary>
        /// Work times
        /// </summary>
        public ICollection<WorkTime> WorkTimes
        {
            get;
            set;
        }

        public static eTimeEvent TimeOfEvent_DefaultValue = eTimeEvent.eInside;

        public enum eTimeEvent
        {
            eAllTime = 0,
            eInside = 1,
            eOutside = 2
        }

        /// <summary>
        /// Time of events
        /// </summary>
        [DefaultValue(eTimeEvent.eInside)]
        public eTimeEvent TimeOfEvents
        {
            get;
            set;
        }

        public User()
        {
            this.Groups = new List<Group>();
            this.Phones = new List<Phone>();
            this.EMails = new List<EMail>();
            this.WorkTimes = new List<WorkTime>();

            this.Configs = new List<Config>();

            this.TimeOfEvents = TimeOfEvent_DefaultValue;
        }
    }
}
