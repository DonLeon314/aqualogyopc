﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCRestService
{

   /// <summary>
    ///     
    /// </summary>
    [DataContract]
    public class AqualogyOPCRestServiceFault
    {
        [DataMember]
        public String CustomError
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public AqualogyOPCRestServiceFault()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        public AqualogyOPCRestServiceFault( String error )
        {
            this.CustomError = error;
        }
    }
}
