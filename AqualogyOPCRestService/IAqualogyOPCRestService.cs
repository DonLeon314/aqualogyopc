﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AqualogyOPCRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAqualogyOPCRestService" in both code and config file together.
    [ServiceContract]
    public interface IAqualogyOPCRestService
    {
        [OperationContract]
        AqualogyOPCConfigorationInfo[] GetConfigurations();

        [OperationContract]
        [FaultContract( typeof( AqualogyOPCRestServiceFault ) )]
        Boolean StopProcessing();

        [OperationContract]
        [FaultContract( typeof( AqualogyOPCRestServiceFault ) )]
        Boolean StartProcessing();

        [OperationContract]
        [FaultContract( typeof( AqualogyOPCRestServiceFault ) )]
        Boolean RestartProcessing();
    }
}
