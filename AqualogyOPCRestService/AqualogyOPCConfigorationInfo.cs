﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AqualogyOPCRestService
{
    [DataContract]
    public class AqualogyOPCConfigorationInfo
    {
        [DataMember]
        public Int32 Id
        {
            get;
            set;
        }

        [DataMember]
        public String Name
        {
            get;
            set;
        }

        [DataMember]
        public Boolean State
        {
            get;
            set;
        }

        public AqualogyOPCConfigorationInfo( Int32 id, String name, Boolean state )
        {
            this.Id = id;
            this.Name = name;
            this.State = state;
        }
    }
}
