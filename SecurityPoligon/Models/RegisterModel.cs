﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AspNetIdentityApp.Models
{
    public class RegisterModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType( DataType.Password )]
        public string Password { get; set; }

        [Required]
        [Compare( "Password", ErrorMessage = "Passwords !=" )]
        [DataType( DataType.Password )]
        public string PasswordConfirm { get; set; }
    }
}