﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityPoligon.Models
{
    public class SecurityContext:
            IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// 
        /// </summary>
        public SecurityContext():
                base( "DbConnection" )
        {
        }

        public static SecurityContext Create()
        {
            return new SecurityContext();
        }
    }
}