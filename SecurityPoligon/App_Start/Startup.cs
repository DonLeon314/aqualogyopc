﻿using Microsoft.Owin;
using Owin;
//using AspNetIdentityApp.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using System.Security;
using SecurityPoligon.Models;

[assembly: OwinStartup( typeof( AspNetIdentityApp.Startup ) )]

namespace AspNetIdentityApp
{
    public class Startup
    {
        public void Configuration( IAppBuilder app )
        {
            
            app.CreatePerOwinContext<SecurityPoligon.Models.SecurityContext>( SecurityPoligon.Models.SecurityContext.Create );
            app.CreatePerOwinContext<ApplicationUserManager>( ApplicationUserManager.Create );
            app.UseCookieAuthentication( new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString( "/Account/Login" ),
            } );



        }
    }
}