﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SecurityPoligon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecurityPoligon.Controllers
{
    public class HomeController: Controller
    {
       // [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {

            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().
                                    FindById( System.Web.HttpContext.Current.User.Identity.GetUserId() );


            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}