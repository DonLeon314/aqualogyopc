﻿using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient
{
    public class OPCAutoresubscribeArgs
    {
        public OPCConnectionInfo ConnectionInfo
        {
            get;
            private set;
        }

        public IReadOnlyCollection<IOPCUASubscribeItem> OldItems
        {
            get;
            private set;
        }

        public IReadOnlyCollection<IOPCUASubscribeItem> NewItems
        {
            get;
            set;
        }        

        public OPCAutoresubscribeArgs( OPCConnectionInfo connectionInfo, IReadOnlyCollection<IOPCUASubscribeItem> oldItems )
        {
            this.ConnectionInfo = connectionInfo;
            this.OldItems = oldItems;
            this.NewItems = oldItems;
        }

    }
}
