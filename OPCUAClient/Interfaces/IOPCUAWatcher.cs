﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient.Interfaces
{
    public interface IOPCUAWatcher
    {
        void UnSubscribe( IEnumerable<IOPCUASubscribeItem> unsubscribeItems );
        Boolean Subscribe( IEnumerable<IOPCUASubscribeItem> subscribeItems );

        Object LoadValue( Int32 connectionId, String tagName );
    }
}
