﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient.Interfaces
{
    /// <summary>
    /// Connection information
    /// </summary>
    public interface IOPCUAConnectionInfo
    {
        Int32 Id
        {
            get;
        }

        String UrlServer
        {
            get;
        }

        Int32 PublishingInterval
        {
            get;
        }
        
        Int32 TimeOut
        {
            get;
        }
        
        Int32 DefaultSamplingInterval
        {
            get;
        }

        Int32 DefaultQueueSize
        {
            get;
        }
    }
}
