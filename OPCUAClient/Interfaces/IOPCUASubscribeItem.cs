﻿using System;

namespace OPCUAClient.Interfaces
{
    public interface IOPCUASubscribeItem
    {
        /// <summary>
        /// OPC Item unique key
        /// </summary>
        String Key
        {
            get;            
        }

        /// <summary>
        /// OPC Item name
        /// </summary>
        String ItemName
        {
            get;
        }

        Int32 SamplingInterval
        {
            get;
        }

        Int32 QueueSize
        {
            get;
        }

        Int32 IdServer
        {
            get;
        }
    }
}
