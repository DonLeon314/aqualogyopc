﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOPCConnectionInfos
    {
        IOPCUAConnectionInfo FindConnectionInfo( Int32 idServer );
    }
}
