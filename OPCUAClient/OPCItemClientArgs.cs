﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient
{
    public class OPCItemClientArgs:
            EventArgs
    {
        public OPCItemConnectionArgs ConnectionArgs
        {
            get;
            set;
        }

        public OPCItemClientArgs( OPCItemConnectionArgs connectionArgs )
        {
            this.ConnectionArgs = connectionArgs;
        }
    }
}
