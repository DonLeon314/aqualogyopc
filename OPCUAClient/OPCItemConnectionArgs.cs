﻿using Opc.Ua;
using Opc.Ua.Client;
using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;

namespace OPCUAClient
{
    public class OPCItemConnectionArgs:
            EventArgs
    {
        public MonitoredItemNotificationEventArgs EventArgs
        {
            get;
            private set;
        }

        public IOPCUASubscribeItem IPCUASubscribeItem
        {
            get;
            private set;
        }

        public OPCConnectionInfo ConnectionInfo
        {
            get;
            set;
        }

        public IEnumerable<DataValue> Values
        {
            get;
            private set;
        }

        public OPCItemConnectionArgs( MonitoredItemNotificationEventArgs args, IOPCUASubscribeItem iPCUASubscribeItem, 
                                      OPCConnectionInfo connectionInfo, IEnumerable<DataValue> values )
        {
            this.EventArgs = args;
            this.IPCUASubscribeItem = iPCUASubscribeItem;
            this.Values = values;
            this.ConnectionInfo = connectionInfo;
        }
    }
}
