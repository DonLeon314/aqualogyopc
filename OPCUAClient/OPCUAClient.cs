﻿using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient
{
    public class OPCUAClient:
                IOPCUAWatcher
    {
        #region OPCConnections
        protected class OPCConnectionsCollection:
                KeyedCollection<Int32,OPCConnection>
        {
            public OPCConnectionsCollection()
            {

            }

            protected override Int32 GetKeyForItem( OPCConnection item )
            {
                return item.Id;
            }
        }

        private OPCConnectionsCollection _Connections = new OPCConnectionsCollection();

        #endregion
        

        /// <summary>
        /// 
        /// </summary>
        public EventHandler<OPCItemClientArgs> OnChangeItem;
        public EventHandler<OPCAutoresubscribeArgs> OnAutoResubscribe;


        private IOPCConnectionInfos _iOPCConnectionInfos = null;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iOPCConnectionInfos"></param>
        public OPCUAClient( IOPCConnectionInfos iOPCConnectionInfos = null )
        {
            this._iOPCConnectionInfos = iOPCConnectionInfos;
        }

        #region interface IOPCUAWatcher
        
        /// <summary>
        /// Subscribe items
        /// </summary>
        /// <param name="connectionKey"></param>
        /// <param name="subscribeItems"></param>
        /// <returns></returns>
        public Boolean Subscribe( IEnumerable<IOPCUASubscribeItem> subscribeItems )
        {
            List<String> keys = new List<String>();

            // Group items
            Dictionary<Int32, List<IOPCUASubscribeItem>> group_items = GroupItems( subscribeItems );

            foreach( var portion_items in group_items )
            {
                if( false == this._Connections.Contains( portion_items.Key ) )
                {
                    throw new Exception( String.Format( "Invalid URL" ) );
                }

                foreach( var item in portion_items.Value )
                {
                    OPCConnection connection = this._Connections[portion_items.Key];
                    connection.Subscribe( portion_items.Value );
                }
            }

            return true;
        }

        /// <summary>
        /// Unsubscribe items
        /// </summary>
        /// <param name="unsubscribeItems"></param>
        public void UnSubscribe( IEnumerable<IOPCUASubscribeItem> unsubscribeItems )
        {
            // Group items
            Dictionary<Int32, List<IOPCUASubscribeItem>> group_items = GroupItems( unsubscribeItems );

            foreach( var portion_items in group_items )
            {
                if( false == this._Connections.Contains( portion_items.Key ) )
                {
                    throw new Exception( String.Format( "Invalid URL" ) );
                }

                OPCConnection connection = this._Connections[portion_items.Key];
                connection.UnSubscribe( portion_items.Value );
            }
        }

        public Object LoadValue( Int32 connectionId, String tagName )
        {
            if( this._Connections.Contains( connectionId ) )
            {
                OPCConnection connection = this._Connections[connectionId];

                return connection.Read( tagName );
            }
            
            return null;
        }

        #endregion


        /// <summary>
        /// 
        /// </summar  y>
        /// <param name="subscrideItems"></param>
        /// <returns></returns>
        private Dictionary<Int32, List<IOPCUASubscribeItem>> GroupItems( IEnumerable<IOPCUASubscribeItem> subscrideItems )
        {
            Dictionary<Int32, List<IOPCUASubscribeItem>> result = new Dictionary<Int32, List<IOPCUASubscribeItem>>();

            foreach( var item in subscrideItems )
            {                
                OPCConnection connection = null;
                if( this._Connections.Contains( item.IdServer ) )
                {
                    connection = this._Connections[item.IdServer];
                }
                else
                {
                    IOPCUAConnectionInfo ci = null;
                    if( null != this._iOPCConnectionInfos )
                    { 
                        ci = this._iOPCConnectionInfos.FindConnectionInfo( item.IdServer );
                    }

                    if( ci == null )
                    {
                        //TODO: info
                        continue;
                    }

                    connection = new OPCConnection( item.IdServer, ci.UrlServer, ( ci == null ? 10 : ci.PublishingInterval ),
                                                         ( ci == null ? 600 : ci.TimeOut ),
                                                         ( ci == null ? 5 : ci.DefaultSamplingInterval ),
                                                         ( ci == null ? 10 : ci.DefaultQueueSize ) );

                    connection.OnChangeItem += ( o, e ) =>
                    {
                        if( this.OnChangeItem != null )
                        {
                            this.OnChangeItem( this, new OPCItemClientArgs( e ) );
                        }
                    };

                    connection.OnAutoResubscribe += ( o, e ) =>
                    {
                        if( this.OnAutoResubscribe != null )
                        {
                            this.OnAutoResubscribe( this, e );
                        }
                    };

                    this._Connections.Add( connection );
                }

                List<IOPCUASubscribeItem> items = null;
                if( result.ContainsKey( item.IdServer ) )
                {
                    items = result[item.IdServer];
                }
                else
                {
                    items = new List<IOPCUASubscribeItem>();
                    result.Add( item.IdServer, items );
                }

                items.Add( item );
            }

            return result;
        }        
    }
}
