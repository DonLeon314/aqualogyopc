﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace OPCUAClient
{
    internal class MonitorsStorage
    {
        #region Monitors collection
        private class MonitorExCollection:
                KeyedCollection<String,MonitoredItemEx>
        {

            public MonitorExCollection() :
                base( StringComparer.CurrentCultureIgnoreCase )
            {

            }

            protected override String GetKeyForItem( MonitoredItemEx item )
            {
                return item.Item.Key;
            }
        };

        private MonitorExCollection _Collection = new MonitorExCollection();

        #endregion

        public MonitorsStorage()
        {

        }

        public Boolean Contains( String key )
        {
            return this._Collection.Contains( key );
        }

        public IReadOnlyCollection<MonitoredItemEx> GetItems()
        {
            return this._Collection;
        }

        /// <summary>
        /// Safe add monitor
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns></returns>
        public Boolean Add( MonitoredItemEx newItem )
        {
            if( this._Collection.Contains( newItem.Item.Key ) )
            {
                return false;
            }

            this._Collection.Add( newItem );

            return true;
        }

        public void Remove( IEnumerable<String> keys )
        {

            this._Collection.Remove( "abracadabra" );

            foreach( var key in keys )
            {
                if( this._Collection.Contains( key ) )
                {
                    this._Collection.Remove( key );
                }
            }
        }
    }
}
