﻿using Opc.Ua.Client;
using OPCUAClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient
{
    internal class MonitoredItemEx:
            MonitoredItem
    {
        public IOPCUASubscribeItem Item
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="iOPCUASubscribeItem"></param>
        public MonitoredItemEx( IOPCUASubscribeItem iOPCUASubscribeItem )
        {
            this.Item = iOPCUASubscribeItem;    
        }
    }
}
