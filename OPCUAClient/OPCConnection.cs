﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using OPCUAClient.Interfaces;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.X509Certificates;

namespace OPCUAClient
{
    public class OPCConnection
    {
        private Int32 _Timeout;        
        private Int32 _PublishingInterval;

        private Int32 _SamplingInterval = 0;
        private Int32 _QueueSize = 0;

        private Object _SyncObject = new Object();

        private Session _Session = null;
        private ApplicationConfiguration _Configuration = null;
        private Subscription _Subscription = null;

        private MonitorsStorage _SubscribesMonitors = new MonitorsStorage();

        private System.Threading.Timer _ReconnectTimer = null;
        private Int32 _ReconnectCount = 0;

        public EventHandler<OPCItemConnectionArgs> OnChangeItem;
        public EventHandler<OPCAutoresubscribeArgs> OnAutoResubscribe;

        public Int32 Id
        {
            get;
            set;
        }
        public String UrlServer
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public OPCConnection( Int32 id, String serverUrl, Int32 publishingInterval, Int32 timeOut, Int32 samplingInterval, Int32 queueSize )
        {
            this.Id = id;

            this._PublishingInterval = publishingInterval;
            this._Timeout = timeOut;

            this._SamplingInterval = samplingInterval;
            this._QueueSize = queueSize;

            this.UrlServer = serverUrl.Trim();            
        }

        /// <summary>
        /// Subscribe items
        /// </summary>
        /// <param name="subscribeItems"></param>
        public void Subscribe( IEnumerable<IOPCUASubscribeItem> subscribeItems )
        {
            List<MonitoredItemEx> monitors = new List<MonitoredItemEx>();

            foreach( var tag in subscribeItems )
            {
                MonitoredItemEx monitor = new MonitoredItemEx( tag )
                {
                    DisplayName = tag.ItemName,
                    SamplingInterval = ( tag.SamplingInterval == 0 ? this._SamplingInterval : tag.SamplingInterval ),
                    QueueSize = (uint)( tag.QueueSize == 0 ? this._QueueSize : tag.QueueSize )
                };

                NodeId node = tag.ItemName;
                monitor.StartNodeId = node;
                monitor.MonitoringMode = MonitoringMode.Reporting;

                monitors.Add( monitor );
            }

            WatchMonitors( monitors );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscribeItems"></param>
        public void UnSubscribe( IEnumerable<IOPCUASubscribeItem> subscribeItems )
        {
            if( this._Subscription == null )
            {
                return;
            }

            List<MonitoredItemEx> for_del = new List<MonitoredItemEx>();
            
            foreach( var monitor in this._Subscription.MonitoredItems )
            {
                MonitoredItemEx monitor_ex = monitor as MonitoredItemEx;

                if( monitor_ex != null )
                {
                    String key = monitor_ex.Item.Key;

                    if( null != subscribeItems.Where( item => String.Compare( key, item.Key, true ) == 0 ).FirstOrDefault() )
                    {
                        for_del.Add( monitor_ex );                        
                    }
                }
            }

            this._Subscription.RemoveItems( for_del );
            this._Subscription.ApplyChanges();

            this._SubscribesMonitors.Remove( for_del.Select( m => m.Item.Key ) );

            if( this._Subscription.MonitoredItemCount == 0 )
            {
                Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="monitors"></param>
        /// <param name="IsFirst"></param>
        private void WatchMonitors( IReadOnlyCollection<MonitoredItemEx> monitors, Boolean IsFirst = true )
        {
            lock( this._SyncObject )
            {
                if( this._Session == null )
                {
                    try
                    {
                        Open();

                    } catch( Exception ex )
                    {

                        if( this._ReconnectTimer == null )
                        {
                            this._ReconnectTimer = new Timer( this.Reconnect, "TimerReconnect", 10000, 10000 );
                        }
                        return;
                    }
                };

                if( this._Subscription == null )
                {
                    if( false == CreateSurscribtion() )
                    {
                        throw new Exception( "Create subscription error" );
                    }
                }

                if( IsFirst )
                {
                    foreach( MonitoredItemEx monitor in monitors )
                    {
                        monitor.Notification += ( item, e ) =>
                        {
                            MonitoredItemEx monitor_ex = item as MonitoredItemEx;
                            if( monitor_ex != null )
                            {
                                if( this.OnChangeItem != null )
                                {
                                    this.OnChangeItem( this, new OPCItemConnectionArgs( e, monitor_ex.Item, 
                                                                                        new OPCConnectionInfo( this.UrlServer ),
                                                                                        item.DequeueValues() ) );
                                }
                            }
                        };
                    }
                }

                monitors = monitors.Where( m => this._SubscribesMonitors.Add( m ) ).ToList().AsReadOnly();

                List<MonitoredItemEx> ms = new List<MonitoredItemEx>();
                Int32 portion = 0;
                Int32 cnt = 0;
                foreach( var monitor in monitors )
                {
                    cnt++;
                    portion++;
                    ms.Add( monitor );

                    if( portion > 100 || portion == monitors.Count )
                    {
                        this._Subscription.AddItems( ms );
                        this._Subscription.ApplyChanges();
                        portion = 0;
                        ms.Clear();
                    }
                }

                Debug.WriteLine( String.Format( "Watch:{0} items in server {1}...", monitors.Count, this.UrlServer ) );

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Boolean CreateSurscribtion()
        {
            this._Subscription = new Subscription( this._Session.DefaultSubscription )
            {
                PublishingInterval = this._PublishingInterval,
                TimestampsToReturn = TimestampsToReturn.Server
            };
            Boolean result = this._Session.AddSubscription( this._Subscription );
            if( result == false )
            {
                this._Subscription = null;
                return false;
            }

            this._Subscription.Create();
            return true;
        }


        public void Close()
        {
            try
            {
                this._Subscription.Dispose();                
                this._Session.Close();
                this._Session.Dispose();

            } catch( Exception ex )
            {

            }

            this._Subscription = null;

            this._Session = null;

            this._Configuration = null;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Open()
        {
            if( this._Configuration == null )
            {

                this._Configuration = new ApplicationConfiguration
                {
                    ApplicationType = ApplicationType.Client,
                    CertificateValidator = new CertificateValidator(),
                    ClientConfiguration = new ClientConfiguration(),

                };
               
            }
        
            String host = "opc.tcp:" + this.UrlServer;
            EndpointDescription endpointDescription = SelectEndpoint( host, false );

            EndpointConfiguration endpointConfiguration = EndpointConfiguration.Create( null );
            ConfiguredEndpoint endpoint = new ConfiguredEndpoint( null, endpointDescription, endpointConfiguration );




            this._Session = Session.Create( this._Configuration, endpoint, false, true, "", (uint)( 1000 * this._Timeout ), null, null );
      
            this._Session.KeepAlive += _Session_KeepAlive;
            this._Session.KeepAliveInterval = 10000;

            Debug.WriteLine( String.Format( "Open:{0} - OK!", this.UrlServer ) );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="e"></param>
        private void _Session_KeepAlive( Session session, KeepAliveEventArgs e )
        {
            if( e.CurrentState == ServerState.Unknown || e.CurrentState == ServerState.Shutdown )
            {
                if( this._ReconnectTimer == null )
                {
                    this._Session.KeepAlive -= _Session_KeepAlive;
                    this._ReconnectTimer = new Timer( this.Reconnect, "TimerReconnect", 5000, 5000 );

                    Debug.WriteLine( String.Format("Reconnect {0}...", this.UrlServer ) );
                }
            }
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="discoveryUrl"></param>
        /// <param name="useSecurity"></param>
        /// <returns></returns>
        private EndpointDescription SelectEndpoint( string discoveryUrl, bool useSecurity )
        {
            Uri uri = new Uri( discoveryUrl );
            EndpointDescription selectedEndpoint = null;

            using( DiscoveryClient client = DiscoveryClient.Create( uri ) )
            {
                EndpointDescriptionCollection endpoints = client.GetEndpoints( null );

                for( int i = 0; i < endpoints.Count; i++ )
                {
                    EndpointDescription endpoint = endpoints[i];

                    if( endpoint.EndpointUrl.StartsWith( uri.Scheme ) )
                    {
                        if( useSecurity )
                        {
                            if( endpoint.SecurityMode == MessageSecurityMode.None )
                                continue;
                        }
                        else
                        {
                            if( endpoint.SecurityMode != MessageSecurityMode.None )
                                continue;
                        }

                        if( selectedEndpoint == null )
                            selectedEndpoint = endpoint;

                        if( endpoint.SecurityLevel > selectedEndpoint.SecurityLevel )
                            selectedEndpoint = endpoint;
                    }
                }

                if( selectedEndpoint == null && endpoints.Count > 0 )
                    selectedEndpoint = endpoints[0];
                return selectedEndpoint;
            }
        }


        /// <summary>
        /// Reconnect
        /// </summary>
        /// <param name="obj"></param>
        private void Reconnect( Object obj )
        {
            this._ReconnectCount++;

            Debug.WriteLine( String.Format( "Reconnect {0} Iteraqtion:{1}", this.UrlServer, this._ReconnectCount ) );


            lock( this._SyncObject )
            {
                try
                {
                    if( this._Session != null )
                    {
                        this._Session.Close();
                        this._Session = null;
                    }
                    this._Subscription = null;

                    if( this._ReconnectTimer != null )
                    {
                        this._ReconnectTimer.Dispose();
                        this._ReconnectTimer = null;
                    }

                    Open();
                    
                } catch( Exception ex )
                {
                    this._ReconnectTimer = new Timer( this.Reconnect, "TimerReconnect", 10000, 10000 );
                    return;
                }

                MonitorsStorage oldmonitores = null;
                try
                {
                    oldmonitores = this._SubscribesMonitors;
                    this._SubscribesMonitors = new MonitorsStorage();

                    IReadOnlyCollection<IOPCUASubscribeItem> raw_items = oldmonitores.GetItems().Select( m => m.Item ).ToList();

                    if( this.OnAutoResubscribe != null )
                    {
                        var a = new OPCAutoresubscribeArgs( new OPCConnectionInfo( this.UrlServer ), raw_items );
                        this.OnAutoResubscribe( this, a );
                        raw_items = a.NewItems;
                    }

                    if( raw_items != null )
                    {
                        List<MonitoredItemEx> monitores = new List<MonitoredItemEx>();

                        foreach( IOPCUASubscribeItem item in raw_items )
                        {
                            MonitoredItemEx monitor = new MonitoredItemEx( item )
                            {
                                DisplayName = item.ItemName,
                                SamplingInterval = ( item.SamplingInterval == 0 ? this._SamplingInterval : item.SamplingInterval ),
                                QueueSize = (uint)( item.QueueSize == 0 ? this._QueueSize : item.QueueSize )
                            };

                            NodeId node = item.ItemName;
                            monitor.StartNodeId = node;
                            monitor.MonitoringMode = MonitoringMode.Reporting;

                            monitores.Add( monitor );
                        }

                        WatchMonitors( monitores, true );
                    }

                } catch( Exception ex )
                {
                    if( oldmonitores != null )
                    {
                        this._SubscribesMonitors = oldmonitores;
                    }
                }
            }
        }

        public Object Read( String tagName )
        {
            NodeId node = tagName;

            DataValue read_result = new DataValue();
            
            //TODO: no inplement
            for( Int32 i = 0; i < 10; i++ )
            {
                read_result = this._Session.ReadValue( node );
                if( DataValue.IsGood( read_result ) )
                {                    
                    break;
                }
                else
                {
                    Debug.WriteLine( "OPC read item error!" );
                }
            }

            return read_result.Value;
        }

    }
}
