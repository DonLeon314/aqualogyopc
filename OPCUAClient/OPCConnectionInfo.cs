﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPCUAClient
{
    public class OPCConnectionInfo
    {
        public String ServerUrl
        {
            get;
            private set;
        }

        public OPCConnectionInfo( String serverUrl )
        {
            this.ServerUrl = serverUrl; 
        }
    }
}
