El tipo de alarma definido a día de hoy se puede saber por la columna "Grupo Notificacion":
 - CAPATAZ -> Aviso
 - CAPATAZ/JEFE -> Alerta
 - CAPATAZ/JEFE/GUARDIA -> Alarma

 