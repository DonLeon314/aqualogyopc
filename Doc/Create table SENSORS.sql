USE [AqualogyOPC]
GO

/****** Object:  Table [dbo].[Sensors]    Script Date: 5/22/2018 5:42:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Sensors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](200) NOT NULL,
	[Date] [datetime] NOT NULL,
	[OPCTagID] [nvarchar](200) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Sensors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


